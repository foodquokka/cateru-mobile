export class DrinkItem {
    order_id: number;
    tableno: number;
    name: string;
    menu_id: number;
    detail_id: number;
    quantity: number;
}
