import { Component, OnInit } from '@angular/core';
import { WaiterService } from '../service/waiter.service';
import { NavController, ToastController } from '@ionic/angular';
import { Drinks } from 'src/app/kitchen/ktab1/drinks';
import { RestService } from 'src/app/kitchen/rest.service';





@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  drinkss = new Drinks();
  orderselected = false;
  isenabled = [false, false, false, false];
 
 
  orderLists: any;
  orderDrinksArray = new Array();
  orderCard: string[] = [];
  drinks: any;
  // qty: any;
  // name: any;
  // status: any;
  
  counter = 0;
  prepareStatus: boolean;

  constructor(
    public navCtrl: NavController,
    public restService: RestService,
    private waiterservice: WaiterService,
    private toastCtlr: ToastController
  ) {

  }
  ngOnInit() {
  }

  // ionViewWillEnter() {
  //   this.getDrinkList();
  // }
  // getOrderQty(id) {
  //   this.restService.getOrderQty(id).then(data => {
  //     this.qty = (data as any).items;
  //   });
  // }

  getDrinkList() {
    this.waiterservice.getOrderDrinks().then(data => {
      this.drinks = (data as any).orders;
    });
  }
  updateStatus(order) {
    console.log('orders', order);
    this.restService.postStatus(order).then(data => {
      this.orderLists = data;
    });
  }


  prepare(id, index) {
    this.restService.changeOrderStatusToPrepare(id);
    this.isPrepare(id);
    this.isenabled[index] = this.prepareStatus;
    this.presentToastPrepare();
  }
  isPrepare(id) {
    this.restService.isPreparing(id).then(data => {
      this.prepareStatus = (data as any).status;
    });
    this.doRefresh();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getDrinkList();
    }, 1500);
  }
  async presentToastPrepare() {
    const toast = await this.toastCtlr.create({
      message: 'Order being prepared',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
