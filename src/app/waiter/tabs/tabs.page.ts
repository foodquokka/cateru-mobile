import { Component, OnInit} from '@angular/core';
import { WaiterService } from '../service/waiter.service';
import { Events } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  billoutList = [];
  billoutCounter = 2;
  notif = 0;

  constructor(
    private waiterService: WaiterService, 
    private events: Events,
    private authenticationService: AuthenticationService) { 
    events.subscribe('order:created', (order, time) => {
      if(order !== null){
        this.notif = order;
      }
      console.log('Welcome', order, 'at', time);
  });
}
  ngOnInit() {
    this.getBilloutList();
  }

  getBilloutList() {
    this.waiterService.processGetBilloutList().then(data => {
      this.billoutList = (data as any).table;
      this.billoutCounter = this.billoutList.length;
    });
  }
  logout(){
    this.authenticationService.logout();
  }
}
