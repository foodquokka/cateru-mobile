import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { IPAddress } from 'src/app/ipaddress';



@Injectable({
  providedIn: 'root'
})
export class WaiterService {
  IPaddress = new IPAddress;

  IP = this.IPaddress.getIP();

  getDrinksListURL = this.IP + 'order/drinklist';   // api/order/drinklist
  drinkReadyURL = this.IP + 'order/statusready/';
  readyToServeURL = this.IP + 'order/readytoserve';
  allServedItemsURL = this.IP + 'order/servedorders';
  getOccupiedTablesURL = this.IP + 'table/occupied';
  clearTableURL = this.IP + 'table/cleartable/';
  billOutListURL = this.IP + 'cashier/billOutList';
  kitchenURL = this.IP + 'kitchen';
  servingURL = this.IP + 'order/servingmenus';
  // allTableURL = this.IP + 'table/tablelist';
  allTableURL = this.IP + 'table/occupied';
  orderPerTableURL = this.IP + 'table/orderspertablewaiter/';
  changeStatustoServe = this.IP + 'order/changestatustoserve/';
  changeStatustoServing = this.IP + 'order/changestatustoserving/';
  servingStatusByTableNoURL = this.IP + 'servingStatusByTableNo/';
  getNotificationURL = this.IP + 'getCallNotification/';
  getBillOutListURL = this.IP + 'getBilloutListByTableNo/';
  resolveCustomerCallURL = this.IP + 'resolveCustomerCall/';


  constructor(
    private http: HttpClient,
    public alertController: AlertController,
  ) { }

  getWaitingOrders() {
    return new Promise(resolve => {
      this.http.get(this.kitchenURL).subscribe(orderlist => {
        resolve(orderlist);
      }, err => {
        console.log(err);
      });
    });
  }
  changeOrderStatustoServe(id) { // customer
    return new Promise(resolve => {
      this.http.post(this.changeStatustoServe + id, id).subscribe(res => {
        resolve(res);
        console.log(res);
      },
        err => {
          console.log('ERROR!: ', err);
        });
    });
  }
  changeOrderStatustoServing(id) { // customer
    return new Promise(resolve => {
      this.http.post(this.changeStatustoServing + id, id).subscribe(res => {
        resolve(res);
        console.log(res);
      },
        err => {
          console.log('ERROR!: ', err);
        });
    });
  }
  // GET DRINK LIST
  getOrderDrinks() {
    return new Promise(resolve => {
      this.http.get(this.getDrinksListURL).subscribe(drinklist => {
        resolve(drinklist);
        console.log(drinklist);
      }, err => {
        console.log(err);
      });
    });
  }

  // SET DRINK STATUS TO READY
  sendToDrinkReady(id) {
    console.log('service received: ', id);
    this.drinkReady(id).then(orderdetail => {
      console.log(orderdetail);
    });
  }

  drinkReady(id) {
    console.log('service will process: ', id);
    return new Promise((resolve, reject) => {
      this.http.post(this.drinkReadyURL + id, id)
        .subscribe(res => {
          console.log(res);
          this.successAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            this.errorAlert('Error! Try again.');
          });
    });
  }

  // GET READY TO SERVE LIST
  processReadyList() {
    return new Promise(resolve => {
      this.http.get(this.readyToServeURL).subscribe(readylist => {
        resolve(readylist);
        console.log(readylist);
      }, err => {
        console.log(err);
      });
    });
  }

  // GET ALL SERVED
  processAllServed() {
    return new Promise(resolve => {
      this.http.get(this.allServedItemsURL).subscribe(servedlist => {
        resolve(servedlist);
        console.log(servedlist);
      }, err => {
        console.log(err);
      });
    });
  }

  // ALERT FUNCTIONS
  async errorAlert(message) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      //   message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async successAlert(message) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      // message: message,
      buttons: [
        {
          text: 'OK'
        }]
    });
    await alert.present();
  }

  // GET OCCUPIED TABLES
  processGetOccupiedTables() {
    console.log(this.getOccupiedTablesURL);
    return new Promise(resolve => {
      this.http.get(this.getOccupiedTablesURL).subscribe(occupiedList => {
        resolve(occupiedList);
        console.log(occupiedList);
      }, err => {
        console.log(err);
      });
    });
  }

  // GET BILLOUT LIST
  processGetBilloutList() {
    return new Promise(resolve => {
      this.http.get(this.billOutListURL).subscribe(billoutList => {
        resolve(billoutList);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllServingMenus() {
    return new Promise(resolve => {
      this.http.get(this.servingURL).subscribe(servingmenulist => {
        resolve(servingmenulist);
        console.log(servingmenulist);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllTable() {
    return new Promise(resolve => {
      this.http.get(this.allTableURL).subscribe(tables => {
        resolve(tables);
        console.log(tables);
      }, err => {
        console.log(err);
      });
    });
  }

  //WAITERMODAL
  getOrderPerTable(tableno) {
    return new Promise(resolve => {
      this.http.get(this.orderPerTableURL + tableno).subscribe(orders => {
        resolve(orders);
        console.log(orders);
      }, err => {
        console.log(err);
      });
    });
  }

  getItemsForServing(tableno) {
    return new Promise(resolve => {
      this.http.get(this.servingStatusByTableNoURL + tableno).subscribe(orders => {
        resolve(orders);
        console.log(orders);
      }, err => {
        console.log(err);
      });
    });
  }
  getNotification(tableno) {
    return new Promise(resolve => {
      this.http.get(this.getNotificationURL + tableno).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getBillOutList(tableno) {
    return new Promise(resolve => {
      this.http.get(this.getBillOutListURL + tableno).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  resolveCall(tableno) {
    return new Promise((resolve, reject) => {
      this.http.post(this.resolveCustomerCallURL + tableno, tableno)
        .subscribe(res => {
          console.log(res);
          resolve(res);

        },
          err => {
            console.log('ERROR!: ', err);
            this.errorAlert(err);
          });
    });
  }

  clearTable(tableno){
    return new Promise((resolve, reject) => {
      this.http.post(this.clearTableURL + tableno, tableno)
      .subscribe(res => {
        console.log(res);
        resolve(res);
      }, 
      err => {
        console.log('ERROR!:', err);
        this.errorAlert(err);
      });
    });
  }

}


