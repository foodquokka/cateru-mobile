import { Component, OnInit  } from '@angular/core';
import { RestService } from 'src/app/kitchen/rest.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  orderlist: any;
  drinks: any;

  constructor(
    public restService: RestService) {
  }
  ngOnInit() {
     this.getAllCompleteDrinks();
  }
  // ionViewWillEnter(){
  //   this.getAllCompleteDrinks();
  // }
  
  getAllCompleteDrinks(){
    this.restService.getAllCompleteDrinks().then(data => {
      this.drinks = (data as any).orders;
      console.log(this.drinks);
    })
  }
}
