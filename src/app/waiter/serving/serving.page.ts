import { Component, OnInit } from '@angular/core';
import { WaiterService } from '../service/waiter.service';

@Component({
  selector: 'app-serving',
  templateUrl: './serving.page.html',
  styleUrls: ['./serving.page.scss'],
})
export class ServingPage implements OnInit {
  serving:any;

  constructor(private waiterservice: WaiterService) { }

  ngOnInit() {
  }
  refreshpage(){
    this.ionViewWillEnter();
  }
  ionViewWillEnter() {
    this.getServingMenus();
  } 
  getServingMenus(){
    this.waiterservice.getAllServingMenus().then(data => {
      this.serving = (data as any).result;
      console.log(this.serving);
    });
  }

}
