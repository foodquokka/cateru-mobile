import { Component, OnInit } from '@angular/core';
import {  ToastController } from '@ionic/angular';
import { RestService } from 'src/app/kitchen/rest.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page implements OnInit {
  orders: any;
  isenabled: any;
  drinks: any;

  constructor(
    private restService: RestService,
    private toastCtlr: ToastController
  ) { }

  ngOnInit() {
    this.getPrepareDrinks();
  }
  // ionViewWillEnter(){
    
  //   this.getPrepareDrinks();
  // }

  
  getPrepareDrinks(){
    this.restService.getPrepareDrinks().then(data => {
      this.drinks = (data as any).orders;
      console.log(this.drinks);
    })
  }
  async presentToastFinish() {
    const toast = await this.toastCtlr.create({
      message: 'Order finished!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  finish(id){
      this.restService.changeOrderStatusToFinish(id);
      this.presentToastFinish();
      this.doRefresh(id);
    }
  doRefresh(event) {
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getPrepareDrinks();
      event.target.complete();
    }, 1500);
  }

}