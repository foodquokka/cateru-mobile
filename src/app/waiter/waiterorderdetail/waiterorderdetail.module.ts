import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WaiterorderdetailPage } from './waiterorderdetail.page';

const routes: Routes = [
  {
    path: '',
    component: WaiterorderdetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WaiterorderdetailPage]
})
export class WaiterorderdetailPageModule {}
