import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController, ToastController, NavParams } from '@ionic/angular';
import { AdminService } from 'src/app/customer/services/admin.service';
import { StorageserviceService } from 'src/app/services/storageservice.service';
import { CartService } from 'src/app/customer/services/cart.service';
import { ActivatedRoute } from '@angular/router';
import { WaiterService } from '../service/waiter.service';
import { OrderID } from './orderid.model';

@Component({
  selector: 'app-waiterorderdetail',
  templateUrl: './waiterorderdetail.page.html',
  styleUrls: ['./waiterorderdetail.page.scss'],
})
export class WaiterorderdetailPage implements OnInit {
  @Input("tableno") tableno;


  orderid = {} as OrderID
  isenabled = true;
  menuDetails: any;
  order_id: any;
  items: string;
  qty: any = 1;
  flag: number;
  orders: any;
  counter = 0;
  count: any;

  constructor(
    public modalController: ModalController,
    public adminService: AdminService,
    private alertController: AlertController,
    private toastCtlr: ToastController,
    public cartService: CartService,
    public router: ActivatedRoute,
    public navParam: NavParams,
    public waiterservice: WaiterService,
  ) { }

  ngOnInit() {

  }
  ionViewWillEnter() {
    this.getOrderPerTable();

  }

  minusQty() {
    if (this.qty > 1) {
      this.qty -= 1;
    }
  }
  getOrderPerTable() {
    this.waiterservice.getOrderPerTable(this.tableno).then(data => {
      this.orders = (data as any).orders;
      console.log(this.orders);
    });
  }

  addQty() {
    this.qty += 1;
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Order added to cart!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  async presentToastServing() {
    const toast = await this.toastCtlr.create({
      message: 'Order is being served!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  changeStatusToServing(id) {
    this.waiterservice.changeOrderStatustoServing(id);
    this.doRefresh(id);
    this.presentToastServing();
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getOrderPerTable();
      event.target.complete();
    }, 1000);
  }

  resolveCall(tableno) {
    this.waiterservice.resolveCall(tableno).then(() => {
      this.presentToastCallResolved();
    });
  }
  async presentToastCallResolved() {
    const toast = await this.toastCtlr.create({
      message: 'Call has been resolved!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  clearTable(tableno) {
    this.waiterservice.clearTable(tableno);
    console.log('CLEAR TABLE');
    console.log(tableno);
  }
}




