import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaiterorderdetailPage } from './waiterorderdetail.page';

describe('WaiterorderdetailPage', () => {
  let component: WaiterorderdetailPage;
  let fixture: ComponentFixture<WaiterorderdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaiterorderdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaiterorderdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
