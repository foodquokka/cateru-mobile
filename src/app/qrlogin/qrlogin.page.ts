import { Component, OnInit, NgZone } from '@angular/core';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Platform, NavController } from '@ionic/angular';
import { Order } from '../customer/classes/order';
import { QrloginService } from '../../app/qrlogin/qrlogin.service';
import { AdminService } from '../customer/services/admin.service';
import { AlertController } from '@ionic/angular';
import { Infos } from './infos';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { DeviceTableService } from '../services/device-table.service';

import { TableClearPage } from '../customer/table-clear/table-clear.page';
import { DeviceInfo } from './deviceInfo.model';
import { Device } from './device.model';
import { TableNo } from '../table/tableno.model';
import { StorageserviceService } from 'src/app/services/storageservice.service';
import { TableStatus } from './tablestatus.model';



@Component({
  selector: 'app-qrlogin',
  templateUrl: './qrlogin.page.html',
  styleUrls: ['./qrlogin.page.scss'],
})
export class QrloginPage implements OnInit {

  scannedData: any;
  employee_info: any;
  UUID = {} as Device;
  num = {} as TableNo; 
  tableStatus = {} as TableStatus;
  deviceinfo = {} as DeviceInfo;

  orderInfo;
  infos: [];
  userid: number;
  order: Order;
  custid: any;
  tableno: any;
  username: string; // from ion-input
  table = {};
  encodedData = '';
  QRSCANNED_DATA: string;
  isOn = false;
  qrScan: any;
  pos: string;
  position: any;
  scannedinfos: Infos[] = Array();
  deviceNo: any;
  isSuccess: boolean;
  custName;
  empid: any;

 
  orderID: any;
  tbNo: any;
  tables: any;
  selectedTable: any;
  new: any;
  exist: any;
  isShow = false;
  isShowOccupiedTable = false;
  occupiedtables: any;
  tabless: any;
  status: any;
  orders: any;
  order_id: any;
  no: any;

  constructor(
    public alertController: AlertController,
    public platform: Platform,
    public dialog: Dialogs,
    public navctlr: NavController,
    public qr: QRScanner,
    private qrService: QrloginService,
    public router: Router,
    private authService: AuthenticationService,
    private adminService: AdminService,
    private deviceTable: DeviceTableService,
    private storageService: StorageserviceService,
    private devicetable: DeviceTableService,
    private route: Router

  ) {
    this.platform.backButton.subscribeWithPriority(0, () => {
      document.getElementsByTagName('body')[0].style.opacity = '1';
    });
  }
  ngOnInit() {

  }
  ionviewWillEnter() {
    this.getAllAvailableTableNo();
    this.getTableStatusNotPaid();
  }

  goToCustomerTable(order_id) {
    console.log('GOTOCUSTOMER')
    console.log(order_id);
    this.storageService.saveOrderID(order_id);
    this.route.navigate(['confirmedorderlist', order_id]);
  }
  getOrderByTableNo(tableno) {
    this.qrService.getOrderByTableNo(tableno).then(data => {
      this.orders = (data as any).details;
    });
    console.log(tableno)
  }
  getTableStatusNotPaid() {
    this.deviceTable.getTableStatusNotPaid().then(data => {
      this.tabless = (data as any).tables
      console.log(this.tabless);
    });
  }
 
  startScan() {
    this.qr.prepare().then((status: QRScannerStatus) => {
      if (status.authorized) {
        this.qr.useFrontCamera();
        this.isOn = true;
        document.getElementsByTagName('body')[0].style.opacity = ' 0 ';
        const scanSub = this.qr.scan().subscribe((id: any) => {
          this.isOn = false;
          this.QRSCANNED_DATA = id;
          console.log(id);
          this.qrService.verifyQRLogin(this.QRSCANNED_DATA).then(data => {
            console.log(data);
            if (data === null) {
              this.closeScanner();
              scanSub.unsubscribe();
            } else {
              this.infos = (data as any).employee_info;
              console.log(this.infos);

              let route = 'qrlogin';
              let isAuthenticated = false;

              for (const info of this.infos) {
                this.authService.login((info as any).sessionid);
                this.position = (info as any).emp_pos;
                console.log(this.position);
                this.empid = (info as any).emp_id;
                isAuthenticated = this.authService.isAuthenticated();

                if (this.position === 'waiter') {
                  console.log('HELLO');
                  this.router.navigate(['/table', this.empid]);
                }
                else{
                  this.closeScanner();
                  scanSub.unsubscribe();
                }
                break;
              }
              this.closeScanner();
              scanSub.unsubscribe();
            }
          });
        });
        this.qr.show();
      } else if (status.denied) {
        this.qrScan = this.qr.scan().subscribe((text: string) => {
          document.getElementsByTagName('body')[0].style.opacity = '1';
          this.qrScan.unsubscribe();
          this.dialog.alert(text);
        }, (err) => {
          this.dialog.alert(JSON.stringify(err));
        });
      }
    })
      .catch((e: any) => console.log('Error is', e));
  }

  getStatus(status) {
    return this.status = status;
  }
  closeScanner() {
    this.isOn = false;
    this.qr.hide();
    this.qr.destroy();
    document.getElementsByTagName('body')[0].style.opacity = '1';
  }
  successLogin(data) {
    this.qrService.saveOrdersData(this.orderInfo);
  }
  random(): number {
    const rand = Math.floor(Math.random() * 20) + 1;
    return rand;
  }

  setCustId(id) {
    this.custid = id;
  }
  getCustId() {
    console.log(this.custid);
  }
  getOccupiedTable() {
    this.qrService.getOccupiedTable().then(data => {
      this.occupiedtables = (data as any).OccupiedTables;
    });
  }

  saveTransactionInfo() {
    this.deviceinfo.custid = this.random();
    this.deviceinfo.empid = this.empid;
    this.deviceinfo.date_ordered = Date.now();
    this.deviceinfo.tableno = this.no;

    this.adminService.postToOrder(this.deviceinfo).then(data => {
      this.storageService.saveOrderID((data as any).order_id);
      console.log(this.deviceinfo);
    });
    console.log(this.no);
    this.setTableOccupied();


  }
  getAllAvailableTableNo() {
    this.devicetable.getAllAvailableTable().then(data => {
      this.tables = (data as any).AvailableTables;
      console.log(this.tables);
    });

  }
  getTableNo() {
    return this.selectedTable;
  }
  setTableNo(selectedTable) {
    this.selectedTable = selectedTable;
  }
  setTableOccupied() {
    this.tableStatus.tableno = this.selectedTable;
    this.tableStatus.status = 'Occupied';
    this.qrService.setTableStatus(this.tableStatus);
    console.log('set table status');
  }


}
