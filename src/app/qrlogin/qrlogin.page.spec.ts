import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrloginPage } from './qrlogin.page';

describe('QrloginPage', () => {
  let component: QrloginPage;
  let fixture: ComponentFixture<QrloginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrloginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrloginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
