import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { IPAddress } from '../ipaddress';


@Injectable({
  providedIn: 'root'
})
export class QrloginService {
  
  IPaddress = new IPAddress;

  IP = this.IPaddress.getIP();
  verifyqrAPI = this.IP + 'QRcode/verify/';
  savelogAPI = this.IP + 'QRcode/savelog';
  saveToOrdersAPI = this.IP + 'loginSuccess'
  saveTransactionInfoURL = this.IP + 'order/startorder';
  setCustomerInfoURL = this.IP + 'customer/setNewCustomer';
  settablestatusURL = this.IP + 'settablestatus';
  getAllOccupiedTableURL = this.IP + 'table/occupied';
  getOrderByTableNoURL = this.IP + 'table/orderspertable/';
  private data = [];
  orderID: any;

  constructor(
    private http: HttpClient, 
    private router: Router,
    public alertController: AlertController,
  ) { }


  saveLog(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.savelogAPI, data)
      .subscribe(logs => {
        console.log(logs);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  saveOrdersData(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.saveToOrdersAPI, data)
      .subscribe(logs => {
        console.log(logs);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }

  verifyQRLogin( id ) {
    return new Promise(resolve => {
      this.http.get(this.verifyqrAPI + id ).subscribe(data => {
        resolve(data);
      },
        err => {
          console.log('ERROR!: ', err);
        //  this.errorAlert('Permission not');
        }); 
    });
  }
 
  saveCustomerInfo(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.setCustomerInfoURL, data)
        .subscribe(res => {
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  saveTransactionInfo(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.saveTransactionInfoURL, data)
        .subscribe(res => {
          console.log(res);
          this.setOrderID((res as any).order_id);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  setOrderID(id) {
    this.orderID = id;
  }
  getOrderID() {
    return this.orderID;
  }


  async successAlert(successMessage) {
    console.log(successMessage);
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: successMessage,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigateByUrl('/starting-page');
          }
        }]
    });
    await alert.present();
  }
  setData(id, data) {
    this.data[id] = data;
  }
  getData(id) {
    return this.data[id];
  }

  setTableStatus(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.settablestatusURL, data)
        .subscribe(res => {
          resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  getOccupiedTable(){
    return new Promise(resolve => {
      this.http.get(this.getAllOccupiedTableURL).subscribe(data => {
        resolve(data);
      },
        err => {
          console.log('ERROR!: ', err);
        });
    });
  }
  getOrderByTableNo(tableno){
    return new Promise(resolve => {
      this.http.get(this.getOrderByTableNoURL+tableno).subscribe(data => {
        resolve(data);
        console.log(data);
      },
        err => {
          console.log('ERROR!: ', err);
        });
    });
  }



}
