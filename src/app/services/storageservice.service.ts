import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class StorageserviceService {
  newData = [];

  data = new BehaviorSubject('');
  currentData = this.data.asObservable();


  constructor(
    private nativeStorage: NativeStorage, 
    private authService: AuthenticationService,
    private router: Router
  ) { }

  changeData(data: string){
      this.data.next(data);
  }
 
  saveOrderID(orderid){
    this.nativeStorage.setItem('orderid', orderid);
    console.log('saveorderid', orderid);
  }
  saveUsername(username){
    this.nativeStorage.setItem('username',username);
  }
  saveTableNo(tableno){
    console.log('savetableno');
    this.nativeStorage.setItem('tableno',tableno);
  }
  getTableNo(){
    console.log('gettableno');
    return this.nativeStorage.getItem('tableno');
  }
  getUsername(){
    return this.nativeStorage.getItem('username');
  }
  getOrderID(){
    return this.nativeStorage.getItem('orderid');
  }
  saveRole(role){
    this.nativeStorage.setItem('role',role);
    console.log('role',role);
  }
  checkRole(){
    return this.nativeStorage.getItem('role').then(res => {
      if(res === 'admin'){
        this.router.navigate(['/dashboard']);
      }else{
        this.router.navigate(['/qrlogin']);
      }
      this.authService.logout();
    });
  }
}
