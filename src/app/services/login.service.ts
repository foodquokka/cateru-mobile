import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPAddress } from '../ipaddress';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  IPaddress = new IPAddress;
  
  IP = this.IPaddress.getIP();

  loginURL = this.IP + 'login';
  getPositionURL = this.IP + 'employee/getposition/';
  constructor(
    private http: HttpClient
  ) { console.log(this.IP) }

  login(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.loginURL,data)
      .subscribe(res => {
        resolve(res);
        //console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  } 

  getPosition(username){
    return new Promise(resolve => {
      this.http.get(this.getPositionURL + username).subscribe(data => {
      resolve(data);
      console.log(data);
       // this.setTableNo((data as any).tableno);
      },
        err => {
          console.log('ERROR!: ', err);
        //  this.errorAlert('Permission not');
        });
    });
  }
}
