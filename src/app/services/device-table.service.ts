import { Injectable } from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { HttpClient} from '@angular/common/http';
import { Uid } from '@ionic-native/uid/ngx';
import { IPAddress } from '../ipaddress';



@Injectable({
  providedIn: 'root'
})
export class DeviceTableService {

  IPaddress = new IPAddress;
  
  IP = this.IPaddress.getIP();
  // IP = 'http://192.168.101.5/api';
  setDeviceIdURL = this.IP + 'setdeviceid';
  getTableNoURL = this.IP + 'getTableNo/';
  getAllTableList = this.IP + 'table/tablelist';
  getdevicetablenoURL = this.IP + 'getdevicetableno/';
  gettablestatusnotpaidURL = this.IP + 'gettablestatusnotpaid';
  getAvailableTableURL = this.IP + 'table/getAvailableTable';

  tableno: number;
  uuid: any;

  constructor(private uid: Uid, private http: HttpClient) {
  }
  getDeviceID(uid) {
    this.setDeviceID(uid).then(data => {
      console.log(data); 
    });
  }

setDeviceID(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.setDeviceIdURL, data)
    .subscribe(res => {
      console.log('set');
      console.log(res);
    },
    err => {
      console.log('ERROR!: ', err);
    });
  });
}
getDeviceNo(id) {
  return new Promise(resolve => {
    this.http.get(this.getTableNoURL + id).subscribe(data => {
      resolve(data);
     // this.setTableNo((data as any).tableno);
    },
      err => {
        console.log('ERROR!: ', err);
      //  this.errorAlert('Permission not');
      });
  });
}
getdevicetableno(deviceuid) {
  console.log(this.getdevicetablenoURL + deviceuid);
  return new Promise(resolve => {
    this.http.get(this.getdevicetablenoURL + deviceuid)
    .subscribe(data => {
      resolve(data);
      console.log(data);
    }, error => {
      console.log(error);
    });
  });
} 
getAllAvailableTable(){
  return new Promise(resolve => {
    this.http.get(this.getAvailableTableURL)
    .subscribe(data => {
      resolve(data);
      console.log(data);
    }, error => {
      console.log(error);
    });
  });
}
setTableNo(tableno: number) {
  this.tableno = tableno;
  console.log(this.tableno);
}

getTableNo(): number { 
  console.log('GETTABLENO', this.tableno);
  return this.tableno;
}

getTableList() {
  return new Promise(resolve => {
    this.http.get(this.getAllTableList).subscribe(data => {
      resolve(data);
      console.log(data);
    }, err => {
      console.log(err);
    });
  });
}
  getDeviceUUID() {
    console.log(this.uid.UUID);
    return this.uid.UUID;
  }
  getTableStatusNotPaid() {
    return new Promise(resolve => {
      this.http.get(this.gettablestatusnotpaidURL).subscribe(data => {
        resolve(data);
       // this.setTableNo((data as any).tableno);
      },
        err => {
          console.log('ERROR!: ', err);
        //  this.errorAlert('Permission not');
        });
    });
  }

}
