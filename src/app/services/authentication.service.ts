import { Platform } from '@ionic/angular';
import { Injectable, ɵConsole } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Router } from '@angular/router';

const TOKEN_KEY = 'auth-t oken';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authenticationState = new BehaviorSubject(false);

  // TOKEN_KEY: any;
  empPos: any;
  URL: any;
  currentUserValue: any;
  flag: number;

  constructor(private nativeStorage: NativeStorage, private plt: Platform, private route: Router ) {
    this.plt.ready().then(() => {
     this.checkToken();
    });
   }

   checkToken() {
     this.nativeStorage.getItem(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
     });
    }
  getPos() {
      return this.empPos;
  }

  setPos(empPos) {
      this.empPos = empPos;
      return this.empPos;
  }
   login(id) {
     return this.nativeStorage.setItem(TOKEN_KEY, id).then(() => {
        console.log(TOKEN_KEY);
        this.authenticationState.next(true);
     });
   }
   logout() {
     return this.nativeStorage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
     });
   }

   isAuthenticated() {
     return this.authenticationState.value;
   }

}
