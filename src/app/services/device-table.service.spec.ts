import { TestBed } from '@angular/core/testing';

import { DeviceTableService } from './device-table.service';

describe('DeviceTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviceTableService = TestBed.get(DeviceTableService);
    expect(service).toBeTruthy();
  });
});
