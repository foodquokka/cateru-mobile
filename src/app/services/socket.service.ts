import { Injectable } from '@angular/core';

import { Socket } from 'ngx-socket-io';
import { Drinks } from '../waiter/orders.model';
import { BehaviorSubject, Observable , of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
messages=[];
constructor(private socket: Socket) {  }

ngOnInit(){
    this.socket.connect();
  }
sendMessage(msg: string){
    this.socket.emit('message', msg);
}

getMessage(){
  return this.socket.fromEvent('message').subscribe(message => {
    this.messages.push(message);
  });
}





 }
