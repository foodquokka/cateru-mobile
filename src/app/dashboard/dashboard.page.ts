import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { StorageserviceService } from '../services/storageservice.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(
    private router : Router,
    private modalCtlr: ModalController,
    private storageService: StorageserviceService
  ) { }

  ngOnInit() {
  }
  isAdmin(role){
    if(role === 'admin'){
      this.storageService.saveRole(role);
      this.presentModal();
    }
  }
  isCustomer(role){
    if(role === 'customer'){
      this.storageService.saveRole(role);
      this.router.navigate(['/qrlogin']);
    }
  }
  async presentModal() {
    const modal = await this.modalCtlr.create({
      component: LoginPage,
      
     // cssClass: 'my-custom-modal-css'
     
    });
    await modal.present();
  }

}
