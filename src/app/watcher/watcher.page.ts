import { Component, OnInit } from '@angular/core';
import { WaiterService } from '../waiter/service/waiter.service';
import { AlertController, ModalController } from '@ionic/angular';
import { WaiterorderdetailPage } from '../waiter/waiterorderdetail/waiterorderdetail.page';

@Component({
  selector: 'app-watcher',
  templateUrl: './watcher.page.html',
  styleUrls: ['./watcher.page.scss'],
})
export class WatcherPage implements OnInit {
  drinklist: any;
  cardBackgroundColor = 'primary';
  cardColor = 'black';
  orderCard: string[] = [];
  flag = false;
  color :any;

  tables: any;
  counter: any;
  count = [];
  calls = [];
  billout = [];
  c: void;
  cnt: any;
  constructor(
    private waiterService: WaiterService,
    public alertCtrl: AlertController,
    private modalController: ModalController,
   
    ) {
     
    }

  ngOnInit() { 
   
  }
  refreshpage(){
   this.ionViewWillEnter();
  }
  ionViewWillEnter() {
    this.getDrinksList();
    this.getAllTable();
  
  } 
  getAllTable(){
    this.waiterService.getAllTable().then(data => {
      this.tables = (data as any).OccupiedTables;
      for(const table of this.tables){
        this.getItemsForServing(table.tableno);
        this.getConcernNotification(table.tableno);
        this.getBillOut(table.tableno);
      }
    });
  }
  getDrinksList() {
    this.waiterService.getOrderDrinks().then(data => {
      this.drinklist = (data as any).result;
    });
  }
  async presentmodal(tableno) {
    const modal = await this.modalController.create({
      component: WaiterorderdetailPage,
      componentProps: { tableno: tableno},
    });
    await modal.present();
  }
  
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAllTable();
      event.target.complete();  
      this.calls.length = 0; 
      this.count.length = 0;
      this.billout.length = 0;
    }, 1500);
  }

  setCount(data){
    this.count.push(data);
    console.log(this.count);
  }
  
  getItemsForServing(tableno){
    this.waiterService.getItemsForServing(tableno).then(data => {
      var count = (data as any).counter;
      this.setCount(count); 
    });
   
  }
  setCalls(calls){
    this.calls.push(calls);
    console.log(this.calls);
  }
  getConcernNotification(tableno){
    this.waiterService.getNotification(tableno).then(data => {
      var calls = (data as any).concern;
      this.setCalls(calls);
    });
  }
  setBillout(billout){
    this.billout.push(billout);
    console.log(this.billout);
  
  }

  getBillOut(tableno){
    this.waiterService.getBillOutList(tableno).then(data => {
      let billout = (data as any).billout;
      this.setBillout(billout);
    });
  }

}
