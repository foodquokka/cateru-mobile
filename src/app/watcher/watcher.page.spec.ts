import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatcherPage } from './watcher.page';

describe('WatcherPage', () => {
  let component: WatcherPage;
  let fixture: ComponentFixture<WatcherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatcherPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatcherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
