import { TestBed } from '@angular/core/testing';

import { ConfirmedorderlistService } from './confirmedorderlist.service';

describe('ConfirmedorderlistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmedorderlistService = TestBed.get(ConfirmedorderlistService);
    expect(service).toBeTruthy();
  });
});
