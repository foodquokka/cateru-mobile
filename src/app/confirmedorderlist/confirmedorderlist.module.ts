import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ConfirmedorderlistPage } from './confirmedorderlist.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmedorderlistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConfirmedorderlistPage]
})
export class ConfirmedorderlistPageModule {}
