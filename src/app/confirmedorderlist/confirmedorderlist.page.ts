import { Component, OnInit, NgZone, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmedorderlistService } from './confirmedorderlist.service';
import { ChangeStatus } from './changeStatus.model';
import { Total } from '../customer/menu/orderlist/total.model';
import { ModalController, ToastController } from '@ionic/angular';
import { Billout } from './billout.model';
import { CartService } from '../customer/services/cart.service';
import { Order } from './order.model';
import { ApiService } from '../cashier/service/api.service';
import { ConfirmedlistmodalPage } from '../confirmedlistmodal/confirmedlistmodal.page';
import { ChangeKitchenStatus } from './changekitchenstatus.model';
import { Kitchenorders } from '../customer/classes/kitchenorders';
import { AdminService } from '../customer/services/admin.service';
import { Servekitchen } from './serveKitchen.model';


@Component({
  selector: 'app-confirmedorderlist',
  templateUrl: './confirmedorderlist.page.html',
  styleUrls: ['./confirmedorderlist.page.scss'],
})
export class ConfirmedorderlistPage implements OnInit {
  selectedMenuID: any;
  items: string;
  flag: number;
  confirm = "CONFIRM";

  orderArray = [];
  kitchen: Kitchenorders;
  stat: any;
  statusBtn = [] as any;

  defaultStatus = "SERVE";
  servedOrderList: any;
  serves: any;
  quantity = [] as any;
  samplequantity: any;
  displayPendingOrders: any;
  bundleprices: any;
  dataIsLoaded = false;
  displayServedOrders: any;
  finalorders: { nonBundle: any[]; bundle: any[]; };
  tot: any;
  bundleTotalPrices: unknown;
  list: any;
  displayorders: { nonBundle: any[]; bundle: any[]; };
  orderTotal: any;
  pendingOrderMatrix: any[];
  constructor(
    private router: ActivatedRoute,
    private confirmedorderlistservice: ConfirmedorderlistService,
    private cartservice: CartService,
    private apiservice: ApiService,
    private modalController: ModalController,
    private toastCtlr: ToastController

  ) {
  }
  id = {} as ChangeKitchenStatus;
  orderid: any;
  orderlist: any;
  orderDetailId: any;
  status = {} as ChangeStatus;

  settotal = {} as Total;
  bill = {} as Billout;
  total = 0;
  finaltotal = 0;
  od: any;
  qty: any;
  oldStatus: any;
  sampleqty: any;
  curQty: any;
  inputqty: any;
  orderID = {} as Order;
  served = {} as Servekitchen;
  item: any;
  isenabled = [false, false, false, false, false, false];

  servedItemMatrix: any;


  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      this.orderid = Number(params.get('order_id'));
      console.log(this.orderid);
    });
    this.apiservice.getTotal(this.orderid).then(data => {
      this.orderTotal = (data);
    }).then(() => {
      this.getPendingOrders();
      this.getServedOrders();
      this.orderID.order_id = this.orderid;
    });
    console.log("ngOnInit");
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      console.log("doRefresh");
      //this.getDataAsAWhole();

      this.getPendingOrders();
      this.getServedOrders();
      //event.target.complete();
    }, 1500);
  }

  getPendingOrders() {
    this.confirmedorderlistservice.getWaitingOrderByID(this.orderid).then(data => {
      this.orderlist = (data as any).orders;
    }).then(() => {
      //generate matrix for dropdown menu
      this.pendingOrderMatrix = this.genServiceMatrix(this.orderlist);
    });
  }

  getServedOrders() {
    this.confirmedorderlistservice.getServedOrderByID(this.orderid).then(data => {
      this.servedOrderList = (data as any).orders;
    });
  }
  getMatrixEntry(index) {
    return this.servedItemMatrix[index];
  }
  genServiceMatrix(data) {
    let retArray = [];
    for (let i = 0; i < data.length; i++) {
      let retArrayElement = [];
      for (let x = 1; x <= data[i].served; x++)
        retArrayElement.push(x);
      retArray.push(retArrayElement)
    }
    return retArray;
  }


  getTotal() {
    this.apiservice.getTotal(this.orderid).then(data => {
      this.orderTotal = (data);
    });
  }

  removeserveItem(id) {
    this.confirmedorderlistservice.removeServeItem(id);

  }
  changeOrderStatus(id) {
    this.confirmedorderlistservice.changeKitchenStatus(id);
    this.confirmedorderlistservice.changeOrderStatusToServed(id);
    console.log(id);

  }
  async presentModal() {
   // console.log(orderid);
    const modal = await this.modalController.create({
      component: ConfirmedlistmodalPage,
     // componentProps: { orderid: orderid },
      // cssClass: 'my-custom-modal-css'
    });
    await modal.present();
  }
  serve(qty, id) {
    console.log(qty);
    console.log(id);
    this.served.id = id;
    this.served.serveItem = qty;
    this.confirmedorderlistservice.setServedQty(this.served);
    this.confirmedorderlistservice.checkQty(id);
    this.doRefresh();
  }

  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Please wait for the waiter for the bill.',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  billout() {
    this.bill.order_id = this.orderid;
    this.bill.status = 'billout';
    // this.bill.total = this.total;
    this.confirmedorderlistservice.billout(this.bill);
    this.presentModal();
   // this.presentToast();

  }
  getBundlePrices() {
    this.confirmedorderlistservice.getBundlePrices().then(data => {
      this.bundleprices = (data);
      // console.log('BUNDLEPRICES');
      // console.log(this.bundleprices);
    });

  }
}
