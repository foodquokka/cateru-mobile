import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPAddress } from '../ipaddress';


@Injectable({
  providedIn: 'root'
})
export class ConfirmedorderlistService {
  IPaddress = new IPAddress;

  IP = this.IPaddress.getIP();
  getOrderByIDurl = this.IP + 'order/getorderbyid/';
  getServedOrderByIDurl = this.IP + 'order/getServedOrderByID/';
  getWaitingOrderByIDurl = this.IP + 'order/getwaitingorderbyid/';
  getBundlePriceByIdURL = this.IP + 'menu/getBundlePriceById';
  getTotalURL = this.IP + 'cashier/getSubTotal/';
  sendBillInfoURL = this.IP + 'cashier/sendbillinfo';
  changestatus = this.IP + 'order/changestatustoserved/';
  removeServedItemURL =this.IP + 'removeFromKitchenOrders/';
  serveMenu = this.IP + 'order/status/served';
  changeKitchenStatusURL = this.IP + 'servemenu';
  getServeQtyURL = this.IP + 'order/getservequantity/';
  setServedQtyURL = this.IP + 'order/setServedQty';
  checkQtyURL = this.IP + 'order/checkQty/'; 
  isServedURL = this.IP + 'order/isServed/';
  setRatingURL = this.IP +'rating/setrating';
 
  constructor(public http: HttpClient) { }

  setRating(data){
    return new Promise(resolve => {
      this.http.post(this.setRatingURL, data).subscribe(res => {
          resolve(res);
          console.log("setrating");
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  getOrderByID(orderid: number) {
    return new Promise(resolve => {
      this.http.get(this.getOrderByIDurl + orderid)
      .subscribe(data => {
        console.log("getOrderByID"); 
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err); 
      });
    });
  }
  removeServeItem(id){
    return new Promise(resolve => {
      this.http.post(this.removeServedItemURL + id, id).subscribe(res => {
       // resolve(res);
          console.log("removeServeItem");
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  changeKitchenStatus(data){
    return new Promise(resolve => {
      this.http.post(this.changeKitchenStatusURL, data).subscribe(res => {
          resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  serveOrder(data){
    return new Promise(resolve => {
      this.http.post(this.serveMenu, data).subscribe(res => {
          resolve(res);
          console.log(res);
         },
           err => {
             console.log('ERROR!: ', err);
           });
    });
  }
  getServeQty(data){
    return new Promise(resolve => {
      this.http.get(this.getServeQtyURL + data)
      .subscribe(data => {
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err); 
      });
    });
  }
  setServedQty(id){
    return new Promise(resolve => {
      this.http.post(this.setServedQtyURL, id).subscribe(res => {
          resolve(res);
          console.log(res);
         },
           err => {
             console.log('ERROR!: ', err);
           });
    });
  }
  
  changeOrderStatusToServed(id){
    return new Promise(resolve => {
      this.http.post(this.changestatus + id, id).subscribe(res => {
       // resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  getTotal(orderid){
    return new Promise(resolve => {
      this.http.get(this.getTotalURL + orderid).subscribe(data => {
         resolve(data); 
         console.log(data);
      
      }, err => {
        console.log(err);
      });
    });
  }
  billout(data){
    return new Promise(resolve => {
      this.http.post(this.sendBillInfoURL, data).subscribe(res => {
       // resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  checkQty(id){
    return new Promise(resolve => {
      this.http.post(this.checkQtyURL+id,id).subscribe(res => {
        resolve(res);
        console.log(res);
      }, err => {
        console.log('ERROR: ', err);
      });
    });
  }
  isServed(id){
    return new Promise(resolve => {
      this.http.get(this.isServedURL + id).subscribe(data => {
         resolve(data); 
         console.log(data);
      
      }, err => {
        console.log(err);
      });
    });
  }
  getServedOrderByID(id){
    return new Promise(resolve => {
      this.http.get(this.getServedOrderByIDurl + id)
      .subscribe(data => {
        console.log("getServedOrderByID");
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err); 
      });
    });
  }
  getWaitingOrderByID(id){
    return new Promise(resolve => {
      this.http.get(this.getWaitingOrderByIDurl + id)
      .subscribe(data => {
        console.log("getServedOrderByID");
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err); 
      });
    });
  }

  getBundlePrices(){
    return new Promise(resolve => {
      this.http.get(this.getBundlePriceByIdURL)
      .subscribe(data => {
        console.log("getBundlePrices");
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err); 
      });
    });
  }

  
}
