import { Component, OnInit } from '@angular/core';
import { ManagerauthPage } from '../receptionist/managerauth/managerauth.page';
import { RestService } from '../receptionist/services/rest.service';
import { Tables } from '../receptionist/rtab5/tables.model';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.page.html',
  styleUrls: ['./manager.page.scss'],
})
export class ManagerPage implements OnInit {
  availabletables: any;
  occupiedtables: any;
  tables = {} as Tables;
  transferfrom: any;
  transferto: any;

  isShowBtn = false;
  firstTable: any;
  secondTable: any;
  showMerge = true;
  showTransfer = true;

  constructor(
    private restService: RestService,
    private toastCtlr: ToastController) { }

  ngOnInit() {

    this.getAllAvailableTable();
    this.getAllOccupiedTable();
  }
  ionViewWillEnter() {
    this.getAllAvailableTable();
    this.getAllOccupiedTable();
    this.transferfrom = "";
    this.transferto = "";
    this.firstTable = "";
    this.secondTable = "";
  }
  getAllAvailableTable() {
    this.restService.getAvailableTable().then(data => {
      this.availabletables = (data as any).AvailableTables;
    });
  }
  getAllOccupiedTable() {
    this.restService.getOccupiedTable().then(data => {
      this.occupiedtables = (data as any).OccupiedTables;
    });
  }

  transferTable() {
    let tables = {
      transferfrom: this.transferfrom,
      transferto: this.transferto
    }
    this.restService.tabletransfer(tables).then(() => {
      this.doRefresh();
    }).then(() => {
      this.presentToast();
    });
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.ionViewWillEnter();
    }, 1500);
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Transfer success!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  setTransferFrom(selectedValue) {
    this.transferfrom = selectedValue;
    console.log(this.transferfrom);
  }
  setTransferTo(selectedValue) {
    this.transferto = selectedValue;
    console.log(this.transferto);
  }
  setFirstTable(firstTable) {
    this.firstTable = firstTable;
    console.log(this.firstTable);
  }
  setSecondTable(secondTable) {
    this.secondTable = secondTable;
    console.log(this.secondTable);
  }
  mergeTable() {
    let mergeTable = {
      firsttable: parseInt(this.firstTable),
      secondtable: parseInt(this.secondTable)
    }
    this.restService.mergeTables(mergeTable).then(() => {
      this.doRefresh();
    }).then(() => {
      this.presentToast();
    });
  }
  showMergeTable() {
    this.showMerge = false;
    this.showTransfer = true;
  }
  showTransferTable() {
    this.showMerge = true;
    this.showTransfer = false;
  }

}


