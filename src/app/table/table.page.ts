import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DeviceTableService } from '../services/device-table.service';
import { Uid } from '@ionic-native/uid/ngx';
import { Table } from './table.model';
import { Devicetable } from './devicetable';
import { StorageserviceService } from '../services/storageservice.service';
import { DeviceInfo } from '../qrlogin/deviceInfo.model';
import { AdminService } from '../customer/services/admin.service';
import { QrloginService } from '../qrlogin/qrlogin.service';
import { TableStatus } from '../qrlogin/tablestatus.model';
import { CartService } from '../customer/services/cart.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.page.html',
  styleUrls: ['./table.page.scss'],
})
export class TablePage implements OnInit {

  tableno: any;
  tablelist: any;
  selectedTable: number;
  Table = {} as Table;
  tables: any;
  scannedData: any;
  employee_info: any;
  orderInfo;
  infos: [];
  userid: number;
  custid: any;
  username: string; // from ion-input
  table = {};
  encodedData = '';
  QRSCANNED_DATA: string;
  isOn = false;
  qrScan: any;
  pos: string;
  position: any;
  deviceNo: any;
  isSuccess: boolean;
  custName;
  empid: any;
  tableStatus = {} as TableStatus;
  deviceinfo = {} as DeviceInfo;
  orderID: any;
  tbNo: any;

  new: any;
  exist: any;
  isShow = false;
  isShowOccupiedTable = false;
  occupiedtables: any;
  tabless: any;
  status: any;
  orders: any;
  order_id: any;
  no: any;
  ordid: any;

  constructor(
    private deviceTableService: DeviceTableService,
    private auth: AuthenticationService,
    private route: Router,
    private uid: Uid,
    private storageservice: StorageserviceService,
    private adminservice: AdminService,
    private devicetable: DeviceTableService,
    private qrService: QrloginService,
    private routerr: ActivatedRoute,
    private cartService: CartService,
  ) {

  }

  ngOnInit() {
    this.routerr.paramMap.subscribe(params => {
      this.empid = Number(params.get('empid'));
      console.log(this.empid);
    });
  }
  refreshpage() {
    this.ionViewWillEnter();
  }
  ionViewWillEnter() {
    this.getTableList();
  }
  closedevice() {
    this.auth.logout();
    this.route.navigate(['qrlogin']);
  }

  getTableList() {
    this.deviceTableService.getTableList().then(data => {
      this.tablelist = (data as any).allTables;
      console.log(this.tablelist);
    });
  }
  setTableNo() {
    this.tableno = this.selectedTable;
    console.log(this.tableno);
    this.deviceTableService.setTableNo(this.tableno);
  }
  setDeviceUID() {
    this.Table.deviceuid = this.uid.UUID;
    this.deviceTableService.setDeviceID(this.Table);
  }
  getTableNo() {
    const uid = this.uid.UUID;
    this.deviceTableService.getdevicetableno(uid);
    console.log('hello');

  }
  openMenu(tableno) {
    console.log(tableno);

    this.qrService.getOrderByTableNo(tableno).then(data => { // check if naay na send na nga order to kitchen
      this.orders = (data as any).order_id;

      console.log(this.orders);
      //current customer
      if (this.orders != '') {
        console.log(this.orders, tableno);
        this.goToCustomerTable(this.orders,tableno);
      } else {
        //new customer
        console.log(tableno);
        this.saveTransactionInfo(tableno);
      }
    });
  }

  goToCustomerTable(order_id,tableno) {
    console.log(order_id);
    console.log(tableno);
     this.storageservice.saveOrderID(order_id);
     this.route.navigate(['/menu'], { queryParams: { order_id: JSON.stringify({ order_id: order_id,tableno: tableno, isNewCust: false }) } });
  }
  saveTransactionInfo(tableno) {
    this.deviceinfo.custid = this.random();
    this.deviceinfo.empid = this.empid;
    this.deviceinfo.date_ordered = Date.now();
    this.deviceinfo.tableno = tableno;

    console.log(this.deviceinfo);
    this.adminservice.postToOrder(this.deviceinfo).then(data => {
      this.storageservice.saveOrderID((data as any).order_id);
      // this.route.navigate(['/menu',(data as any).order_id]);
      this.route.navigate(['/menu'],
        { queryParams: { order_id: JSON.stringify({ order_id: (data as any).order_id, tableno: tableno, isNewCust: true }) } });
      console.log(this.deviceinfo);
    });


    this.setTableOccupied(tableno);
    this.storageservice.saveTableNo(tableno);
  }

  setTableOccupied(tableno) {
    console.log(tableno);
    this.tableStatus.tableno = tableno;
    this.tableStatus.status = 'Occupied';
    this.qrService.setTableStatus(this.tableStatus);
    console.log('set table status');
  }

  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getTableList();
      event.target.complete();
    }, 1500);
  }
  successLogin(data) {
    this.qrService.saveOrdersData(this.orderInfo);
  }
  random(): number {
    const rand = Math.floor(Math.random() * 20) + 1;
    return rand;
  }

  setCustId(id) {
    this.custid = id;
  }
  getCustId() {
    console.log(this.custid);
  }
  getOccupiedTable() {
    this.qrService.getOccupiedTable().then(data => {
      this.occupiedtables = (data as any).OccupiedTables;
    });
  }

}
