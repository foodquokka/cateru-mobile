import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { 
    path: 'starting-page/:id',
    loadChildren: './starting-page/starting-page.module#StartingPagePageModule'
  },
  { path: 'cartmodal', loadChildren: './menu/cartmodal/cartmodal.module#CartmodalPageModule' },
  { path: 'promo-modal', loadChildren: './menu/promo-modal/promo-modal.module#PromoModalPageModule' }
];
@NgModule({
 // declarations: [],
  imports: [
   // CommonModule
   RouterModule.forChild(routes)
  ],
  exports: [ RouterModule]
})
export class CustomerrRoutingModule { }
