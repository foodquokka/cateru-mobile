import { Component, OnInit, Input } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-promo-modal',
  templateUrl: './promo-modal.page.html',
  styleUrls: ['./promo-modal.page.scss'],
})
export class PromoModalPage implements OnInit {
  @Input("bundleid") bundleid;
  promos: any;

  constructor( public adminService: AdminService) { }

  ngOnInit() {
    this.getPromoByBundleId();
  }

  getPromoByBundleId(){
    this.adminService.getPromoByBundleId(this.bundleid).then(data => {
      this.promos = (data as any).menus;
      console.log(this.promos);
    });
  }

}
