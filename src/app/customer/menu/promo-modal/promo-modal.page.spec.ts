import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoModalPage } from './promo-modal.page';

describe('PromoModalPage', () => {
  let component: PromoModalPage;
  let fixture: ComponentFixture<PromoModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
