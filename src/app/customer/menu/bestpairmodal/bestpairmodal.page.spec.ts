import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestpairmodalPage } from './bestpairmodal.page';

describe('BestpairmodalPage', () => {
  let component: BestpairmodalPage;
  let fixture: ComponentFixture<BestpairmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestpairmodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestpairmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
