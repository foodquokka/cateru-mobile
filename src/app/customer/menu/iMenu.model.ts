export interface Menu {
    menuID: number;
    name: string;
    price: any;
    details: string;
    images: any;
    servingsize: number;
}
