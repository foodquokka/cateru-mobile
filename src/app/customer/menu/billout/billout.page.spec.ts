import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BilloutPage } from './billout.page';

describe('BilloutPage', () => {
  let component: BilloutPage;
  let fixture: ComponentFixture<BilloutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BilloutPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BilloutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
