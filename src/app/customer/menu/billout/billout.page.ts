import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { OrderList } from '../../classes/order-list';
import { Discount } from '../../classes/discount';

@Component({
  selector: 'app-billout',
  templateUrl: './billout.page.html',
  styleUrls: ['./billout.page.scss'],
})
export class BilloutPage implements OnInit {
  servedList;
  orderData: any;
  orderPromise: Promise<{}>;
  order_id: number;
  total = 0;
  discount: any;
  discountedTotal: any;
  discounted: boolean;
  discountValue: any;
  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.order_id = this.adminService.getOrderID();
    this.getList();
  }

  getList() {
    this.adminService.getServedOL(this.order_id).then(list => {
      console.log(list);
      this.servedList = (list as any).list;
      // getting total
      console.log('SERVED LIST: ', this.servedList[0]);
      for (const item of this.servedList) {
        console.log('subtotal: ', item.subtotal);
        this.total = this.total + item.subtotal;
      }
      console.log(this.total);
    });
  }
  
  onSelect(value) {
    console.log('SELECTED VALUE: ', value);
    if (value === 'senior') {
      this.discounted = true;
    } else {
      this.discounted = false;
    }
    const billoutdata = new Discount();
    billoutdata.discountType = value;

    this.adminService.sendDiscountType(billoutdata);
    this.orderPromise = this.adminService.getOrder(this.order_id);
    this.orderPromise.then(servedlist => {
      this.orderData = (servedlist as any).order;
      console.log('ORDERDATA', this.orderData);
      this.discountValue = this.orderData.discount;
      this.discountedTotal = this.orderData.discountedTotal;
      console.log('DISCOUNT', this.discountValue, 'DISCOUNTED', this.discountedTotal);
    });
  }

  billoutConfirm() {

    this.adminService.pay(this.orderData);
  }

}
