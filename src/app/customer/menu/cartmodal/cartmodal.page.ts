import { Component, OnInit, Input } from '@angular/core';
import { AlertController, ToastController, ModalController, NavParams, Events, NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../../services/admin.service';
import { StorageserviceService } from 'src/app/services/storageservice.service';
import { CartService } from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';
import { Menuclass } from '../menuclass';
import { DomSanitizer } from '@angular/platform-browser';
import { BestPair } from './bestpair.model';
import { Location } from '@angular/common';



@Component({
  selector: 'app-cartmodal',
  templateUrl: './cartmodal.page.html',
  styleUrls: ['./cartmodal.page.scss'],
})
export class CartmodalPage implements OnInit {

  @Input("menuID") menuID;

  private cartItemCount = new BehaviorSubject(0);

  slideOpts = {
    slidesPerView: 6,
    freeMode: true,
    //initialSlide: 1,
    //speed: 400
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }
  //menuID;
  menuDetails: any;
  order_id: any;
  items: string;
  qty: any = 1;
  flag: number;
  details: any[];
  menus: Menuclass;
  pairs: any;
  allmenu: any;
  allMenu: any;
  pair: any;
  tno: any;
  bestpair = {} as BestPair;
  finalpair = [];
  modal: HTMLIonModalElement;
  final: any;
  menudetail: any;
  menuss: any;
  data: any;
  c: any;

  constructor(
    public modalController: ModalController,
    public adminService: AdminService,
    private toastCtlr: ToastController,
    private storageService: StorageserviceService,
    public cartService: CartService,
    public router: ActivatedRoute,
    public navParam: NavParams,
    private sanitizer: DomSanitizer,
    private location: Location

  ) {


  }
  ngOnInit() {
    this.showMenuDetail();

    let id = { "menuID": this.menuID };
    console.log(id);
    this.adminService.getPair(id).then(data => {
      this.pairs = (data as any).menu;
      console.log("PAIRS:");
      console.log(this.pairs);
      for (const pair1 of this.pairs) {
        let data = {
          image: this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + pair1.image),
          name: pair1.name,
          menuID: pair1.menuID
        };
        this.finalpair.push(data);
      }
    });

    this.storageService.getOrderID().then(data => {
      this.order_id = (data);
    });

    this.adminService.getMenuDetail(this.menuID).then(data => {
      this.menuDetails = (data as any).menudetail;
    });

    this.storageService.currentData.subscribe(items => this.items = items);
    this.cartItemCount = this.getCartItemCount();
  }

  setData(data) {
    console.log(data);
    this.finalpair.push(data);
    console.log(this.finalpair);
  }
  myBackButton() {
    this.location.back();
  }

  showMenuDetail() {
    this.allmenu = [];
    return this.adminService.getMenuDetail(this.menuID).then(data => {
      this.allMenu = (data as any).menudetail;

      for (const c of this.allMenu) {
        this.menus = new Menuclass();
        this.menus.menuID = c.menuID;
        this.menus.details = c.detail;
        this.menus.name = c.name;
        this.menus.price = c.price;
        this.menus.servingsize = c.serving_size;
        this.menus.images = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + c.image);
        this.allmenu.push(this.menus);
      }
      console.log(this.allmenu);
    });
  }
  getMenuDetailByID(menuID) {
    this.adminService.getMenuDetail(menuID).then(data => {
      this.menuss = (data as any).menus;
      console.log(this.menuss);
    });
  }
  minusQty() {
    if (this.qty > 1) {
      this.qty -= 1;
    }
  }
  getCartItemCount() {
    return this.cartItemCount;
  }

  addQty() {
    return this.qty += 1;
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Order added to cart',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  async itemExistInCartToast(){
    const toast = await this.toastCtlr.create({
      message: 'Item is in cart',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  async addToCart(menuID) {
    this.data = Array();
    let existed = false;
    let itemsToBeSent = {};
    this.storageService.getOrderID().then(data => {
      this.order_id = (data);
      this.cartService.getAllCartItems(this.order_id).then(data => {
        this.c = (data as any).items;
        console.log('cart');
        console.log(this.c);
        for (const c of this.c) {
          if (c.menuID == menuID && c.bundleid == null) {
            existed = true;
          }
        }
        if (!existed) {
          console.log(existed);
          console.log(this.order_id);
            itemsToBeSent = {
              menuID: menuID,
              order_id: this.order_id,
              qty: this.qty
            }
              this.data = itemsToBeSent;
              console.log(this.data);
    
          this.cartService.addToCart(this.data);
          this.presentToast();
            this.doRefresh();
        }else{
          this.itemExistInCartToast();
          //this.dismiss();
        }

      });
    }); 
    this.dismiss();
  }
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.data;
    }, 2000);

  }
  getPair() {
    this.adminService.getPair(this.menuID).then(data => {
      this.pairs = (data as any).transactions;
      console.log(this.pairs);
    })
  }
  bestPairMenu(menuID) {
    this.allmenu = [];
    return this.adminService.getMenuDetail(menuID).then(data => {
      this.allMenu = (data as any).menudetail;

      for (const c of this.allMenu) {
        this.menus = new Menuclass();
        this.menus.menuID = c.menuID;
        this.menus.details = c.detail;
        this.menus.name = c.name;
        this.menus.price = c.price;
        this.menus.servingsize = c.serving_size;
        this.menus.images = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + c.image);
        this.allmenu.push(this.menus);
      }
      console.log(this.allmenu);
    });
  }


  subtractQty() {
    return this.qty -= 1;
  }

}

