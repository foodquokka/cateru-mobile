export class Menuclass {
    menuID: number;
    name: string;
    price: any;
    details: string;
    images: any;
    servingsize: number;
}
