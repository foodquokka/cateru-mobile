import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertController, Events } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { AdminService } from '../../services/admin.service';
import { Customer } from '../../classes/customer';
import { OrderDetail } from 'src/app/customer/classes/order-detail';
import { ActionSheetController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
// import { CartService } from '../../services/cart.service';
// import { Cartitems } from '../../classes/cartitems';
import { DeviceTableService } from 'src/app/services/device-table.service';
import { StorageserviceService } from 'src/app/services/storageservice.service';
import { Total } from './total.model';
// import { SocketService } from 'src/app/services/socket.service';
// import { Observable, Subscription } from 'rxjs';
// import { startWith } from 'rxjs/operators';
// import { Drinks } from 'src/app/waiter/orders.model';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Socket } from 'ngx-socket-io';
import { CartService } from '../../services/cart.service';
import { SocketService } from 'src/app/services/socket.service';
import { Kitchen } from 'src/app/kitchen/ktab1/kitchen';
import { KitchenOrders } from './kitchen.model';
import { Kitchenorders } from '../../classes/kitchenorders';



@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.page.html',
  styleUrls: ['./orderlist.page.scss'],
})
export class OrderlistPage implements OnInit {

  kitchenorders = {}as KitchenOrders;
  itemPrice;
  qty;
  cart;
  orders;
  data: any;
  tableno: number;
  inputValue = [{ name: 'Myra' }];
  customer: Customer;
  // itemPrice: (id: any) => any;
  orderList: OrderDetail[] = [];
  itemName: string;
  orderListTotal = 0;

  // UI CONTROL
  disablePlaceOrderButton;
  disableServedButton;

  buttonClicked = false;
  unserved = true;
  checkedItem: OrderDetail[] = [];
  servedItems: OrderDetail[] = [];
  servedQty: number;
  servedQtySet = false;
  qtyCard = false; // card for quantity input
  itemQty: number;
  jsonObject: any;
  selectedItems: any[];
  total: any;
  tablenum: number;
  deviceuid: string;
  orderDetails: any;
  items: any;
  cartData: any;
  orderid:any;
  settotal = {} as Total;
  item: string;
  order_id: Promise<any>;
  sample: any;
  orderArray = [];
  kitchenOrder;
  

  

  constructor(
    public http: HttpClient,
    public alertController: AlertController,
    private adminService: AdminService,
    public actionSheetController: ActionSheetController,
    public navCtrl: NavController,
    private router: Router,
    // private cartService: CartService,
    private storageService: StorageserviceService,
    // private deviceTableService: DeviceTableService,
    // private route: ActivatedRoute,
    private events: Events,
    private socket: SocketService,
    private cartservice: CartService,
    private route: ActivatedRoute

  ) {
   // this.getAllCartItems();
  
  }

  ngOnInit() {
   
    this.storageService.currentData.subscribe(data =>
        this.cartData = data
        );
    
        this.cart = JSON.parse(this.cartData);
    console.log(this.cart);

   
  }
  goToMenu() {
    this.navCtrl.navigateForward('/menu');
  }


  async callWaiterAlert() {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
      buttons: ['OK']
    });

    await alert.present();
  }

  getOrderList() {
   return this.orderList = this.adminService.getOrderList();
  }
  remove(menuID) {
    const processed = this.cart.filter(value => value.menuID !== menuID);
    this.storageService.changeData(JSON.stringify(processed));
    this.cart = processed;

    console.log(this.cart);
  }

  addQty(menuID) {
    ++this.cart.find(item => item.menuID === menuID).qty;
  } 
  subtractQty(menuID) {
   --this.cart.find(item => item.menuID === menuID).qty;
  }
  getTotalPerItem(menuID) {
    const qty = this.cart.find(item => item.menuID === menuID).qty;
    const price = this.cart.find(item => item.menuID === menuID).price * qty;

    return price;
  }
 
  subtotal() {
      let total = 0;
      for (const data of this.cart) {
        total += (data.price * data.qty);
      }
      // return this.formatPrice(total);
      return total;
  }
  formatPrice(value) {
    const val = (value / 1);
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  sendOrderList() {
     console.log(this.cart);
    for (const c of this.cart) {
      this.orderid = c.order_id;
      this.orders = new OrderDetail();
      this.orders.menuID = c.menuID;
      this.orders.order_id = c.order_id;
      this.orders.orderQty = c.qty;
      this.orders.status = 'waiting';
      this.orders.subtotal = this.getTotalPerItem(c.menuID);
      this.orders.date_ordered = Date.now();
      this.orders.updated_at = Date.now();
      this.orderList.push(this.orders);

    
      
      console.log(this.orderList);
      this.adminService.saveToTemporaryKitchenTable(this.orders);
     
    }
  
  this.adminService.processPlaceOrder(this.orderList); 
   
   // this.saveToKitchen();
  
   

   // this.socket.sendMessage('from orderlist');
   
   // this.sample = 'HELLO';
    this.router.navigate(['/confirmedorderlist', this.orderid]);
    this.emptyCurrentCart();
   
  }

 
  goToConfirmedOrders(){
    console.log(this.orderid);
    this.router.navigate(['/confirmedorderlist', this.orderid]);
   // this.emptyCurrentCart();
  }
  emptyCurrentCart(){ 
   const processed = (this.cart = []);
    this.storageService.changeData(JSON.stringify(processed));
   this.cart = processed;


   console.log(this.cart);
  }
  getAllCartItems(){
    
    this.cartservice.getItems(this.orderid).then(data => {
      this.cart = (data as any).data;
      console.log(this.cart);
    })
  }



  setTotal(){
    this.settotal.order_id = this.orderid;
    this.settotal.total = this.subtotal();
    this.adminService.setTotal(this.settotal);

    console.log('SETTOTAL');
  }
 


}
