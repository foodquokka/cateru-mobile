import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AdminService } from '../../services/admin.service';

import { OrderDetail } from 'src/app/customer/classes/order-detail';
import { CartService} from '../../services/cart.service';
import { BehaviorSubject } from 'rxjs';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Cartitems } from '../../classes/cartitems';
import { DeviceTableService } from 'src/app/services/device-table.service';
import { StorageserviceService } from 'src/app/services/storageservice.service';
@Component({
  selector: 'app-menudetail',
  templateUrl: './menudetail.page.html',
  styleUrls: ['./menudetail.page.scss'],
})

export class MenudetailPage implements OnInit {
  qty: any = 1;
  selected: any;
  selectedMenuID: any;
  menuDetails;
  fname: any;
  menuItem: any;
  tableNo = 1;
  subtotal: number;
  Orderdetail = {} as OrderDetail;
  tempItemContainer;
  orderList: OrderDetail[] = [];
  cartOrder: Cartitems[] = [];
  orderID: number;
  order_id: any;
  cart;
  menus = [];
  private cartItemCount = new BehaviorSubject(0);
  deviceuid: any;
  sample: any;
  tablenum: number;
  items: string;
  flag: number;

  // @ViewChild('cart', { static: false, read: ElementRef})fab: ElementRef;
  constructor(
    public adminService: AdminService,
    public cartService: CartService,
    public alertController: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private storageService: StorageserviceService,
    private deviceTableService: DeviceTableService
    ) {
    }

  ngOnInit() {

    // get ID from url
    this.route.paramMap.subscribe(params => {
      this.selectedMenuID = Number(params.get('menuID'));
    });

    console.log(this.selectedMenuID);

    this.adminService.getMenuDetail(this.selectedMenuID).then(data => {
        this.menuDetails = (data as any).menudetail;
        console.log( this.menuDetails);
      });
    this.sendSelectedMenuID();

    this.storageService.currentData.subscribe( items => this.items = items );

    this.cartItemCount = this.getCartItemCount();

  }
  async callWaiterAlert() {
    const alert = await this.alertController.create({
      header: null  ,
      subHeader: null,
      message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
      buttons: ['OK']
    });
    await alert.present();
  }
  minusQty() {
    if (this.qty > 1) {
      this.qty -= 1;
    }
  }
  getCartItemCount() {
    console.log('GET CART ITEM COUNT');
    return this.cartItemCount;
  }

  addQty() {
    this.qty += 1;
  }
  addToCart() {

    this.storageService.getOrderID().then(data => {
      this.order_id = (data);

      console.log(this.menuDetails);
      for (const menu of this.menuDetails) {
    const itemsToBeSent = {
      menuID : this.selectedMenuID,
      orderID : this.order_id,
      name : menu.name,
      qty : this.qty,
      image: menu.images,
      price : menu.price
    };

    if (this.items === '') {
      const currentData = new Array<any>(itemsToBeSent);
      this.storageService.changeData(JSON.stringify(currentData));
      this.cartItemCount.next(this.cartItemCount.value + 1);
      console.log('option 1', currentData);
      this.cartItemCount.next(this.cartItemCount.value + 1);
      console.log(this.cartItemCount.value);
    } else {
       const currentData = JSON.parse(this.items);
       for (const item of currentData) {
         if (item.menuID === this.selectedMenuID) {
           this.flag = 1;
           break;
           }
         }
       if (this.flag !== 1) {
          currentData.push(itemsToBeSent);
          this.storageService.changeData(JSON.stringify(currentData));
          console.log('option 2', currentData);
          this.cartItemCount.next(this.cartItemCount.value + 1);
      }
    }
    }
    });
  }

  openCart() {
    this.storageService.getOrderID().then(data => {
      this.orderID = data
    });
     //this.router.navigate(['orderlist']);
    console.log(this.orderID);
  }

  sendSelectedMenuID() {
    this.adminService.setSelectedMenuID(this.selectedMenuID);
  }




}
