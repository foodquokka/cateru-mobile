import { Component, OnInit} from '@angular/core';
import { AlertController,ToastController } from '@ionic/angular';
import { AdminService } from '../services/admin.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Menu } from './iMenu.model';
import { ModalController } from '@ionic/angular';
import { StorageserviceService } from 'src/app/services/storageservice.service';
import { CartService } from '../services/cart.service';
import { CartmodalPage } from './cartmodal/cartmodal.page';
import { DomSanitizer } from '@angular/platform-browser';
import { Menuclass } from './menuclass';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  slideOpts = {
    slidesPerView: 5,
    freeMode: false,

    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }

  };

  allMenu: any;
  allmenu: Menuclass[] = [];
  allPromos = [];
  menus = {} as Menu;
 // id: any;
  order_id: any;
  showPromo: boolean;
  showMenus: boolean;
  menuDetails: any;
  allCategories: any;
  orderid: any;
  cart: any;
  total: any;
  promos: any;
  showMenu: boolean;
  bundleid: any;
  menuID: any;
  data: any;
  cartitems: [];
  qty: any = 1;
  packages: any;
  tblno: any;
  foodQuokka: any;
  subtotalperitem: number;
  c: any;
  categoryname: string;
  constructor(
    public adminService: AdminService,
    public alertController: AlertController,
    private route: ActivatedRoute,
    private router: Router,
    private storageService: StorageserviceService,
    public cartService: CartService,
    private modalController: ModalController,
    private sanitizer: DomSanitizer,
    private toastCtlr: ToastController,
    private decimalPipe: DecimalPipe
  ) {
  }

  ngOnInit() {
    this.getAllCategories();
    this.showAllMenu();
    this.getPromo();
    //FOR AVAILABLE
    this.route.queryParams.subscribe(params => {
      let data = JSON.parse(params['order_id']);
      console.log("I'm data from json query params");
      console.log(data);
      if (data.isNewCust) {
        this.orderid = data.order_id;
        this.tblno = data.tableno;
        console.log("data.orderid NEW CUSTOMER");
        console.log(data.orderid);
        this.getOrderId(this.orderid);
      }
      else {
        this.orderid = data.order_id;
        this.tblno = data.tableno;
        console.log("data.orderid NOT NEW CUSTOMER");
        console.log(data.order_id);
        console.log(this.tblno);
        this.getOrderId(this.order_id);
      }
    });
    this.populateCheckoutQuokka();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.populateCheckoutQuokka();
    }, 1000);

  }

  getOrderId(orderid) {
    console.log("data input from getorderId", orderid);
    this.order_id = orderid;
    console.log("this.order_id[getOrderId()]", this.order_id);
  }
  populateCheckoutQuokka() {
    this.foodQuokka = [];
    console.log("FQ", this.orderid);

    this.cartService.getCartItems(this.orderid).then(data => {
      this.cart = (data as any).items;
     
    }).then(() => {
      this.cartService.getBundleItems(this.orderid).then(data => {
        this.packages = (data as any).send; 
        this.foodQuokka = this.cart;
        this.packages.forEach(element => {
          element.forEach(x => {
            console.log("MAP_FQ");
            console.log(x);
            this.orderWithFoodQuokka({
              name: x.bundlename,
              price: parseInt(x.price),
              qty: parseInt(x.qty),
              order_id: parseInt(x.order_id),
              id: parseInt(x.id),
              bundleid: parseInt(x.id)
            });
          });
        });
        this.total = this.subtotal();
      });
    });
   // this.doRefresh();
  }

  orderWithFoodQuokka(data) {
    this.foodQuokka.push(data);
    console.log(this.foodQuokka);
  }


showAllMenu() { //USED
  this.categoryname = "All Menu";
    this.allmenu = [];
    return this.adminService.getData().then(data => {
      this.allMenu = (data as any).result;

      for (const c of this.allMenu) {
        this.menus = new Menuclass();
        this.menus.menuID = c.menuID;
        this.menus.details = c.details;
        this.menus.name = c.name;
        this.menus.price = c.price;
        this.menus.servingsize = c.servingsize;
        this.menus.images = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + c.image);
        this.allmenu.push(this.menus);
      }
      console.log(this.allmenu);
      this.showMenus = false;
      this.showPromo = true;
    });

  }
  getPromo() {
    this.categoryname = "Promo";
    this.allPromos = [];
    this.adminService.getPromo().then(data => {
      this.allMenu = (data as any).menus;

      for (const c of this.allMenu) {

        let data = {
          name: c.name,
          bundlename: c.bundlename,
          bundleid: c.bundleid,
          price: c.price,
          servingsize: c.servingsize,
          menuID: c.menuID
        };
        this.allPromos.push(data);
      }
      console.log(this.allPromos);
    });
    this.showPromo = false;
    this.showMenus = true;

  }
  getMenuByCategory(id,categoryname) {
    this.categoryname = categoryname;
    this.allmenu = [];
    this.adminService.getMenuByCategory(id).then(data => {
      this.allMenu = (data as any).allitems;

      for (const c of this.allMenu) {

        this.menus = new Menuclass();
        this.menus.menuID = c.menuID;
        this.menus.details = c.details;
        this.menus.name = c.name;
        this.menus.price = c.price;
        this.menus.servingsize = c.servingsize;
        this.menus.images = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + c.image);
        this.allmenu.push(this.menus);
      }
      console.log(this.allmenu);
      this.showMenus = false;
      this.showPromo = true;
    });

  }
  getAllCategories() {
    this.adminService.getAllCategory().then(data => {
      this.allCategories = (data as any).allcategories;
      console.log(this.allCategories);
    });
  }
  goToMyOrders() {
    console.log(this.order_id);
    this.router.navigate(['/confirmedorderlist', this.orderid]);
  }
  async presentModal(menuID) {
    console.log(menuID);
    let modal = await this.modalController.create({
      component: CartmodalPage,
      componentProps: { menuID: menuID },
      cssClass: 'my-custom-modal-css'

    });
    
    modal.onDidDismiss().then(() => {
      this.doRefresh();
    });
    modal.present();
  }



  increaseQtyQuokka(id) {
    ++this.foodQuokka.find(item => item.id === id).qty;
    this.total = this.subtotal();
  }
  decreaseQtyQuokka(id) {
    if (this.foodQuokka.find(item => item.id === id).qty !== 1) {
      --this.foodQuokka.find(item => item.id === id).qty;
    }
    this.total = this.subtotal();
  }
  getSubTotal(price, quantity) {
    return price * quantity;
  }
  subtotal() {
    let subtotal = 0;
    for (const data of this.foodQuokka) {
      subtotal += (data.price * data.qty);
    }
    return this.decimalPipe.transform(subtotal, '1.2-2');
  }
  remove(id, bundleid) {
    //cCHECK IF BUNDLE OR NOT
    console.log("Remove", id, "bundle?", bundleid);
    if (bundleid) {
      this.cartService.removebundleitemfromcart(id);
    }
    else
      this.cartService.removeItemFromCart(id);
    this.doRefresh();
  }
  removeAllItemByOrderID() {
    this.cartService.removeItem(this.orderid);
  }
  removebundleitemfromcart(bundleid) {
    this.cartService.removebundleitemfromcart(bundleid);
    this.doRefresh();
  }
  formatPrice(value) {
    const val = (value / 1);
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  sendOrderlistQuokka() {
    this.adminService.saveToTemporaryKitchenTable(this.foodQuokka)
    let info = {
      total: parseFloat(this.total.replace(/[^\d\.]/g, '')),
      order_id: this.orderid
    }
    console.log(info);
    this.adminService.setOrderTotal(info).then(() => {
      this.router.navigate(['/confirmedorderlist', this.orderid]);
      this.removeAllItemByOrderID();
      this.doRefresh();
    });
    

  }
  emptyCurrentCart() {
    const processed = (this.cart = []);
    this.storageService.changeData(JSON.stringify(processed));
    this.cart = processed;

    console.log(this.cart);
  }

  
  callWaiter() { 
    this.adminService.callWaiter(2);
    this.presentToast();
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

  async addToCart(bundleid) {
    this.data = Array();
    let existed = false;
    let itemsToBeSent = {};
    this.cartService.getAllCartItems(this.orderid).then(data => {
      this.c = (data as any).items;

      console.log(this.orderid);
      console.log(this.c);
      for (const c of this.c) {
        if (c.bundleid == bundleid) {
          existed = true;
        }
      }
      if (!existed) {
        console.log(existed);
        for (const promo of this.allPromos) {
          itemsToBeSent = {
            order_id: this.orderid,
            qty: this.qty,
            bundleid: parseInt(bundleid)
          }
          if (promo.bundleid === bundleid) {
            this.data =itemsToBeSent;
            break;
          }
        }
        this.cartService.addBundleToCart(this.data);
      }

    });
    this.doRefresh();
  }






}
