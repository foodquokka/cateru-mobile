import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableClearPage } from './table-clear.page';

describe('TableClearPage', () => {
  let component: TableClearPage;
  let fixture: ComponentFixture<TableClearPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableClearPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableClearPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
