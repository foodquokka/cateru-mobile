import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-table-clear',
  templateUrl: './table-clear.page.html',
  styleUrls: ['./table-clear.page.scss'],
})
export class TableClearPage implements OnInit {
  tableNum: number;
  table = {};

  constructor(private adminService: AdminService, private route: ActivatedRoute) { }

  ngOnInit() {
    // this.route.paramMap.subscribe(params => {
    //   this.tableNum = Number(params.get('tableno'));
    // });
    this.tableNum = this.adminService.getTableNumber();
    this.table = {tableno: this.tableNum};
    console.log(this.table);
  }

  clearTable() {
    this.adminService.sendToClearTable(this.table);
  }
}
