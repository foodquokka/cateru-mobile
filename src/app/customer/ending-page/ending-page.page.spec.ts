import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndingPagePage } from './ending-page.page';

describe('EndingPagePage', () => {
  let component: EndingPagePage;
  let fixture: ComponentFixture<EndingPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndingPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndingPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
