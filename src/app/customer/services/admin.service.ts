import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OrderDetail } from '../classes/order-detail';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { OrderList } from '../classes/order-list';
import { DeviceTableService } from 'src/app/services/device-table.service';
import { IPAddress } from 'src/app/ipaddress';
import { DomSanitizer } from '@angular/platform-browser';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  IPaddress = new IPAddress;
  IP = this.IPaddress.getIP();
     
  getPairURL = this.IP + 'apriori/getpairs';
  getEmpNameURL = this.IP + 'employee/employeename/';
  setCustomerInfoURL = this.IP + 'customer/setNewCustomer';
  getAvailableTableURL = this.IP + 'table/getAvailableTable';
  getMenuByCategoryURL = this.IP + 'menu/getmenubycategory/';
  getAllCategoriesURL = this.IP + 'menu/categorylist';
  getAllMenuURL = this.IP + 'menu/list?mode=list';
  getMenuDetailURL = this.IP + 'menu/menudetail/';
  getlatestorderidURL = this.IP + 'order/getlatestorderid/';
  getWaitingOrderListURL = this.IP + 'order/myorders/';
  getServedOrderListURL = this.IP + 'order/myorders/served/';
  postOrderAPIURL = this.IP + 'order/startorder';
  postOrderDetailAPIURL = this.IP + 'order/placeorder';
  setTotalURL  = this.IP + 'cashier/setTotal';
  changeStatusServeURL = this.IP + 'order/status/served/'; // for waiter change status
  discountURL = this.IP + 'order/myorders/discount/';
  getOrderURL = this.IP + 'order/allOrders/';
  confirmPaymentURL = this.IP + 'order/confirmPayment';
  clearTableURL = this.IP + 'table/cleartable/';
  saveToTemporaryKitchenTableURL = this.IP + 'saveToTemporaryKitchenTable';

  callWaiterURL = this.IP + 'callWaiter/';
  getPromoURL = this.IP + 'menu/getBundleMenus';
  getPromoByBundleIdURL = this.IP + 'menu/getPromoByBundleID/';
  setOrderTotalURL = this.IP + 'order/settotal';
  

  userID: number;
  loadedData: any;
  menuItem: any;
  tempArray: any;
  orderID: number;
  placeOrderMessage: any;
  orderList: OrderDetail[] = [];
  menuPrice: any;
  menuName: any;
  selectedMenuID: any;
  ordertotal: any;
  placeOrderStatus: boolean;
  orderdetailID: any;
  oList: any;
  servedItems = [];
  orderData: any;
  tableNum: number;
  result: any;
  servedListPromise: Promise<{}>;
  tempservedlist = [];
  servedItem: OrderList;
  tableno: number;
  datamenus: {};

  constructor(
    public http: HttpClient, 
    private router: Router, 
    public alertController: AlertController, 
    private deviceTableService: DeviceTableService,
    private sanitizer: DomSanitizer) {
    console.log('Hello from AdminService');
    //this.getData();
  }

//WAITERCALL
setOrderTotal(data){
  return new Promise(resolve => {
    this.http.post(this.setOrderTotalURL, data).subscribe(res => {
      resolve(res);
      console.log(res);
    }, err => {
      console.log('ERROR: ', err);
    });
  });
}
getPromo(){
  return new Promise(resolve => {
    this.http.get(this.getPromoURL).subscribe(data => {
      console.log(data);
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
}
getPromoByBundleId(bundleid){
  return new Promise((resolve, reject) => {
  this.http.post(this.getPromoByBundleIdURL + bundleid, bundleid).subscribe(data => {
  //  console.log(data);
    resolve(data);
  }, err =>{
    console.log(err);
  });
});
}
callWaiter(tableno){
    return new Promise((resolve, reject) => {
      this.http.post(this.callWaiterURL+tableno, tableno)
        .subscribe(res => {
          resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
}

  saveToTemporaryKitchenTable(data){ 
    console.log(data);
    return new Promise((resolve, reject) => {
      this.http.post(this.saveToTemporaryKitchenTableURL, data)
        .subscribe(res => {
          resolve(res);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
         //   this.errorAlert('Place order failed.');
          });
    });
  }
  // setQtyToServe(id){
  //   return new Promise((resolve, reject) => {
  //     this.http.post(this.setQtyServedURL + id, id)
  //       .subscribe(res => {
  //         resolve(res);
  //         console.log(res);
  //       },
  //         err => {
  //           console.log('ERROR!: ', err);
  //         });
  //   });
  // }
  get() {
    return new Promise(resolve => {
      this.http.get(this.getAllMenuURL).subscribe(data => {
       
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getPair(data) {
    console.log(data);
    return new Promise(resolve => {
      this.http.post(this.getPairURL ,data).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getData() {
    return new Promise(resolve => {
      this.http.get(this.getAllMenuURL).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getMenuByCategory(id){
    return new Promise(resolve => {
      this.http.get(this.getMenuByCategoryURL + id).subscribe(data => {
        resolve(data);
       // console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  setLoadedData(data) {
    this.loadedData = data.allMenus;
  }
  getLoadedData() {
    return this.loadedData;
  }


  // S E R V E D
  getServedOL(id) {
    console.log(this.getServedOrderListURL + id);
    return new Promise(resolve => {
      this.http.get(this.getServedOrderListURL + id).subscribe(olist => {
        resolve(olist);
        this.setServedItems(olist);
      }, err => {
        console.log(err);
      });
    });
  }

  setServedItems(item) {
    this.servedItems = item;
    console.log('ADMIN SERVICE SET SERVED ITEMS: ', this.servedItems);
  }

  getServedItems() {
    console.log('getter', this.servedItems);
    return this.servedItems;
  }

  // optional for passing data
  setMenuItem(item) {
    this.menuItem = item;
  }
  getMenuItem() {
    return this.menuItem;
  }
  // /optional

  setSelectedMenuID(menuID) {
    this.selectedMenuID = menuID;
  }

  getMenuByID(id) {
    const menuList = this.getLoadedData();
    for (const menuItem of menuList) {
      if (menuItem.menuID === id) {
        console.log('getmenubyId processed: ' + menuItem);
        return menuItem;
      }
    }
  }
  getAllCategory(){
    console.log(this.getAllCategoriesURL);
    return new Promise(resolve => {
      this.http.get(this.getAllCategoriesURL).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  // getMenuPrice() {
  //   this.menuPrice = this.getMenuPriceByID(this.selectedMenuID);
  //   return this.menuPrice;
  // }

  getMenuPrice() {
    return this.menuPrice;
  }
  getMenuPriceByID(id) {
    const menuList = this.getLoadedData();
    for (const menuItem of menuList) {
      if (menuItem.menuID == id) {
        return menuItem.price;
      }
    }
  }
  getMenuName() {
    this.menuName = this.getMenuNameByID(this.selectedMenuID);
    return this.menuName;
  }
  getMenuNameByID(id) {
    const menuList = this.getLoadedData();
    for (const menuItem of menuList) {
      if (menuItem.menuID === id) {
        return menuItem.name;
      }
    }
  }

  setMenuName(menuName) {
    this.menuName = menuName;
  }

  setMenuPrice(menuPrice) {
    this.menuPrice = menuPrice;
  }
  //for adding to order list
  setTempArray(tempItemContainer) {  //receiving from menudetail tempitemcontainer
    this.tempArray = tempItemContainer;
  }
  getTempArray() {
    return this.tempArray;
  }

  setOrderList(orderlist) {
    this.orderList = orderlist;
  }

  getOrderList() {
    return this.orderList;
  }
  /// ORDER DETAIL POST --triggered at orderList
  placeOrder(postData) {
    console.log(postData);
    return this.processPlaceOrder(postData).then(orderdetail => {
      console.log(orderdetail);
    });
  }
  processPlaceOrder(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.postOrderDetailAPIURL, data)
        .subscribe(res => {
          console.log(res);
          this.placeOrderSuccessAlert('Your order has been sent!');
        },
          err => {
            console.log('ERROR!: ', err);
            this.errorAlert('Place order failed.');
          });
    });
  }

  placeOrderSuccess(bool) {
    this.placeOrderStatus = bool;
  }
  getPlaceOrderStatus() {
    return this.placeOrderStatus;
  }

  // ORDER POST --triggered at starting-page
  getOrderID() {
    return this.orderID;
  }
  getLatestOrderID(tableno) {
    return new Promise(resolve => {
      this.http.get(this.getlatestorderidURL + tableno).subscribe(data => {
        resolve(data);
        this.orderID = (data as any).orderid;
       // console.log('latestorderid', this.orderID);
      }, err => {
        console.log(err);
      });
    });
  }
  // PAREHAA SA GETTABLENO
  postToOrder(orderdata) {
    return new Promise((resolve, reject) => {
      this.http.post(this.postOrderAPIURL, orderdata)
        .subscribe(res => {
          console.log('posttoorder', res);
          resolve(res);
          this.setTableNo((res as any).tableno);
          //this.setOrderID((res as any).order_id);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  setTableNo(tableno){
    this.tableno = tableno;
  }
  getTableNum(){
    return this.deviceTableService.getTableNo();
  }
  getTableNo(){
    return this.tableno;
  }
  async errorAlert(errorMessage) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: errorMessage,
      buttons: ['OK']
    });
    await alert.present();
  }
  async successAlert(successMessage) {
    console.log(successMessage);
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: successMessage,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigateByUrl('/menu');
          }
        }]
    });
    await alert.present();
  }
  async setStatusAlertSuccess(successMessage, item) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: successMessage,
      buttons: [
        {
          text: 'OK'
        }]
    });
    await alert.present();
  }
  async placeOrderSuccessAlert(successMessage) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: successMessage,
      buttons: ['OK']
    });
    await alert.present();
  }

  async confirmPaymentAlert(successMessage) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: successMessage,
      buttons: [
        {
          text: 'OK'
          , handler: () => {
            this.router.navigateByUrl('/ending-page/' + this.orderID);
          }
        }]
    });
    await alert.present();
  }
  setTotal(data){
    return new Promise((resolve, reject) => {
          this.http.post(this.setTotalURL, data)
            .subscribe(res => {
              console.log(res);
            });
        });
  }
 


  getID() {
    return new Promise(resolve => {
      this.http.get(this.changeStatusServeURL).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getOrderDetailID() {
    console.log(this.changeStatusServeURL);
    this.getID().then(data => {
      this.setOrderDetailID(data);
    });
  }

  setOrderDetailID(data) {
    this.orderdetailID = data.id;
    console.log(this.orderdetailID);
  }

  returnID() {
    return this.orderdetailID;
  }

  //  B I L L  O U T

  sendDiscountType(billoutdata) {
    this.processSendDiscount(billoutdata).then(orderData => {
      console.log(orderData);
    });
  }

  processSendDiscount(billoutdata) {
    return new Promise((resolve, reject) => {
      this.http.post(this.discountURL + this.orderID, billoutdata)
        .subscribe(res => {
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }

  getOrder(id) {
    console.log(this.getOrderURL + id);
    return new Promise(resolve => {
      this.http.get(this.getOrderURL + id).subscribe(oData => {
        resolve(oData);
      }, err => {
        console.log(err);
      });
    });
  }

  pay(order) {
    this.confirmPayment(order).then(o => {
      console.log(o);
    });
  }

  confirmPayment(order) {
    return new Promise((resolve, reject) => {
      this.http.post(this.confirmPaymentURL, order)
        .subscribe(res => {
          console.log(res);
          this.confirmPaymentAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }


  // CLEAR TABLE 
  sendToClearTable(id) {
    console.log('service received: ', id);
    this.clearTable(id).then(reply => {
      console.log(reply);
    });
  }

  clearTable(id) {
    console.log('service will clear TAble num: ', id.tableno);
    return new Promise((resolve, reject) => {
      this.http.post(this.clearTableURL + id.tableno, id)
        .subscribe(res => {
          console.log(res);
          this.placeOrderSuccessAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            this.errorAlert('Error! Try again.');
          });
    });
  }
  setTableNumber(num) {
    this.tableNum = num;
  }
  getTableNumber() {
    return this.tableNum;
  }
  getEmpName( id ){
    return new Promise (resolve => {
      this.http.get(this.getEmpNameURL + id).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getAvailableTable(){
    return new Promise (resolve => {
      this.http.get(this.getAvailableTableURL).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getMenuDetail( id ){
    return new Promise (resolve => {
      this.http.get(this.getMenuDetailURL + id).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  saveCustomerInfo(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.setCustomerInfoURL, data)
        .subscribe(res => {
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
            this.errorAlert('Error! Try again.');
          });
    });
  }
 
 

  
}
