import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { BehaviorSubject } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { IPAddress } from 'src/app/ipaddress';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  IPaddress = new IPAddress;

  ip = this.IPaddress.getIP();
  savetocartURL = this.ip + 'addtocart';
  saveCartBundleURL = this.ip +'saveCartBundle';
  removefromcartURL = this.ip + 'removeitemsfromcart/';
  getItemsURL = this.ip + 'getItems/';
  getCartItemsURL = this.ip + 'getCartItems/';
  getBundleItemsURL  = this.ip + 'getBundleItems/';
  getAllCartItemsURL = this.ip + 'getAllCartItems/';
  removeitemfromcartURL = this.ip + 'removeitemfromcart/';
  removebundleitemfromcartURL = this.ip + 'removebundleitemfromcart/';

  updateOrderQtyURL = this.ip + 'menu/addOrderQty';
  private cart = [];



  private cartItemCount = new BehaviorSubject(0);
  tableno: any;
  constructor(
    private nativeStorage: NativeStorage,
    public http: HttpClient, ) { }
    
  add(data) {
    console.log(data);
    this.addToCart(data).then(orders => {
      console.log(orders);
    });
  }


  addToCart(orderdata) {
    console.log(orderdata);
    return new Promise((resolve, reject) => {
      this.http.post(this.savetocartURL, orderdata)
        .subscribe(res => {
          resolve(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });

  }
  addBundleToCart(orderdata) {
    console.log(orderdata);
    return new Promise((resolve, reject) => {
      this.http.post(this.saveCartBundleURL, orderdata)
        .subscribe(res => {
          resolve(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });

  }
  setTableNo(tableno) {
    this.tableno = tableno;
  }
  getTableNo() {
    return this.tableno;
  }
  removeItem(order_id) {
    return new Promise((resolve, reject) => {
      this.http.post(this.removefromcartURL+ order_id, order_id)
        .subscribe(res => {
          resolve(res);
          console.log(order_id);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });

  }

  getItems(tableno) {
    console.log(tableno);
    return new Promise(resolve => {
      this.http.get(this.getItemsURL + tableno).subscribe(data => {
        resolve(data);
        console.log(data);
      },
        err => {
          console.log('ERROR!: ', err);
          //  this.errorAlert('Permission not');
        });
    });

  }
  getAllCartItems(orderid){
    return new Promise(resolve => {
      this.http.get(this.getAllCartItemsURL + orderid).subscribe(data =>{
        resolve(data);
        console.log(data);
      }, err => {
        console.log('ERROR: ', err);
      });
    });
  }
  getBundleItems(orderid){
    return new Promise(resolve => {
      this.http.get(this.getBundleItemsURL + orderid).subscribe(data => {
        resolve(data);
        console.log(data);
      },
        err => {
          console.log('ERROR: ', err);
        });
    });
  }
  getCartItems(orderid) {
    
    return new Promise(resolve => {
      this.http.get(this.getCartItemsURL + orderid).subscribe(data => {
        resolve(data);
        console.log(data);
      },
        err => {
          console.log('ERROR: ', err);
        });
    });
  }
  removeItemFromCart(id) {
    console.log("this.removebundleitemfromcart");
    return new Promise((resolve, reject) => {
      this.http.post(this.removeitemfromcartURL+id ,id)
        .subscribe(res => {
          resolve(res);
          console.log(id);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  removebundleitemfromcart(bundleid){
    console.log("this.removebundleitemfromcart");
    return new Promise((resolve, reject) => {
      this.http.post(this.removebundleitemfromcartURL + bundleid ,bundleid)
        .subscribe(res => {
          resolve(res);
          console.log(bundleid);
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
          });
    });
  }
  updateOrderQty(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.updateOrderQtyURL+ data, data)
        .subscribe(res => {
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
           // this.err('Error! Try again.');
          });
    });
  }
  


}
