// orderlist is the data after PLACE ORDER
//first 7 data from DB
export class OrderList {
    tableno: number;
    detail_id: number;
    order_id: number;
    orderQty: number;
    menuID: number;
    menuName: string;
    subtotal: number;

    price: number;
    unservedOrderQty: number;
    servedQty = 0;
    name: any;
    isChecked: boolean;
    status = 'waiting';
}
