
export class OrderDetail {
    id: number;
    order_id: number; // foreign key from ORDER table
    orderQty: number;
    qtyServed: number;
    menuID: number;
    bundleid: number;
    status: string;
    subtotal: number;
    updated_at: number; // Date.now() = current time
    date_ordered: number; // Date.now() = current time

    // name: string;
    // price: number;
    // isChecked: boolean;
    // unservedOrderQty: number;
  
}
