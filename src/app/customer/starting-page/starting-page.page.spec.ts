import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartingPagePage } from './starting-page.page';

describe('StartingPagePage', () => {
  let component: StartingPagePage;
  let fixture: ComponentFixture<StartingPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartingPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartingPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
