import { Component, OnInit } from '@angular/core';
import { Order } from '../classes/order';
import { AdminService } from '../services/admin.service';
import { MenuPage } from '../menu/menu.page';
import { AlertController } from '@ionic/angular';
import { QrloginService } from 'src/app/qrlogin/qrlogin.service';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-starting-page',
  templateUrl: './starting-page.page.html',
  styleUrls: ['./starting-page.page.scss'],
})
export class StartingPagePage implements OnInit {
  selected: any ;
  dataRcv: any;
  dataRcvs: any;
  fname: string;
  custNum: number;
  order: Order;
  username: any; // from ion-input
  name: any;
  table = {};
  availabletables = {};
  selectedTable: any;
  tableno: number;
  constructor(
    private adminService: AdminService,
    private qrloginService: QrloginService,
    public alertController: AlertController,
    public activeRoute: ActivatedRoute,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
      this.dataRcv = this.activeRoute.snapshot.paramMap.get('id');
      this.adminService.getEmpName(this.dataRcv).then(data => {
        this.dataRcvs = (data as any).data;
        for (const info of this.dataRcvs) {
            this.fname = (info as any).name;
        }
    });
  }
//   logout(){
//       this.authService.logout();
//   }
  startOrdering() {
    this.order = new Order();
    this.order.empid = this.dataRcv;
    this.order.custid = 5; // randomnumber
    this.order.tableno = 3;
    this.order.date_ordered = Date.now();

    console.log('LOGIN DETAILS: ', this.order);
    this.adminService.postToOrder(this.order);
  }
  async errorAlert() {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: 'Invalid username.',
      buttons: ['OK']
    });
    await alert.present();
  }

  clearTable() {
    this.table = {tableno: 1};
    this.adminService.sendToClearTable(this.table);
  }

  getAvailableTable() {
    this.adminService.getAvailableTable().then(data => {
        this.availabletables = (data as any).data;
        console.log(this.availabletables);
    });
  }
  selectedTableFn() {
    console.log(this.selectedTable);
    let selected = this.selectedTable; // Just did this in order to avoid changing the next lines of code :P
    this.table = selected.tableno;
  }
}
