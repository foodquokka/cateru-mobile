import { NgModule } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'kitchen', loadChildren: './kitchen/ktabs/ktabs.module#TabsPageModule' },
  { path: 'receptionist', loadChildren: './receptionist/rtabs/rtabs.module#TabsPageModule' },
  { path: 'waiter', loadChildren: './waiter/tabs/tabs.module#TabsPageModule' },

  { path: '', redirectTo: 'dashboard', pathMatch: 'full' }, // Changes in redirectTo when the app is ready
  {
    path: 'starting-page/:id',
    loadChildren: './customer/starting-page/starting-page.module#StartingPagePageModule'
  },
  {
    path: 'customer',
    canActivate: [AuthGuard],
    loadChildren: './customer/customerr-routing.module#CustomerrRoutingModule'
  },
  {
    path: 'menu',
    children: [
      {
        path: '',
        loadChildren: './customer/menu/menu.module#MenuPageModule'
      },
      {
        path: ':order_id',
        loadChildren: './customer/menu/menu.module#MenuPageModule'
      }
    ]
  },
  {
    path: 'orderlist',
    children: [
      {
        path: '',
        loadChildren: './customer/menu/orderlist/orderlist.module#OrderlistPageModule'
      },
      {
        path: ':orderid',
        loadChildren: './customer/menu/orderlist/orderlist.module#OrderlistPageModule'
      }
    ]
  },
  {
    path: 'billout/:order_id',
    loadChildren: './customer/menu/billout/billout.module#BilloutPageModule'
  },
  {
    path: 'ending-page/:order_id',
    loadChildren: './customer/ending-page/ending-page.module#EndingPagePageModule'
  },
  { path: 'table-clear', loadChildren: './customer/table-clear/table-clear.module#TableClearPageModule' },
  { path: 'qrlogin', loadChildren: './qrlogin/qrlogin.module#QrloginPageModule' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  { path: 'cartitem', loadChildren: './cartitem/cartitem.module#CartitemPageModule' },
  { path: 'table', loadChildren: './table/table.module#TablePageModule' },
  {
    path: 'table',
    children: [
      {
        path: '',
        loadChildren: './table/table.module#TablePageModule'
      },
      {
        path: ':empid',
        loadChildren: './table/table.module#TablePageModule'
      }
    ]
  },
  {
    path: 'confirmedorderlist',
    children: [
      {
        path: '',
        loadChildren: './confirmedorderlist/confirmedorderlist.module#ConfirmedorderlistPageModule'
      },
      {
        path: ':order_id',
        loadChildren: './confirmedorderlist/confirmedorderlist.module#ConfirmedorderlistPageModule'
      }
    ]
  },
  {
    path: 'printing',
    children: [
      {
        path: '',
        loadChildren: './cashier/printing/printing.module#PrintingPageModule'
      },
      {
        path: ':order_id',
        loadChildren: './cashier/printing/printing.module#PrintingPageModule'
      }
    ]
  },
  { path: 'drinks', loadChildren: './kitchen/drinks/drinks.module#DrinksPageModule' },
  { path: 'serving', loadChildren: './waiter/serving/serving.module#ServingPageModule' },
  { path: 'waiterorderdetail', loadChildren: './waiter/waiterorderdetail/waiterorderdetail.module#WaiterorderdetailPageModule' },
  {
    path: 'confirmedlistmodal',
    children: [
      {
        path: '',
        loadChildren: './confirmedlistmodal/confirmedlistmodal.module#ConfirmedlistmodalPageModule'
      },
      {
        path: ':order_id',
        loadChildren: './confirmedlistmodal/confirmedlistmodal.module#ConfirmedlistmodalPageModule'
      },

    ]
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  {
    path: 'cashier',
    children: [
      {
        path: '',
        loadChildren: './cashier/home/home.module#HomePageModule'
      },
      {
        path: ':username',
        loadChildren: './cashier/home/home.module#HomePageModule'
      }
    ]
  },
  { path: 'bestpairmodal', loadChildren: './bestpairmodal/bestpairmodal.module#BestpairmodalPageModule' },
  { path: 'add-customer-modal', loadChildren: './receptionist/add-customer-modal/add-customer-modal.module#AddCustomerModalPageModule' },
  { path: 'rtab5', loadChildren: './receptionist/rtab5/rtab5.module#Rtab5PageModule' },
  { path: 'managerauth', loadChildren: './receptionist/managerauth/managerauth.module#ManagerauthPageModule' },
  { path: 'manager', loadChildren: './manager/manager.module#ManagerPageModule' },
  { path: 'rating', loadChildren: './rating/rating.module#RatingPageModule' },
  { path: 'watcher', loadChildren: './watcher/watcher.module#WatcherPageModule' },  { path: 'serve-menu', loadChildren: './serve-menu/serve-menu.module#ServeMenuPageModule' },


]

@NgModule({
  imports: [
    RouterModule.forRoot(routes), //{ preloadingStrategy: PreloadAllModules
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
