import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AdminService } from './customer/services/admin.service';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HttpModule } from '@angular/http';
import { Uid } from '@ionic-native/uid/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { AuthenticationService } from './services/authentication.service';
import { File } from '@ionic-native/file/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { FormsModule } from '@angular/forms';
import { Base64 } from '@ionic-native/base64/ngx';
import { CartmodalPage } from './customer/menu/cartmodal/cartmodal.page';
import { MenuPage } from './customer/menu/menu.page';

import { WaiterorderdetailPage } from './waiter/waiterorderdetail/waiterorderdetail.page';
import { Tab1Page } from './kitchen/ktab1/ktab1.page';
import { LoginPage } from './login/login.page';
import { ModalPage } from './cashier/modal/modal.page';
import { DatePipe, DecimalPipe } from '@angular/common';
import { ConfirmedlistmodalPage } from './confirmedlistmodal/confirmedlistmodal.page';
import { AddCustomerModalPage } from './receptionist/add-customer-modal/add-customer-modal.page';
import { PromoModalPage } from './customer/menu/promo-modal/promo-modal.page';
import { ManagerauthPage } from './receptionist/managerauth/managerauth.page';

@NgModule({
  declarations: [AppComponent, CartmodalPage, WaiterorderdetailPage, LoginPage,ModalPage,ConfirmedlistmodalPage,AddCustomerModalPage,PromoModalPage,ManagerauthPage],
  entryComponents: [CartmodalPage, WaiterorderdetailPage,LoginPage,ModalPage,ConfirmedlistmodalPage,AddCustomerModalPage,PromoModalPage,ManagerauthPage],
  imports: [
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    // tslint:disable-next-line: deprecation
    HttpModule,
    IonicModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    
    DatePipe,
    DecimalPipe,
    AuthenticationService,
    StatusBar,
    SplashScreen, AdminService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    QRScanner,
    Dialogs,
    NativeStorage, 
    Uid,
    UniqueDeviceID,
    AndroidPermissions,
    Base64,
    File,
    MenuPage,
    Tab1Page,
    SMS
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
