import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Login } from './login.model';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { StorageserviceService } from '../services/storageservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  logininfo = {} as Login

  username;
  password;
  info: any;
  position: any;
  pos: any;
  data: any;
  hiddenLoginFormDiv: boolean;
  hiddenEmployeePosDiv: boolean;
  constructor(
    private loginservice: LoginService,
    private router: Router,
    private modalCtlr: ModalController,
    private auth: AuthenticationService,
    private storageService: StorageserviceService,
    private toastCtlr: ToastController
  ) { }

  ngOnInit() {
  }

  login() {
    console.log('hello');
    this.logininfo.password = this.password;
    this.logininfo.username = this.username;
    console.log(this.logininfo);
    this.loginservice.login(this.logininfo).then(data => {
      this.data = data;
      console.log(this.data);
      if (this.data) {
        this.getPosition(this.logininfo.username);
        this.auth.login(this.logininfo.username);
        this.storageService.saveUsername(this.logininfo.username);
      }
    });

  }
  getPosition(username) {
    this.loginservice.getPosition(username).then(data => {
      this.position = (data as any).position;
      for (const i of this.position) {
        this.pos = i.position;
        console.log('for loop');
        break;
      }
      if (this.pos === 'watcher') {
        this.router.navigate(['/watcher']);
      } else if(this.pos === 'waiter'){
        this.router.navigate(['/waiter']);
      }
      else if (this.pos === 'cashier') {
        console.log(this.pos);
        this.router.navigate(['/cashier', username]);
      } else if (this.pos === 'receptionist') {
        this.router.navigate(['/receptionist']);
      } else if (this.pos === 'kitchenstaff') {
        this.router.navigate(['/kitchen']);
      } else if (this.pos === 'manager') {
        this.router.navigate(['/manager']);
      } 
    });
    this.dismiss();

  }
  dismiss() {
    this.modalCtlr.dismiss({
      'dismissed': true
    });
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Username or Password is incorrect',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
}
