import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPAddress } from 'src/app/ipaddress';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  
  constructor(
    private httpClient: HttpClient
    
    ) {}

 IPaddress = new IPAddress;
 URL = this.IPaddress.getIP();

  getBillOutDetails() {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'cashier/billOutList').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getReceiptDetails(id) {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'cashier/getreceiptdetail/' + id).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getOrderRecord(id) {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'cashier/getbillinfo/' + id).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  receiveUpdatedBill(allData) {
    console.log(allData);
    this.postUpdatedBill(allData).then(data => {
      console.log(data);
    });
  }

  postUpdatedBill(data) {
    console.log(data);
    return new Promise(resolve => {
      this.httpClient.post(this.URL + 'cashier/printReceipt', data).subscribe(d => {
        resolve(d);
        console.log(d);
      }, err => {
        console.log(err);
      });
    });
  }
  getTotal(orderid) {
    //  orderid=1;
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.URL + 'cashier/gettotal/' + orderid).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getCashTendered(orderid) {
    //  orderid=1;
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.URL + 'cashier/getcash/' + orderid).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getChange(orderid) {
    //  orderid=1;
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.URL + 'cashier/getchange/' + orderid).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  setTableAvailable(tableno) {
    return new Promise(resolve => {
      this.httpClient.post(this.URL + 'settableavailable/' + tableno, tableno).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getEmployeeName(empname) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.URL + 'employee/employeename/' + empname).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  cancelOrder(data) {
    return new Promise(resolve => {
      this.httpClient.post(this.URL + 'order/decreaseTotal/' + data, data).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }



}
