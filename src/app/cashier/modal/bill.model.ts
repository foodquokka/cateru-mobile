export interface Bill {
    order_id: number;
    cashTender: number;
    change: number;
    status: string;
    total:any;
 }
 