import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController, ToastController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { Router } from '@angular/router';
import { Bill } from './bill.model';
import { LoginService } from 'src/app/services/login.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  billInfo = {} as Bill;
  amount: number;

  ID;
  updatedData: any;
  allTotal;
  discount;

  finalTotal: any;
  isNotReady = true;
  total: any;
  orderno: any;
  orders: any;
  custID: any;
  tableno: any;
  order_id: any;
  tNo: any;
  password: any;
  username: any;
  data: unknown;
  position: any;
  pos: any;

  // tslint:disable-next-line: max-line-length
  constructor(
    private router: Router,
    public modalController: ModalController,
    public navParam: NavParams,
    public apiService: ApiService,
    private alertController: AlertController,
    private toastCtlr: ToastController,
    private loginservice: LoginService,
    private auth: AuthenticationService
  ) {

    this.ID = this.navParam.get('id');
    console.log(this.ID);

  }

  ngOnInit() {

  }


  cancelMenuFromOrders() {
    this.apiService.cancelOrder(this.ID).then(() => {
      this.presentToast();
      this.closeModal();
    });
  }
  confirmDelete() {
    let employee_info = {
      username: this.username,
      password: this.password,
    }
    this.loginservice.login(employee_info).then(data => {
      this.data = data;
      if (this.data) {
        this.getPosition(employee_info.username);
        this.auth.login(employee_info.username);
      }
    });

  }
  getPosition(username) {
    this.loginservice.getPosition(username).then(data => {
      this.position = (data as any).position;
      for (const i of this.position) {
        this.pos = i.position;
        console.log('for loop');
        break;
      }
      if (this.pos === 'manager') {
        this.cancelMenuFromOrders();

      }
    }).then(() => {
      this.presentToast();
      this.closeModal();
    });

  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Order Cancelled!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  closeModal() {
    this.modalController.dismiss();
  }


}
