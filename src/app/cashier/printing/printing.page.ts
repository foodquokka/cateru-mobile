import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../service/api.service';
import { DatePipe } from '@angular/common';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-printing',
  templateUrl: './printing.page.html',
  styleUrls: ['./printing.page.scss'],
})
export class PrintingPage implements OnInit {

  orderid: any;
  orders: any;
  date: any;
  myDate:number = Date.now();
  
  empName:any;
  total: any;
  tableno:any;
  cash: any;
  change: any;
  username: any;
  displayPaidOrders: { nonBundle: any[]; bundle: any[]; };
  dataIsLoaded: boolean;
  constructor(
    private router: ActivatedRoute,
    private apiservice: ApiService,
    private datePipe: DatePipe,
    private loadingController: LoadingController,
    private toastCtlr: ToastController
  ) { }

  ngOnInit() {

    this.router.queryParams.subscribe( params => {
      let data = JSON.parse(params['order_id']);
      console.log(data);
      this.orderid = data.order_id;
      this.username = data.name;
    })
    console.log('THIS ORDERID',this.orderid);
    console.log('name', this.username);
    this.getReceiptDetails();
  }
  ionViewWillEnter(){
    this.getReceiptDetails() ;
    this.getTotal();
    this.presentLoading();
  }

  getReceiptDetails(){
    console.log(this.empName,this.date);
      this.apiservice.getReceiptDetails(this.orderid).then(data => {
      this.orders = (data as any).records;
     for(const info of this.orders){
       this.empName = info.empfirstname;
       this.tableno = info.tableno;
       this.date = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
       this.cash = info.cashTender;
       this.change = info.change;
     }
    }).then(() => {
      this.displayPaidOrders = { nonBundle: [], bundle: [] };
      let bundleDetails = { name: '',price: 0,qty:0,subtotal:0, content: [] };
      let processedCount = 1;
      this.orders.forEach(item => {
        if (item.bundleid) {
          if (bundleDetails.name === '')
            bundleDetails = { name: item.bundlename ,price:item.bundleprice,subtotal: item.subtotal,qty: item.orderNum, content: [item] };
          else if (bundleDetails.name !== item.bundlename  && bundleDetails.name !== '') {
            this.displayPaidOrders.bundle.push(bundleDetails);
            bundleDetails = { name: item.bundlename ,price:item.bundleprice,subtotal: item.subtotal,qty: item.orderNum, content: [item] };
          }
          else {
            bundleDetails.content.push(item);
          }
        }
        else
          this.displayPaidOrders.nonBundle.push(item);
        processedCount += 1;
        if (processedCount == this.orders.length) {
          this.displayPaidOrders.bundle.push(bundleDetails);
        }
      });
      console.log(this.displayPaidOrders);
      
      this.dataIsLoaded = true;
  });
  }
  getTotal(){
    this.apiservice.getTotal(this.orderid).then(data => {
      this.total = (data);
    }).then(()=>{
      this.apiservice.getCashTendered(this.orderid).then(data => {
        this.cash = (data);
      }).then(()=>{
        this.apiservice.getChange(this.orderid).then(data => {
          this.change = (data);
        });
    });
  });
}
  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'printing your receipt',
      duration: 2000,
    });
    await loading.present();

   const { role, data } = await loading.onDidDismiss();
    this.presentToast();
   // this.setTableAvailable();
    //this.route.navigate(['/cashier']);
    console.log('Loading dismissed!');
   
  }
  async presentToast(){
    const toast = await this.toastCtlr.create({
      message: 'Receipt printed!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  // setTableAvailable(){
  //   console.log(this.tableno);
  //   this.apiservice.setTableAvailable(this.tableno);
  // }

}
