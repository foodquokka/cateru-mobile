import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, ModalController, ToastController } from '@ionic/angular';
import { ApiService } from '../service/api.service';
import { ModalPage } from '../modal/modal.page';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Bill } from '../modal/bill.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  billInfo = {} as Bill;
  tables: any;
  id: any;
  now = new Date();
  empid: any;
  orders: any;
  no: any;
  custID: any;
  tableno: any;
  tNo: any;
  ID: any;
  amount: any;
  total: any;
  finalTotal: number;
  isNotReady: boolean;
  orderdetails: any;
  order_id: any;
  name: any;
  fname: any;
  chix = [];
  displayBilloutOrders: { nonBundle: any[]; bundle: any[]; };
  billoutOrderList: any;
  bundleprices: any;
  dataIsLoaded = false;
  isclicked = false;
  remove = true;
  tableNum: any;

  constructor(
    public navCtrl: NavController,
    public apiService: ApiService,
    public router: Router,
    public route: ActivatedRoute,
    public modalController: ModalController,
    public toastCtlr: ToastController,
  ) {


  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.empid = String(params.get('username'));
      console.log(this.empid);
    });

    this.getBillOut();
    this.getEmployeeName(this.empid);
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getBillOut();
      this.getOrders();
      event.target.complete();
    }, 1500);
  }
  // ionViewWIllEnter() {
  //   this.getBillOut();
  // }

  setTableNo(tableno){
    this.tableNum = tableno;
    this.getOrders();
  }
  getBillOut() {
    this.apiService.getBillOutDetails().then(data => {
      this.tables = (data as any).table;
      console.log('GETBILLOUT');
      console.log(this.tables);
    });
  }

  orderId(bills: any) {
    console.log(bills);
    this.router.navigateByUrl('details/' + bills);
  }
  getOrders() {
    this.apiService.getOrderRecord(this.tableNum).then(data => {
      this.orders = (data as any).records;
      for (const info of this.orders) {
        this.custID = info.custid;
        this.tableno = info.tableno;
        this.order_id = info.order_id;
        this.tNo = info.tableno;
      }
      this.getTotal(this.order_id);
    }).then(() => {
      this.displayBilloutOrders = { nonBundle: [], bundle: [] };
      let bundleDetails = { name: '', price: 0, qty: 0, subtotal: 0, content: [] };
      let processedCount = 1;
      this.orders.forEach(item => {
        if (item.bundleid) {
          if (bundleDetails.name === '')
            bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
          else if (bundleDetails.name !== item.bundlename && bundleDetails.name !== '') {
            this.displayBilloutOrders.bundle.push(bundleDetails);
            bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
          }
          else {
            bundleDetails.content.push(item);
          }
        }
        else
          this.displayBilloutOrders.nonBundle.push(item);
        processedCount += 1;
        if (processedCount == this.orders.length) {
          this.displayBilloutOrders.bundle.push(bundleDetails);
        }
      });
      console.log(this.displayBilloutOrders);

      this.dataIsLoaded = true;
    });
  }


  async  PrintReceipt() {
    if (this.amount >= this.total) {
      this.finalTotal = this.amount - this.total;
      this.sendUpdatedBill();
      // this.router.navigate(['/printing'],
      //   { queryParams: { order_id: JSON.stringify({ order_id: this.order_id, name: this.fname }) } });

    } else if (this.amount === null) {
      const toast = await this.toastCtlr.create({
        message: 'Amount is required!',
        duration: 2000
      });
      toast.present();
    }
    this.doRefresh(event);
  }

  click() {
    this.isNotReady = false;
  }

  sendUpdatedBill() {
    this.billInfo.order_id = this.order_id;
    this.billInfo.cashTender = this.amount;
    this.billInfo.change = this.finalTotal;
    this.billInfo.status = 'paid';
    this.billInfo.total = this.total;
    console.log(this.billInfo);
    this.apiService.postUpdatedBill(this.billInfo).then(()=>{
      this.router.navigate(['/printing'],
      { queryParams: { order_id: JSON.stringify({ order_id: this.order_id, name: this.fname }) } });
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getTotal(order_id) {
    this.apiService.getTotal(order_id).then(data => {
      this.total = (data);

      console.log(this.total);
    });
  }
  getEmployeeName(username) {
    this.apiService.getEmployeeName(username).then(data => {
      this.name = (data as any).data;
      for (const name of this.name) {
        this.fname = name.empfirstname;

      }
    });
  }
  isClicked(id) {
    this.remove = false;
  }
  cancellItem(id) {
    this.apiService.cancelOrder(id).then(() => {
      this.presentToast();
    });
  }
  async presentToast() {
    const toast = await this.toastCtlr.create({
      message: 'Order Cancelled!',
      duration: 3000,
      position: 'top'
    }); 
    this.doRefresh(event);
    toast.present();
  }

}


