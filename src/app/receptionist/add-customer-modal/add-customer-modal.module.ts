import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddCustomerModalPage } from './add-customer-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AddCustomerModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddCustomerModalPage]
})
export class AddCustomerModalPageModule {}
