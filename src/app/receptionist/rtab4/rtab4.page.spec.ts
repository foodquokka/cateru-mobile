import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rtab4Page } from './rtab4.page';

describe('Rtab4Page', () => {
  let component: Rtab4Page;
  let fixture: ComponentFixture<Rtab4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Rtab4Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rtab4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
