import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { ApiService } from 'src/app/cashier/service/api.service';

@Component({
  selector: 'app-rtab4',
  templateUrl: './rtab4.page.html',
  styleUrls: ['./rtab4.page.scss'],
})
export class Rtab4Page implements OnInit {
  notifiedcustomers: any;
  tab: any;
  array = [];

  constructor(
    private restservice: RestService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.getNotifiedCustomer();
  }

  ionViewWillEnter() {
    this.getNotifiedCustomer();
  }
  getNotifiedCustomer() {

    this.restservice.getNotifiedCustomer().then(data => {
      this.notifiedcustomers = (data as any).notified;
      console.log(this.notifiedcustomers);
    });
  }

  setStatusPresent(custid) {
    console.log(custid);
    this.restservice.presentCustomer(custid);
    this.ionViewWillEnter();
  }
  async removeQueue(custid,tableno) {
    console.log(custid);
    this.restservice.statusCancelled(custid).then(() =>{
      this.apiService.setTableAvailable(tableno).then(()=>{
          this.doRefresh();
      });
    });
  
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getNotifiedCustomer();
      //event.target.complete();
    }, 1500);
  }

}
