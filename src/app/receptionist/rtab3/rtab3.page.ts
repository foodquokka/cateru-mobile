import { Component } from '@angular/core';
import { RestService } from '../services/rest.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'rtab3.page.html',
  styleUrls: ['rtab3.page.scss']
})
export class Tab3Page {

  tables: any;
  // constructor(){}
  constructor(public servicesProvider: RestService) {
    this.getTables();
  }

  getTables() {
    this.servicesProvider.getTables()
      .then(data => {
        this.tables = data;
        this.tables = this.tables.allTables;
        console.log(this.tables);
      });
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getTables();
      event.target.complete();
    }, 1500);
  }
}
