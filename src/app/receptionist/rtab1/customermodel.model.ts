export interface CustomerModel {
    name: string;
    phonenumber: string;
    status:string;
    partysize: number;
 }