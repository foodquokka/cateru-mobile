import { Component } from '@angular/core';
import { RestService } from '../services/rest.service';
import { CustomerModel } from './customermodel.model';
import { ModalController } from '@ionic/angular';
import { AddCustomerModalPage } from '../add-customer-modal/add-customer-modal.page';


@Component({
  selector: 'app-tab1',
  templateUrl: 'rtab1.page.html',
  styleUrls: ['rtab1.page.scss']
})
export class Tab1Page {
  customer = {} as CustomerModel;
  customers = [];

  name: string;
  partysize: number;
  phonenumber: any;
  status: string;

  constructor(
    public serviceProvider: RestService,
    public modalController: ModalController
  ) { }

  cancelBtn() {
    console.log('CancelBTN!!!');
    this.partysize = null;
    this.name = "";
    this.phonenumber = "";

  }
  // validate() {
  //   Validators.pattern('^(09|\+639)\d{9}$');
  // }
  addBtn() {
    this.customer.name = this.name;
    this.customer.partysize = this.partysize;
    this.customer.phonenumber = this.phonenumber;
    this.customer.status = 'reserved';
    this.serviceProvider.addCustomer(this.customer);

    this.presentModal();
    this.cancelBtn();
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: AddCustomerModalPage,
      cssClass: 'AddCustomerModalPageCSS'
    });
    return await modal.present();
  }
  
}
