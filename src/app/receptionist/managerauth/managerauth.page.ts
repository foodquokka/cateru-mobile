import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalController, ToastController, NavParams } from '@ionic/angular';
import { RestService } from 'src/app/receptionist/services/rest.service';

@Component({
  selector: 'app-managerauth',
  templateUrl: './managerauth.page.html',
  styleUrls: ['./managerauth.page.scss'],
})
export class ManagerauthPage implements OnInit {
  data: any;
  username: any;
  password: any;
  position: any;
  role: any;
  from :any;
  to:any;
  constructor(
    private loginservice: LoginService, 
    private auth: AuthenticationService,
    public modalController: ModalController,
    public toastCtlr: ToastController,
    public restService: RestService
    ) { }

  ngOnInit() {
   console.log(this.from, this.to);
  }
  confirmTableTransfer(){
    let tables = {
      transferfrom: this.from,
      transferto: this.to
    }
    this.restService.tabletransfer(tables);
   // this.presentToast();
    this.closeModal();
  }
  getManagerAuth() {
    let employee_info = {
      username: this.username,
      password: this.password,
    }
    this.loginservice.login(employee_info).then(data => {
      this.data = data;
      if (this.data) {
        this.getPosition(employee_info.username);
        this.auth.login(employee_info.username);
      }
    });

  }
  getPosition(username) {
    this.loginservice.getPosition(username).then(data => {
      this.position = (data as any).position;
      for (const i of this.position) {
        this.role = i.position;
        console.log('for loop');
        break;
      }
      if (this.role === 'manager') {
        this.confirmTableTransfer();

      }
    }).then(()=>{
      this.presentToast();
      this.closeModal();
    });
    
  }
  async presentToast() {
    const toast =  await this.toastCtlr.create({
      message: 'Transfer success!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  closeModal() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

}
