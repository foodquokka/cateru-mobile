import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerauthPage } from './managerauth.page';

describe('ManagerauthPage', () => {
  let component: ManagerauthPage;
  let fixture: ComponentFixture<ManagerauthPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerauthPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerauthPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
