import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './rtabs.page';

const routes: Routes = [
  {
    path: 'rtabs',
    component: TabsPage,
    children: [
      {
        path: 'rtab1',
        children: [
          {
            path: '',
            loadChildren: '../rtab1/rtab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'rtab2',
        children: [
          {
            path: '',
            loadChildren: '../rtab2/rtab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'rtab3',
        children: [
          {
            path: '',
            loadChildren: '../rtab3/rtab3.module#Tab3PageModule'
          }
        ]
      },
      {
        path: 'rtab4',
        children: [
          {
            path: '',
            loadChildren: '../rtab4/rtab4.module#Rtab4PageModule'
          }
        ]
      },
      {
        path: 'rtab5',
        children: [
          {
            path: '',
            loadChildren: '../rtab5/rtab5.module#Rtab5PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/receptionist/rtabs/rtab1',
        pathMatch: 'full'
      }
    ]
  },
 
  {
    path: '',
    redirectTo: '/receptionist/rtabs/rtab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
