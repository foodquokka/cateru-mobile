import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPAddress } from 'src/app/ipaddress';


@Injectable({
  providedIn: 'root'
})
export class RestService {

  queue = [];
   IPaddress = new IPAddress;

   url = this.IPaddress.getIP();

   tablelisturl = this.url + 'table/tablelist';
   addcustomerurl = this.url + 'addcustomer';
   reservedcustomerurl= this.url + 'getreservedcustomer';
   statusPresentUrl =this.url + 'status/present/';
   statusNotifiedUrl = this.url + 'status/notified/';
   statusCancelledURL = this.url + 'status/cancelled/'
   getNotifiedCustomersURL = this.url + 'getnotifiedcustomer';
   sendSMSURL = this.url + 'sendSMS/';
   assignTableURL = this.url +'table/assignTable';
   getphonenumURL = this.url + 'getphonenumber/';
   getOccupiedTableURL=this.url +'table/occupied';
   getAvailableTableURL = this.url + 'table/getAvailableTable';
   tabletransferURL = this.url +'tabletransfer';
   mergeTablesURL = this.url +'mergeTables';

  
  constructor(public http: HttpClient) { }

  getTables() {
    return new Promise(resolve => {
      this.http.get(this.tablelisturl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  enqueue(pk, psize, cname, pno) {
    this.queue.push({
      key: pk,
      size: psize,
      name: cname,
      no: pno
    });

    console.log('addQueue()', this.queue);

    // this.removeItem(1);//test
  }

  // removeItem(key) {
  //   for (let q of this.queue) {
  //     console.log(q.key);
  //   }
  // }

  getQueue() {
    return this.queue;
  }
  addCustomer(data){
    console.log(data);
    return new Promise((resolve, reject) => {
      this.http.post(this.addcustomerurl,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  reservedcustomerslist() {
    return new Promise(resolve => {
      this.http.get(this.reservedcustomerurl).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  presentCustomer(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.statusPresentUrl + data,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  statusNotified(custid){
    return new Promise((resolve, reject) => {
      this.http.post(this.statusNotifiedUrl +custid, custid)
      .subscribe(res => {
        resolve(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  statusCancelled(custid){
    return new Promise((resolve, reject) => {
      this.http.post(this.statusCancelledURL+ custid, custid)
      .subscribe(res => {
        resolve(res);
      },
      err => {
        console.log('ERROR!: ',err);
      });
    })
  }
  getNotifiedCustomer(){
    return new Promise((resolve, reject) => {
      this.http.get(this.getNotifiedCustomersURL).subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ',err);
      });
    });
  }
  sendSMS(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.sendSMSURL + data,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  assignTable(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.assignTableURL ,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  getPhonenumber(data){
    return new Promise((resolve, reject) => {
      this.http.get(this.getphonenumURL+data, data)
      .subscribe(res => {
        resolve(res);
       // console.log(res);
      });
    });
  }
  getAvailableTable(){
    return new Promise((resolve, reject) => {
      this.http.get(this.getAvailableTableURL)
      .subscribe(res => {
        resolve(res);
       console.log(res);
      });
    });
  }
  getOccupiedTable(){
    return new Promise((resolve, reject) => {
      this.http.get(this.getOccupiedTableURL)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      });
    });
  }
  tabletransfer(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.tabletransferURL ,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  mergeTables(data){
    return new Promise((resolve, reject) => {
      this.http.post(this.mergeTablesURL ,data)
      .subscribe(res => {
        resolve(res);
        console.log(res);
      },
      err => {
        console.log('ERROR!: ', err);
      });
    });
  }
  
 
}
