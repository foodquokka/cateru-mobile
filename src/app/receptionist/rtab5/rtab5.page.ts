import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/receptionist/services/rest.service';
import { Tables } from './tables.model';
import { ModalController } from '@ionic/angular';
import { ManagerauthPage } from '../managerauth/managerauth.page';

@Component({
  selector: 'app-rtab5',
  templateUrl: './rtab5.page.html',
  styleUrls: ['./rtab5.page.scss'],
})
export class Rtab5Page implements OnInit {
  availabletables: any;
  occupiedtables: any;
  tables = {} as Tables;
  transferfrom: any;
  transferto: any;
  
  isShowBtn = false;
  firstTable: any;
  secondTable: any;
  showMerge = true;
  showTransfer =  true;

  constructor( private restService: RestService, public modalController: ModalController) { }

  ngOnInit() {
    
    this.getAllAvailableTable();
    this.getAllOccupiedTable();
  }
  ionViewWillEnter(){
    this.getAllAvailableTable();
    this.getAllOccupiedTable();
    this.transferfrom = "";
    this.transferto = "";
    this.firstTable = "";
    this.secondTable = "";
  }
  getAllAvailableTable(){
    this.restService.getAvailableTable().then(data => {
      this.availabletables = (data as any).AvailableTables;
    });
  }
  getAllOccupiedTable(){
    this.restService.getOccupiedTable().then(data => {
      this.occupiedtables = (data as any).OccupiedTables;
    });
  }

  async transferTable(){
    const from = this.transferfrom;
    const to = this.transferto;
    const modal = await this.modalController.create({
      component: ManagerauthPage,
      componentProps: { from, to}
    });
    modal.onDidDismiss().then(() => {
      this.ionViewWillEnter();
      //  this.cartTable();
    });
    //await modal.present();
     await modal.present();
    // this.ionViewWillEnter();
  } 
  setTransferFrom(selectedValue){
    this.transferfrom = selectedValue;
    console.log(this.transferfrom); 
  }
  setTransferTo(selectedValue){
    this.transferto = selectedValue;
    console.log(this.transferto);
  }
  setFirstTable(firstTable){
    this.firstTable = firstTable;
    console.log(this.firstTable);
  }
  setSecondTable(secondTable){
    this.secondTable = secondTable;
    console.log(this.secondTable);
  }
  mergeTable(){
    let mergeTable = {
      firsttable: parseInt(this.firstTable),
      secondtable: parseInt(this.secondTable)
    }
  this.restService.mergeTables(mergeTable);
  this.ionViewWillEnter();
  }
  showMergeTable(){
    this.showMerge = false;
    this.showTransfer = true;
  }
  showTransferTable(){
    this.showMerge = true;
    this.showTransfer = false;
  }
  
}
