import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rtab5Page } from './rtab5.page';

describe('Rtab5Page', () => {
  let component: Rtab5Page;
  let fixture: ComponentFixture<Rtab5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Rtab5Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rtab5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
