import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { SMS } from '@ionic-native/sms/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { ToastController } from '@ionic/angular';
import { SMSModel } from './smsmodel.model';
import { QrloginService } from 'src/app/qrlogin/qrlogin.service';
import { ApiService } from 'src/app/cashier/service/api.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'rtab2.page.html',
  styleUrls: ['rtab2.page.scss']
})
export class Tab2Page implements OnInit {

  //phoneNumber = '639479845480';
  SMS = {} as SMSModel;
  name: string;
  phonenumber: string;
  tab: any;
  array = [];
  list: any;
  toast: any;
  custid: number;
  customerid: any;
  info: any;
  pno: any;
  AvailableTables: any;
  selectedAvailableTable: any;
  queue: any;
  arrayList: any[];
  id: any;

  constructor(
    public serviceProvider: RestService,
    private toastCtrl: ToastController,
    private qrLoginService: QrloginService
  ) {
    this.tables();
    this.queueTable();
    this.getAvailableTable();

  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.queueTable();
  }

  tables() {
    this.serviceProvider.getTables().then(data => {
      this.tab = data;
      this.tab = this.tab.allTables;
      // console.log(this.tab);
    });
  }

  getAvailableTable() {
    this.serviceProvider.getAvailableTable().then(data => {
      this.AvailableTables = (data as any).AvailableTables;
      console.log(this.AvailableTables)
    });
  }
  onChange() {
    // console.log(index);
    // this.selectedAvailableTable = index;
    console.log(this.selectedAvailableTable);
  }
  queueTable() {
    this.serviceProvider.reservedcustomerslist().then(data => {
      this.list = (data as any).reservedcustomers;
      console.log(this.list);
    })
  }
  removeQueue() {
    let counter = 0;
    for (const customer of this.list) {
      for (const table of this.AvailableTables) {
        if (table.capacity >= customer.partysize) {
          // console.log('Message sent to: ' + i.no);
          this.array.splice(counter, 1);
          // console.log(this.array[counter]);
        }
      }
      counter++;
    }
  }
  getCustId(custid) {
    //  console.log(this.customerid); 
    console.log(custid);
    this.id = custid;
    return this.id;

  }
  sendSMS() {
    let dataArray = Array()
    let info: any;
    console.log(this.selectedAvailableTable);
    this.serviceProvider.sendSMS(this.id).then(() => {
      info = {
        custid: this.id,
        tableno: parseInt(this.selectedAvailableTable)
      }
      dataArray = info;
      console.log(dataArray);
      this.serviceProvider.assignTable(dataArray);
      this.presentToast();
    }).then(() => {
      this.setTableReserved();
    });
    this.doRefresh();
  }

  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.queueTable();
      this.getAvailableTable();
      //   event.target.complete();
    }, 1500);
  }
  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'Message Sent!',
      position: 'middle',
      animated: true,
      duration: 1500,
      cssClass: "my-custom-class"
    });

    toast.present();
  }
  async setStatusCancelled() {
    this.serviceProvider.statusCancelled(this.id).then(() => {
      setTimeout(() => {
        console.log('Async operation has ended');
        this.queueTable();
        this.id.target.complete();
      }, 1500);
    });
  }

  setTableReserved() {
    let dataArray = Array()
    let info: any;

    info = {
      tableno: this.selectedAvailableTable,
      status: 'Reserved'
    }
    dataArray = info;
    this.qrLoginService.setTableStatus(dataArray);
  }


}
