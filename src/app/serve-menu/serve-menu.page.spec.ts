import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeMenuPage } from './serve-menu.page';

describe('ServeMenuPage', () => {
  let component: ServeMenuPage;
  let fixture: ComponentFixture<ServeMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServeMenuPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServeMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
