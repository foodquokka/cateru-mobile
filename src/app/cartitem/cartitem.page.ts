import { Component, OnInit } from '@angular/core';
//import { MQTTService } from  'ionic-mqtt';

@Component({
  selector: 'app-cartitem',
  templateUrl: './cartitem.page.html',
  styleUrls: ['./cartitem.page.scss'],
})
export class CartitemPage implements OnInit {

  // private _mqttClient:any;
  // private MQTT_CONFIG: {
  //   host:string,
  //   port: number,
  //   clientId: string,
  // } = {
  //   host: "172.30.14.189",
  //   port: 8000,
  //   clientId: "mqtt",
  // };

  // private TOPIC: string[] = [];
  // constructor( private mqttService : MQTTService) { }

  ngOnInit() {
    // this._mqttClient = this.mqttService.loadingMqtt(
    //   this._onConnectionLost, 
    //   this._onMessageArrived, 
    //   this.TOPIC, 
    //   this.MQTT_CONFIG
    //   );
  }
  private _onConnectionLost(responseObject) {
    // connection listener
    // ...do actions when connection lost
    console.log('_onConnectionLost', responseObject);
  }

  private _onMessageArrived(message) {
    // message listener
    // ...do actions with arriving message
    console.log('message', message);
  }
  // public function for sending and publishing mqtt messages

  public sendMessage() {
    console.log('sendMessage')
    // this.mqttService.sendMessage("hello", "MESSAGE");
  }

  public publishMessage() {
    console.log('publishMessage')
    // this.mqttService.publishMessage("TOPIC", "MESSAGE");
  }

}
