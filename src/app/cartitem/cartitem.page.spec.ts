import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartitemPage } from './cartitem.page';

describe('CartitemPage', () => {
  let component: CartitemPage;
  let fixture: ComponentFixture<CartitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartitemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
