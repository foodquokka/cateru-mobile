import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { ConfirmedorderlistService } from '../confirmedorderlist/confirmedorderlist.service';
import { ModalController, ToastController } from '@ionic/angular';
//import { IonicRatingModule } from 'ionic4-rating';

@Component({
  selector: 'app-confirmedlistmodal',
  templateUrl: './confirmedlistmodal.page.html',
  styleUrls: ['./confirmedlistmodal.page.scss'],
})
export class ConfirmedlistmodalPage implements OnInit {

  @Input() rating: number;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter();

  label: any;

  constructor(
    private confirmedOrderService: ConfirmedorderlistService,
    public modalController: ModalController,
    public toastCltr: ToastController
  ) {
  }

  ngOnInit() { }
  ionViewWillEnter() { }


  rate(index: number) {
    this.rating = index;
    this.ratingChange.emit(this.rating);
  }
  isAboveRating(index: number): boolean {
    return index > this.rating;
  }
  getColor(index: number) {
    if (this.isAboveRating(index)) {
      return COLORS.GREY;
    }
    switch (this.rating) {
      case 1: this.label = 'POOR';
        return COLORS.YELLOW;
      case 2: this.label = 'BELOW AVERAGE';
        return COLORS.YELLOW;
      case 3: this.label = 'AVERAGE';
        return COLORS.YELLOW;
      case 4: this.label = 'ABOVE AVERAGE';
        return COLORS.YELLOW;
      case 5: this.label = 'EXCELLENT';
        return COLORS.YELLOW;
      default: return COLORS.GREY;
    }

  }
  setRating() {
    console.log('SET RATING');
    console.log(this.rating);
    let star = {
      'star': this.rating
    }
    this.confirmedOrderService.setRating(star).then(() => {
      this.dismiss();

    }).then(() => {
      this.presentToast();
    });
  }
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  async presentToast() {
    const toast = await this.toastCltr.create({
      message: 'Thank you!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

}
enum COLORS {
  GREY = "#E0E0E0",
  YELLOW = "#FFCA28"
}

