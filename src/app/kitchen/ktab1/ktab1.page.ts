import { Component } from '@angular/core';
import { RestService } from '../rest.service';
import { NavController, ToastController, AlertController } from '@ionic/angular';
import { Kitchen } from './kitchen';

@Component({
  selector: 'app-tab1',
  templateUrl: 'ktab1.page.html',
  styleUrls: ['ktab1.page.scss']
})
export class Tab1Page {

  kitchen = new Kitchen();
  //orderselected = false;
  isenabled = [false,false,false,false];
  // isNotReady = true;
  // color = '';
  // ready = '';
  orders: any;
  orderLists: any;
  orderArray = new Array();
  //orderDrinksArray = new Array();

  //orderCard: string[] = [];
 // drinks: any;
  qty: any;
  //globalArray: any;
  //name: any;
  status: any;
  //finalArray =[];
  //arr: any;
  //arr2:any;
  //counter= 0;
  prepareStatus: boolean;
  constructor(
    public navCtrl: NavController, 
    public restService: RestService,
    private toastCtlr: ToastController
    ) {
 
  }
 
  // ionViewWillEnter(){
  //   this.getOrders();
  // }
  getOrderQty(id){
    this.restService.getOrderQty(id).then(data => {
      this.qty = (data as any).items;
    });
  }
  getOrders() {
    this.restService.getOrderList().then(data => {
      this.orders = (data as any).orders;
    });
  }

 
  updateStatus(order) {
    console.log('orders', order);
    this.restService.postStatus(order).then(data => {
      this.orderLists = data;
      console.log(this.orders);
      this.splice(order);
    });
  }

  splice(order) {
    let counter = 0;
    for (let i of this.orderArray) {
      if (i.order_id === order) {
        console.log('if counter' + counter);
        console.log('i.order_id: ' + i.order_id);
        console.log('order: ' + order);
        console.log(this.orderArray.splice(counter, 1));
        break;
      } else {
        counter++;
        console.log(counter);
      }
    }
  }

 prepare(id,index) {
 this.restService.changeOrderStatusToPrepare(id);
 this.isPrepare(id);
 this.isenabled[index] = this.prepareStatus;

   this.presentToastPrepare();
  }
  isPrepare(id){
    this.restService.isPreparing(id).then(data => {
      this.prepareStatus = (data as any).status;
    //  return this.prepareStatus;
    });
    this.doRefresh();
  }
  doRefresh() {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getOrders();
    }, 1500);
  }
  async presentToastPrepare() {
    const toast = await this.toastCtlr.create({
      message: 'Order being prepared',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

 


}
