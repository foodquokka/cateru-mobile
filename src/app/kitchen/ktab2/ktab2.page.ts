import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'ktab2.page.html',
  styleUrls: ['ktab2.page.scss']
})
export class Tab2Page implements OnInit{
 
  orderlist: any;
  drinks: any;

  constructor(
    public restService: RestService) {
  }
  ngOnInit() {
     this.getComplete();
  }
  // ionViewWillEnter(){
  //   this.getComplete();
  // }
  getComplete() {
   this.restService.getAllCompleteList().then(data => {
     this.orderlist = (data as any).orders;
     console.log(this.orderlist);
   });
  }

}
