import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { IPAddress } from '../ipaddress';



@Injectable({
  providedIn: 'root'
})
export class RestService {
  IPaddress = new IPAddress;

  URL = this.IPaddress.getIP();

  finishURL = this.URL + 'finish/';

  removeFromKitchenOrdersURL = this.URL + 'removeFromKitchenOrders';
  getDrinksListURL = this.URL + 'order/drinklist';
  drinkReadyURL = this.URL + 'order/statusready/';
  readyToServeURL = this.URL + 'order/readytoserve';
  allServedItemsURL = this.URL + 'order/servedorders';
  changeStatusToPrepareURL = this.URL + 'order/preparestatus/';
  changeStatusToFinishURL = this.URL + 'order/finishstatus/';
  getOrderQtyCountURL = this.URL + 'getOrderQty/';
  getStatusPrepareURL = this.URL + 'getAllPreparedItems';
  getAllCompleteListURL = this.URL + 'getAllCompleteList';
  getPrepareDrinksURL = this.URL + 'orders/getPrepareDrinks';
  getAllCompleteDrinksURL = this.URL + 'orders/getAllCompleteDrinks';



  constructor(
    private httpClient: HttpClient,
    public alertController: AlertController) {
  }

  getOrderQty(id) {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'getOrderQty/' + id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getOrderList() {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'getKitchenOrders').subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }

  postStatus(order: any) {
    return new Promise(resolve => {
      this.httpClient.post(this.URL + 'order/statusready/' + order, order).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getCompleteList() {
    return new Promise(resolve => {
      this.httpClient.get(this.URL + 'kitchen/completedOrders').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }
  processGetDrinks() {
    console.log(this.getDrinksListURL);
    return new Promise(resolve => {
      this.httpClient.get(this.getDrinksListURL).subscribe(drinklist => {
        resolve(drinklist);
        console.log(drinklist);
      }, err => {
        console.log(err);
      });
    });
  }
  sendToDrinkReady(id) {
    console.log('service received: ', id);
    this.drinkReady(id).then(orderdetail => {
      console.log(orderdetail);
    });
  }

  drinkReady(id) {
    console.log('service will process: ', id);
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.drinkReadyURL + id, id)
        .subscribe(res => {
          console.log(res);
          // this.successAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            // this.errorAlert('Error! Try again.');
          });
    });
  }

  // GET READY TO SERVE LIST
  processReadyList() {
    return new Promise(resolve => {
      this.httpClient.get(this.readyToServeURL).subscribe(readylist => {
        resolve(readylist);
        console.log(readylist);
      }, err => {
        console.log(err);
      });
    });
  }

  // GET ALL SERVED
  processAllServed() {
    return new Promise(resolve => {
      this.httpClient.get(this.allServedItemsURL).subscribe(servedlist => {
        resolve(servedlist);
        console.log(servedlist);
      }, err => {
        console.log(err);
      });
    });
  }
  removeFromKitchenOrders(id) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.removeFromKitchenOrdersURL, id)
        .subscribe(res => {
          console.log(res);
          // this.successAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            // this.errorAlert('Error! Try again.');
          });
    });
  }
  changeOrderStatusToPrepare(id) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.changeStatusToPrepareURL + id, id)
        .subscribe(res => {
          console.log(res);
          // this.successAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            // this.errorAlert('Error! Try again.');
          });
    });
  }
  changeKitchenStatus(id) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.URL + 'prepare/' + id, id)
        .subscribe(res => {
          console.log(res);
          // this.successAlert((res as any).message);
        },
          err => {
            console.log('ERROR!: ', err);
            // this.errorAlert('Error! Try again.');
          });
    });
  }
  changeOrderStatusToFinish(id) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.finishURL + id, id)
        .subscribe(res => {
          console.log(res);
        },
          err => {
            console.log('ERROR!: ', err);
            // this.errorAlert('Error! Try again.');
          });
    });
  }
  isPreparing(id) {
    return new Promise(resolve => {
      this.httpClient.get(this.getStatusPrepareURL + id, id).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllPrepareItems() {
    return new Promise(resolve => {
      this.httpClient.get(this.getStatusPrepareURL).subscribe(res => {
        resolve(res);
        console.log(res);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllCompleteList() {
    return new Promise(resolve => {
      this.httpClient.get(this.getAllCompleteListURL).subscribe(res => {
        resolve(res);
        console.log(res);
      }, err => {
        console.log(err);
      });
    });
  }
  getPrepareDrinks() {
    return new Promise(resolve => {
      this.httpClient.get(this.getPrepareDrinksURL).subscribe(res => {
        resolve(res);
        console.log(res);
      }, err => {
        console.log(err);
      });
    });
  }
  getAllCompleteDrinks() {
    return new Promise(resolve => {
      this.httpClient.get(this.getAllCompleteDrinksURL).subscribe(res => {
        resolve(res);
        console.log(res);
      }, err => {
        console.log(err);
      });
    });
  }

  async errorAlert(message) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async successAlert(message) {
    const alert = await this.alertController.create({
      header: null,
      subHeader: null,
      message: message,
      buttons: [
        {
          text: 'OK'
        }]
    });
    await alert.present();
  }


}
