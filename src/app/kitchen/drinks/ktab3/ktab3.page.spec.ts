import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ktab3Page } from './ktab3.page';

describe('Ktab3Page', () => {
  let component: Ktab3Page;
  let fixture: ComponentFixture<Ktab3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ktab3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ktab3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
