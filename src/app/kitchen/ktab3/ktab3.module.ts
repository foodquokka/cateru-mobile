import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { Ktab3Page } from './ktab3.page';

// const routes: Routes = [
//   {
//     path: '',
//     component: Ktab3Page
//   }
// ];

@NgModule({
  imports: [
      IonicModule,
      CommonModule,
      FormsModule,
    RouterModule.forChild([{ path: '', component: Ktab3Page }])
  ],
  declarations: [Ktab3Page]
})
export class Ktab3PageModule {}
