import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-ktab3',
  templateUrl: './ktab3.page.html',
  styleUrls: ['./ktab3.page.scss'],
})
export class Ktab3Page implements OnInit {
  orders: any;
  isenabled: any;

  constructor(
    private restService: RestService,
    private toastCtlr: ToastController
  ) { }

  ngOnInit() {
    this.getAllPrepareItems();
  }
  // ionViewWillEnter(){
  //   this.getAllPrepareItems();
  // }

  getAllPrepareItems(){
    this.restService.getAllPrepareItems().then(data => {
      this.orders = (data as any).orders;
      console.log(this.orders);
    });
  }
 
  async presentToastFinish() {
    const toast = await this.toastCtlr.create({
      message: 'Order finished!',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  finish(id){
      this.restService.changeOrderStatusToFinish(id);
      this.presentToastFinish();
      this.doRefresh(id);
    }
  doRefresh(event) {
    setTimeout(() => {
      console.log('Async operation has ended');
      this.getAllPrepareItems();
      event.target.complete();
    }, 1500);
  }
 

}
