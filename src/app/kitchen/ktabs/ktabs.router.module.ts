import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './ktabs.page';

const routes: Routes = [
  {
    path: 'ktabs',
    component: TabsPage,
    children: [
      {
        path: 'ktab1',
        children: [
          {
            path: '',
            loadChildren: '../ktab1/ktab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'ktab2',
        children: [
          {
            path: '',
            loadChildren: '../ktab2/ktab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'ktab3',
        children: [
          {
            path: '',
            loadChildren: '../ktab3/ktab3.module#Ktab3PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/kitchen/ktabs/ktab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/kitchen/ktabs/ktab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
