import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ktabs3Page } from './ktabs3.page';

describe('Ktabs3Page', () => {
  let component: Ktabs3Page;
  let fixture: ComponentFixture<Ktabs3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ktabs3Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ktabs3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
