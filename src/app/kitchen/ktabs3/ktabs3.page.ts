import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-ktabs3',
  templateUrl: './ktabs3.page.html',
  styleUrls: ['./ktabs3.page.scss'],
})
export class Ktabs3Page implements OnInit {
  orders: any;

  constructor(
    private restService: RestService
  ) { }

  ngOnInit() {
  }
  getAllPreparedItems(){
    this.restService.getAllPrepareItems().then(data => {
      this.orders = (data as any).orders;
      console.log(this.orders);
    });
  }

}
