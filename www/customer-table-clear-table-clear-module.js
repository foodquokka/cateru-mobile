(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-table-clear-table-clear-module"],{

/***/ "./src/app/customer/table-clear/table-clear.module.ts":
/*!************************************************************!*\
  !*** ./src/app/customer/table-clear/table-clear.module.ts ***!
  \************************************************************/
/*! exports provided: TableClearPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableClearPageModule", function() { return TableClearPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _table_clear_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./table-clear.page */ "./src/app/customer/table-clear/table-clear.page.ts");







var routes = [
    {
        path: '',
        component: _table_clear_page__WEBPACK_IMPORTED_MODULE_6__["TableClearPage"]
    }
];
var TableClearPageModule = /** @class */ (function () {
    function TableClearPageModule() {
    }
    TableClearPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_table_clear_page__WEBPACK_IMPORTED_MODULE_6__["TableClearPage"]]
        })
    ], TableClearPageModule);
    return TableClearPageModule;
}());



/***/ }),

/***/ "./src/app/customer/table-clear/table-clear.page.html":
/*!************************************************************!*\
  !*** ./src/app/customer/table-clear/table-clear.page.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n    <ion-title text-center>Clear Table</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-button color=\"dark\" (click)=\"clearTable()\">Clear Table</ion-button>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/customer/table-clear/table-clear.page.scss":
/*!************************************************************!*\
  !*** ./src/app/customer/table-clear/table-clear.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyL3RhYmxlLWNsZWFyL3RhYmxlLWNsZWFyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/customer/table-clear/table-clear.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/customer/table-clear/table-clear.page.ts ***!
  \**********************************************************/
/*! exports provided: TableClearPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableClearPage", function() { return TableClearPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var TableClearPage = /** @class */ (function () {
    function TableClearPage(adminService, route) {
        this.adminService = adminService;
        this.route = route;
        this.table = {};
    }
    TableClearPage.prototype.ngOnInit = function () {
        // this.route.paramMap.subscribe(params => {
        //   this.tableNum = Number(params.get('tableno'));
        // });
        this.tableNum = this.adminService.getTableNumber();
        this.table = { tableno: this.tableNum };
        console.log(this.table);
    };
    TableClearPage.prototype.clearTable = function () {
        this.adminService.sendToClearTable(this.table);
    };
    TableClearPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-table-clear',
            template: __webpack_require__(/*! ./table-clear.page.html */ "./src/app/customer/table-clear/table-clear.page.html"),
            styles: [__webpack_require__(/*! ./table-clear.page.scss */ "./src/app/customer/table-clear/table-clear.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], TableClearPage);
    return TableClearPage;
}());



/***/ })

}]);
//# sourceMappingURL=customer-table-clear-table-clear-module.js.map