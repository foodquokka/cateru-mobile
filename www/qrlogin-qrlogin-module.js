(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["qrlogin-qrlogin-module"],{

/***/ "./src/app/qrlogin/qrlogin.module.ts":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.module.ts ***!
  \*******************************************/
/*! exports provided: QrloginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrloginPageModule", function() { return QrloginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _qrlogin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./qrlogin.page */ "./src/app/qrlogin/qrlogin.page.ts");







var routes = [
    {
        path: '',
        component: _qrlogin_page__WEBPACK_IMPORTED_MODULE_6__["QrloginPage"]
    }
];
var QrloginPageModule = /** @class */ (function () {
    function QrloginPageModule() {
    }
    QrloginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_qrlogin_page__WEBPACK_IMPORTED_MODULE_6__["QrloginPage"]]
        })
    ], QrloginPageModule);
    return QrloginPageModule;
}());



/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.html":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content id=\"content\">\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-button [routerLink]=\"['/dashboard']\">\r\n        <ion-icon ios=\"ios-arrow-back\" md=\"md-arrow-back\">Switch As Admin</ion-icon>\r\n      </ion-button>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <ion-grid>\r\n    <div class=\"outer\">\r\n      <div class=\"middle\">\r\n        <ion-row>\r\n          <ion-col text-center>\r\n            <ion-text id=\"cateru\">CaterU</ion-text>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-card class=\"inner\">\r\n          <ion-row text-center>\r\n            <ion-col>\r\n              <ion-card-content>\r\n              </ion-card-content>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row text-center>\r\n            <ion-col>\r\n              <ion-button class=\"startBtn\" (click)=\"startScan()\">Open Menu</ion-button>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-card>\r\n      </div>\r\n    </div>\r\n\r\n  </ion-grid>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.scss":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#content {\n  --background: linear-gradient(#7f1a1f, #3f0f11 );\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.outer {\n  display: table;\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.middle {\n  display: table-cell;\n  vertical-align: middle; }\n\n.tableBix {\n  position: absolute;\n  height: 250px;\n  width: 100%; }\n\n.inner {\n  margin-left: auto;\n  margin-right: auto;\n  width: 50%;\n  height: 200px;\n  padding: 20px 0;\n  border-style: solid; }\n\n#cateru {\n  color: #ffffff;\n  font-size: 110px;\n  font-family: billionthine; }\n\n.label {\n  color: #ffffff;\n  font-size: 130%; }\n\n.inputBox {\n  --background: #d3d3d3;\n  width: 50%;\n  border-radius: 5px;\n  margin-left: 24.5%;\n  margin-top: 15px;\n  color: #000000; }\n\n.startBtn {\n  --background: #8d272c; }\n\n.toolbar-container {\n  background-color: #8d272c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXJsb2dpbi9EOlxcU2Nob29sXFxjYXRlcnUtbW9iaWxlL3NyY1xcYXBwXFxxcmxvZ2luXFxxcmxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdJLGdEQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLHNCQUFxQixFQUFBOztBQUd6QjtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdmO0VBQ0ksbUJBQW1CO0VBQ25CLHNCQUFzQixFQUFBOztBQUUxQjtFQUNJLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsV0FBVyxFQUFBOztBQUdmO0VBQ0ksaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsYUFBYTtFQUNiLGVBQWU7RUFDZixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLHlCQUF5QixFQUFBOztBQUU3QjtFQUNJLGNBQWM7RUFDZCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0kscUJBQWE7RUFDYixVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUVsQjtFQUNJLHFCQUFhLEVBQUE7O0FBRWpCO0VBQ0cseUJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9xcmxvZ2luL3FybG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2NvbnRlbnQge1xyXG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi9hc3NldHMvaW1nL2JhY2tncm91bmQuanBnJyk7XHJcbiAgICAvLyBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uL2ltZy9nZXRzdGFydC5wbmcnKTtcclxuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KCM3ZjFhMWYsICMzZjBmMTEgKTtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6Y292ZXI7XHJcbn1cclxuXHJcbi5vdXRlciB7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1pZGRsZSB7XHJcbiAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG4udGFibGVCaXh7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBoZWlnaHQ6IDI1MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5pbm5lciB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxufVxyXG5cclxuI2NhdGVydSB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTEwcHg7XHJcbiAgICBmb250LWZhbWlseTogYmlsbGlvbnRoaW5lO1xyXG59XHJcbi5sYWJlbCB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTMwJTtcclxufVxyXG4uaW5wdXRCb3gge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZDNkM2QzO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNC41JTtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG4uc3RhcnRCdG4ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjOGQyNzJjO1xyXG59XHJcbi50b29sYmFyLWNvbnRhaW5lcntcclxuICAgYmFja2dyb3VuZC1jb2xvcjogICM4ZDI3MmM7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.ts":
/*!*****************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.ts ***!
  \*****************************************/
/*! exports provided: QrloginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrloginPage", function() { return QrloginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_dialogs_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/dialogs/ngx */ "./node_modules/@ionic-native/dialogs/ngx/index.js");
/* harmony import */ var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/qr-scanner/ngx */ "./node_modules/@ionic-native/qr-scanner/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app/qrlogin/qrlogin.service */ "./src/app/qrlogin/qrlogin.service.ts");
/* harmony import */ var _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../customer/services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/device-table.service */ "./src/app/services/device-table.service.ts");
/* harmony import */ var src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/storageservice.service */ "./src/app/services/storageservice.service.ts");












var QrloginPage = /** @class */ (function () {
    function QrloginPage(alertController, platform, dialog, navctlr, qr, qrService, router, authService, adminService, deviceTable, storageService, devicetable, route) {
        this.alertController = alertController;
        this.platform = platform;
        this.dialog = dialog;
        this.navctlr = navctlr;
        this.qr = qr;
        this.qrService = qrService;
        this.router = router;
        this.authService = authService;
        this.adminService = adminService;
        this.deviceTable = deviceTable;
        this.storageService = storageService;
        this.devicetable = devicetable;
        this.route = route;
        this.UUID = {};
        this.num = {};
        this.tableStatus = {};
        this.deviceinfo = {};
        this.table = {};
        this.encodedData = '';
        this.isOn = false;
        this.scannedinfos = Array();
        this.isShow = false;
        this.isShowOccupiedTable = false;
        this.platform.backButton.subscribeWithPriority(0, function () {
            document.getElementsByTagName('body')[0].style.opacity = '1';
        });
    }
    QrloginPage.prototype.ngOnInit = function () {
    };
    QrloginPage.prototype.ionviewWillEnter = function () {
        this.getAllAvailableTableNo();
        this.getTableStatusNotPaid();
    };
    QrloginPage.prototype.goToCustomerTable = function (order_id) {
        console.log('GOTOCUSTOMER');
        console.log(order_id);
        this.storageService.saveOrderID(order_id);
        this.route.navigate(['confirmedorderlist', order_id]);
    };
    QrloginPage.prototype.getOrderByTableNo = function (tableno) {
        var _this = this;
        this.qrService.getOrderByTableNo(tableno).then(function (data) {
            _this.orders = data.details;
        });
        console.log(tableno);
    };
    QrloginPage.prototype.getTableStatusNotPaid = function () {
        var _this = this;
        this.deviceTable.getTableStatusNotPaid().then(function (data) {
            _this.tabless = data.tables;
            console.log(_this.tabless);
        });
    };
    QrloginPage.prototype.startScan = function () {
        var _this = this;
        this.qr.prepare().then(function (status) {
            if (status.authorized) {
                _this.qr.useFrontCamera();
                _this.isOn = true;
                document.getElementsByTagName('body')[0].style.opacity = ' 0 ';
                var scanSub_1 = _this.qr.scan().subscribe(function (id) {
                    _this.isOn = false;
                    _this.QRSCANNED_DATA = id;
                    console.log(id);
                    _this.qrService.verifyQRLogin(_this.QRSCANNED_DATA).then(function (data) {
                        console.log(data);
                        if (data === null) {
                            _this.closeScanner();
                            scanSub_1.unsubscribe();
                        }
                        else {
                            _this.infos = data.employee_info;
                            console.log(_this.infos);
                            var route = 'qrlogin';
                            var isAuthenticated = false;
                            for (var _i = 0, _a = _this.infos; _i < _a.length; _i++) {
                                var info = _a[_i];
                                _this.authService.login(info.sessionid);
                                _this.position = info.emp_pos;
                                console.log(_this.position);
                                _this.empid = info.emp_id;
                                isAuthenticated = _this.authService.isAuthenticated();
                                if (_this.position === 'waiter') {
                                    console.log('HELLO');
                                    _this.router.navigate(['/table', _this.empid]);
                                }
                                else {
                                    _this.closeScanner();
                                    scanSub_1.unsubscribe();
                                }
                                break;
                            }
                            _this.closeScanner();
                            scanSub_1.unsubscribe();
                        }
                    });
                });
                _this.qr.show();
            }
            else if (status.denied) {
                _this.qrScan = _this.qr.scan().subscribe(function (text) {
                    document.getElementsByTagName('body')[0].style.opacity = '1';
                    _this.qrScan.unsubscribe();
                    _this.dialog.alert(text);
                }, function (err) {
                    _this.dialog.alert(JSON.stringify(err));
                });
            }
        })
            .catch(function (e) { return console.log('Error is', e); });
    };
    QrloginPage.prototype.getStatus = function (status) {
        return this.status = status;
    };
    QrloginPage.prototype.closeScanner = function () {
        this.isOn = false;
        this.qr.hide();
        this.qr.destroy();
        document.getElementsByTagName('body')[0].style.opacity = '1';
    };
    QrloginPage.prototype.successLogin = function (data) {
        this.qrService.saveOrdersData(this.orderInfo);
    };
    QrloginPage.prototype.random = function () {
        var rand = Math.floor(Math.random() * 20) + 1;
        return rand;
    };
    QrloginPage.prototype.setCustId = function (id) {
        this.custid = id;
    };
    QrloginPage.prototype.getCustId = function () {
        console.log(this.custid);
    };
    QrloginPage.prototype.getOccupiedTable = function () {
        var _this = this;
        this.qrService.getOccupiedTable().then(function (data) {
            _this.occupiedtables = data.OccupiedTables;
        });
    };
    QrloginPage.prototype.saveTransactionInfo = function () {
        var _this = this;
        this.deviceinfo.custid = this.random();
        this.deviceinfo.empid = this.empid;
        this.deviceinfo.date_ordered = Date.now();
        this.deviceinfo.tableno = this.no;
        this.adminService.postToOrder(this.deviceinfo).then(function (data) {
            _this.storageService.saveOrderID(data.order_id);
            console.log(_this.deviceinfo);
        });
        console.log(this.no);
        this.setTableOccupied();
    };
    QrloginPage.prototype.getAllAvailableTableNo = function () {
        var _this = this;
        this.devicetable.getAllAvailableTable().then(function (data) {
            _this.tables = data.AvailableTables;
            console.log(_this.tables);
        });
    };
    QrloginPage.prototype.getTableNo = function () {
        return this.selectedTable;
    };
    QrloginPage.prototype.setTableNo = function (selectedTable) {
        this.selectedTable = selectedTable;
    };
    QrloginPage.prototype.setTableOccupied = function () {
        this.tableStatus.tableno = this.selectedTable;
        this.tableStatus.status = 'Occupied';
        this.qrService.setTableStatus(this.tableStatus);
        console.log('set table status');
    };
    QrloginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-qrlogin',
            template: __webpack_require__(/*! ./qrlogin.page.html */ "./src/app/qrlogin/qrlogin.page.html"),
            styles: [__webpack_require__(/*! ./qrlogin.page.scss */ "./src/app/qrlogin/qrlogin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _ionic_native_dialogs_ngx__WEBPACK_IMPORTED_MODULE_2__["Dialogs"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["QRScanner"],
            _app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__["QrloginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__["AuthenticationService"],
            _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_6__["AdminService"],
            _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__["DeviceTableService"],
            src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_10__["StorageserviceService"],
            _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__["DeviceTableService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], QrloginPage);
    return QrloginPage;
}());



/***/ })

}]);
//# sourceMappingURL=qrlogin-qrlogin-module.js.map