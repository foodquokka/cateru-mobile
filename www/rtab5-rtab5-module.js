(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab5-rtab5-module"],{

/***/ "./src/app/receptionist/rtab5/rtab5.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab5/rtab5.module.ts ***!
  \****************************************************/
/*! exports provided: Rtab5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rtab5PageModule", function() { return Rtab5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rtab5_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab5.page */ "./src/app/receptionist/rtab5/rtab5.page.ts");







var routes = [
    {
        path: '',
        component: _rtab5_page__WEBPACK_IMPORTED_MODULE_6__["Rtab5Page"]
    }
];
var Rtab5PageModule = /** @class */ (function () {
    function Rtab5PageModule() {
    }
    Rtab5PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_rtab5_page__WEBPACK_IMPORTED_MODULE_6__["Rtab5Page"]]
        })
    ], Rtab5PageModule);
    return Rtab5PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab5/rtab5.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab5/rtab5.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>Manage Table</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div id=\"InnerDiv\" text-center>\n    <ion-button (click)=\"showMergeTable()\">Merge Tables</ion-button>\n    <ion-button (click)=\"showTransferTable()\">Transfer Table</ion-button>\n  </div>\n  <div id=\"InnerDiv\" padding [hidden]=\"showMerge\">\n    <ion-list style=\"width: 50%;\">\n      <ion-item>\n        <ion-label>MERGE TABLE </ion-label>\n        <ion-select placeholder=\"First Table\" [(ngModel)]=\"firstTable\">\n          <ion-select-option *ngFor=\"let table of availabletables\" value=\"{{table.tableno}}\"\n            (ionChange)=\"setFirstTable(transferfrom)\" name=\"firstTable\">{{ table.tableno}}</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n    <ion-list style=\"width: 50%;\">\n      <ion-item>\n        <ion-label>AND</ion-label>\n        <ion-select placeholder=\"Second Table\" [(ngModel)]=\"secondTable\">\n          <ion-select-option *ngFor=\"let table of availabletables\" value=\"{{table.tableno}}\"\n            (ionChange)=\"setSecondTable(secondTable)\" name=\"secondTable\">Table No: {{ table.tableno }}\n          </ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n    <ion-button (click)=\"mergeTable()\">MERGE</ion-button>\n  </div>\n\n  <div id=\"InnerDiv\" padding [hidden]=\"showTransfer\">\n    <ion-list style=\"width: 50%;\">\n      <ion-item>\n        <ion-label>FROM</ion-label>\n        <ion-select placeholder=\"Transfer from\" [(ngModel)]=\"transferfrom\">\n          <ion-select-option *ngFor=\"let table of occupiedtables\" value=\"{{table.tableno}}\"\n            (ionChange)=\"setTransferFrom(transferfrom)\" name=\"transferfrom\">{{ table.tableno}}</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n    <ion-list style=\"width: 50%;\">\n      <ion-item>\n        <ion-label>TO</ion-label>\n        <ion-select placeholder=\"Transfer to\" [(ngModel)]=\"transferto\">\n          <ion-select-option *ngFor=\"let table of availabletables\" value=\"{{table.tableno}}\"\n            (ionChange)=\"setTransferTo(transferto)\" name=\"transferto\">Table No: {{ table.tableno }}</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n    <ion-button (click)=\"transferTable()\">TRANSFER</ion-button>\n  </div>\n  <div id=\"OutsideDiv\">\n    <div id=\"FirstInnerDiv\">\n      <div id=\"OccupiedTableLabel\" text-center>\n        <h3>OCCUPIED TABLES</h3>\n      </div>\n      <div id=\"OccupiedGrid\">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-6 *ngFor=\"let table of occupiedtables\">\n              <ion-card class=\"occupied\" fill=\"outline\" border-style=\"solid\" mode=\"ios\" text-center padding tappable\n                (click)=\"presentmodal(t.tableno)\">\n                <h2>TABLE NO: {{ table.tableno }}</h2>\n                <p>Capacity: {{ table.capacity}}</p>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n    <div id=\"SecondInnerDiv\" >\n      <div>\n        <div id=\"AvailableTableLabel\" text-center>\n          <h3>AVAILABLE TABLES</h3>\n        </div>\n      </div>\n      <div id=\"AvailableGrid\">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12 *ngFor=\"let table of availabletables\">\n              <ion-card class=\"occupied\" fill=\"outline\" border-style=\"solid\" mode=\"ios\" text-center padding tappable\n                (click)=\"presentmodal(t.tableno)\">\n                <h2>TABLE NO: {{ table.tableno}}</h2>\n                <p>Capacity: {{ table.capacity}}</p>\n              </ion-card>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </div>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab5/rtab5.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab5/rtab5.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#OutsideDiv {\n  display: flex;\n  flex: 1;\n  justify-content: space-between;\n  height: 80vh;\n  overflow: auto;\n  scroll-behavior: smooth; }\n\n#FirstInnerDiv {\n  display: flex;\n  padding: inherit;\n  border: 1px solid #d9d9d9;\n  padding: 10px;\n  margin-left: 10px;\n  margin-bottom: 10px;\n  flex: 1;\n  overflow: scroll; }\n\n#SecondInnerDiv {\n  display: flex;\n  border: 1px solid #d9d9d9;\n  padding: 10px;\n  margin-left: 10px;\n  margin-bottom: 10px;\n  flex: 1;\n  overflow: scroll; }\n\n#SecondInnerDiv ion-card-content {\n    margin-top: 70px; }\n\n#SecondInnerDiv ion-icon {\n    font-size: 30px; }\n\n#InnerDiv {\n  display: flex;\n  padding: inherit;\n  border: 1px solid #d9d9d9;\n  padding: 10px;\n  margin-left: 10px;\n  margin-bottom: 10px;\n  flex: 1;\n  overflow: scroll; }\n\n#OccupiedTableLabel {\n  position: absolute;\n  width: 46%;\n  font-weight: bold;\n  font-size: 20px;\n  color: white;\n  background-color: black; }\n\n#OccupiedGrid {\n  margin-top: 20px; }\n\n#AvailableTableLabel {\n  position: absolute;\n  width: 46%;\n  font-weight: bold;\n  font-size: 20px;\n  color: white;\n  background-color: black; }\n\n#AvailableGrid {\n  margin-top: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWI1L0Q6XFxTY2hvb2xcXGNhdGVydS1tb2JpbGUvc3JjXFxhcHBcXHJlY2VwdGlvbmlzdFxccnRhYjVcXHJ0YWI1LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQWE7RUFDYixPQUFPO0VBQ1AsOEJBQThCO0VBRTlCLFlBQVk7RUFDWixjQUFjO0VBQ2QsdUJBQXVCLEVBQUE7O0FBRTNCO0VBQ0ksYUFBWTtFQUNaLGdCQUFnQjtFQUVoQix5QkFBeUI7RUFDekIsYUFBYTtFQUdiLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLGdCQUFnQixFQUFBOztBQUVwQjtFQUlJLGFBQVk7RUFDWix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsT0FBTztFQUNQLGdCQUFnQixFQUFBOztBQVZwQjtJQUVRLGdCQUFnQixFQUFBOztBQUZ4QjtJQVlRLGVBQWUsRUFBQTs7QUFHdkI7RUFDSSxhQUFZO0VBQ1osZ0JBQWdCO0VBRWhCLHlCQUF5QjtFQUN6QixhQUFhO0VBR2IsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLFlBQVk7RUFDWix1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxnQkFBZ0IsRUFBQTs7QUFFcEI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsWUFBWTtFQUNaLHVCQUF1QixFQUFBOztBQUczQjtFQUNJLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWI1L3J0YWI1LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNPdXRzaWRlRGl2e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXg6IDE7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAvL21hcmdpbi10b3A6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiA4MHZoO1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICBzY3JvbGwtYmVoYXZpb3I6IHNtb290aDtcclxufVxyXG4jRmlyc3RJbm5lckRpdntcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIHBhZGRpbmc6IGluaGVyaXQ7XHJcbiAgICAvL2JhY2tncm91bmQtY29sb3I6IGFxdWE7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZDlkOWQ5O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIC8vYm94LXNoYWRvdzogNXB4IDJweCAjZDlkOWQ5O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZmxleDogMTtcclxuICAgIG92ZXJmbG93OiBzY3JvbGw7XHJcbn1cclxuI1NlY29uZElubmVyRGl2e1xyXG4gICAgaW9uLWNhcmQtY29udGVudHtcclxuICAgICAgICBtYXJnaW4tdG9wOiA3MHB4O1xyXG4gICAgfVxyXG4gICAgZGlzcGxheTpmbGV4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q5ZDlkOTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4OiAxO1xyXG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICAgIGlvbi1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIH1cclxufVxyXG4jSW5uZXJEaXZ7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBwYWRkaW5nOiBpbmhlcml0O1xyXG4gICAgLy9iYWNrZ3JvdW5kLWNvbG9yOiBhcXVhO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q5ZDlkOTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgIC8vIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAvL2JveC1zaGFkb3c6IDVweCAycHggI2Q5ZDlkOTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXg6IDE7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG59XHJcbiNPY2N1cGllZFRhYmxlTGFiZWx7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogNDYlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuXHJcbn1cclxuI09jY3VwaWVkR3JpZHtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuI0F2YWlsYWJsZVRhYmxlTGFiZWx7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogNDYlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuXHJcbn1cclxuI0F2YWlsYWJsZUdyaWR7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/receptionist/rtab5/rtab5.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab5/rtab5.page.ts ***!
  \**************************************************/
/*! exports provided: Rtab5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rtab5Page", function() { return Rtab5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_receptionist_services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/receptionist/services/rest.service */ "./src/app/receptionist/services/rest.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _managerauth_managerauth_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../managerauth/managerauth.page */ "./src/app/receptionist/managerauth/managerauth.page.ts");





var Rtab5Page = /** @class */ (function () {
    function Rtab5Page(restService, modalController) {
        this.restService = restService;
        this.modalController = modalController;
        this.tables = {};
        this.isShowBtn = false;
        this.showMerge = true;
        this.showTransfer = true;
    }
    Rtab5Page.prototype.ngOnInit = function () {
        this.getAllAvailableTable();
        this.getAllOccupiedTable();
    };
    Rtab5Page.prototype.ionViewWillEnter = function () {
        this.getAllAvailableTable();
        this.getAllOccupiedTable();
        this.transferfrom = "";
        this.transferto = "";
        this.firstTable = "";
        this.secondTable = "";
    };
    Rtab5Page.prototype.getAllAvailableTable = function () {
        var _this = this;
        this.restService.getAvailableTable().then(function (data) {
            _this.availabletables = data.AvailableTables;
        });
    };
    Rtab5Page.prototype.getAllOccupiedTable = function () {
        var _this = this;
        this.restService.getOccupiedTable().then(function (data) {
            _this.occupiedtables = data.OccupiedTables;
        });
    };
    Rtab5Page.prototype.transferTable = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var from, to, modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        from = this.transferfrom;
                        to = this.transferto;
                        return [4 /*yield*/, this.modalController.create({
                                component: _managerauth_managerauth_page__WEBPACK_IMPORTED_MODULE_4__["ManagerauthPage"],
                                componentProps: { from: from, to: to }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function () {
                            _this.ionViewWillEnter();
                            //  this.cartTable();
                        });
                        //await modal.present();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        //await modal.present();
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Rtab5Page.prototype.setTransferFrom = function (selectedValue) {
        this.transferfrom = selectedValue;
        console.log(this.transferfrom);
    };
    Rtab5Page.prototype.setTransferTo = function (selectedValue) {
        this.transferto = selectedValue;
        console.log(this.transferto);
    };
    Rtab5Page.prototype.setFirstTable = function (firstTable) {
        this.firstTable = firstTable;
        console.log(this.firstTable);
    };
    Rtab5Page.prototype.setSecondTable = function (secondTable) {
        this.secondTable = secondTable;
        console.log(this.secondTable);
    };
    Rtab5Page.prototype.mergeTable = function () {
        var mergeTable = {
            firsttable: parseInt(this.firstTable),
            secondtable: parseInt(this.secondTable)
        };
        this.restService.mergeTables(mergeTable);
        this.ionViewWillEnter();
    };
    Rtab5Page.prototype.showMergeTable = function () {
        this.showMerge = false;
        this.showTransfer = true;
    };
    Rtab5Page.prototype.showTransferTable = function () {
        this.showMerge = true;
        this.showTransfer = false;
    };
    Rtab5Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rtab5',
            template: __webpack_require__(/*! ./rtab5.page.html */ "./src/app/receptionist/rtab5/rtab5.page.html"),
            styles: [__webpack_require__(/*! ./rtab5.page.scss */ "./src/app/receptionist/rtab5/rtab5.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_receptionist_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
    ], Rtab5Page);
    return Rtab5Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab5-rtab5-module.js.map