(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-customerr-routing-module"],{

/***/ "./src/app/customer/customerr-routing.module.ts":
/*!******************************************************!*\
  !*** ./src/app/customer/customerr-routing.module.ts ***!
  \******************************************************/
/*! exports provided: CustomerrRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerrRoutingModule", function() { return CustomerrRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");


//import { CommonModule } from '@angular/common';

var routes = [
    {
        path: 'starting-page/:id',
        loadChildren: './starting-page/starting-page.module#StartingPagePageModule'
    },
    { path: 'cartmodal', loadChildren: './menu/cartmodal/cartmodal.module#CartmodalPageModule' },
    { path: 'promo-modal', loadChildren: './menu/promo-modal/promo-modal.module#PromoModalPageModule' }
];
var CustomerrRoutingModule = /** @class */ (function () {
    function CustomerrRoutingModule() {
    }
    CustomerrRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            // declarations: [],
            imports: [
                // CommonModule
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], CustomerrRoutingModule);
    return CustomerrRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=customer-customerr-routing-module.js.map