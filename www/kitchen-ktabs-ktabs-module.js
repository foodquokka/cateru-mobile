(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["kitchen-ktabs-ktabs-module"],{

/***/ "./src/app/kitchen/ktabs/ktabs.module.ts":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktabs/ktabs.module.ts ***!
  \***********************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ktabs_router_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ktabs.router.module */ "./src/app/kitchen/ktabs/ktabs.router.module.ts");
/* harmony import */ var _ktabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ktabs.page */ "./src/app/kitchen/ktabs/ktabs.page.ts");







var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ktabs_router_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
            ],
            declarations: [_ktabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/kitchen/ktabs/ktabs.page.html":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktabs/ktabs.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs >\r\n\r\n  <ion-tab-bar slot=\"bottom\" color=\"primary\">\r\n    <ion-tab-button tab=\"ktab1\">\r\n      <ion-icon name=\"list\"></ion-icon>\r\n      <ion-label>Orders</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"ktab3\">\r\n      <ion-icon name=\"stopwatch\"></ion-icon>\r\n      <ion-label>Preparing</ion-label>\r\n    </ion-tab-button> \r\n\r\n    <ion-tab-button tab=\"ktab2\">\r\n      <ion-icon name=\"done-all\"></ion-icon>\r\n      <ion-label>Completed</ion-label>\r\n    </ion-tab-button> \r\n\r\n    <!-- <ion-tab-button tab=\"tab3\">\r\n      <ion-icon name=\"send\"></ion-icon>\r\n      <ion-label>Tab Three</ion-label>\r\n    </ion-tab-button> -->\r\n    \r\n  </ion-tab-bar>\r\n  \r\n</ion-tabs>\r\n"

/***/ }),

/***/ "./src/app/kitchen/ktabs/ktabs.page.scss":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktabs/ktabs.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2tpdGNoZW4va3RhYnMva3RhYnMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/kitchen/ktabs/ktabs.page.ts":
/*!*********************************************!*\
  !*** ./src/app/kitchen/ktabs/ktabs.page.ts ***!
  \*********************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TabsPage = /** @class */ (function () {
    function TabsPage() {
    }
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! ./ktabs.page.html */ "./src/app/kitchen/ktabs/ktabs.page.html"),
            styles: [__webpack_require__(/*! ./ktabs.page.scss */ "./src/app/kitchen/ktabs/ktabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());



/***/ }),

/***/ "./src/app/kitchen/ktabs/ktabs.router.module.ts":
/*!******************************************************!*\
  !*** ./src/app/kitchen/ktabs/ktabs.router.module.ts ***!
  \******************************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ktabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ktabs.page */ "./src/app/kitchen/ktabs/ktabs.page.ts");




var routes = [
    {
        path: 'ktabs',
        component: _ktabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'ktab1',
                children: [
                    {
                        path: '',
                        loadChildren: '../ktab1/ktab1.module#Tab1PageModule'
                    }
                ]
            },
            {
                path: 'ktab2',
                children: [
                    {
                        path: '',
                        loadChildren: '../ktab2/ktab2.module#Tab2PageModule'
                    }
                ]
            },
            {
                path: 'ktab3',
                children: [
                    {
                        path: '',
                        loadChildren: '../ktab3/ktab3.module#Ktab3PageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/kitchen/ktabs/ktab1',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/kitchen/ktabs/ktab1',
        pathMatch: 'full'
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=kitchen-ktabs-ktabs-module.js.map