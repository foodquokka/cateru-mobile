(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab4-rtab4-module"],{

/***/ "./src/app/receptionist/rtab4/rtab4.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab4/rtab4.module.ts ***!
  \****************************************************/
/*! exports provided: Rtab4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rtab4PageModule", function() { return Rtab4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rtab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab4.page */ "./src/app/receptionist/rtab4/rtab4.page.ts");







var routes = [
    {
        path: '',
        component: _rtab4_page__WEBPACK_IMPORTED_MODULE_6__["Rtab4Page"]
    }
];
var Rtab4PageModule = /** @class */ (function () {
    function Rtab4PageModule() {
    }
    Rtab4PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_rtab4_page__WEBPACK_IMPORTED_MODULE_6__["Rtab4Page"]]
        })
    ], Rtab4PageModule);
    return Rtab4PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab4/rtab4.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab4/rtab4.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>\n      Notified List\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content\n      pullingIcon=\"arrow-dropdown\"\n      pullingText=\"Pull to refresh\"\n      refreshingSpinner=\"circles\"\n      refreshingText=\"Refreshing...\">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-card-content>\n    <ion-row>\n      <ion-col>\n        <ion-list size-md=\"4\">Name</ion-list>\n      </ion-col>\n\n      <ion-col>\n        <ion-list size-md=\"4\">Party size</ion-list>\n      </ion-col>\n\n      <ion-col>\n        <ion-list size-md=\"4\">Phonenumber</ion-list>\n      </ion-col>\n\n      <ion-col>\n        <ion-list size-md=\"4\">Table No</ion-list>\n      </ion-col>\n\n      <ion-col>\n        <ion-list size-md=\"4\">Time</ion-list>\n      </ion-col>\n\n      <ion-col>\n        <ion-list size-md=\"4\">Action</ion-list>\n      </ion-col>\n\n    </ion-row>\n  </ion-card-content>\n\n  <ion-row *ngFor=\"let customer of notifiedcustomers\">\n    <ion-col>\n      <ion-list>\n        <ion-item>{{ customer.name }}</ion-item>\n      </ion-list>\n    </ion-col>\n\n    <ion-col>\n      <ion-list>\n        <ion-item>{{ customer.partysize }}</ion-item>\n      </ion-list>\n    </ion-col>\n\n    <ion-col>\n      <ion-list>\n        <ion-item>{{ customer.phonenumber }}</ion-item>\n      </ion-list>\n    </ion-col>\n\n    <ion-col>\n      <ion-list>\n        <ion-item>{{ customer.tableno }}</ion-item>\n      </ion-list>\n    </ion-col>\n\n    <ion-col>\n      <ion-list>\n        <ion-item>{{ customer.time_notified | date:'shortTime'}}</ion-item>\n      </ion-list>\n    </ion-col>\n\n    <ion-col>\n      <ion-button color=\"success\" (click)=\"setStatusPresent(customer.custid)\" >Confirm</ion-button>\n      <ion-button (click)=\"removeQueue(customer.custid,customer.tableno)\" color=\"danger\">Cancel</ion-button>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab4/rtab4.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab4/rtab4.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY2VwdGlvbmlzdC9ydGFiNC9ydGFiNC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtab4/rtab4.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab4/rtab4.page.ts ***!
  \**************************************************/
/*! exports provided: Rtab4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Rtab4Page", function() { return Rtab4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");
/* harmony import */ var src_app_cashier_service_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/cashier/service/api.service */ "./src/app/cashier/service/api.service.ts");




var Rtab4Page = /** @class */ (function () {
    function Rtab4Page(restservice, apiService) {
        this.restservice = restservice;
        this.apiService = apiService;
        this.array = [];
    }
    Rtab4Page.prototype.ngOnInit = function () {
        this.getNotifiedCustomer();
    };
    Rtab4Page.prototype.ionViewWillEnter = function () {
        this.getNotifiedCustomer();
    };
    Rtab4Page.prototype.getNotifiedCustomer = function () {
        var _this = this;
        this.restservice.getNotifiedCustomer().then(function (data) {
            _this.notifiedcustomers = data.notified;
            console.log(_this.notifiedcustomers);
        });
    };
    Rtab4Page.prototype.setStatusPresent = function (custid) {
        console.log(custid);
        this.restservice.presentCustomer(custid);
        this.ionViewWillEnter();
    };
    Rtab4Page.prototype.removeQueue = function (custid, tableno) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log(custid);
                this.restservice.statusCancelled(custid).then(function () {
                    _this.apiService.setTableAvailable(tableno).then(function () {
                        _this.doRefresh();
                    });
                });
                return [2 /*return*/];
            });
        });
    };
    Rtab4Page.prototype.doRefresh = function () {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getNotifiedCustomer();
            //event.target.complete();
        }, 1500);
    };
    Rtab4Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rtab4',
            template: __webpack_require__(/*! ./rtab4.page.html */ "./src/app/receptionist/rtab4/rtab4.page.html"),
            styles: [__webpack_require__(/*! ./rtab4.page.scss */ "./src/app/receptionist/rtab4/rtab4.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            src_app_cashier_service_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]])
    ], Rtab4Page);
    return Rtab4Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab4-rtab4-module.js.map