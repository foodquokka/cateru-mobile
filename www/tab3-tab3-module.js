(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./src/app/waiter/tab3/tab3.module.ts":
/*!********************************************!*\
  !*** ./src/app/waiter/tab3/tab3.module.ts ***!
  \********************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/waiter/tab3/tab3.page.ts");







var Tab3PageModule = /** @class */ (function () {
    function Tab3PageModule() {
    }
    Tab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }])
            ],
            declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
        })
    ], Tab3PageModule);
    return Tab3PageModule;
}());



/***/ }),

/***/ "./src/app/waiter/tab3/tab3.page.html":
/*!********************************************!*\
  !*** ./src/app/waiter/tab3/tab3.page.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n</ion-header>\n<ion-toolbar color=\"primary\">\n  <ion-title>\n    Complete\n  </ion-title>\n</ion-toolbar>\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-card class=\"card\">\n          <ion-card-content>\n            <ion-row>\n              <ion-col text-center>\n                <h1>DRINKS</h1>\n              </ion-col>\n            </ion-row>\n            <ion-row text-center>\n              <ion-col>TIME</ion-col>\n              <ion-col>NAME</ion-col>\n            </ion-row>\n            <ion-item text-center *ngFor=\"let order of drinks;let i = index\">\n              <ion-label>\n                {{order.updated_at  | date:'shortTime'}}\n              </ion-label>\n              <ion-label>\n                {{order.name}}\n              </ion-label>\n            </ion-item>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/waiter/tab3/tab3.page.scss":
/*!********************************************!*\
  !*** ./src/app/waiter/tab3/tab3.page.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  height: 50px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FpdGVyL3RhYjMvRDpcXFNjaG9vbFxcY2F0ZXJ1LW1vYmlsZS9zcmNcXGFwcFxcd2FpdGVyXFx0YWIzXFx0YWIzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvd2FpdGVyL3RhYjMvdGFiMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xuICAgIGhlaWdodDogNTBweCAhaW1wb3J0YW50O1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/waiter/tab3/tab3.page.ts":
/*!******************************************!*\
  !*** ./src/app/waiter/tab3/tab3.page.ts ***!
  \******************************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/kitchen/rest.service */ "./src/app/kitchen/rest.service.ts");



var Tab3Page = /** @class */ (function () {
    function Tab3Page(restService) {
        this.restService = restService;
    }
    Tab3Page.prototype.ngOnInit = function () {
        this.getAllCompleteDrinks();
    };
    // ionViewWillEnter(){
    //   this.getAllCompleteDrinks();
    // }
    Tab3Page.prototype.getAllCompleteDrinks = function () {
        var _this = this;
        this.restService.getAllCompleteDrinks().then(function (data) {
            _this.drinks = data.orders;
            console.log(_this.drinks);
        });
    };
    Tab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab3',
            template: __webpack_require__(/*! ./tab3.page.html */ "./src/app/waiter/tab3/tab3.page.html"),
            styles: [__webpack_require__(/*! ./tab3.page.scss */ "./src/app/waiter/tab3/tab3.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab3Page);
    return Tab3Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module.js.map