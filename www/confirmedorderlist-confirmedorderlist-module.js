(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["confirmedorderlist-confirmedorderlist-module"],{

/***/ "./src/app/confirmedorderlist/confirmedorderlist.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.module.ts ***!
  \*****************************************************************/
/*! exports provided: ConfirmedorderlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedorderlistPageModule", function() { return ConfirmedorderlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirmedorderlist.page */ "./src/app/confirmedorderlist/confirmedorderlist.page.ts");







var routes = [
    {
        path: '',
        component: _confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmedorderlistPage"]
    }
];
var ConfirmedorderlistPageModule = /** @class */ (function () {
    function ConfirmedorderlistPageModule() {
    }
    ConfirmedorderlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmedorderlistPage"]]
        })
    ], ConfirmedorderlistPageModule);
    return ConfirmedorderlistPageModule;
}());



/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"dark\">\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-title>My Orders</ion-title>\r\n    </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n    <!-- <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n        <ion-refresher-content pullingIcon=\"arrow-dropdown\" pullingText=\"Pull to refresh\" refreshingSpinner=\"circles\"\r\n            refreshingText=\"Refreshing...\">\r\n        </ion-refresher-content>\r\n    </ion-refresher> -->\r\n    <div id=\"FlexOrderContainer\">\r\n        <div id=\"FlexOrderFirstBox\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col>\r\n                        <ion-card text-center>\r\n                            <h2>PENDING ORDERS</h2>\r\n                            <ion-card-content text-center>\r\n                                <ion-row text-center id=\"menuCard-title\">\r\n                                    <ion-col>Name</ion-col>\r\n                                    <ion-col>No. of Orders</ion-col>\r\n                                    <ion-col>Items to Serve</ion-col>\r\n                                    <ion-col>Action</ion-col>\r\n                                </ion-row>\r\n                                <div id=\"FlexOrderFirstBoxDetails\">\r\n                                    <div>\r\n                                        <ion-row text-center *ngFor=\"let item of orderlist; let i=index\">\r\n                                            <ion-col>\r\n                                                <p style=\"text-transform: uppercase; color: chartreuse;\">\r\n                                                    {{ item.bundlename }}</p>{{item.name }}\r\n                                            </ion-col>\r\n                                            <ion-col>{{ item.orderNum }}</ion-col>\r\n                                            <ion-col>\r\n                                                <ion-select interface=\"popover\" [(ngModel)]=\"item.served\"\r\n                                                    placeholder=\"Number of items to serve\">\r\n                                                    <div *ngFor=\"let n of pendingOrderMatrix[i];\">\r\n                                                        <ion-select-option value=\"{{ n }}\">\r\n                                                            {{ n }}\r\n                                                        </ion-select-option>\r\n                                                    </div>\r\n                                                </ion-select>\r\n                                            </ion-col>\r\n                                            <ion-col text-center>\r\n                                                <ion-button (click)=\"serve(item.served,item.id)\"\r\n                                                    style=\"width: 100px !important;\">SERVE</ion-button>\r\n                                            </ion-col>\r\n                                        </ion-row>\r\n                                    </div>\r\n                                </div>\r\n                            </ion-card-content>\r\n                        </ion-card>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </div>\r\n        <div id=\"FlexOrderSecondBox\">\r\n            <ion-grid>\r\n                <ion-row>\r\n                    <ion-col>\r\n                        <ion-card text-center>\r\n                            <h2>SERVED ORDERS</h2>\r\n                            <ion-card-content text-center>\r\n                                <ion-row text-center id=\"menuCard-title\">\r\n                                    <ion-col>Name</ion-col>\r\n                                    <ion-col>Status</ion-col>\r\n                                </ion-row>\r\n                                <div id=\"FlexOrderFirstBoxDetails\">\r\n                                    <ion-row text-center *ngFor=\"let item of servedOrderList\">\r\n                                        <ion-col>\r\n                                            <p style=\"text-transform: uppercase; color: chartreuse;\">\r\n                                                {{ item.bundlename }}</p>{{item.name }}\r\n                                        </ion-col>\r\n                                        <ion-col style=\"color: chartreuse;\">\r\n                                            <ion-icon name=\"checkmark\"></ion-icon>COMPLETED\r\n                                        </ion-col>\r\n                                    </ion-row>\r\n                                </div>\r\n                            </ion-card-content>\r\n                        </ion-card>\r\n                    </ion-col>\r\n                </ion-row>\r\n            </ion-grid>\r\n        </div>\r\n    </div>\r\n</ion-content>\r\n<ion-footer>\r\n    <div id=\"footerdiv\" style=\"width: 50%;\">\r\n        <ion-button (click)=\"billout()\">BILLOUT</ion-button>\r\n        <h1 style=\"font-weight: bold; color: cornsilk; float: right;\"> TOTAL: {{ orderTotal }}</h1>\r\n    </div>\r\n\r\n</ion-footer>"

/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#menuCard-title {\n  background-color: grey;\n  color: whitesmoke;\n  font-weight: bold;\n  font-size: 20px; }\n\n#menuCard-detail {\n  font-size: 18px;\n  border-bottom: 1px solid grey;\n  border-left: none;\n  border-right: none; }\n\n#confirmedInnerDiv {\n  overflow: scroll; }\n\nion-input {\n  border-radius: 5px;\n  border: 2px solid grey;\n  max-height: 200px;\n  float: center; }\n\nion-button {\n  width: 150px !important;\n  height: 50px !important;\n  font-size: 20px; }\n\n#FlexOverall {\n  display: flex;\n  flex-direction: column;\n  margin: 0;\n  position: fixed;\n  width: 100vw;\n  height: 100vh;\n  overflow: hidden; }\n\n#FlexOrderContainer {\n  display: flex;\n  flex: 1;\n  width: 100%;\n  overflow: hidden;\n  height: 100%; }\n\n#FlexOrderFirstBox {\n  display: flex;\n  width: 50%;\n  height: 100%;\n  flex: 1;\n  overflow: auto; }\n\n#FlexOrderFirstBox ion-card {\n    width: 100%; }\n\n#FlexOrderFirstBox ion-row {\n    width: 100%; }\n\n#FlexOrderFirstBoxDetails {\n  overflow: auto;\n  height: 100%; }\n\n#FlexOrderTitle {\n  position: fixed; }\n\n#FlexOrderSecondBox {\n  display: flex;\n  flex: 1;\n  width: 50vw;\n  height: 100%;\n  overflow: auto; }\n\n#FlexFirstInnerSecondBox {\n  flex: 1; }\n\n#FlexSecondInnerSecondBox {\n  flex: 1; }\n\n#FlexBillOutCard {\n  position: fixed;\n  bottom: 0;\n  width: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZmlybWVkb3JkZXJsaXN0L0Q6XFxTY2hvb2xcXGNhdGVydS1tb2JpbGUvc3JjXFxhcHBcXGNvbmZpcm1lZG9yZGVybGlzdFxcY29uZmlybWVkb3JkZXJsaXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFzQjtFQUN0QixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxlQUFlO0VBQ2YsNkJBQTRCO0VBQzVCLGlCQUFpQjtFQUNqQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxrQkFBa0I7RUFDbEIsc0JBQXNCO0VBRXRCLGlCQUFpQjtFQUNqQixhQUFhLEVBQUE7O0FBR2pCO0VBQ0ksdUJBQXVCO0VBQ3ZCLHVCQUF1QjtFQUN2QixlQUFlLEVBQUE7O0FBS25CO0VBQ0ksYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixTQUFTO0VBQ1QsZUFBZTtFQUNmLFlBQVk7RUFDWixhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0ksYUFBYTtFQUNiLE9BQU87RUFDUCxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFFaEI7RUFDSSxhQUFZO0VBQ1osVUFBVTtFQUNWLFlBQVk7RUFDWixPQUFPO0VBQ1AsY0FBYyxFQUFBOztBQUxsQjtJQU9RLFdBQVcsRUFBQTs7QUFQbkI7SUFVUSxXQUFXLEVBQUE7O0FBR25CO0VBQ0ksY0FBYztFQUNkLFlBQVksRUFBQTs7QUFFaEI7RUFDSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksYUFBWTtFQUNaLE9BQU87RUFDUCxXQUFXO0VBQ1gsWUFBWTtFQUNaLGNBQWMsRUFBQTs7QUFFbEI7RUFDSSxPQUFPLEVBQUE7O0FBR1g7RUFDSSxPQUFPLEVBQUE7O0FBRVg7RUFDSSxlQUFlO0VBQ2YsU0FBUztFQUNULFVBQVUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbmZpcm1lZG9yZGVybGlzdC9jb25maXJtZWRvcmRlcmxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI21lbnVDYXJkLXRpdGxle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogZ3JleTtcclxuICAgIGNvbG9yOiB3aGl0ZXNtb2tlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuI21lbnVDYXJkLWRldGFpbHtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGJvcmRlci1ib3R0b206MXB4IHNvbGlkIGdyZXk7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuI2NvbmZpcm1lZElubmVyRGl2e1xyXG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcclxufVxyXG5cclxuaW9uLWlucHV0e1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYm9yZGVyOiAycHggc29saWQgZ3JleTtcclxuICAgIC8vbWF4LXdpZHRoOiA4MHB4O1xyXG4gICAgbWF4LWhlaWdodDogMjAwcHg7XHJcbiAgICBmbG9hdDogY2VudGVyO1xyXG5cclxufVxyXG5pb24tYnV0dG9ue1xyXG4gICAgd2lkdGg6IDE1MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBoZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuXHJcbi8vRkxFWFxyXG4jRmxleE92ZXJhbGx7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAxMDB2dztcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbiNGbGV4T3JkZXJDb250YWluZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleDogMTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4jRmxleE9yZGVyRmlyc3RCb3h7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZmxleDogMTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgaW9uLWNhcmR7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgICBpb24tcm93e1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG59XHJcbiNGbGV4T3JkZXJGaXJzdEJveERldGFpbHN7XHJcbiAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4jRmxleE9yZGVyVGl0bGV7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbn1cclxuI0ZsZXhPcmRlclNlY29uZEJveHtcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGZsZXg6IDE7XHJcbiAgICB3aWR0aDogNTB2dztcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcbiNGbGV4Rmlyc3RJbm5lclNlY29uZEJveHtcclxuICAgIGZsZXg6IDE7XHJcbiAgXHJcbn1cclxuI0ZsZXhTZWNvbmRJbm5lclNlY29uZEJveHtcclxuICAgIGZsZXg6IDE7XHJcbn1cclxuI0ZsZXhCaWxsT3V0Q2FyZHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIHdpZHRoOiA1MCU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.ts ***!
  \***************************************************************/
/*! exports provided: ConfirmedorderlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedorderlistPage", function() { return ConfirmedorderlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirmedorderlist.service */ "./src/app/confirmedorderlist/confirmedorderlist.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _customer_services_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../customer/services/cart.service */ "./src/app/customer/services/cart.service.ts");
/* harmony import */ var _cashier_service_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../cashier/service/api.service */ "./src/app/cashier/service/api.service.ts");
/* harmony import */ var _confirmedlistmodal_confirmedlistmodal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../confirmedlistmodal/confirmedlistmodal.page */ "./src/app/confirmedlistmodal/confirmedlistmodal.page.ts");








var ConfirmedorderlistPage = /** @class */ (function () {
    function ConfirmedorderlistPage(router, confirmedorderlistservice, cartservice, apiservice, modalController, toastCtlr) {
        this.router = router;
        this.confirmedorderlistservice = confirmedorderlistservice;
        this.cartservice = cartservice;
        this.apiservice = apiservice;
        this.modalController = modalController;
        this.toastCtlr = toastCtlr;
        this.confirm = "CONFIRM";
        this.orderArray = [];
        this.statusBtn = [];
        this.defaultStatus = "SERVE";
        this.quantity = [];
        this.dataIsLoaded = false;
        this.id = {};
        this.status = {};
        this.settotal = {};
        this.bill = {};
        this.total = 0;
        this.finaltotal = 0;
        this.orderID = {};
        this.served = {};
        this.isenabled = [false, false, false, false, false, false];
    }
    ConfirmedorderlistPage.prototype.ngOnInit = function () {
        var _this = this;
        this.router.paramMap.subscribe(function (params) {
            _this.orderid = Number(params.get('order_id'));
            console.log(_this.orderid);
        });
        this.apiservice.getTotal(this.orderid).then(function (data) {
            _this.orderTotal = (data);
        }).then(function () {
            _this.getPendingOrders();
            _this.getServedOrders();
            _this.orderID.order_id = _this.orderid;
        });
        console.log("ngOnInit");
    };
    ConfirmedorderlistPage.prototype.doRefresh = function () {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            console.log("doRefresh");
            //this.getDataAsAWhole();
            _this.getPendingOrders();
            _this.getServedOrders();
            //event.target.complete();
        }, 1500);
    };
    ConfirmedorderlistPage.prototype.getPendingOrders = function () {
        var _this = this;
        this.confirmedorderlistservice.getWaitingOrderByID(this.orderid).then(function (data) {
            _this.orderlist = data.orders;
        }).then(function () {
            //generate matrix for dropdown menu
            _this.pendingOrderMatrix = _this.genServiceMatrix(_this.orderlist);
        });
    };
    ConfirmedorderlistPage.prototype.getServedOrders = function () {
        var _this = this;
        this.confirmedorderlistservice.getServedOrderByID(this.orderid).then(function (data) {
            _this.servedOrderList = data.orders;
        });
    };
    ConfirmedorderlistPage.prototype.getMatrixEntry = function (index) {
        return this.servedItemMatrix[index];
    };
    ConfirmedorderlistPage.prototype.genServiceMatrix = function (data) {
        var retArray = [];
        for (var i = 0; i < data.length; i++) {
            var retArrayElement = [];
            for (var x = 1; x <= data[i].served; x++)
                retArrayElement.push(x);
            retArray.push(retArrayElement);
        }
        return retArray;
    };
    ConfirmedorderlistPage.prototype.getTotal = function () {
        var _this = this;
        this.apiservice.getTotal(this.orderid).then(function (data) {
            _this.orderTotal = (data);
        });
    };
    ConfirmedorderlistPage.prototype.removeserveItem = function (id) {
        this.confirmedorderlistservice.removeServeItem(id);
    };
    ConfirmedorderlistPage.prototype.changeOrderStatus = function (id) {
        this.confirmedorderlistservice.changeKitchenStatus(id);
        this.confirmedorderlistservice.changeOrderStatusToServed(id);
        console.log(id);
    };
    ConfirmedorderlistPage.prototype.presentModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _confirmedlistmodal_confirmedlistmodal_page__WEBPACK_IMPORTED_MODULE_7__["ConfirmedlistmodalPage"],
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ConfirmedorderlistPage.prototype.serve = function (qty, id) {
        console.log(qty);
        console.log(id);
        this.served.id = id;
        this.served.serveItem = qty;
        this.confirmedorderlistservice.setServedQty(this.served);
        this.confirmedorderlistservice.checkQty(id);
        this.doRefresh();
    };
    ConfirmedorderlistPage.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Please wait for the waiter for the bill.',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ConfirmedorderlistPage.prototype.billout = function () {
        this.bill.order_id = this.orderid;
        this.bill.status = 'billout';
        // this.bill.total = this.total;
        this.confirmedorderlistservice.billout(this.bill);
        this.presentModal();
        // this.presentToast();
    };
    ConfirmedorderlistPage.prototype.getBundlePrices = function () {
        var _this = this;
        this.confirmedorderlistservice.getBundlePrices().then(function (data) {
            _this.bundleprices = (data);
            // console.log('BUNDLEPRICES');
            // console.log(this.bundleprices);
        });
    };
    ConfirmedorderlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirmedorderlist',
            template: __webpack_require__(/*! ./confirmedorderlist.page.html */ "./src/app/confirmedorderlist/confirmedorderlist.page.html"),
            styles: [__webpack_require__(/*! ./confirmedorderlist.page.scss */ "./src/app/confirmedorderlist/confirmedorderlist.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmedorderlistService"],
            _customer_services_cart_service__WEBPACK_IMPORTED_MODULE_5__["CartService"],
            _cashier_service_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
    ], ConfirmedorderlistPage);
    return ConfirmedorderlistPage;
}());



/***/ })

}]);
//# sourceMappingURL=confirmedorderlist-confirmedorderlist-module.js.map