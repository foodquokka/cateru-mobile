(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rating-rating-module"],{

/***/ "./src/app/rating/rating.module.ts":
/*!*****************************************!*\
  !*** ./src/app/rating/rating.module.ts ***!
  \*****************************************/
/*! exports provided: RatingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingPageModule", function() { return RatingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rating_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rating.page */ "./src/app/rating/rating.page.ts");







var routes = [
    {
        path: '',
        component: _rating_page__WEBPACK_IMPORTED_MODULE_6__["RatingPage"]
    }
];
var RatingPageModule = /** @class */ (function () {
    function RatingPageModule() {
    }
    RatingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_rating_page__WEBPACK_IMPORTED_MODULE_6__["RatingPage"]]
        })
    ], RatingPageModule);
    return RatingPageModule;
}());



/***/ }),

/***/ "./src/app/rating/rating.page.html":
/*!*****************************************!*\
  !*** ./src/app/rating/rating.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>Rating</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div id=\"MainContainer\">\n    <div id=\"InnerContainer\">\n      <ion-icon ios=\"ios-star\" md=\"md-star\"></ion-icon>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/rating/rating.page.scss":
/*!*****************************************!*\
  !*** ./src/app/rating/rating.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JhdGluZy9yYXRpbmcucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/rating/rating.page.ts":
/*!***************************************!*\
  !*** ./src/app/rating/rating.page.ts ***!
  \***************************************/
/*! exports provided: RatingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingPage", function() { return RatingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RatingPage = /** @class */ (function () {
    function RatingPage() {
    }
    RatingPage.prototype.ngOnInit = function () {
    };
    RatingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rating',
            template: __webpack_require__(/*! ./rating.page.html */ "./src/app/rating/rating.page.html"),
            styles: [__webpack_require__(/*! ./rating.page.scss */ "./src/app/rating/rating.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RatingPage);
    return RatingPage;
}());



/***/ })

}]);
//# sourceMappingURL=rating-rating-module.js.map