(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["menu-promo-modal-promo-modal-module"],{

/***/ "./src/app/customer/menu/promo-modal/promo-modal.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/customer/menu/promo-modal/promo-modal.module.ts ***!
  \*****************************************************************/
/*! exports provided: PromoModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromoModalPageModule", function() { return PromoModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _promo_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./promo-modal.page */ "./src/app/customer/menu/promo-modal/promo-modal.page.ts");







var routes = [
    {
        path: '',
        component: _promo_modal_page__WEBPACK_IMPORTED_MODULE_6__["PromoModalPage"]
    }
];
var PromoModalPageModule = /** @class */ (function () {
    function PromoModalPageModule() {
    }
    PromoModalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_promo_modal_page__WEBPACK_IMPORTED_MODULE_6__["PromoModalPage"]]
        })
    ], PromoModalPageModule);
    return PromoModalPageModule;
}());



/***/ })

}]);
//# sourceMappingURL=menu-promo-modal-promo-modal-module.js.map