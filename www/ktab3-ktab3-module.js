(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ktab3-ktab3-module"],{

/***/ "./src/app/kitchen/ktab3/ktab3.module.ts":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab3/ktab3.module.ts ***!
  \***********************************************/
/*! exports provided: Ktab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ktab3PageModule", function() { return Ktab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ktab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ktab3.page */ "./src/app/kitchen/ktab3/ktab3.page.ts");







// const routes: Routes = [
//   {
//     path: '',
//     component: Ktab3Page
//   }
// ];
var Ktab3PageModule = /** @class */ (function () {
    function Ktab3PageModule() {
    }
    Ktab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _ktab3_page__WEBPACK_IMPORTED_MODULE_6__["Ktab3Page"] }])
            ],
            declarations: [_ktab3_page__WEBPACK_IMPORTED_MODULE_6__["Ktab3Page"]]
        })
    ], Ktab3PageModule);
    return Ktab3PageModule;
}());



/***/ }),

/***/ "./src/app/kitchen/ktab3/ktab3.page.html":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab3/ktab3.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Preparing\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-card class=\"card\">\r\n          <ion-card-content>\r\n            <ion-row>\r\n              <ion-col text-center>\r\n                <h1>MEALS</h1>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row text-center>\r\n              <ion-col>TIME</ion-col>\r\n              <ion-col>NAME</ion-col>\r\n              <ion-col>ACTION</ion-col>\r\n            </ion-row>\r\n            <ion-item  text-center *ngFor=\"let order of orders;let i = index\">\r\n              <ion-label>\r\n                {{order.updated_at  | date:'shortTime'}}\r\n              </ion-label>\r\n              <ion-label>\r\n                {{order.name}}\r\n              </ion-label>\r\n              <br>\r\n              <ion-label>\r\n                <ion-button (click)=\"finish(order.id)\" [disabled]=\"false\">FINISH</ion-button>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/kitchen/ktab3/ktab3.page.scss":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab3/ktab3.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2tpdGNoZW4va3RhYjMva3RhYjMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/kitchen/ktab3/ktab3.page.ts":
/*!*********************************************!*\
  !*** ./src/app/kitchen/ktab3/ktab3.page.ts ***!
  \*********************************************/
/*! exports provided: Ktab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ktab3Page", function() { return Ktab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/kitchen/rest.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var Ktab3Page = /** @class */ (function () {
    function Ktab3Page(restService, toastCtlr) {
        this.restService = restService;
        this.toastCtlr = toastCtlr;
    }
    Ktab3Page.prototype.ngOnInit = function () {
        this.getAllPrepareItems();
    };
    // ionViewWillEnter(){
    //   this.getAllPrepareItems();
    // }
    Ktab3Page.prototype.getAllPrepareItems = function () {
        var _this = this;
        this.restService.getAllPrepareItems().then(function (data) {
            _this.orders = data.orders;
            console.log(_this.orders);
        });
    };
    Ktab3Page.prototype.presentToastFinish = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Order finished!',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    Ktab3Page.prototype.finish = function (id) {
        this.restService.changeOrderStatusToFinish(id);
        this.presentToastFinish();
        this.doRefresh(id);
    };
    Ktab3Page.prototype.doRefresh = function (event) {
        var _this = this;
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getAllPrepareItems();
            event.target.complete();
        }, 1500);
    };
    Ktab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ktab3',
            template: __webpack_require__(/*! ./ktab3.page.html */ "./src/app/kitchen/ktab3/ktab3.page.html"),
            styles: [__webpack_require__(/*! ./ktab3.page.scss */ "./src/app/kitchen/ktab3/ktab3.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], Ktab3Page);
    return Ktab3Page;
}());



/***/ })

}]);
//# sourceMappingURL=ktab3-ktab3-module.js.map