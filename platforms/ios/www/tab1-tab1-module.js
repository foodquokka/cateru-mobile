(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./src/app/waiter/tab1/tab1.module.ts":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.module.ts ***!
  \********************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/waiter/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.html":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"dark\">\n        <ion-title>Table List</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    <div>\n        <ion-button color=\"primary\">Occupied</ion-button>\n        <ion-button color=\"secondary\">Available</ion-button>\n    </div>\n    <ion-grid>\n        <ion-row >  \n           \n            <ion-col col-12 *ngFor=\"let t of tables let i=index\" >\n           \n                <ion-card [color]=\"t.status == 'Occupied' ? 'primary' : 'secondary'\" fill=\"outline\" border-style=\"solid\" mode=\"ios\" text-center padding tappable (click)=\"presentmodal(t.tableno)\">\n                  \n                     <div class=\"badge\"><ion-badge color=\"secondary\">{{ counter }}</ion-badge></div>\n                    <h2 >TABLE NO: {{ t.tableno}}</h2>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n    \n</ion-content>\n<ion-footer>\n<ion-button button (click)=\"refreshpage()\" class=\"refreshbtn\">REFRESH</ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.scss":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n#status {\n  color: blue; }\n\n#tablecard {\n  width: 50%;\n  height: 20%; }\n\n.refreshbtn {\n  width: 100%; }\n\n.badge {\n  float: right;\n  font-size: 24px;\n  color: black; }\n\nion-badge {\n  font-size: 36px;\n  color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FpdGVyL3RhYjEvRDpcXFRoZXNpc1xcQ2F0ZXJVQXBwL3NyY1xcYXBwXFx3YWl0ZXJcXHRhYjFcXHRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLFdBQVcsRUFBQTs7QUFFYjtFQUNFLFVBQVU7RUFDVixXQUFXLEVBQUE7O0FBRWI7RUFDRSxXQUFXLEVBQUE7O0FBRWI7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLFlBQVksRUFBQTs7QUFFZDtFQUNFLGVBQWU7RUFDZixZQUNGLEVBQUEiLCJmaWxlIjoic3JjL2FwcC93YWl0ZXIvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4jc3RhdHVze1xuICBjb2xvcjogYmx1ZTtcbn1cbiN0YWJsZWNhcmR7XG4gIHdpZHRoOiA1MCU7XG4gIGhlaWdodDogMjAlO1xufVxuLnJlZnJlc2hidG57XG4gIHdpZHRoOiAxMDAlO1xufVxuLmJhZGdle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuaW9uLWJhZGdle1xuICBmb250LXNpemU6IDM2cHg7XG4gIGNvbG9yOiBibGFja1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.ts":
/*!******************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.ts ***!
  \******************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_waiter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/waiter.service */ "./src/app/waiter/service/waiter.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_confirmedorderlist_confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/confirmedorderlist/confirmedorderlist.service */ "./src/app/confirmedorderlist/confirmedorderlist.service.ts");
/* harmony import */ var src_app_services_socket_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var _waiterorderdetail_waiterorderdetail_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../waiterorderdetail/waiterorderdetail.page */ "./src/app/waiter/waiterorderdetail/waiterorderdetail.page.ts");








// import { Observable, Subscription } from 'rxjs';
// import { Socket } from 'ngx-socket-io';
// import * as io from 'socket.io-client';
var Tab1Page = /** @class */ (function () {
    function Tab1Page(waiterService, confirmedOrderList, router, alertCtrl, modalController, socket) {
        this.waiterService = waiterService;
        this.confirmedOrderList = confirmedOrderList;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.socket = socket;
        this.cardBackgroundColor = 'primary';
        this.cardColor = 'black';
        this.orderCard = [];
        this.flag = false;
        this.status = {};
        this.orders = [];
        this.list = [];
        // this.socket.on('drinklist', this.drinklist);
    }
    Tab1Page.prototype.ngOnInit = function () {
        this.tableStatus();
    };
    Tab1Page.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    Tab1Page.prototype.ionViewWillEnter = function () {
        this.getDrinksList();
        this.getAllTable();
    };
    Tab1Page.prototype.getAllTable = function () {
        var _this = this;
        this.waiterService.getAllTable().then(function (data) {
            _this.tables = data.allTables;
            console.log(_this.tables);
        });
    };
    Tab1Page.prototype.getDrinksList = function () {
        var _this = this;
        this.waiterService.getOrderDrinks().then(function (data) {
            _this.drinklist = data.result;
        });
    };
    // async showConfirm(orderid) {
    //   console.log(orderid);
    //   const confirm = this.alertCtrl.create({
    //     message: 'Prepare menu?',
    //     buttons: [
    //       {
    //         text: 'Prepare',
    //         handler: () => {
    //           console.log(this.cardBackgroundColor);
    //           for (const orders of this.drinklist) {
    //             if (orderid === orders.id) {
    //             this.status.id = orderid;
    //             this.status.status = 'preparing';
    //             }
    //           }
    //           this.confirmedOrderList.changeOrderStatus(this.status);
    //           console.log(this.status);
    //         }
    //       },
    //       {
    //         text: 'Ready',
    //         handler: () => {
    //           console.log(this.cardBackgroundColor);
    //           for (const orders of this.drinklist) {
    //             if (orderid === orders.id) {
    //             this.status.id = orderid;
    //             this.status.status = 'serving';
    //             }
    //           }
    //           this.confirmedOrderList.changeOrderStatus(this.status);
    //           console.log(this.status);
    //         }
    //       },
    //       {
    //         text: 'Cancel',
    //         handler: () => {
    //           this.flag = false;
    //           console.log('Agree clicked');
    //         }
    //       }
    //     ]
    //   });
    //   (await confirm).present();
    // }
    Tab1Page.prototype.tableStatus = function () {
        for (var _i = 0, _a = this.tables; _i < _a.length; _i++) {
            var i = _a[_i];
            if (i.status === 'Occupied') {
                this.color = 'primary';
            }
        }
    };
    Tab1Page.prototype.presentmodal = function (tableno) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _waiterorderdetail_waiterorderdetail_page__WEBPACK_IMPORTED_MODULE_7__["WaiterorderdetailPage"],
                            componentProps: { tableno: tableno },
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! ./tab1.page.html */ "./src/app/waiter/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/waiter/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_waiter_service__WEBPACK_IMPORTED_MODULE_2__["WaiterService"],
            src_app_confirmedorderlist_confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmedorderlistService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            src_app_services_socket_service__WEBPACK_IMPORTED_MODULE_6__["SocketService"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module.js.map