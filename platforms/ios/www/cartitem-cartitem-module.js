(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cartitem-cartitem-module"],{

/***/ "./src/app/cartitem/cartitem.module.ts":
/*!*********************************************!*\
  !*** ./src/app/cartitem/cartitem.module.ts ***!
  \*********************************************/
/*! exports provided: CartitemPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartitemPageModule", function() { return CartitemPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _cartitem_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cartitem.page */ "./src/app/cartitem/cartitem.page.ts");







var routes = [
    {
        path: '',
        component: _cartitem_page__WEBPACK_IMPORTED_MODULE_6__["CartitemPage"]
    }
];
var CartitemPageModule = /** @class */ (function () {
    function CartitemPageModule() {
    }
    CartitemPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_cartitem_page__WEBPACK_IMPORTED_MODULE_6__["CartitemPage"]]
        })
    ], CartitemPageModule);
    return CartitemPageModule;
}());



/***/ }),

/***/ "./src/app/cartitem/cartitem.page.html":
/*!*********************************************!*\
  !*** ./src/app/cartitem/cartitem.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>cartitem</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/cartitem/cartitem.page.scss":
/*!*********************************************!*\
  !*** ./src/app/cartitem/cartitem.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NhcnRpdGVtL2NhcnRpdGVtLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/cartitem/cartitem.page.ts":
/*!*******************************************!*\
  !*** ./src/app/cartitem/cartitem.page.ts ***!
  \*******************************************/
/*! exports provided: CartitemPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartitemPage", function() { return CartitemPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CartitemPage = /** @class */ (function () {
    function CartitemPage() {
    }
    CartitemPage.prototype.ngOnInit = function () {
    };
    CartitemPage.prototype.getCart = function () {
    };
    CartitemPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cartitem',
            template: __webpack_require__(/*! ./cartitem.page.html */ "./src/app/cartitem/cartitem.page.html"),
            styles: [__webpack_require__(/*! ./cartitem.page.scss */ "./src/app/cartitem/cartitem.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartitemPage);
    return CartitemPage;
}());



/***/ })

}]);
//# sourceMappingURL=cartitem-cartitem-module.js.map