(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-menu-billout-billout-module"],{

/***/ "./src/app/customer/classes/discount.ts":
/*!**********************************************!*\
  !*** ./src/app/customer/classes/discount.ts ***!
  \**********************************************/
/*! exports provided: Discount */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Discount", function() { return Discount; });
var Discount = /** @class */ (function () {
    function Discount() {
    }
    return Discount;
}());



/***/ }),

/***/ "./src/app/customer/menu/billout/billout.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/customer/menu/billout/billout.module.ts ***!
  \*********************************************************/
/*! exports provided: BilloutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BilloutPageModule", function() { return BilloutPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _billout_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./billout.page */ "./src/app/customer/menu/billout/billout.page.ts");







var routes = [
    {
        path: '',
        component: _billout_page__WEBPACK_IMPORTED_MODULE_6__["BilloutPage"]
    }
];
var BilloutPageModule = /** @class */ (function () {
    function BilloutPageModule() {
    }
    BilloutPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_billout_page__WEBPACK_IMPORTED_MODULE_6__["BilloutPage"]]
        })
    ], BilloutPageModule);
    return BilloutPageModule;
}());



/***/ }),

/***/ "./src/app/customer/menu/billout/billout.page.html":
/*!*********************************************************!*\
  !*** ./src/app/customer/menu/billout/billout.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button></ion-back-button>\n      </ion-buttons>\n      <ion-title>Bill Out</ion-title>\n      <!-- <ion-title>Order List</ion-title> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid>\n        <ion-card id=\"menuCard\"><ion-card-content>\n        <ion-row><ion-text id=\"guideComment\">Please select a discount type:</ion-text></ion-row>\n        <ion-row>\n            <ion-item>\n            <ion-label>Discount Type:</ion-label>\n            <ion-select [interfaceOptions]=\"customPopoverOptions\" interface=\"popover\" #c [(ngModel)]=\"c.value\" (ngModelChange)=\"onSelect(c.value)\">\n                <ion-select-option value=\"senior\">Senior</ion-select-option>\n                <ion-select-option value=\"none\">None</ion-select-option>\n            </ion-select>\n            </ion-item>\n        </ion-row>\n        <ion-row>\n            <ion-card class=\"card\" id=\"billoutCard\"> \n                <ion-row><ion-col>\n                    <div>\n                        <ion-row>\n                            <ion-col size=\"3\" size-md>QUANTITY</ion-col>\n                            <ion-col size=\"3\" size-md>NAME</ion-col>\n                            <ion-col text-end size=\"6\" size-md>SUBTOTAL</ion-col>\n                        </ion-row>\n                    </div> \n                    <div>\n                        <ion-row *ngFor=\"let item of servedList\">\n                            <ion-col size=\"3\" size-md>{{ item.orderQty }}</ion-col>\n                            <ion-col size=\"3\" size-md>{{ item.menuName }}</ion-col>\n                            <ion-col text-end size=\"6\" size-md>{{ item.subtotal }}</ion-col>\n                        </ion-row>\n                    </div>\n                </ion-col></ion-row>\n            </ion-card>\n        </ion-row>\n            <!-- TOTAL DETAILS -->\n        <ion-row>\n            <ion-card id=\"totalDetails\" class=\"ion-float-right card\"> \n                <ion-row>\n                    <ion-col><ion-text>Total:</ion-text></ion-col>\n                    <ion-col text-end><ion-text>{{ total }}</ion-text></ion-col>\n                </ion-row>\n                <div *ngIf=\"discounted\">\n                    <ion-row>\n                        <ion-col><ion-text>Discount:</ion-text></ion-col>\n                        <ion-col text-end><ion-text>{{ discountValue }}</ion-text></ion-col>\n                    </ion-row>\n                    <ion-row>\n                        <ion-col><ion-text>Discounted Total:</ion-text></ion-col>\n                        <ion-col text-end><ion-text>{{ discountedTotal }}</ion-text></ion-col>\n                    </ion-row>\n                </div>\n            </ion-card>\n        </ion-row>\n        <ion-row>\n            <ion-text id=\"guideComment\" class=\"right\">Please review your payment details before pressing the Confirm button</ion-text>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button color=\"dark\" expand=\"block\" (click)=\"billoutConfirm()\">Confirm</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-card-content></ion-card>\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/customer/menu/billout/billout.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/customer/menu/billout/billout.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#billoutCard {\n  width: 100%; }\n\n#totalDetails {\n  width: 50%;\n  margin: auto;\n  margin-right: 2%; }\n\n.right {\n  margin: auto;\n  margin-right: 2%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvbWVudS9iaWxsb3V0L0Q6XFxUaGVzaXNcXENhdGVyVUFwcC9zcmNcXGFwcFxcY3VzdG9tZXJcXG1lbnVcXGJpbGxvdXRcXGJpbGxvdXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksVUFBVTtFQUNWLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTs7QUFFcEI7RUFDSSxZQUFZO0VBQ1osZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jdXN0b21lci9tZW51L2JpbGxvdXQvYmlsbG91dC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYmlsbG91dENhcmQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbiN0b3RhbERldGFpbHMge1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogMiU7XHJcbn1cclxuLnJpZ2h0IHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogMiU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/customer/menu/billout/billout.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/customer/menu/billout/billout.page.ts ***!
  \*******************************************************/
/*! exports provided: BilloutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BilloutPage", function() { return BilloutPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _classes_discount__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../classes/discount */ "./src/app/customer/classes/discount.ts");




var BilloutPage = /** @class */ (function () {
    function BilloutPage(adminService) {
        this.adminService = adminService;
        this.total = 0;
    }
    BilloutPage.prototype.ngOnInit = function () {
        this.order_id = this.adminService.getOrderID();
        this.getList();
    };
    BilloutPage.prototype.getList = function () {
        var _this = this;
        this.adminService.getServedOL(this.order_id).then(function (list) {
            console.log(list);
            _this.servedList = list.list;
            // getting total
            console.log('SERVED LIST: ', _this.servedList[0]);
            for (var _i = 0, _a = _this.servedList; _i < _a.length; _i++) {
                var item = _a[_i];
                console.log('subtotal: ', item.subtotal);
                _this.total = _this.total + item.subtotal;
            }
            console.log(_this.total);
        });
    };
    BilloutPage.prototype.onSelect = function (value) {
        var _this = this;
        console.log('SELECTED VALUE: ', value);
        if (value === 'senior') {
            this.discounted = true;
        }
        else {
            this.discounted = false;
        }
        var billoutdata = new _classes_discount__WEBPACK_IMPORTED_MODULE_3__["Discount"]();
        billoutdata.discountType = value;
        this.adminService.sendDiscountType(billoutdata);
        this.orderPromise = this.adminService.getOrder(this.order_id);
        this.orderPromise.then(function (servedlist) {
            _this.orderData = servedlist.order;
            console.log('ORDERDATA', _this.orderData);
            _this.discountValue = _this.orderData.discount;
            _this.discountedTotal = _this.orderData.discountedTotal;
            console.log('DISCOUNT', _this.discountValue, 'DISCOUNTED', _this.discountedTotal);
        });
    };
    BilloutPage.prototype.billoutConfirm = function () {
        this.adminService.pay(this.orderData);
    };
    BilloutPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-billout',
            template: __webpack_require__(/*! ./billout.page.html */ "./src/app/customer/menu/billout/billout.page.html"),
            styles: [__webpack_require__(/*! ./billout.page.scss */ "./src/app/customer/menu/billout/billout.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_admin_service__WEBPACK_IMPORTED_MODULE_2__["AdminService"]])
    ], BilloutPage);
    return BilloutPage;
}());



/***/ })

}]);
//# sourceMappingURL=customer-menu-billout-billout-module.js.map