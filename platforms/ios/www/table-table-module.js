(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["table-table-module"],{

/***/ "./src/app/table/table.module.ts":
/*!***************************************!*\
  !*** ./src/app/table/table.module.ts ***!
  \***************************************/
/*! exports provided: TablePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablePageModule", function() { return TablePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _table_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./table.page */ "./src/app/table/table.page.ts");







var routes = [
    {
        path: '',
        component: _table_page__WEBPACK_IMPORTED_MODULE_6__["TablePage"]
    }
];
var TablePageModule = /** @class */ (function () {
    function TablePageModule() {
    }
    TablePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_table_page__WEBPACK_IMPORTED_MODULE_6__["TablePage"]]
        })
    ], TablePageModule);
    return TablePageModule;
}());



/***/ }),

/***/ "./src/app/table/table.page.html":
/*!***************************************!*\
  !*** ./src/app/table/table.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title> TABLE LIST </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content id=\"content\">\n<ion-grid> \n<ion-row *ngFor=\"let table of tables\">\n  <ion-col>\n    <ion-card (click)= \"goToCustomerTable(table.order_id)\">\n      <ion-card-content>\n         <h1>TABLE NO : {{ table.tableno}}</h1>\n      </ion-card-content>\n    </ion-card>\n  </ion-col>\n</ion-row>\n</ion-grid>\n</ion-content>\n -->\n\n\n <ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-title>Table List</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div>\n      <ion-button color=\"primary\">Occupied</ion-button>\n      <ion-button color=\"secondary\">Available</ion-button>\n  </div>\n  <ion-grid>\n      <ion-row >\n          <ion-col col-12 *ngFor=\"let t of tablelist let i=index\" >\n             \n              <ion-card [color]=\"t.status == 'Occupied' ? 'primary' : 'secondary'\" fill=\"outline\" border-style=\"solid\" mode=\"ios\" text-center padding tappable (click)=\"openMenu(t.tableno)\">\n                  <h2 >{{ t.tableno}}</h2>\n              </ion-card>\n          </ion-col>\n      </ion-row>\n  </ion-grid>\n  \n</ion-content>\n<ion-footer>\n<ion-button button (click)=\"refreshpage()\" class=\"refreshbtn\">REFRESH</ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/table/table.page.scss":
/*!***************************************!*\
  !*** ./src/app/table/table.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#content {\n  --background: linear-gradient(#7f1a1f, #3f0f11 );\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.outer {\n  display: table;\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.middle {\n  display: table-cell;\n  vertical-align: middle; }\n\n.inner {\n  margin-left: auto;\n  margin-right: auto;\n  width: 50%;\n  height: 200px;\n  padding: 20px 0;\n  border-style: solid; }\n\n#cateru {\n  color: #ffffff;\n  font-size: 110px;\n  font-family: billionthine; }\n\n.label {\n  color: #ffffff;\n  font-size: 130%; }\n\n.inputBox {\n  --background: #d3d3d3;\n  width: 50%;\n  border-radius: 5px;\n  margin-left: 24.5%;\n  margin-top: 15px;\n  color: #000000; }\n\n.startBtn {\n  --background: #8d272c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGFibGUvRDpcXFRoZXNpc1xcQ2F0ZXJVQXBwL3NyY1xcYXBwXFx0YWJsZVxcdGFibGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0ksZ0RBQWE7RUFDYiw0QkFBNEI7RUFDNUIsc0JBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksY0FBYztFQUNkLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBR2Y7RUFDSSxtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ksaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsYUFBYTtFQUNiLGVBQWU7RUFDZixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLHlCQUF5QixFQUFBOztBQUU3QjtFQUNJLGNBQWM7RUFDZCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0kscUJBQWE7RUFDYixVQUFVO0VBQ1Ysa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUVsQjtFQUNJLHFCQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC90YWJsZS90YWJsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGVudCB7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZC5qcGcnKTtcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vaW1nL2dldHN0YXJ0LnBuZycpO1xyXG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzdmMWExZiwgIzNmMGYxMSApO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTpjb3ZlcjtcclxufVxyXG5cclxuLm91dGVyIHtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWlkZGxlIHtcclxuICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG4uaW5uZXIge1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgaGVpZ2h0OiAyMDBweDtcclxuICAgIHBhZGRpbmc6IDIwcHggMDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbn1cclxuXHJcbiNjYXRlcnUge1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBmb250LXNpemU6IDExMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IGJpbGxpb250aGluZTtcclxufVxyXG4ubGFiZWwge1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBmb250LXNpemU6IDEzMCU7XHJcbn1cclxuLmlucHV0Qm94IHtcclxuICAgIC0tYmFja2dyb3VuZDogI2QzZDNkMztcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjQuNSU7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbn1cclxuLnN0YXJ0QnRuIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzhkMjcyYztcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/table/table.page.ts":
/*!*************************************!*\
  !*** ./src/app/table/table.page.ts ***!
  \*************************************/
/*! exports provided: TablePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablePage", function() { return TablePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_device_table_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/device-table.service */ "./src/app/services/device-table.service.ts");
/* harmony import */ var _ionic_native_uid_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/uid/ngx */ "./node_modules/@ionic-native/uid/ngx/index.js");
/* harmony import */ var _services_storageservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/storageservice.service */ "./src/app/services/storageservice.service.ts");
/* harmony import */ var _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../customer/services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../qrlogin/qrlogin.service */ "./src/app/qrlogin/qrlogin.service.ts");









var TablePage = /** @class */ (function () {
    function TablePage(deviceTableService, auth, route, uid, storageservice, adminservice, devicetable, qrService, routerr) {
        this.deviceTableService = deviceTableService;
        this.auth = auth;
        this.route = route;
        this.uid = uid;
        this.storageservice = storageservice;
        this.adminservice = adminservice;
        this.devicetable = devicetable;
        this.qrService = qrService;
        this.routerr = routerr;
        this.Table = {};
        this.table = {};
        this.encodedData = '';
        this.isOn = false;
        //UUID = {} as Device;
        //num = {} as TableNo;
        this.tableStatus = {};
        this.deviceinfo = {};
        this.isShow = false;
        this.isShowOccupiedTable = false;
    }
    TablePage.prototype.ngOnInit = function () {
        var _this = this;
        this.routerr.paramMap.subscribe(function (params) {
            _this.empid = Number(params.get('empid'));
            console.log(_this.empid);
        });
    };
    TablePage.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    TablePage.prototype.ionViewWillEnter = function () {
        this.getTableList();
    };
    TablePage.prototype.closedevice = function () {
        this.auth.logout();
        this.route.navigate(['qrlogin']);
    };
    TablePage.prototype.getTableList = function () {
        var _this = this;
        this.deviceTableService.getTableList().then(function (data) {
            _this.tablelist = data.allTables;
            console.log(_this.tablelist);
        });
    };
    TablePage.prototype.setTableNo = function () {
        this.tableno = this.selectedTable;
        console.log(this.tableno);
        this.deviceTableService.setTableNo(this.tableno);
    };
    TablePage.prototype.setDeviceUID = function () {
        this.Table.deviceuid = this.uid.UUID;
        this.deviceTableService.setDeviceID(this.Table);
    };
    TablePage.prototype.getTableNo = function () {
        var uid = this.uid.UUID;
        this.deviceTableService.getdevicetableno(uid);
        console.log('hello');
    };
    // getOrderByTableNo(tableno){
    //   this.qrService.getOrderByTableNo(tableno).then(data => { 
    //     this.orders = (data as any).details;
    //   });
    //   console.log(tableno)
    // }
    TablePage.prototype.openMenu = function (tableno) {
        var _this = this;
        console.log(tableno);
        this.qrService.getOrderByTableNo(tableno).then(function (data) {
            _this.orders = data.orders;
            console.log(_this.orders);
            if (_this.orders != '') {
                for (var _i = 0, _a = _this.orders; _i < _a.length; _i++) {
                    var i = _a[_i];
                    console.log(i.order_id);
                    _this.goToCustomerTable(i.order_id);
                }
            }
            else {
                _this.saveTransactionInfo(tableno);
                // this.ngZone.run(() => {
                //   console.log(route, this);
                _this.route.navigate(['/menu']);
            }
        });
    };
    TablePage.prototype.goToCustomerTable = function (order_id) {
        console.log('HELLO');
        // this.setDeviceUID();
        this.storageservice.saveOrderID(order_id);
        this.route.navigate(['confirmedorderlist', order_id]);
        console.log(order_id);
        // console.log('hello');
    };
    TablePage.prototype.saveTransactionInfo = function (tableno) {
        var _this = this;
        this.deviceinfo.custid = this.random();
        this.deviceinfo.empid = this.empid;
        this.deviceinfo.date_ordered = Date.now();
        this.deviceinfo.tableno = tableno;
        console.log(this.deviceinfo);
        this.adminservice.postToOrder(this.deviceinfo).then(function (data) {
            _this.storageservice.saveOrderID(data.order_id);
            console.log(_this.deviceinfo);
            //   console.log(this.storageService.saveOrderID((data as any).order_id);
        });
        //   console.log(this.no);
        this.setTableOccupied(tableno);
        //   this.route.navigate(['/menu']);
        //});
    };
    // getAllAvailableTableNo(){
    //   this.devicetable.getAllAvailableTable().then(data => {
    //     this.tables = (data as any).AvailableTables;
    //     console.log(this.tables);
    //   });
    // }
    //getTableNo(){
    //   return this.selectedTable;
    // }
    //setTableNo(selectedTable){
    //   this.selectedTable = selectedTable;
    //  // this.startScan();
    //   // console.log(this.selectedTable);
    // }
    TablePage.prototype.setTableOccupied = function (tableno) {
        console.log(tableno);
        this.tableStatus.tableno = tableno;
        this.tableStatus.status = 'Occupied';
        this.qrService.setTableStatus(this.tableStatus);
        console.log('set table status');
    };
    TablePage.prototype.successLogin = function (data) {
        this.qrService.saveOrdersData(this.orderInfo);
    };
    TablePage.prototype.random = function () {
        var rand = Math.floor(Math.random() * 20) + 1;
        return rand;
    };
    TablePage.prototype.setCustId = function (id) {
        this.custid = id;
    };
    TablePage.prototype.getCustId = function () {
        console.log(this.custid);
    };
    TablePage.prototype.getOccupiedTable = function () {
        var _this = this;
        this.qrService.getOccupiedTable().then(function (data) {
            _this.occupiedtables = data.OccupiedTables;
        });
    };
    TablePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-table',
            template: __webpack_require__(/*! ./table.page.html */ "./src/app/table/table.page.html"),
            styles: [__webpack_require__(/*! ./table.page.scss */ "./src/app/table/table.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_device_table_service__WEBPACK_IMPORTED_MODULE_4__["DeviceTableService"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _ionic_native_uid_ngx__WEBPACK_IMPORTED_MODULE_5__["Uid"],
            _services_storageservice_service__WEBPACK_IMPORTED_MODULE_6__["StorageserviceService"],
            _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_7__["AdminService"],
            _services_device_table_service__WEBPACK_IMPORTED_MODULE_4__["DeviceTableService"],
            _qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_8__["QrloginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], TablePage);
    return TablePage;
}());



/***/ })

}]);
//# sourceMappingURL=table-table-module.js.map