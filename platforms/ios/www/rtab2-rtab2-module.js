(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab2-rtab2-module"],{

/***/ "./src/app/receptionist/rtab2/rtab2.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.module.ts ***!
  \****************************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab2.page */ "./src/app/receptionist/rtab2/rtab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _rtab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_rtab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Queue List\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card-content>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Queue Number</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Party Size</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Customer Name</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Phone Number</ion-list>\r\n      </ion-col>\r\n\r\n    </ion-row>\r\n  </ion-card-content>\r\n\r\n  <ion-row *ngFor=\" let queue of array\">\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.key }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.size }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.name }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.no }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n   \r\n  </ion-row>\r\n\r\n  <div margin-vertical text-right>\r\n    <ion-button (click)=\"removeQueue()\">Remove Queue</ion-button>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY2VwdGlvbmlzdC9ydGFiMi9ydGFiMi5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.ts ***!
  \**************************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");



var Tab2Page = /** @class */ (function () {
    function Tab2Page(serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.array = [];
        this.tables();
        this.queueTable();
        // this.removeQueue();
    }
    Tab2Page.prototype.ngOnInit = function () { };
    Tab2Page.prototype.tables = function () {
        var _this = this;
        this.serviceProvider.getTables().then(function (data) {
            _this.tab = data;
            _this.tab = _this.tab.allTables;
            // console.log(this.tab);
        });
    };
    Tab2Page.prototype.queueTable = function () {
        this.array = this.serviceProvider.getQueue();
        console.log(this.array);
    };
    Tab2Page.prototype.removeQueue = function () {
        var counter = 0;
        for (var _i = 0, _a = this.array; _i < _a.length; _i++) {
            var i = _a[_i];
            for (var _b = 0, _c = this.tab; _b < _c.length; _b++) {
                var t = _c[_b];
                if (t.status === 'Available' && t.capacity === i.size) {
                    console.log('Message sent to: ' + i.no);
                    this.array.splice(counter, 1);
                    // console.log(this.array[counter]);
                }
            }
            counter++;
        }
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! ./rtab2.page.html */ "./src/app/receptionist/rtab2/rtab2.page.html"),
            styles: [__webpack_require__(/*! ./rtab2.page.scss */ "./src/app/receptionist/rtab2/rtab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab2-rtab2-module.js.map