(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bestpairmodal-bestpairmodal-module"],{

/***/ "./src/app/bestpairmodal/bestpairmodal.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/bestpairmodal/bestpairmodal.module.ts ***!
  \*******************************************************/
/*! exports provided: BestpairmodalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BestpairmodalPageModule", function() { return BestpairmodalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _bestpairmodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bestpairmodal.page */ "./src/app/bestpairmodal/bestpairmodal.page.ts");







var routes = [
    {
        path: '',
        component: _bestpairmodal_page__WEBPACK_IMPORTED_MODULE_6__["BestpairmodalPage"]
    }
];
var BestpairmodalPageModule = /** @class */ (function () {
    function BestpairmodalPageModule() {
    }
    BestpairmodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_bestpairmodal_page__WEBPACK_IMPORTED_MODULE_6__["BestpairmodalPage"]]
        })
    ], BestpairmodalPageModule);
    return BestpairmodalPageModule;
}());



/***/ }),

/***/ "./src/app/bestpairmodal/bestpairmodal.page.html":
/*!*******************************************************!*\
  !*** ./src/app/bestpairmodal/bestpairmodal.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>bestpairmodal</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/bestpairmodal/bestpairmodal.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/bestpairmodal/bestpairmodal.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Jlc3RwYWlybW9kYWwvYmVzdHBhaXJtb2RhbC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/bestpairmodal/bestpairmodal.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/bestpairmodal/bestpairmodal.page.ts ***!
  \*****************************************************/
/*! exports provided: BestpairmodalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BestpairmodalPage", function() { return BestpairmodalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BestpairmodalPage = /** @class */ (function () {
    function BestpairmodalPage() {
    }
    BestpairmodalPage.prototype.ngOnInit = function () {
    };
    BestpairmodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-bestpairmodal',
            template: __webpack_require__(/*! ./bestpairmodal.page.html */ "./src/app/bestpairmodal/bestpairmodal.page.html"),
            styles: [__webpack_require__(/*! ./bestpairmodal.page.scss */ "./src/app/bestpairmodal/bestpairmodal.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BestpairmodalPage);
    return BestpairmodalPage;
}());



/***/ })

}]);
//# sourceMappingURL=bestpairmodal-bestpairmodal-module.js.map