(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab2-rtab2-module"],{

/***/ "./src/app/receptionist/rtab2/rtab2.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.module.ts ***!
  \****************************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab2.page */ "./src/app/receptionist/rtab2/rtab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _rtab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_rtab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Queue List\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content text-center>\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content pullingIcon=\"arrow-dropdown\" pullingText=\"Pull to refresh\" refreshingSpinner=\"circles\"\r\n      refreshingText=\"Refreshing...\">\r\n    </ion-refresher-content>\r\n  </ion-refresher>\r\n\r\n  <!-- <ion-button (click)=\"sendSMS()\">SEND SMS</ion-button> -->\r\n  <ion-card-content text-center>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">\r\n          <ion-item>PRIORITY NUMBER </ion-item>\r\n        </ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">\r\n          <ion-item>PARTY SIZE</ion-item>\r\n        </ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">\r\n          <ion-item>CUSTOMER NAME</ion-item>\r\n        </ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">\r\n          <ion-item>PHONE NUMBER</ion-item>\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-card-content>\r\n  <ion-row text-center *ngFor=\" let queue of list, let i = index\">\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          <ion-radio-group [(ngModel)]=\"customerid\" (click)=\"getCustId(queue.custid)\">\r\n            <ion-radio slot=\"start\" value=\"{{ queue.custid}}\" name=\"customerid\"> </ion-radio>\r\n          </ion-radio-group>\r\n          {{ queue.custid }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.partysize }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.name }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>\r\n          {{ queue.phonenumber }}\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n  </ion-row>\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-col>\r\n    <ion-list>\r\n      <ion-item>\r\n        <ion-select placeholder=\"Select One\" interface=\"popover\" class=\"selector\" [(ngModel)]=\"selectedAvailableTable\">\r\n          <ion-select-option *ngFor=\"let table of AvailableTables\" class=\"option\" value=\"{{ table.tableno}}\"\r\n            selected=\"selectedAvailableTable\">Table No. {{ table.tableno}} <p> | SEATS: {{ table.capacity}}</p></ion-select-option>\r\n        </ion-select>\r\n        <ion-button color=\"success\" (click)=\"sendSMS()\">SEND SMS</ion-button>\r\n        <ion-button (click)=\"setStatusCancelled()\" color=\"danger\"> REMOVE</ion-button>\r\n      </ion-item>\r\n    </ion-list>\r\n  </ion-col>\r\n</ion-footer>"

/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".selector {\n  font-size: 16px;\n  line-height: 1.2;\n  letter-spacing: 0.1px;\n  color: #29235e;\n  width: 70% !important;\n  max-width: 100% !important;\n  justify-content: center;\n  background-color: #ffffff;\n  text-align: center; }\n  .selector p {\n    font-size: 5px;\n    font-style: italic;\n    color: grey; }\n  .option {\n  font-size: 8px;\n  line-height: 1.2;\n  letter-spacing: 0.1px;\n  color: #29235e;\n  width: 50% !important;\n  max-width: 50% !important;\n  justify-content: center;\n  height: 20px; }\n  ion-option .list {\n  font-size: 8px !important;\n  max-width: 100% !important; }\n  .ion-option-items .item {\n  font-size: 8px !important;\n  max-width: 100% !important; }\n  .select-popover {\n  font-size: 8px !important;\n  max-width: 50% !important; }\n  ion-footer ion-button {\n  width: 100px !important;\n  height: 60px !important; }\n  ion-card-content ion-item {\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWIyL0Q6XFxTY2hvb2xcXGNhdGVydS1tb2JpbGUvc3JjXFxhcHBcXHJlY2VwdGlvbmlzdFxccnRhYjJcXHJ0YWIyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUVJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxxQkFBcUI7RUFDckIsMEJBQTBCO0VBQzFCLHVCQUF1QjtFQUV2Qix5QkFBeUI7RUFDekIsa0JBQWtCLEVBQUE7RUFYdEI7SUFhTSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLFdBQVcsRUFBQTtFQUdmO0VBRUUsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixxQkFBcUI7RUFDckIsY0FBYztFQUNkLHFCQUFxQjtFQUNyQix5QkFBeUI7RUFDekIsdUJBQXVCO0VBQ3ZCLFlBQVksRUFBQTtFQUlkO0VBRUUseUJBQXlCO0VBQ3pCLDBCQUEwQixFQUFBO0VBRTVCO0VBRUUseUJBQXlCO0VBQ3pCLDBCQUEwQixFQUFBO0VBRTdCO0VBRUcseUJBQXlCO0VBQ3pCLHlCQUF5QixFQUFBO0VBRTNCO0VBRUksdUJBQXVCO0VBQ3ZCLHVCQUF1QixFQUFBO0VBRzNCO0VBRUksaUJBQWlCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9yZWNlcHRpb25pc3QvcnRhYjIvcnRhYjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5zZWxlY3RvciB7XHJcbiAgICAvL2ZvbnQtZmFtaWx5OiBTRk5TRGlzcGxheTtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMC4xcHg7XHJcbiAgICBjb2xvcjogIzI5MjM1ZTtcclxuICAgIHdpZHRoOiA3MCUgIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAvL2JvcmRlcjogc29saWQgMnB4ICNlOGViZjA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIHB7XHJcbiAgICAgIGZvbnQtc2l6ZTogNXB4O1xyXG4gICAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICAgIGNvbG9yOiBncmV5O1xyXG4gICAgfVxyXG4gIH1cclxuICAub3B0aW9uIHtcclxuICAgLy8gZm9udC1mYW1pbHk6IFNGTlNEaXNwbGF5O1xyXG4gICAgZm9udC1zaXplOiA4cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMS4yO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMXB4O1xyXG4gICAgY29sb3I6ICMyOTIzNWU7XHJcbiAgICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgIFxyXG4gIH1cclxuXHJcbiAgaW9uLW9wdGlvbiAubGlzdHtcclxuICAvLyAgZm9udC1mYW1pbHk6IFNGTlNEaXNwbGF5O1xyXG4gICAgZm9udC1zaXplOiA4cHggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuaW9uLW9wdGlvbi1pdGVtcyAuaXRlbXtcclxuICAgLy8gZm9udC1mYW1pbHk6IFNGTlNEaXNwbGF5O1xyXG4gICAgZm9udC1zaXplOiA4cHggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gIH1cclxuIC5zZWxlY3QtcG9wb3ZlciB7XHJcbiAgIC8vIGZvbnQtZmFtaWx5OiBTRk5TRGlzcGxheSAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiA4cHggIWltcG9ydGFudDtcclxuICAgIG1heC13aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIGlvbi1mb290ZXJ7XHJcbiAgICBpb24tYnV0dG9ue1xyXG4gICAgICB3aWR0aDogMTAwcHggIWltcG9ydGFudDtcclxuICAgICAgaGVpZ2h0OiA2MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGlvbi1jYXJkLWNvbnRlbnR7XHJcbiAgICBpb24taXRlbXtcclxuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcbiAgfVxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtab2/rtab2.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab2/rtab2.page.ts ***!
  \**************************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/qrlogin/qrlogin.service */ "./src/app/qrlogin/qrlogin.service.ts");





var Tab2Page = /** @class */ (function () {
    function Tab2Page(serviceProvider, toastCtrl, qrLoginService) {
        this.serviceProvider = serviceProvider;
        this.toastCtrl = toastCtrl;
        this.qrLoginService = qrLoginService;
        //phoneNumber = '639479845480';
        this.SMS = {};
        this.array = [];
        this.tables();
        this.queueTable();
        this.getAvailableTable();
    }
    Tab2Page.prototype.ngOnInit = function () { };
    Tab2Page.prototype.ionViewWillEnter = function () {
        this.queueTable();
    };
    Tab2Page.prototype.tables = function () {
        var _this = this;
        this.serviceProvider.getTables().then(function (data) {
            _this.tab = data;
            _this.tab = _this.tab.allTables;
            // console.log(this.tab);
        });
    };
    Tab2Page.prototype.getAvailableTable = function () {
        var _this = this;
        this.serviceProvider.getAvailableTable().then(function (data) {
            _this.AvailableTables = data.AvailableTables;
            console.log(_this.AvailableTables);
        });
    };
    Tab2Page.prototype.onChange = function () {
        // console.log(index);
        // this.selectedAvailableTable = index;
        console.log(this.selectedAvailableTable);
    };
    Tab2Page.prototype.queueTable = function () {
        var _this = this;
        this.serviceProvider.reservedcustomerslist().then(function (data) {
            _this.list = data.reservedcustomers;
            console.log(_this.list);
        });
    };
    Tab2Page.prototype.removeQueue = function () {
        var counter = 0;
        for (var _i = 0, _a = this.list; _i < _a.length; _i++) {
            var customer = _a[_i];
            for (var _b = 0, _c = this.AvailableTables; _b < _c.length; _b++) {
                var table = _c[_b];
                if (table.capacity >= customer.partysize) {
                    // console.log('Message sent to: ' + i.no);
                    this.array.splice(counter, 1);
                    // console.log(this.array[counter]);
                }
            }
            counter++;
        }
    };
    Tab2Page.prototype.getCustId = function (custid) {
        //  console.log(this.customerid); 
        console.log(custid);
        this.id = custid;
        return this.id;
    };
    Tab2Page.prototype.sendSMS = function () {
        var _this = this;
        var dataArray = Array();
        var info;
        console.log(this.selectedAvailableTable);
        this.serviceProvider.sendSMS(this.id).then(function () {
            info = {
                custid: _this.id,
                tableno: parseInt(_this.selectedAvailableTable)
            };
            dataArray = info;
            console.log(dataArray);
            _this.serviceProvider.assignTable(dataArray);
            _this.presentToast();
        }).then(function () {
            _this.setTableReserved();
        });
        this.doRefresh();
    };
    Tab2Page.prototype.doRefresh = function () {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.queueTable();
            _this.getAvailableTable();
            //   event.target.complete();
        }, 1500);
    };
    Tab2Page.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: 'Message Sent!',
                            position: 'middle',
                            animated: true,
                            duration: 1500,
                            cssClass: "my-custom-class"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab2Page.prototype.setStatusCancelled = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.serviceProvider.statusCancelled(this.id).then(function () {
                    setTimeout(function () {
                        console.log('Async operation has ended');
                        _this.queueTable();
                        _this.id.target.complete();
                    }, 1500);
                });
                return [2 /*return*/];
            });
        });
    };
    Tab2Page.prototype.setTableReserved = function () {
        var dataArray = Array();
        var info;
        info = {
            tableno: this.selectedAvailableTable,
            status: 'Reserved'
        };
        dataArray = info;
        this.qrLoginService.setTableStatus(dataArray);
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! ./rtab2.page.html */ "./src/app/receptionist/rtab2/rtab2.page.html"),
            styles: [__webpack_require__(/*! ./rtab2.page.scss */ "./src/app/receptionist/rtab2/rtab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            src_app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_4__["QrloginService"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab2-rtab2-module.js.map