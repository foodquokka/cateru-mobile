(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["serving-serving-module"],{

/***/ "./src/app/waiter/serving/serving.module.ts":
/*!**************************************************!*\
  !*** ./src/app/waiter/serving/serving.module.ts ***!
  \**************************************************/
/*! exports provided: ServingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServingPageModule", function() { return ServingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _serving_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./serving.page */ "./src/app/waiter/serving/serving.page.ts");







var routes = [
    {
        path: '',
        component: _serving_page__WEBPACK_IMPORTED_MODULE_6__["ServingPage"]
    }
];
var ServingPageModule = /** @class */ (function () {
    function ServingPageModule() {
    }
    ServingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_serving_page__WEBPACK_IMPORTED_MODULE_6__["ServingPage"]]
        })
    ], ServingPageModule);
    return ServingPageModule;
}());



/***/ }),

/***/ "./src/app/waiter/serving/serving.page.html":
/*!**************************************************!*\
  !*** ./src/app/waiter/serving/serving.page.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-button slot=\"start\" color=\"secondary\"> \n    <ion-icon slot=\"icon-only\"   name=\"log-out\">LOGOUT</ion-icon> \n    </ion-button>\n        <ion-toolbar color=\"dark\">\n            <ion-title text-center>Serving List</ion-title>\n        </ion-toolbar>\n    </ion-header>\n    \n    <ion-content scrollY=\"false\">\n        <ion-grid>\n            <ion-row>\n                <ion-col size=\"3\" *ngFor=\"let card of serving\">\n                        <ion-card-content>\n                            <ion-row>\n                                <ion-text id = \"status\" >STATUS: {{ card.status }}</ion-text>\n                            </ion-row>\n                            <ion-text id=\"tableNum\">Table No. {{ card.tableno }}</ion-text>\n                            <ion-row>\n                                <div>    \n                                    <ion-col><ion-text>• {{ card.name }} -</ion-text></ion-col>\n                                    <ion-col text-end><ion-text>{{ card.quantity }}</ion-text></ion-col>\n                                </div>\n                            </ion-row>\n                        </ion-card-content>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n        <ion-button (click)=\"refreshpage()\">REFRESH</ion-button>\n    </ion-content>\n"

/***/ }),

/***/ "./src/app/waiter/serving/serving.page.scss":
/*!**************************************************!*\
  !*** ./src/app/waiter/serving/serving.page.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dhaXRlci9zZXJ2aW5nL3NlcnZpbmcucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/waiter/serving/serving.page.ts":
/*!************************************************!*\
  !*** ./src/app/waiter/serving/serving.page.ts ***!
  \************************************************/
/*! exports provided: ServingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServingPage", function() { return ServingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_waiter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/waiter.service */ "./src/app/waiter/service/waiter.service.ts");



var ServingPage = /** @class */ (function () {
    function ServingPage(waiterservice) {
        this.waiterservice = waiterservice;
    }
    ServingPage.prototype.ngOnInit = function () {
    };
    ServingPage.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    ServingPage.prototype.ionViewWillEnter = function () {
        this.getServingMenus();
    };
    ServingPage.prototype.getServingMenus = function () {
        var _this = this;
        this.waiterservice.getAllServingMenus().then(function (data) {
            _this.serving = data.result;
            console.log(_this.serving);
        });
    };
    ServingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-serving',
            template: __webpack_require__(/*! ./serving.page.html */ "./src/app/waiter/serving/serving.page.html"),
            styles: [__webpack_require__(/*! ./serving.page.scss */ "./src/app/waiter/serving/serving.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_waiter_service__WEBPACK_IMPORTED_MODULE_2__["WaiterService"]])
    ], ServingPage);
    return ServingPage;
}());



/***/ })

}]);
//# sourceMappingURL=serving-serving-module.js.map