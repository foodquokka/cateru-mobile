(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab3-rtab3-module"],{

/***/ "./src/app/receptionist/rtab3/rtab3.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.module.ts ***!
  \****************************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab3.page */ "./src/app/receptionist/rtab3/rtab3.page.ts");







var Tab3PageModule = /** @class */ (function () {
    function Tab3PageModule() {
    }
    Tab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _rtab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }])
            ],
            declarations: [_rtab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
        })
    ], Tab3PageModule);
    return Tab3PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Table List\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content text-center>\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content pullingIcon=\"arrow-dropdown\" pullingText=\"Pull to refresh\" refreshingSpinner=\"circles\"\r\n      refreshingText=\"Refreshing...\">\r\n    </ion-refresher-content>\r\n  </ion-refresher>\r\n <ion-card-content text-center >\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">TABLE NUMBER</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">SEATS</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">STATUS</ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-row *ngFor=\"let table of tables\" >\r\n      <ion-col>\r\n        <ion-list>\r\n       {{ table.tableno }}\r\n        </ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list>\r\n          {{ table.capacity }}\r\n        </ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list >\r\n      {{ table.status }}\r\n        </ion-list>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n  </ion-card-content>    \r\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-row {\n  border: none !important;\n  padding: none !important; }\n\n.header .col {\n  background-color: lightgrey; }\n\nion-col {\n  border: solid 1px grey;\n  border-right-style: none; }\n\nion-col:last-child {\n  border-right: solid 1px grey; }\n\nion-row:last-child .col {\n  border-bottom: solid 1px grey; }\n\nion-row:nth-child(even) ion-col, ion-row:nth-child(even) ion-list {\n  background-color: lightgrey; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWIzL0Q6XFxTY2hvb2xcXGNhdGVydS1tb2JpbGUvc3JjXFxhcHBcXHJlY2VwdGlvbmlzdFxccnRhYjNcXHJ0YWIzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLHVCQUF1QjtFQUN2Qix3QkFBd0IsRUFBQTs7QUFFNUI7RUFDSSwyQkFBMEIsRUFBQTs7QUFHNUI7RUFDRSxzQkFBc0I7RUFDdEIsd0JBQXdCLEVBQUE7O0FBRzFCO0VBQ0UsNEJBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsNkJBQTZCLEVBQUE7O0FBRS9CO0VBRUssMkJBQTRCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9yZWNlcHRpb25pc3QvcnRhYjMvcnRhYjMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXJvd3tcclxuICBcclxuICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5oZWFkZXIgLmNvbCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOmxpZ2h0Z3JleTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLWNvbCB7XHJcbiAgICBib3JkZXI6IHNvbGlkIDFweCBncmV5O1xyXG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiBub25lO1xyXG4gIH1cclxuICBcclxuICBpb24tY29sOmxhc3QtY2hpbGQge1xyXG4gICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCAxcHggZ3JleTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLXJvdzpsYXN0LWNoaWxkIC5jb2wge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4IGdyZXk7XHJcbiAgfVxyXG4gIGlvbi1yb3c6bnRoLWNoaWxkKGV2ZW4pe1xyXG4gICAgaW9uLWNvbCwgaW9uLWxpc3R7XHJcbiAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAgbGlnaHRncmV5O1xyXG4gICAgfVxyXG4gICBcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.ts ***!
  \**************************************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");



var Tab3Page = /** @class */ (function () {
    // constructor(){}
    function Tab3Page(servicesProvider) {
        this.servicesProvider = servicesProvider;
        this.getTables();
    }
    Tab3Page.prototype.getTables = function () {
        var _this = this;
        this.servicesProvider.getTables()
            .then(function (data) {
            _this.tables = data;
            _this.tables = _this.tables.allTables;
            console.log(_this.tables);
        });
    };
    Tab3Page.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getTables();
            event.target.complete();
        }, 1500);
    };
    Tab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab3',
            template: __webpack_require__(/*! ./rtab3.page.html */ "./src/app/receptionist/rtab3/rtab3.page.html"),
            styles: [__webpack_require__(/*! ./rtab3.page.scss */ "./src/app/receptionist/rtab3/rtab3.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab3Page);
    return Tab3Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab3-rtab3-module.js.map