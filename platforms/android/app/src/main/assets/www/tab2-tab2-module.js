(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"],{

/***/ "./src/app/waiter/tab2/tab2.module.ts":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.module.ts ***!
  \********************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab2.page */ "./src/app/waiter/tab2/tab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.html":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"primary\">\n      <ion-title>\n        Preparing\n      </ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-card class=\"card\">\n            <ion-card-content>\n              <ion-row>\n                <ion-col text-center>\n                  <h1>DRINKS</h1>\n                </ion-col>\n              </ion-row>\n              <ion-row text-center>\n                <ion-col>TIME</ion-col>\n                <ion-col>NAME</ion-col>\n                <ion-col>ACTION</ion-col>\n              </ion-row>\n              <ion-item text-center *ngFor=\"let order of drinks;let i = index\">\n                <ion-label>\n                  {{order.updated_at  | date:'shortTime'}}\n                </ion-label>\n                <ion-label>\n                  {{order.name}}\n                </ion-label>\n                <br>\n                <ion-label>\n                  <ion-button (click)=\"finish(order.id)\" [disabled]=\"false\">FINISH</ion-button>\n                </ion-label>\n              </ion-item>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-content>"

/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.scss":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  height: 50px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FpdGVyL3RhYjIvRDpcXFNjaG9vbFxcY2F0ZXJ1LW1vYmlsZS9zcmNcXGFwcFxcd2FpdGVyXFx0YWIyXFx0YWIyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvd2FpdGVyL3RhYjIvdGFiMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xyXG4gIGhlaWdodDogNTBweCAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.ts":
/*!******************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.ts ***!
  \******************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/kitchen/rest.service */ "./src/app/kitchen/rest.service.ts");




var Tab2Page = /** @class */ (function () {
    function Tab2Page(restService, toastCtlr) {
        this.restService = restService;
        this.toastCtlr = toastCtlr;
    }
    Tab2Page.prototype.ngOnInit = function () {
        this.getPrepareDrinks();
    };
    // ionViewWillEnter(){
    //   this.getPrepareDrinks();
    // }
    Tab2Page.prototype.getPrepareDrinks = function () {
        var _this = this;
        this.restService.getPrepareDrinks().then(function (data) {
            _this.drinks = data.orders;
            console.log(_this.drinks);
        });
    };
    Tab2Page.prototype.presentToastFinish = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Order finished!',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab2Page.prototype.finish = function (id) {
        this.restService.changeOrderStatusToFinish(id);
        this.presentToastFinish();
        this.doRefresh(id);
    };
    Tab2Page.prototype.doRefresh = function (event) {
        var _this = this;
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getPrepareDrinks();
            event.target.complete();
        }, 1500);
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! ./tab2.page.html */ "./src/app/waiter/tab2/tab2.page.html"),
            styles: [__webpack_require__(/*! ./tab2.page.scss */ "./src/app/waiter/tab2/tab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_3__["RestService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab2-tab2-module.js.map