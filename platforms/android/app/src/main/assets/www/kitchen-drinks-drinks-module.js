(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["kitchen-drinks-drinks-module"],{

/***/ "./src/app/kitchen/drinks/drinks.module.ts":
/*!*************************************************!*\
  !*** ./src/app/kitchen/drinks/drinks.module.ts ***!
  \*************************************************/
/*! exports provided: DrinksPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrinksPageModule", function() { return DrinksPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _drinks_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./drinks.page */ "./src/app/kitchen/drinks/drinks.page.ts");







var routes = [
    {
        path: '',
        component: _drinks_page__WEBPACK_IMPORTED_MODULE_6__["DrinksPage"]
    }
];
var DrinksPageModule = /** @class */ (function () {
    function DrinksPageModule() {
    }
    DrinksPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_drinks_page__WEBPACK_IMPORTED_MODULE_6__["DrinksPage"]]
        })
    ], DrinksPageModule);
    return DrinksPageModule;
}());



/***/ }),

/***/ "./src/app/kitchen/drinks/drinks.page.html":
/*!*************************************************!*\
  !*** ./src/app/kitchen/drinks/drinks.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>drinks</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/kitchen/drinks/drinks.page.scss":
/*!*************************************************!*\
  !*** ./src/app/kitchen/drinks/drinks.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2tpdGNoZW4vZHJpbmtzL2RyaW5rcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/kitchen/drinks/drinks.page.ts":
/*!***********************************************!*\
  !*** ./src/app/kitchen/drinks/drinks.page.ts ***!
  \***********************************************/
/*! exports provided: DrinksPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrinksPage", function() { return DrinksPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DrinksPage = /** @class */ (function () {
    function DrinksPage() {
    }
    DrinksPage.prototype.ngOnInit = function () {
    };
    DrinksPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-drinks',
            template: __webpack_require__(/*! ./drinks.page.html */ "./src/app/kitchen/drinks/drinks.page.html"),
            styles: [__webpack_require__(/*! ./drinks.page.scss */ "./src/app/kitchen/drinks/drinks.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DrinksPage);
    return DrinksPage;
}());



/***/ })

}]);
//# sourceMappingURL=kitchen-drinks-drinks-module.js.map