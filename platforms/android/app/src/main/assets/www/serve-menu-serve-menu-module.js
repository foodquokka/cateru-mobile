(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["serve-menu-serve-menu-module"],{

/***/ "./src/app/serve-menu/serve-menu.module.ts":
/*!*************************************************!*\
  !*** ./src/app/serve-menu/serve-menu.module.ts ***!
  \*************************************************/
/*! exports provided: ServeMenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServeMenuPageModule", function() { return ServeMenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _serve_menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./serve-menu.page */ "./src/app/serve-menu/serve-menu.page.ts");







var routes = [
    {
        path: '',
        component: _serve_menu_page__WEBPACK_IMPORTED_MODULE_6__["ServeMenuPage"]
    }
];
var ServeMenuPageModule = /** @class */ (function () {
    function ServeMenuPageModule() {
    }
    ServeMenuPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_serve_menu_page__WEBPACK_IMPORTED_MODULE_6__["ServeMenuPage"]]
        })
    ], ServeMenuPageModule);
    return ServeMenuPageModule;
}());



/***/ }),

/***/ "./src/app/serve-menu/serve-menu.page.html":
/*!*************************************************!*\
  !*** ./src/app/serve-menu/serve-menu.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>serve-menu</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/serve-menu/serve-menu.page.scss":
/*!*************************************************!*\
  !*** ./src/app/serve-menu/serve-menu.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlcnZlLW1lbnUvc2VydmUtbWVudS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/serve-menu/serve-menu.page.ts":
/*!***********************************************!*\
  !*** ./src/app/serve-menu/serve-menu.page.ts ***!
  \***********************************************/
/*! exports provided: ServeMenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServeMenuPage", function() { return ServeMenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ServeMenuPage = /** @class */ (function () {
    function ServeMenuPage() {
    }
    ServeMenuPage.prototype.ngOnInit = function () {
    };
    ServeMenuPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-serve-menu',
            template: __webpack_require__(/*! ./serve-menu.page.html */ "./src/app/serve-menu/serve-menu.page.html"),
            styles: [__webpack_require__(/*! ./serve-menu.page.scss */ "./src/app/serve-menu/serve-menu.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ServeMenuPage);
    return ServeMenuPage;
}());



/***/ })

}]);
//# sourceMappingURL=serve-menu-serve-menu-module.js.map