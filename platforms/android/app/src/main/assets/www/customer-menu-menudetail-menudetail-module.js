(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-menu-menudetail-menudetail-module"],{

/***/ "./src/app/customer/menu/menudetail/menudetail.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/menudetail/menudetail.module.ts ***!
  \***************************************************************/
/*! exports provided: MenudetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenudetailPageModule", function() { return MenudetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menudetail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menudetail.page */ "./src/app/customer/menu/menudetail/menudetail.page.ts");







var routes = [
    {
        path: '',
        component: _menudetail_page__WEBPACK_IMPORTED_MODULE_6__["MenudetailPage"]
    }
];
var MenudetailPageModule = /** @class */ (function () {
    function MenudetailPageModule() {
    }
    MenudetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_menudetail_page__WEBPACK_IMPORTED_MODULE_6__["MenudetailPage"]]
        })
    ], MenudetailPageModule);
    return MenudetailPageModule;
}());



/***/ }),

/***/ "./src/app/customer/menu/menudetail/menudetail.page.html":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/menudetail/menudetail.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header> \r\n  <ion-toolbar color=\"dark\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>Menu Detail</ion-title>\r\n           <ion-button (click)=\"openCart()\">\r\n      <div class=\"cart-length\"></div>\r\n      <div class=\"cart-length\">{{ cartItemCount.value }}</div>\r\n      <ion-icon name=\"cart\" ></ion-icon>\r\n    </ion-button>\r\n  </ion-toolbar>\r\n    \r\n</ion-header>\r\n<ion-content scrollY=\"true\">\r\n   \r\n  <ion-card id=\"menuCard\" class=\"darkCard\"><ion-card-content>\r\n      <ion-grid>\r\n          <ion-row>\r\n              <ion-col id=\"imageContainer\"*ngFor=\"let menu of menuDetails\">\r\n             \r\n             \r\n               <ion-thumbnail><ion-img src=\"data:image/jpeg;base64,{{ menu.images }}\" alt='Select Image' ></ion-img>  </ion-thumbnail> \r\n              \r\n                  <ion-row><ion-text class=\"menudetail menudetailname bold\" name >{{ menu.name }}</ion-text></ion-row>\r\n                  <ion-row><ion-text class=\"menudetail menudetaildescrip italicized\">{{ menu.details }}</ion-text></ion-row>\r\n                  <ion-row><ion-text class=\"menudetail menudetailprice\">₱{{ menu.price }}</ion-text></ion-row>\r\n                  <ion-row><ion-text class=\"menudetail menudetailsize\">Serving Size: {{ menu.serving_size }} persons</ion-text></ion-row>\r\n                  <ion-row text-center>\r\n                    <ion-col>\r\n                      <ion-text class=\"menudetail menudetailsize\">Quantity:</ion-text>\r\n                      <ion-text class=\"qtyVal\">{{ qty }}</ion-text>\r\n                      <ion-button class=\"qtyBtn\" color=\"dark\" (click)=\"minusQty()\"><ion-icon name=\"remove\"></ion-icon></ion-button>  \r\n                      <ion-button class=\"qtyBtn\" color=\"dark\" (click)=\"addQty()\"><ion-icon name=\"add\"></ion-icon></ion-button>\r\n                    </ion-col>\r\n                  </ion-row>\r\n              \r\n              </ion-col> \r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col><ion-text>Recommended:</ion-text></ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col>\r\n                  <ion-card class=\"card\"><ion-card-content>\r\n                      <ion-row>\r\n                          <ion-col text-center>\r\n                            <ion-icon class=\"recommendedImage\" name=\"image\"></ion-icon>\r\n                          </ion-col>\r\n                          <ion-col text-center>\r\n                            <ion-icon class=\"recommendedImage\" name=\"image\"></ion-icon>\r\n                          </ion-col>\r\n                          <ion-col text-center>\r\n                            <ion-icon class=\"recommendedImage\" name=\"image\"></ion-icon>\r\n                          </ion-col>\r\n                          <ion-col text-center>\r\n                            <ion-icon class=\"recommendedImage\" name=\"image\"></ion-icon>\r\n                          </ion-col>\r\n                      </ion-row>\r\n                  </ion-card-content></ion-card>\r\n              </ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n              <ion-col text-center>\r\n                  <ion-button text-center color=\"secondary\" (click)=\"addToCart(menuDetails)\">Add to Order</ion-button>\r\n              </ion-col>\r\n          </ion-row>\r\n      </ion-grid>\r\n  </ion-card-content></ion-card>\r\n\r\n  <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\" >\r\n    <ion-fab-button color=\"dark\"><ion-icon name=\"arrow-dropup\"></ion-icon></ion-fab-button>\r\n    <ion-fab-list side=\"top\">\r\n        <ion-fab-button color=\"dark\" routerLink=\"/orderlist\"><ion-icon name=\"list-box\"></ion-icon></ion-fab-button>\r\n        <ion-label>Order List</ion-label>\r\n        <ion-fab-button color=\"dark\" (click)=\"callWaiterAlert()\"><ion-icon name=\"call\"></ion-icon></ion-fab-button>\r\n        <ion-label>Call a Waiter</ion-label>\r\n        <ion-fab-button color=\"dark\" routerLink=\"/menu\"><ion-icon name=\"list-box\"></ion-icon></ion-fab-button>\r\n        <ion-label>Menu</ion-label>\r\n    </ion-fab-list>\r\n  </ion-fab>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/customer/menu/menudetail/menudetail.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/menudetail/menudetail.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".qtyBtn {\n  height: 30px;\n  width: 45px;\n  padding-bottom: 7px; }\n\n.qtyVal {\n  color: #000000;\n  font-weight: bold;\n  background-color: #e4e4e4;\n  padding-right: 80px;\n  padding-top: 5px;\n  padding-bottom: 5px;\n  border-radius: 5px;\n  border: solid;\n  border-color: #ac3838;\n  border-width: 2px; }\n\n.recommendedImage {\n  height: 75px;\n  width: 75px; }\n\n#imageContainer {\n  border: solid;\n  border-color: rgba(173, 173, 173, 0.096);\n  border-width: 2px; }\n\n.menudetailimage {\n  margin-left: 150%;\n  width: 150px;\n  height: auto; }\n\n.menudetailname {\n  font-family: 'linlibertine';\n  font-size: 30px; }\n\n.menudetaildescrip {\n  font-size: 17px; }\n\n.menudetailprice {\n  font-size: 17px; }\n\n.menudetailsize {\n  font-size: 17px; }\n\nion-text {\n  color: #d8d8d8; }\n\nion-fab-button {\n  height: 70px;\n  width: 70px; }\n\n.cart-icon {\n  font-size: 50px; }\n\n.cart-length {\n  color: var(--ion-color-primary);\n  position: absolute;\n  top: 18px;\n  left: 25px;\n  font-weight: 600;\n  font-size: 1em;\n  min-width: 25px;\n  z-index: 10; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvbWVudS9tZW51ZGV0YWlsL0Q6XFxUaGVzaXNcXENhdGVyVUFwcC9zcmNcXGFwcFxcY3VzdG9tZXJcXG1lbnVcXG1lbnVkZXRhaWxcXG1lbnVkZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUV6QixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHFCQUE4QjtFQUM5QixpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdmO0VBQ0ksYUFBYTtFQUNiLHdDQUF3QztFQUN4QyxpQkFBaUIsRUFBQTs7QUFHckI7RUFDSSxpQkFBaUI7RUFDakIsWUFBWTtFQUNaLFlBQVksRUFBQTs7QUFFaEI7RUFDSSwyQkFBMkI7RUFDM0IsZUFBZSxFQUFBOztBQUVuQjtFQUNJLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLGNBQWMsRUFBQTs7QUFFbEI7RUFDSSxZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdiO0VBQ0UsZUFBZSxFQUFBOztBQUdqQjtFQUNFLCtCQUErQjtFQUMvQixrQkFBa0I7RUFDbEIsU0FBUztFQUNULFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZixXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jdXN0b21lci9tZW51L21lbnVkZXRhaWwvbWVudWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLnF0eUJ0biB7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICB3aWR0aDogNDVweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiA3cHg7XHJcbn1cclxuXHJcbi5xdHlWYWwge1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNlNGU0ZTQ7XHJcbiAgICAvLyBwYWRkaW5nLWxlZnQ6IDgwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA4MHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBib3JkZXI6IHNvbGlkO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMTcyLCA1NiwgNTYpO1xyXG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XHJcbn1cclxuXHJcbi5yZWNvbW1lbmRlZEltYWdlIHtcclxuICAgIGhlaWdodDogNzVweDtcclxuICAgIHdpZHRoOiA3NXB4O1xyXG59XHJcblxyXG4jaW1hZ2VDb250YWluZXIge1xyXG4gICAgYm9yZGVyOiBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiYSgxNzMsIDE3MywgMTczLCAwLjA5Nik7XHJcbiAgICBib3JkZXItd2lkdGg6IDJweDtcclxufVxyXG5cclxuLm1lbnVkZXRhaWxpbWFnZSB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTUwJTtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIGhlaWdodDogYXV0bztcclxufVxyXG4ubWVudWRldGFpbG5hbWUge1xyXG4gICAgZm9udC1mYW1pbHk6ICdsaW5saWJlcnRpbmUnO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG59XHJcbi5tZW51ZGV0YWlsZGVzY3JpcHtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxufVxyXG4ubWVudWRldGFpbHByaWNle1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG59XHJcbi5tZW51ZGV0YWlsc2l6ZSB7XHJcbiAgICBmb250LXNpemU6IDE3cHg7XHJcbn1cclxuXHJcbmlvbi10ZXh0IHtcclxuICAgIGNvbG9yOiAjZDhkOGQ4O1xyXG59XHJcbmlvbi1mYWItYnV0dG9uIHtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIHdpZHRoOiA3MHB4O1xyXG4gIH1cclxuICAgXHJcbiAgLmNhcnQtaWNvbiB7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgfVxyXG4gICBcclxuICAuY2FydC1sZW5ndGgge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMThweDtcclxuICAgIGxlZnQ6IDI1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgZm9udC1zaXplOiAxZW07XHJcbiAgICBtaW4td2lkdGg6IDI1cHg7XHJcbiAgICB6LWluZGV4OiAxMDtcclxuICB9XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/customer/menu/menudetail/menudetail.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/customer/menu/menudetail/menudetail.page.ts ***!
  \*************************************************************/
/*! exports provided: MenudetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenudetailPage", function() { return MenudetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/customer/services/cart.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_services_device_table_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/device-table.service */ "./src/app/services/device-table.service.ts");
/* harmony import */ var src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storageservice.service */ "./src/app/services/storageservice.service.ts");









var MenudetailPage = /** @class */ (function () {
    // @ViewChild('cart', { static: false, read: ElementRef})fab: ElementRef;
    function MenudetailPage(adminService, cartService, alertController, route, router, storageService, deviceTableService) {
        this.adminService = adminService;
        this.cartService = cartService;
        this.alertController = alertController;
        this.route = route;
        this.router = router;
        this.storageService = storageService;
        this.deviceTableService = deviceTableService;
        this.qty = 1;
        this.tableNo = 1;
        this.Orderdetail = {};
        this.orderList = [];
        this.cartOrder = [];
        this.menus = [];
        this.cartItemCount = new rxjs__WEBPACK_IMPORTED_MODULE_6__["BehaviorSubject"](0);
    }
    MenudetailPage.prototype.ngOnInit = function () {
        var _this = this;
        // get ID from url
        this.route.paramMap.subscribe(function (params) {
            _this.selectedMenuID = Number(params.get('menuID'));
        });
        console.log(this.selectedMenuID);
        this.adminService.getMenuDetail(this.selectedMenuID).then(function (data) {
            _this.menuDetails = data.menudetail;
            console.log(_this.menuDetails);
        });
        this.sendSelectedMenuID();
        this.storageService.currentData.subscribe(function (items) { return _this.items = items; });
        this.cartItemCount = this.getCartItemCount();
    };
    MenudetailPage.prototype.callWaiterAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: null,
                            subHeader: null,
                            message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MenudetailPage.prototype.minusQty = function () {
        if (this.qty > 1) {
            this.qty -= 1;
        }
    };
    MenudetailPage.prototype.getCartItemCount = function () {
        console.log('GET CART ITEM COUNT');
        return this.cartItemCount;
    };
    MenudetailPage.prototype.addQty = function () {
        this.qty += 1;
    };
    MenudetailPage.prototype.addToCart = function () {
        var _this = this;
        this.storageService.getOrderID().then(function (data) {
            _this.order_id = (data);
            console.log(_this.menuDetails);
            for (var _i = 0, _a = _this.menuDetails; _i < _a.length; _i++) {
                var menu = _a[_i];
                var itemsToBeSent = {
                    menuID: _this.selectedMenuID,
                    orderID: _this.order_id,
                    name: menu.name,
                    qty: _this.qty,
                    image: menu.images,
                    price: menu.price
                };
                if (_this.items === '') {
                    var currentData = new Array(itemsToBeSent);
                    _this.storageService.changeData(JSON.stringify(currentData));
                    _this.cartItemCount.next(_this.cartItemCount.value + 1);
                    console.log('option 1', currentData);
                    _this.cartItemCount.next(_this.cartItemCount.value + 1);
                    console.log(_this.cartItemCount.value);
                }
                else {
                    var currentData = JSON.parse(_this.items);
                    for (var _b = 0, currentData_1 = currentData; _b < currentData_1.length; _b++) {
                        var item = currentData_1[_b];
                        if (item.menuID === _this.selectedMenuID) {
                            _this.flag = 1;
                            break;
                        }
                    }
                    if (_this.flag !== 1) {
                        currentData.push(itemsToBeSent);
                        _this.storageService.changeData(JSON.stringify(currentData));
                        console.log('option 2', currentData);
                        _this.cartItemCount.next(_this.cartItemCount.value + 1);
                    }
                }
            }
        });
    };
    MenudetailPage.prototype.openCart = function () {
        var _this = this;
        this.storageService.getOrderID().then(function (data) {
            _this.orderID = data;
        });
        //this.router.navigate(['orderlist']);
        console.log(this.orderID);
    };
    MenudetailPage.prototype.sendSelectedMenuID = function () {
        this.adminService.setSelectedMenuID(this.selectedMenuID);
    };
    MenudetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-menudetail',
            template: __webpack_require__(/*! ./menudetail.page.html */ "./src/app/customer/menu/menudetail/menudetail.page.html"),
            styles: [__webpack_require__(/*! ./menudetail.page.scss */ "./src/app/customer/menu/menudetail/menudetail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_admin_service__WEBPACK_IMPORTED_MODULE_4__["AdminService"],
            _services_cart_service__WEBPACK_IMPORTED_MODULE_5__["CartService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_8__["StorageserviceService"],
            src_app_services_device_table_service__WEBPACK_IMPORTED_MODULE_7__["DeviceTableService"]])
    ], MenudetailPage);
    return MenudetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=customer-menu-menudetail-menudetail-module.js.map