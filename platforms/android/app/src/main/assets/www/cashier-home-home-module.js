(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cashier-home-home-module"],{

/***/ "./src/app/cashier/home/home.module.ts":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/cashier/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/cashier/home/home.page.html":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <h5 padding>Cashier: {{fname}}</h5>\r\n    <ion-title>\r\n      <h3>CaterU</h3>\r\n    </ion-title>\r\n    <!-- <h3 padding style=\"float: right;\">{{ today | date:'shortTime'}}</h3> -->\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\r\n    <ion-refresher-content pullingIcon=\"arrow-dropdown\" pullingText=\"Pull to refresh\" refreshingSpinner=\"circles\"\r\n      refreshingText=\"Refreshing...\">\r\n    </ion-refresher-content>\r\n  </ion-refresher>\r\n  <div class=\"cashiercontainer\" text-center padding>\r\n    <div class=\"tablebox\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col col-12 *ngFor=\"let a of tables\">\r\n            <!-- <ion-card (click)=\"getOrders(a.tableno)\" mode=\"ios\">\r\n              <!-- <ion-icon name=\"alert\" slot=\"start\" color=\"danger\"></ion-icon> -->\r\n              <!--<h1> Table No: {{ a.tableno }} </h1>\r\n            </ion-card> -->\r\n            <ion-card (click)=\"setTableNo(a.tableno)\" mode=\"ios\">\r\n              <h1> Table No: {{ a.tableno }} </h1>\r\n            </ion-card>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </div>\r\n\r\n    <div class=\"billdetailbox\">\r\n      <ion-card mode=\"ios\">\r\n        <ion-item>\r\n          <ion-card-header>\r\n            <ion-card-title>TABLE NO. {{ tNo }}</ion-card-title>\r\n            <ion-card-subtitle>Cust ID: {{ custID }}</ion-card-subtitle>\r\n            <ion-card-subtitle>Order ID: {{ order_id }}</ion-card-subtitle>\r\n            <!-- <div id=\"cancelBtnDiv\">\r\n              <ion-button (click)=\"isClicked()\"> CANCEL ORDER</ion-button>\r\n            </div> -->\r\n          </ion-card-header>\r\n        </ion-item>\r\n\r\n        <ion-grid>\r\n          <ion-row style=\"background-color: #edebeb;color: white;font-weight: bold;\">\r\n            <ion-col>\r\n              <ion-card-header>\r\n                Menu Name\r\n              </ion-card-header>\r\n            </ion-col>\r\n\r\n            <ion-col>\r\n              <ion-card-header>\r\n                Quantity\r\n              </ion-card-header>\r\n            </ion-col>\r\n\r\n            <ion-col>\r\n              <ion-card-header>\r\n                Price\r\n              </ion-card-header>\r\n            </ion-col>\r\n\r\n            <ion-col>\r\n              <ion-card-header>\r\n                Subtotal\r\n              </ion-card-header>\r\n            </ion-col>\r\n\r\n          </ion-row>\r\n\r\n          <div *ngIf=\"dataIsLoaded == true\">\r\n            <ion-card text-center *ngFor=\"let values of displayBilloutOrders.nonBundle\" text-center padding (click)=\"isClicked(values.id)\">\r\n              <span text-right [hidden]=\"remove\" (click)=\"cancellItem(values.id)\"><ion-icon name=\"close\"></ion-icon></span>\r\n              <ion-row>\r\n                <ion-col>\r\n                  <ion-card-content>\r\n                    {{ values.name }}\r\n                  </ion-card-content>\r\n                </ion-col>\r\n\r\n                <ion-col>\r\n                  <ion-card-content>\r\n                    {{ values.orderNum }}\r\n                  </ion-card-content>\r\n                </ion-col>\r\n\r\n                <ion-col>\r\n                  <ion-card-content>\r\n                    {{ values.price }}\r\n                  </ion-card-content>\r\n                </ion-col>\r\n\r\n                <ion-col>\r\n                  <ion-card-content>\r\n                    {{ values.subtotal }}\r\n                  </ion-card-content>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-card>\r\n            <ion-card>\r\n              <div *ngIf=\"displayBilloutOrders != ''\">\r\n              <div *ngFor=\"let bundle of displayBilloutOrders.bundle\">\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <ion-card-content>\r\n                      {{ bundle.name}}\r\n                    </ion-card-content>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <ion-card-content>\r\n                      {{ bundle.qty}}\r\n                    </ion-card-content>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <ion-card-content>\r\n                      {{ bundle.price }}\r\n                    </ion-card-content>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <ion-card-content>\r\n                      {{ bundle.subtotal }}\r\n                    </ion-card-content>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </div></div>\r\n            </ion-card>\r\n          </div>\r\n          <ion-card>\r\n            <div text-left padding>\r\n              <h5>TOTAL: {{ total }}</h5>  \r\n              <h5>CASH: {{ amount }}</h5>  \r\n              <h5>CHANGE: {{  finalTotal }}</h5>\r\n              <h1>\r\n                <ion-input text-center style=\"background-color:grey;height: 70px; width: 100%;float:right;color: white;\"\r\n                  type=\"number\" [(ngModel)]=\"amount\" placeholder=\"Enter amount\"></ion-input>\r\n              </h1>\r\n            </div>\r\n            <div text-center>\r\n              <ion-button (click)=\"PrintReceipt()\">PRINT RECEIPT</ion-button>\r\n            </div>\r\n          </ion-card>\r\n        </ion-grid>\r\n\r\n      </ion-card>\r\n    </div>\r\n\r\n\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/cashier/home/home.page.scss":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".headertitle {\n  font-size: 50pt; }\n\n.refreshbtn {\n  width: 100%;\n  height: 100px;\n  font-size: 24px; }\n\n.tablebox {\n  width: 50%;\n  float: left;\n  display: inline-block; }\n\n.tablebox ion-card {\n    width: 200px;\n    height: 100px; }\n\n.billdetailbox {\n  display: inline-block;\n  width: 50%;\n  height: 31px; }\n\n.cashiercontainer {\n  position: relative;\n  height: 100%; }\n\n.footer {\n  display: flex; }\n\n.footer > div {\n  flex: 1 0 25%;\n  background-color: antiquewhite;\n  bottom: 0;\n  overflow: hidden; }\n\n.footer > div ion-button {\n    float: right; }\n\n.footer > div ion-input {\n    background-color: gray; }\n\n.orderdetails {\n  border: 1px solid gray;\n  border-left: none;\n  border-right: none; }\n\n#total {\n  float: left;\n  display: inline-block;\n  width: 50%; }\n\n#change {\n  float: left;\n  width: 50%;\n  display: inline-block; }\n\nspan ion-icon {\n  float: right;\n  font-size: 35px;\n  color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FzaGllci9ob21lL0Q6XFxTY2hvb2xcXGNhdGVydS1tb2JpbGUvc3JjXFxhcHBcXGNhc2hpZXJcXGhvbWVcXGhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksZUFBZSxFQUFBOztBQUVuQjtFQUNJLFdBQVc7RUFDWCxhQUFhO0VBQ2IsZUFBZSxFQUFBOztBQUVuQjtFQUVJLFVBQVU7RUFDVixXQUFXO0VBQ1gscUJBQXFCLEVBQUE7O0FBSnpCO0lBTVEsWUFBWTtJQUNaLGFBQWEsRUFBQTs7QUFHckI7RUFFSSxxQkFBcUI7RUFDckIsVUFBUztFQUNULFlBQVcsRUFBQTs7QUFFZjtFQUNJLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBR2hCO0VBS0ksYUFBYSxFQUFBOztBQUVqQjtFQUNJLGFBQVk7RUFDWiw4QkFBOEI7RUFTOUIsU0FBUztFQUNULGdCQUFnQixFQUFBOztBQVpwQjtJQU1RLFlBQVksRUFBQTs7QUFOcEI7SUFTUSxzQkFBc0IsRUFBQTs7QUFLOUI7RUFDSSxzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQXFCdEI7RUFDSSxXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLFVBQVUsRUFBQTs7QUFFZDtFQUNJLFdBQVc7RUFDWCxVQUFVO0VBQ1YscUJBQXFCLEVBQUE7O0FBRXpCO0VBRVEsWUFBWTtFQUNaLGVBQWU7RUFDZixVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jYXNoaWVyL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVydGl0bGVcclxue1xyXG4gICAgZm9udC1zaXplOiA1MHB0O1xyXG59XHJcbi5yZWZyZXNoYnRue1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59XHJcbi50YWJsZWJveHtcclxuICAgXHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBpb24tY2FyZHtcclxuICAgICAgICB3aWR0aDogMjAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIH1cclxufVxyXG4uYmlsbGRldGFpbGJveHtcclxuICAgICBcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgICAgIFxyXG4gICAgd2lkdGg6NTAlOyAgICAgXHJcbiAgICBoZWlnaHQ6MzFweDsgXHJcbn1cclxuLmNhc2hpZXJjb250YWluZXJ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAvLyAgIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuLmZvb3RlcntcclxuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIC8vIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAvLyBib3R0b206IDA7XHJcbiAgICAvLyB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuLmZvb3RlciA+IGRpdntcclxuICAgIGZsZXg6MSAwIDI1JTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGFudGlxdWV3aGl0ZTtcclxuICAgIGlvbi1idXR0b257XHJcbiAgICAgICAgLy8gd2lkdGg6IDE1MHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLy8gaGVpZ2h0OiA5MCU7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgfVxyXG4gICAgaW9uLWlucHV0e1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IGdyYXk7XHJcbiAgICB9XHJcbiAgICBib3R0b206IDA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbi5vcmRlcmRldGFpbHN7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7ICAgIFxyXG59XHJcbi8vICNjYXNodGVuZGVyZWR7XHJcbi8vICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgICB3aWR0aDogMjUlO1xyXG4vLyAgICAgLy9iYWNrZ3JvdW5kLWNvbG9yOiBhbnRpcXVld2hpdGU7XHJcbi8vICAgICBib3JkZXI6IGJ1cmx5d29vZCA7XHJcbi8vICAgICBmbG9hdDogbGVmdDsgXHJcbi8vIH1cclxuLy8gI2lucHV0e1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4vLyAgICAgd2lkdGg6IDUwJTtcclxuLy8gICAgIGJhY2tncm91bmQtY29sb3I6IGFudGlxdWV3aGl0ZTtcclxuLy8gICAgIGJvcmRlcjogYnVybHl3b29kIDtcclxuLy8gICAgIGZsb2F0OiBjZW50ZXI7IFxyXG4vLyB9XHJcbi8vICNwcmludGJ0bntcclxuLy8gICAgIHdpZHRoOiAyNSU7XHJcbi8vICAgICBmbG9hdDogcmlnaHQ7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vIH1cclxuI3RvdGFse1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59XHJcbiNjaGFuZ2V7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuc3BhbntcclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICAgICAvLyB2ZXJ0aWNhbC1hbGlnbjogcmlnaHQ7XHJcbiAgICAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/cashier/home/home.page.ts":
/*!*******************************************!*\
  !*** ./src/app/cashier/home/home.page.ts ***!
  \*******************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/api.service */ "./src/app/cashier/service/api.service.ts");





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, apiService, router, route, modalController, toastCtlr) {
        this.navCtrl = navCtrl;
        this.apiService = apiService;
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.toastCtlr = toastCtlr;
        this.billInfo = {};
        this.now = new Date();
        this.chix = [];
        this.dataIsLoaded = false;
        this.isclicked = false;
        this.remove = true;
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.empid = String(params.get('username'));
            console.log(_this.empid);
        });
        this.getBillOut();
        this.getEmployeeName(this.empid);
    };
    HomePage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getBillOut();
            _this.getOrders();
            event.target.complete();
        }, 1500);
    };
    // ionViewWIllEnter() {
    //   this.getBillOut();
    // }
    HomePage.prototype.setTableNo = function (tableno) {
        this.tableNum = tableno;
        this.getOrders();
    };
    HomePage.prototype.getBillOut = function () {
        var _this = this;
        this.apiService.getBillOutDetails().then(function (data) {
            _this.tables = data.table;
            console.log('GETBILLOUT');
            console.log(_this.tables);
        });
    };
    HomePage.prototype.orderId = function (bills) {
        console.log(bills);
        this.router.navigateByUrl('details/' + bills);
    };
    HomePage.prototype.getOrders = function () {
        var _this = this;
        this.apiService.getOrderRecord(this.tableNum).then(function (data) {
            _this.orders = data.records;
            for (var _i = 0, _a = _this.orders; _i < _a.length; _i++) {
                var info = _a[_i];
                _this.custID = info.custid;
                _this.tableno = info.tableno;
                _this.order_id = info.order_id;
                _this.tNo = info.tableno;
            }
            _this.getTotal(_this.order_id);
        }).then(function () {
            _this.displayBilloutOrders = { nonBundle: [], bundle: [] };
            var bundleDetails = { name: '', price: 0, qty: 0, subtotal: 0, content: [] };
            var processedCount = 1;
            _this.orders.forEach(function (item) {
                if (item.bundleid) {
                    if (bundleDetails.name === '')
                        bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
                    else if (bundleDetails.name !== item.bundlename && bundleDetails.name !== '') {
                        _this.displayBilloutOrders.bundle.push(bundleDetails);
                        bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
                    }
                    else {
                        bundleDetails.content.push(item);
                    }
                }
                else
                    _this.displayBilloutOrders.nonBundle.push(item);
                processedCount += 1;
                if (processedCount == _this.orders.length) {
                    _this.displayBilloutOrders.bundle.push(bundleDetails);
                }
            });
            console.log(_this.displayBilloutOrders);
            _this.dataIsLoaded = true;
        });
    };
    HomePage.prototype.PrintReceipt = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.amount >= this.total)) return [3 /*break*/, 1];
                        this.finalTotal = this.amount - this.total;
                        this.sendUpdatedBill();
                        return [3 /*break*/, 3];
                    case 1:
                        if (!(this.amount === null)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.toastCtlr.create({
                                message: 'Amount is required!',
                                duration: 2000
                            })];
                    case 2:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 3;
                    case 3:
                        this.doRefresh(event);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.click = function () {
        this.isNotReady = false;
    };
    HomePage.prototype.sendUpdatedBill = function () {
        var _this = this;
        this.billInfo.order_id = this.order_id;
        this.billInfo.cashTender = this.amount;
        this.billInfo.change = this.finalTotal;
        this.billInfo.status = 'paid';
        this.billInfo.total = this.total;
        console.log(this.billInfo);
        this.apiService.postUpdatedBill(this.billInfo).then(function () {
            _this.router.navigate(['/printing'], { queryParams: { order_id: JSON.stringify({ order_id: _this.order_id, name: _this.fname }) } });
        });
    };
    HomePage.prototype.closeModal = function () {
        this.modalController.dismiss();
    };
    HomePage.prototype.getTotal = function (order_id) {
        var _this = this;
        this.apiService.getTotal(order_id).then(function (data) {
            _this.total = (data);
            console.log(_this.total);
        });
    };
    HomePage.prototype.getEmployeeName = function (username) {
        var _this = this;
        this.apiService.getEmployeeName(username).then(function (data) {
            _this.name = data.data;
            for (var _i = 0, _a = _this.name; _i < _a.length; _i++) {
                var name_1 = _a[_i];
                _this.fname = name_1.empfirstname;
            }
        });
    };
    HomePage.prototype.isClicked = function (id) {
        this.remove = false;
    };
    HomePage.prototype.cancellItem = function (id) {
        var _this = this;
        this.apiService.cancelOrder(id).then(function () {
            _this.presentToast();
        });
    };
    HomePage.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Order Cancelled!',
                            duration: 3000,
                            position: 'top'
                        })];
                    case 1:
                        toast = _a.sent();
                        this.doRefresh(event);
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/cashier/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/cashier/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=cashier-home-home-module.js.map