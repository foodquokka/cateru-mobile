(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cashier-printing-printing-module"],{

/***/ "./src/app/cashier/printing/printing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.module.ts ***!
  \*****************************************************/
/*! exports provided: PrintingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintingPageModule", function() { return PrintingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _printing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./printing.page */ "./src/app/cashier/printing/printing.page.ts");







var routes = [
    {
        path: '',
        component: _printing_page__WEBPACK_IMPORTED_MODULE_6__["PrintingPage"]
    }
];
var PrintingPageModule = /** @class */ (function () {
    function PrintingPageModule() {
    }
    PrintingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_printing_page__WEBPACK_IMPORTED_MODULE_6__["PrintingPage"]]
        })
    ], PrintingPageModule);
    return PrintingPageModule;
}());



/***/ }),

/***/ "./src/app/cashier/printing/printing.page.html":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>RECEIPT</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content scrollY=\"true\" text-center>\r\n  <div id=\"ReceiptDivContainer\" text-center style=\"background-color: chocolate;\">\r\n    <div id=\"ReceiptDiv\">\r\n      <ion-card class=\"receipt\" scrollY=true>\r\n\r\n        <ion-card-header>\r\n\r\n          <div text-center>\r\n            <!-- <div id=\"Logo\">\r\n              <img src=\"/assets/img/MagesticLogo.png\" alt='Company Logo'>\r\n            </div> -->\r\n            <div>\r\n              <h1>Magestic Restaurant</h1>\r\n              <!-- <h5>Operated by: CaterU Holdings, Inc</h5> -->\r\n              <p>Quiot Pardo, Cebu City</p>\r\n              <p>Tel. No. 520-5598, 417 - 9726</p>\r\n              <!-- <p>VAT Reg, TIN: 008-381-043-091</p>\r\n            <p>SN: 41BZ416</p>\r\n            <p>PN: 0913-082-1562461-091</p>\r\n            <p>MIN: 1303852174</p> -->\r\n              <h4>O F F I C I A L R E C E I P T</h4>\r\n            </div>\r\n          </div>\r\n          <div text-left>\r\n            <p>CASHIER: {{ username }}</p>\r\n            <p>DATE: {{ myDate | date:'short' }} </p>\r\n            <!-- <p>POS No. 10926H</p> \r\n            <p>OR No. 6125341381462</p> -->\r\n          </div>\r\n        </ion-card-header>\r\n        <ion-card-content>\r\n          <div text-center id=\"TableDiv\">\r\n            <ion-grid>\r\n              <ion-row style=\"font-weight: bolder; background-color: grey; color: white;\">\r\n                <ion-col>QTY</ion-col>\r\n                <ion-col>PRODUCT</ion-col>\r\n                <ion-col>PRICE</ion-col>\r\n              </ion-row>\r\n              <div *ngIf=\"dataIsLoaded == true\">\r\n                <ion-row *ngFor=\"let info of displayPaidOrders.nonBundle\" scrollY=\"true\">\r\n                  <ion-col>\r\n                    <p>{{ info.orderNum}}</p>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <p>{{ info.name }}</p>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <p>{{ info.subtotal}}</p>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row *ngFor=\"let info of displayPaidOrders.bundle\" scrollY=\"true\">\r\n\r\n                  <ion-col>\r\n                    <p>{{ info.qty}}</p>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <p>{{ info.name }}</p>\r\n                    <!-- <ion-row *ngFor=\"let detail of info.content\">\r\n                      <p>{{ detail.name}}</p>\r\n                    </ion-row> -->\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <p>{{ info.subtotal}}</p>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </div>\r\n            </ion-grid>\r\n          </div>\r\n          <div id=\"OuterContainer\">\r\n            <div id=\"FirstInnerContainer\">\r\n              <ion-row>\r\n                <p>TOTAL: {{ total }} <br>Cash: {{ cash }} <br>Change:{{ change }}</p>\r\n\r\n              </ion-row>\r\n            </div>\r\n            <div id=\"SecondInnerContainer\" text-center>\r\n              <h1>THANK YOU!</h1>\r\n            </div>\r\n          </div>\r\n        </ion-card-content>\r\n      </ion-card>\r\n    </div>\r\n  </div>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/cashier/printing/printing.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media only screen and (max-width: 150px) {\n  ion-content {\n    font-size: 2vw !important;\n    font-family: 'Monaco' !important; } }\n\np {\n  font-size: 14px;\n  font-weight: bold;\n  font-family: 'Monaco' !important; }\n\nion-card {\n  height: 90%;\n  width: 96%; }\n\n.print {\n  width: 100%;\n  height: 150px;\n  font-size: 24px; }\n\n.receipt {\n  overflow-y: scroll;\n  width: 50vw;\n  position: relative;\n  font-size: 15%;\n  background-color: #edebe8; }\n\n#ReceiptDivContainer {\n  transform: translate(30%, 30%); }\n\n#Logo {\n  left: 43%;\n  position: relative; }\n\n#Logo img {\n    width: 80px;\n    height: 80px; }\n\n#ReceiptDiv {\n  display: inline-block;\n  position: absolute;\n  width: 250px;\n  height: 100vh;\n  left: -5%; }\n\n#HeaderStyle {\n  position: relative;\n  padding: none;\n  height: 50px;\n  background-color: plum; }\n\n#TableDiv ion-col {\n  border: 1px groove;\n  border-color: white; }\n\n#OuterContainer {\n  display: flex;\n  justify-content: column; }\n\n#FirstInnerContainer {\n  display: flex; }\n\n#SecondInnerContainer {\n  display: flex;\n  flex: 1;\n  margin-top: 100px;\n  margin-left: 150px; }\n\n#barcodeImg img {\n  border-radius: none !important;\n  width: 30vw;\n  height: 10vh; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FzaGllci9wcmludGluZy9EOlxcU2Nob29sXFxjYXRlcnUtbW9iaWxlL3NyY1xcYXBwXFxjYXNoaWVyXFxwcmludGluZ1xccHJpbnRpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0k7SUFDRyx5QkFBeUI7SUFDekIsZ0NBQWdDLEVBQUEsRUFDbEM7O0FBR0o7RUFDRSxlQUFlO0VBQ2YsaUJBQWlCO0VBRWpCLGdDQUFnQyxFQUFBOztBQUduQztFQUNJLFdBQVc7RUFDWCxVQUFVLEVBQUE7O0FBRWQ7RUFDSSxXQUFXO0VBQ1gsYUFBYTtFQUNiLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUVYLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QseUJBQXdCLEVBQUE7O0FBRTVCO0VBQ0csOEJBQThCLEVBQUE7O0FBRWpDO0VBRUksU0FBUztFQUNULGtCQUFrQixFQUFBOztBQUh0QjtJQUtRLFdBQVc7SUFDWCxZQUFZLEVBQUE7O0FBR3BCO0VBQ0kscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osYUFBYTtFQUNiLFNBQVMsRUFBQTs7QUFJYjtFQUNJLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsWUFBWTtFQUNaLHNCQUFzQixFQUFBOztBQUUxQjtFQUtRLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBQTs7QUFHM0I7RUFDSSxhQUFhO0VBQ2IsdUJBQXNCLEVBQUE7O0FBRTFCO0VBRUksYUFBYSxFQUFBOztBQU9qQjtFQUdHLGFBQWE7RUFDYixPQUFPO0VBQ1AsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBOztBQUVyQjtFQUVRLDhCQUE4QjtFQUM5QixXQUFXO0VBQ1gsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY2FzaGllci9wcmludGluZy9wcmludGluZy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDE1MHB4KSB7XHJcbiAgICBpb24tY29udGVudCB7IFxyXG4gICAgICAgZm9udC1zaXplOiAydncgIWltcG9ydGFudDsgXHJcbiAgICAgICBmb250LWZhbWlseTogJ01vbmFjbycgIWltcG9ydGFudDtcclxuICAgIH1cclxuIFxyXG59XHJcbiBwe1xyXG4gICBmb250LXNpemU6IDE0cHg7ICBcclxuICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgLy8gZm9udC1zdHlsZTogaXRhbGljO1xyXG4gICBmb250LWZhbWlseTogJ01vbmFjbycgIWltcG9ydGFudDtcclxuIH1cclxuXHJcbmlvbi1jYXJkIHtcclxuICAgIGhlaWdodDogOTAlO1xyXG4gICAgd2lkdGg6IDk2JTtcclxufVxyXG4ucHJpbnR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbn1cclxuLnJlY2VpcHR7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICB3aWR0aDogNTB2dztcclxuICAgLy8gaGVpZ2h0OiA1MHZoO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlOyBcclxuICAgIGZvbnQtc2l6ZTogMTUlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojZWRlYmU4O1xyXG59XHJcbiNSZWNlaXB0RGl2Q29udGFpbmVye1xyXG4gICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgzMCUsIDMwJSk7XHJcbn1cclxuI0xvZ297XHJcbiAgIFxyXG4gICAgbGVmdDogNDMlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaW1ne1xyXG4gICAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICAgIGhlaWdodDogODBweDtcclxuICAgIH1cclxufVxyXG4jUmVjZWlwdERpdntcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiAyNTBweDtcclxuICAgIGhlaWdodDogMTAwdmg7ICBcclxuICAgIGxlZnQ6IC01JTsgXHJcbiAgLy8gIFxyXG4gICAgXHJcbn1cclxuI0hlYWRlclN0eWxle1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZzogbm9uZTtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHBsdW07XHJcbn1cclxuI1RhYmxlRGl2e1xyXG4gICAgLy8gaW9uLXJvd3tcclxuICAgIC8vICAgICBib3JkZXI6IDFweCBncm9vdmU7XHJcbiAgICAvLyB9XHJcbiAgICBpb24tY29se1xyXG4gICAgICAgIGJvcmRlcjogMXB4IGdyb292ZTtcclxuICAgICAgICBib3JkZXItY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG59XHJcbiNPdXRlckNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6Y29sdW1uO1xyXG59XHJcbiNGaXJzdElubmVyQ29udGFpbmVye1xyXG4gIC8vICBmbG9hdDogcmlnaHQ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAvLyBmbGV4OiAxO1xyXG4gICAgLy9qdXN0aWZ5LWNvbnRlbnQ6IHJvdztcclxuICAgIC8vcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC8vICB3aWR0aDogNTAlO1xyXG4gICAgXHJcbn1cclxuI1NlY29uZElubmVyQ29udGFpbmVye1xyXG4gIC8vICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIC8vbWFyZ2luLXRvcDogNTBweDtcclxuICAgZGlzcGxheTogZmxleDtcclxuICAgZmxleDogMTtcclxuICAgbWFyZ2luLXRvcDogMTAwcHg7XHJcbiAgIG1hcmdpbi1sZWZ0OiAxNTBweDtcclxufVxyXG4jYmFyY29kZUltZ3tcclxuICAgIGltZ3tcclxuICAgICAgICBib3JkZXItcmFkaXVzOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2lkdGg6IDMwdnc7XHJcbiAgICAgICAgaGVpZ2h0OiAxMHZoO1xyXG4gICAgfVxyXG4gIFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/cashier/printing/printing.page.ts":
/*!***************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.ts ***!
  \***************************************************/
/*! exports provided: PrintingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintingPage", function() { return PrintingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/api.service */ "./src/app/cashier/service/api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var PrintingPage = /** @class */ (function () {
    function PrintingPage(router, apiservice, datePipe, loadingController, toastCtlr) {
        this.router = router;
        this.apiservice = apiservice;
        this.datePipe = datePipe;
        this.loadingController = loadingController;
        this.toastCtlr = toastCtlr;
        this.myDate = Date.now();
    }
    PrintingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.router.queryParams.subscribe(function (params) {
            var data = JSON.parse(params['order_id']);
            console.log(data);
            _this.orderid = data.order_id;
            _this.username = data.name;
        });
        console.log('THIS ORDERID', this.orderid);
        console.log('name', this.username);
        this.getReceiptDetails();
    };
    PrintingPage.prototype.ionViewWillEnter = function () {
        this.getReceiptDetails();
        this.getTotal();
        this.presentLoading();
    };
    PrintingPage.prototype.getReceiptDetails = function () {
        var _this = this;
        console.log(this.empName, this.date);
        this.apiservice.getReceiptDetails(this.orderid).then(function (data) {
            _this.orders = data.records;
            for (var _i = 0, _a = _this.orders; _i < _a.length; _i++) {
                var info = _a[_i];
                _this.empName = info.empfirstname;
                _this.tableno = info.tableno;
                _this.date = _this.datePipe.transform(_this.myDate, 'yyyy-MM-dd');
                _this.cash = info.cashTender;
                _this.change = info.change;
            }
        }).then(function () {
            _this.displayPaidOrders = { nonBundle: [], bundle: [] };
            var bundleDetails = { name: '', price: 0, qty: 0, subtotal: 0, content: [] };
            var processedCount = 1;
            _this.orders.forEach(function (item) {
                if (item.bundleid) {
                    if (bundleDetails.name === '')
                        bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
                    else if (bundleDetails.name !== item.bundlename && bundleDetails.name !== '') {
                        _this.displayPaidOrders.bundle.push(bundleDetails);
                        bundleDetails = { name: item.bundlename, price: item.bundleprice, subtotal: item.subtotal, qty: item.orderNum, content: [item] };
                    }
                    else {
                        bundleDetails.content.push(item);
                    }
                }
                else
                    _this.displayPaidOrders.nonBundle.push(item);
                processedCount += 1;
                if (processedCount == _this.orders.length) {
                    _this.displayPaidOrders.bundle.push(bundleDetails);
                }
            });
            console.log(_this.displayPaidOrders);
            _this.dataIsLoaded = true;
        });
    };
    PrintingPage.prototype.getTotal = function () {
        var _this = this;
        this.apiservice.getTotal(this.orderid).then(function (data) {
            _this.total = (data);
        }).then(function () {
            _this.apiservice.getCashTendered(_this.orderid).then(function (data) {
                _this.cash = (data);
            }).then(function () {
                _this.apiservice.getChange(_this.orderid).then(function (data) {
                    _this.change = (data);
                });
            });
        });
    };
    PrintingPage.prototype.presentLoading = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, _a, role, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'printing your receipt',
                            duration: 2000,
                        })];
                    case 1:
                        loading = _b.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, loading.onDidDismiss()];
                    case 3:
                        _a = _b.sent(), role = _a.role, data = _a.data;
                        this.presentToast();
                        // this.setTableAvailable();
                        //this.route.navigate(['/cashier']);
                        console.log('Loading dismissed!');
                        return [2 /*return*/];
                }
            });
        });
    };
    PrintingPage.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Receipt printed!',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PrintingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-printing',
            template: __webpack_require__(/*! ./printing.page.html */ "./src/app/cashier/printing/printing.page.html"),
            styles: [__webpack_require__(/*! ./printing.page.scss */ "./src/app/cashier/printing/printing.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], PrintingPage);
    return PrintingPage;
}());



/***/ })

}]);
//# sourceMappingURL=cashier-printing-printing-module.js.map