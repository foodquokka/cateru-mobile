(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-ending-page-ending-page-module"],{

/***/ "./src/app/customer/ending-page/ending-page.module.ts":
/*!************************************************************!*\
  !*** ./src/app/customer/ending-page/ending-page.module.ts ***!
  \************************************************************/
/*! exports provided: EndingPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndingPagePageModule", function() { return EndingPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ending_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ending-page.page */ "./src/app/customer/ending-page/ending-page.page.ts");







var routes = [
    {
        path: '',
        component: _ending_page_page__WEBPACK_IMPORTED_MODULE_6__["EndingPagePage"]
    }
];
var EndingPagePageModule = /** @class */ (function () {
    function EndingPagePageModule() {
    }
    EndingPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_ending_page_page__WEBPACK_IMPORTED_MODULE_6__["EndingPagePage"]]
        })
    ], EndingPagePageModule);
    return EndingPagePageModule;
}());



/***/ }),

/***/ "./src/app/customer/ending-page/ending-page.page.html":
/*!************************************************************!*\
  !*** ./src/app/customer/ending-page/ending-page.page.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button></ion-back-button>\n      </ion-buttons>\n      <!-- <ion-title>Order List</ion-title> -->\n  </ion-toolbar>\n</ion-header>\n  \n<ion-content> \n  <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\" >\n    <ion-fab-button color=\"dark\"><ion-icon name=\"arrow-dropup\"></ion-icon></ion-fab-button>\n\n    <ion-fab-list side=\"top\">\n      <ion-fab-button color=\"dark\" [routerLink]=\"['/table-clear']\">\n      </ion-fab-button>\n      <ion-label>Clear Table</ion-label>\n    </ion-fab-list>\n  </ion-fab>\n</ion-content>\n  \n"

/***/ }),

/***/ "./src/app/customer/ending-page/ending-page.page.scss":
/*!************************************************************!*\
  !*** ./src/app/customer/ending-page/ending-page.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyL2VuZGluZy1wYWdlL2VuZGluZy1wYWdlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/customer/ending-page/ending-page.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/customer/ending-page/ending-page.page.ts ***!
  \**********************************************************/
/*! exports provided: EndingPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndingPagePage", function() { return EndingPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var EndingPagePage = /** @class */ (function () {
    function EndingPagePage() {
    }
    EndingPagePage.prototype.ngOnInit = function () {
    };
    EndingPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ending-page',
            template: __webpack_require__(/*! ./ending-page.page.html */ "./src/app/customer/ending-page/ending-page.page.html"),
            styles: [__webpack_require__(/*! ./ending-page.page.scss */ "./src/app/customer/ending-page/ending-page.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], EndingPagePage);
    return EndingPagePage;
}());



/***/ })

}]);
//# sourceMappingURL=customer-ending-page-ending-page-module.js.map