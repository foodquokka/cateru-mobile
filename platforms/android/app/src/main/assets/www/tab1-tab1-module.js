(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./src/app/kitchen/ktab1/drinks.ts":
/*!*****************************************!*\
  !*** ./src/app/kitchen/ktab1/drinks.ts ***!
  \*****************************************/
/*! exports provided: Drinks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Drinks", function() { return Drinks; });
var Drinks = /** @class */ (function () {
    function Drinks() {
    }
    return Drinks;
}());



/***/ }),

/***/ "./src/app/waiter/tab1/tab1.module.ts":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.module.ts ***!
  \********************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/waiter/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.html":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n</ion-header>\r\n<ion-toolbar color=\"primary\">\r\n  <ion-title>\r\n    Orders\r\n  </ion-title>\r\n</ion-toolbar>\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row >\r\n      <ion-col >\r\n        <ion-card class=\"card\">\r\n          <ion-card-content>\r\n            <ion-row>\r\n              <ion-col text-center>\r\n                <h1>DRINKS</h1>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row text-center>\r\n              <ion-col>NAME</ion-col>\r\n              <ion-col>TIME</ion-col>\r\n              <ion-col>ACTION</ion-col>\r\n            </ion-row>\r\n            <ion-item text-center *ngFor=\"let order of drinks;let i = index\">\r\n              <ion-label>\r\n                {{order.date_ordered | date:'shortTime'}}\r\n              </ion-label>\r\n              <ion-label>\r\n                {{order.name}}\r\n              </ion-label>\r\n              <br>\r\n              <ion-label>\r\n                <ion-button (click)=\"prepare(order.id)\" [disabled]=\"isenabled[id]\">PREPARE</ion-button>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.scss":
/*!********************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  height: 50px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FpdGVyL3RhYjEvRDpcXFNjaG9vbFxcY2F0ZXJ1LW1vYmlsZS9zcmNcXGFwcFxcd2FpdGVyXFx0YWIxXFx0YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvd2FpdGVyL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xyXG4gIGhlaWdodDogNTBweCAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/waiter/tab1/tab1.page.ts":
/*!******************************************!*\
  !*** ./src/app/waiter/tab1/tab1.page.ts ***!
  \******************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_waiter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/waiter.service */ "./src/app/waiter/service/waiter.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_kitchen_ktab1_drinks__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/kitchen/ktab1/drinks */ "./src/app/kitchen/ktab1/drinks.ts");
/* harmony import */ var src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/kitchen/rest.service */ "./src/app/kitchen/rest.service.ts");






var Tab1Page = /** @class */ (function () {
    function Tab1Page(navCtrl, restService, waiterservice, toastCtlr) {
        this.navCtrl = navCtrl;
        this.restService = restService;
        this.waiterservice = waiterservice;
        this.toastCtlr = toastCtlr;
        this.drinkss = new src_app_kitchen_ktab1_drinks__WEBPACK_IMPORTED_MODULE_4__["Drinks"]();
        this.orderselected = false;
        this.isenabled = [false, false, false, false];
        this.orderDrinksArray = new Array();
        this.orderCard = [];
        // qty: any;
        // name: any;
        // status: any;
        this.counter = 0;
    }
    Tab1Page.prototype.ngOnInit = function () {
    };
    // ionViewWillEnter() {
    //   this.getDrinkList();
    // }
    // getOrderQty(id) {
    //   this.restService.getOrderQty(id).then(data => {
    //     this.qty = (data as any).items;
    //   });
    // }
    Tab1Page.prototype.getDrinkList = function () {
        var _this = this;
        this.waiterservice.getOrderDrinks().then(function (data) {
            _this.drinks = data.orders;
        });
    };
    Tab1Page.prototype.updateStatus = function (order) {
        var _this = this;
        console.log('orders', order);
        this.restService.postStatus(order).then(function (data) {
            _this.orderLists = data;
        });
    };
    Tab1Page.prototype.prepare = function (id, index) {
        this.restService.changeOrderStatusToPrepare(id);
        this.isPrepare(id);
        this.isenabled[index] = this.prepareStatus;
        this.presentToastPrepare();
    };
    Tab1Page.prototype.isPrepare = function (id) {
        var _this = this;
        this.restService.isPreparing(id).then(function (data) {
            _this.prepareStatus = data.status;
        });
        this.doRefresh();
    };
    Tab1Page.prototype.doRefresh = function () {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getDrinkList();
        }, 1500);
    };
    Tab1Page.prototype.presentToastPrepare = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Order being prepared',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! ./tab1.page.html */ "./src/app/waiter/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/waiter/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            src_app_kitchen_rest_service__WEBPACK_IMPORTED_MODULE_5__["RestService"],
            _service_waiter_service__WEBPACK_IMPORTED_MODULE_2__["WaiterService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module.js.map