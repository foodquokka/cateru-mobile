(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["watcher-watcher-module"],{

/***/ "./src/app/watcher/watcher.module.ts":
/*!*******************************************!*\
  !*** ./src/app/watcher/watcher.module.ts ***!
  \*******************************************/
/*! exports provided: WatcherPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WatcherPageModule", function() { return WatcherPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _watcher_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./watcher.page */ "./src/app/watcher/watcher.page.ts");







var routes = [
    {
        path: '',
        component: _watcher_page__WEBPACK_IMPORTED_MODULE_6__["WatcherPage"]
    }
];
var WatcherPageModule = /** @class */ (function () {
    function WatcherPageModule() {
    }
    WatcherPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_watcher_page__WEBPACK_IMPORTED_MODULE_6__["WatcherPage"]]
        })
    ], WatcherPageModule);
    return WatcherPageModule;
}());



/***/ }),

/***/ "./src/app/watcher/watcher.page.html":
/*!*******************************************!*\
  !*** ./src/app/watcher/watcher.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-title>Table List</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n      <ion-refresher-content pullingIcon=\"arrow-dropdown\" pullingText=\"Pull to refresh\" refreshingSpinner=\"circles\"\n          refreshingText=\"Refreshing...\">\n      </ion-refresher-content>\n  </ion-refresher>\n  <div>\n      <ion-button color=\"primary\">For serving</ion-button>\n      <ion-button color=\"secondary\">occupied</ion-button>\n  </div>\n  <ion-grid>\n      <ion-row>\n          <ion-col *ngFor=\"let t of tables; let i=index\">\n              <ion-card class=\"occupied\" [color]=\"count[i] != 0 ? 'primary' : 'secondary'\" fill=\"outline\"\n                  border-style=\"solid\" mode=\"ios\" text-center padding tappable (click)=\"presentmodal(t.tableno)\">\n                  <h2>TABLE NO: {{ t.tableno}}</h2>\n                  <ion-card-header style=\"background-color:black; color: gray; font-weight: bold;\">\n                      <h6>CALL WAITER</h6>\n                      <p>\n                          <ion-icon name=\"notifications\" [color]=\"calls[i] != 0 ? 'warning' : 'secondary'\"></ion-icon>\n                      </p>\n                  </ion-card-header>\n                  <div [hidden]=\"billout[i] != 0 ? false : true\">\n                      <h6 style=\" color:crimson; font-weight: bold;\">READY FOR BILLOUT</h6>\n                  </div>\n              </ion-card>\n          </ion-col>\n      </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/watcher/watcher.page.scss":
/*!*******************************************!*\
  !*** ./src/app/watcher/watcher.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n#status {\n  color: blue; }\n\n#tablecard {\n  width: 50%;\n  height: 20%; }\n\n.refreshbtn {\n  width: 100%; }\n\n.badge {\n  float: right;\n  font-size: 24px;\n  color: black; }\n\nion-badge {\n  font-size: 36px;\n  color: white; }\n\n.occupied {\n  width: 250px; }\n\nion-icon {\n  font-size: 30px; }\n\n* {\n  box-sizing: content-box !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2F0Y2hlci9EOlxcU2Nob29sXFxjYXRlcnUtbW9iaWxlL3NyY1xcYXBwXFx3YXRjaGVyXFx3YXRjaGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFnQjtFQUNoQixnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSxXQUFXLEVBQUE7O0FBRWI7RUFDRSxVQUFVO0VBQ1YsV0FBVyxFQUFBOztBQUViO0VBQ0UsV0FBVyxFQUFBOztBQUViO0VBQ0UsWUFBWTtFQUNaLGVBQWU7RUFDZixZQUFZLEVBQUE7O0FBRWQ7RUFDRSxlQUFlO0VBQ2YsWUFDRixFQUFBOztBQUNBO0VBQ0UsWUFBWSxFQUFBOztBQUVkO0VBQ0UsZUFBZSxFQUFBOztBQUVqQjtFQUNFLGtDQUFpQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvd2F0Y2hlci93YXRjaGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XHJcbiAgICBtYXgtaGVpZ2h0OiAzNXZoO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICB9XHJcbiAgI3N0YXR1c3tcclxuICAgIGNvbG9yOiBibHVlO1xyXG4gIH1cclxuICAjdGFibGVjYXJke1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGhlaWdodDogMjAlO1xyXG4gIH1cclxuICAucmVmcmVzaGJ0bntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuYmFkZ2V7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfVxyXG4gIGlvbi1iYWRnZXtcclxuICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgIGNvbG9yOiB3aGl0ZVxyXG4gIH1cclxuICAub2NjdXBpZWR7XHJcbiAgICB3aWR0aDogMjUwcHg7XHJcbiAgfVxyXG4gIGlvbi1pY29ue1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gIH1cclxuICAqe1xyXG4gICAgYm94LXNpemluZzpjb250ZW50LWJveCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAiXX0= */"

/***/ }),

/***/ "./src/app/watcher/watcher.page.ts":
/*!*****************************************!*\
  !*** ./src/app/watcher/watcher.page.ts ***!
  \*****************************************/
/*! exports provided: WatcherPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WatcherPage", function() { return WatcherPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _waiter_service_waiter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../waiter/service/waiter.service */ "./src/app/waiter/service/waiter.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _waiter_waiterorderdetail_waiterorderdetail_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../waiter/waiterorderdetail/waiterorderdetail.page */ "./src/app/waiter/waiterorderdetail/waiterorderdetail.page.ts");





var WatcherPage = /** @class */ (function () {
    function WatcherPage(waiterService, alertCtrl, modalController) {
        this.waiterService = waiterService;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.cardBackgroundColor = 'primary';
        this.cardColor = 'black';
        this.orderCard = [];
        this.flag = false;
        this.count = [];
        this.calls = [];
        this.billout = [];
    }
    WatcherPage.prototype.ngOnInit = function () {
    };
    WatcherPage.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    WatcherPage.prototype.ionViewWillEnter = function () {
        this.getDrinksList();
        this.getAllTable();
    };
    WatcherPage.prototype.getAllTable = function () {
        var _this = this;
        this.waiterService.getAllTable().then(function (data) {
            _this.tables = data.OccupiedTables;
            for (var _i = 0, _a = _this.tables; _i < _a.length; _i++) {
                var table = _a[_i];
                _this.getItemsForServing(table.tableno);
                _this.getConcernNotification(table.tableno);
                _this.getBillOut(table.tableno);
            }
        });
    };
    WatcherPage.prototype.getDrinksList = function () {
        var _this = this;
        this.waiterService.getOrderDrinks().then(function (data) {
            _this.drinklist = data.result;
        });
    };
    WatcherPage.prototype.presentmodal = function (tableno) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _waiter_waiterorderdetail_waiterorderdetail_page__WEBPACK_IMPORTED_MODULE_4__["WaiterorderdetailPage"],
                            componentProps: { tableno: tableno },
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    WatcherPage.prototype.doRefresh = function (event) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getAllTable();
            event.target.complete();
            _this.calls.length = 0;
            _this.count.length = 0;
            _this.billout.length = 0;
        }, 1500);
    };
    WatcherPage.prototype.setCount = function (data) {
        this.count.push(data);
        console.log(this.count);
    };
    WatcherPage.prototype.getItemsForServing = function (tableno) {
        var _this = this;
        this.waiterService.getItemsForServing(tableno).then(function (data) {
            var count = data.counter;
            _this.setCount(count);
        });
    };
    WatcherPage.prototype.setCalls = function (calls) {
        this.calls.push(calls);
        console.log(this.calls);
    };
    WatcherPage.prototype.getConcernNotification = function (tableno) {
        var _this = this;
        this.waiterService.getNotification(tableno).then(function (data) {
            var calls = data.concern;
            _this.setCalls(calls);
        });
    };
    WatcherPage.prototype.setBillout = function (billout) {
        this.billout.push(billout);
        console.log(this.billout);
    };
    WatcherPage.prototype.getBillOut = function (tableno) {
        var _this = this;
        this.waiterService.getBillOutList(tableno).then(function (data) {
            var billout = data.billout;
            _this.setBillout(billout);
        });
    };
    WatcherPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-watcher',
            template: __webpack_require__(/*! ./watcher.page.html */ "./src/app/watcher/watcher.page.html"),
            styles: [__webpack_require__(/*! ./watcher.page.scss */ "./src/app/watcher/watcher.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_waiter_service_waiter_service__WEBPACK_IMPORTED_MODULE_2__["WaiterService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
    ], WatcherPage);
    return WatcherPage;
}());



/***/ })

}]);
//# sourceMappingURL=watcher-watcher-module.js.map