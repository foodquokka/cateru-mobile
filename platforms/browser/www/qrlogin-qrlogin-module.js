(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["qrlogin-qrlogin-module"],{

/***/ "./src/app/qrlogin/qrlogin.module.ts":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.module.ts ***!
  \*******************************************/
/*! exports provided: QrloginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrloginPageModule", function() { return QrloginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _qrlogin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./qrlogin.page */ "./src/app/qrlogin/qrlogin.page.ts");







var routes = [
    {
        path: '',
        component: _qrlogin_page__WEBPACK_IMPORTED_MODULE_6__["QrloginPage"]
    }
];
var QrloginPageModule = /** @class */ (function () {
    function QrloginPageModule() {
    }
    QrloginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_qrlogin_page__WEBPACK_IMPORTED_MODULE_6__["QrloginPage"]]
        })
    ], QrloginPageModule);
    return QrloginPageModule;
}());



/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.html":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<ion-content id=\"content\">\n  <ion-header>\n  <ion-toolbar>\n    <ion-button  [routerLink]=\"['/dashboard']\">\n      <ion-icon ios=\"ios-arrow-back\" md=\"md-arrow-back\">Switch As Admin</ion-icon>\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n<ion-grid>\n  <div class=\"outer\">\n      <div class=\"middle\">\n        <ion-row>\n          <ion-col text-center><ion-text id=\"cateru\">CaterU</ion-text></ion-col>\n        </ion-row>\n        <ion-card class=\"inner\">\n            <ion-row text-center>\n              <ion-col> \n              <ion-card-content>\n               </ion-card-content>\n              </ion-col>\n            </ion-row>\n            <ion-row text-center><ion-col>\n               \n                <ion-button class=\"startBtn\" (click) = \"startScan()\" >Open Menu</ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-card>\n      </div>\n  </div>    \n\n</ion-grid>\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.scss":
/*!*******************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#content {\n  --background: linear-gradient(#7f1a1f, #3f0f11 );\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.outer {\n  display: table;\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.middle {\n  display: table-cell;\n  vertical-align: middle; }\n\n.tableBix {\n  position: absolute;\n  height: 250px;\n  width: 100%; }\n\n.inner {\n  margin-left: auto;\n  margin-right: auto;\n  width: 50%;\n  height: 200px;\n  padding: 20px 0;\n  border-style: solid; }\n\n#cateru {\n  color: #ffffff;\n  font-size: 110px;\n  font-family: billionthine; }\n\n.label {\n  color: #ffffff;\n  font-size: 130%; }\n\n.inputBox {\n  --background: #d3d3d3;\n  width: 50%;\n  border-radius: 5px;\n  margin-left: 24.5%;\n  margin-top: 15px;\n  color: #000000; }\n\n.startBtn {\n  --background: #8d272c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcXJsb2dpbi9EOlxcVGhlc2lzXFxDYXRlclVBcHAvc3JjXFxhcHBcXHFybG9naW5cXHFybG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0ksZ0RBQWE7RUFDYiw0QkFBNEI7RUFDNUIsc0JBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksY0FBYztFQUNkLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBR2Y7RUFDSSxtQkFBbUI7RUFDbkIsc0JBQXNCLEVBQUE7O0FBRTFCO0VBQ0ksa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixXQUFXLEVBQUE7O0FBR2Y7RUFDSSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixhQUFhO0VBQ2IsZUFBZTtFQUNmLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIseUJBQXlCLEVBQUE7O0FBRTdCO0VBQ0ksY0FBYztFQUNkLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxxQkFBYTtFQUNiLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBRWxCO0VBQ0kscUJBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3FybG9naW4vcXJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGVudCB7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWcvYmFja2dyb3VuZC5qcGcnKTtcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vaW1nL2dldHN0YXJ0LnBuZycpO1xyXG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoIzdmMWExZiwgIzNmMGYxMSApO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTpjb3ZlcjtcclxufVxyXG5cclxuLm91dGVyIHtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ubWlkZGxlIHtcclxuICAgIGRpc3BsYXk6IHRhYmxlLWNlbGw7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcbi50YWJsZUJpeHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGhlaWdodDogMjUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmlubmVyIHtcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDA7XHJcbiAgICBib3JkZXItc3R5bGU6IHNvbGlkO1xyXG59XHJcblxyXG4jY2F0ZXJ1IHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgZm9udC1zaXplOiAxMTBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBiaWxsaW9udGhpbmU7XHJcbn1cclxuLmxhYmVsIHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgZm9udC1zaXplOiAxMzAlO1xyXG59XHJcbi5pbnB1dEJveCB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNkM2QzZDM7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDI0LjUlO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG59XHJcbi5zdGFydEJ0biB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICM4ZDI3MmM7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/qrlogin/qrlogin.page.ts":
/*!*****************************************!*\
  !*** ./src/app/qrlogin/qrlogin.page.ts ***!
  \*****************************************/
/*! exports provided: QrloginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrloginPage", function() { return QrloginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_dialogs_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/dialogs/ngx */ "./node_modules/@ionic-native/dialogs/ngx/index.js");
/* harmony import */ var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/qr-scanner/ngx */ "./node_modules/@ionic-native/qr-scanner/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../app/qrlogin/qrlogin.service */ "./src/app/qrlogin/qrlogin.service.ts");
/* harmony import */ var _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../customer/services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/device-table.service */ "./src/app/services/device-table.service.ts");
/* harmony import */ var _ionic_native_uid_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/uid/ngx */ "./node_modules/@ionic-native/uid/ngx/index.js");
/* harmony import */ var src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/services/storageservice.service */ "./src/app/services/storageservice.service.ts");













var QrloginPage = /** @class */ (function () {
    function QrloginPage(alertController, platform, dialog, navctlr, qr, qrService, router, authService, ngZone, uid, adminService, deviceTable, storageService, devicetable, route) {
        this.alertController = alertController;
        this.platform = platform;
        this.dialog = dialog;
        this.navctlr = navctlr;
        this.qr = qr;
        this.qrService = qrService;
        this.router = router;
        this.authService = authService;
        this.ngZone = ngZone;
        this.uid = uid;
        this.adminService = adminService;
        this.deviceTable = deviceTable;
        this.storageService = storageService;
        this.devicetable = devicetable;
        this.route = route;
        this.table = {};
        this.encodedData = '';
        this.isOn = false;
        this.scannedinfos = Array();
        this.UUID = {};
        this.num = {};
        this.tableStatus = {};
        this.deviceinfo = {};
        this.isShow = false;
        this.isShowOccupiedTable = false;
        this.platform.backButton.subscribeWithPriority(0, function () {
            document.getElementsByTagName('body')[0].style.opacity = '1';
        });
    }
    QrloginPage.prototype.ngOnInit = function () {
        // this.getAllAvailableTableNo();
        // this.getTableStatusNotPaid();
        // this.toggleDisplay();
    };
    QrloginPage.prototype.ionviewWillEnter = function () {
        this.getAllAvailableTableNo();
        this.getTableStatusNotPaid();
        //  this.toggleDisplay();
    };
    // toggleDisplay() {
    //   this.isShow = !this.isShow;
    //   this.isShowOccupiedTable = true;
    //   this.ionviewWillEnter();
    // }
    // existCustomerToggledisplay(){
    //   this.isShow = true;
    //   this.isShowOccupiedTable = !this.isShowOccupiedTable;
    //   this.ionviewWillEnter();
    // }
    QrloginPage.prototype.goToCustomerTable = function (order_id) {
        // this.setDeviceUID();
        console.log('GOTOCUSTOMER');
        console.log(order_id);
        this.storageService.saveOrderID(order_id);
        this.route.navigate(['confirmedorderlist', order_id]);
        // console.log('hello');
    };
    QrloginPage.prototype.getOrderByTableNo = function (tableno) {
        var _this = this;
        this.qrService.getOrderByTableNo(tableno).then(function (data) {
            _this.orders = data.details;
        });
        console.log(tableno);
    };
    QrloginPage.prototype.getTableStatusNotPaid = function () {
        var _this = this;
        this.deviceTable.getTableStatusNotPaid().then(function (data) {
            _this.tabless = data.tables;
            console.log(_this.tabless);
        });
    };
    // optionsFn(){
    //   return this.selectedTable;
    // //  console.log(this.selectedTable);
    // }
    QrloginPage.prototype.startScan = function () {
        var _this = this;
        //this.no = this.optionsFn();
        //  console.log(selectedTable);
        this.qr.prepare().then(function (status) {
            if (status.authorized) {
                _this.isOn = true;
                document.getElementsByTagName('body')[0].style.opacity = ' 0 ';
                var scanSub_1 = _this.qr.scan().subscribe(function (id) {
                    console.log('You scanned', id);
                    _this.isOn = false;
                    _this.QRSCANNED_DATA = id;
                    if (_this.QRSCANNED_DATA != null) {
                        _this.qrService.verifyQRLogin(_this.QRSCANNED_DATA).then(function (data) {
                            console.log(data);
                            _this.infos = data.employee_info;
                            console.log(_this.infos);
                            var route = 'qrlogin';
                            var isAuthenticated = false;
                            for (var _i = 0, _a = _this.infos; _i < _a.length; _i++) {
                                var info = _a[_i];
                                _this.authService.login(info.sessionid);
                                _this.position = info.emp_pos;
                                console.log(_this.position);
                                _this.empid = info.emp_id;
                                isAuthenticated = _this.authService.isAuthenticated();
                                if (_this.position === 'waiter') {
                                    console.log('HELLO');
                                    _this.router.navigate(['/table', _this.empid]);
                                }
                                break;
                            }
                            _this.closeScanner();
                            scanSub_1.unsubscribe();
                            console.log('Navigating to ' + route, _this.position, isAuthenticated);
                            //  this.ngZone.run(() => {
                            //    console.log(route, this);
                            //    this.router.navigate([route]);
                            //  });
                        });
                    }
                    // this.closeScanner();
                    // scanSub.unsubscribe();
                });
                console.log('qrShow');
                _this.qr.show();
            }
            else if (status.denied) {
                console.log('Denied');
                _this.qrScan = _this.qr.scan().subscribe(function (text) {
                    document.getElementsByTagName('body')[0].style.opacity = '1';
                    _this.qrScan.unsubscribe();
                    _this.dialog.alert(text);
                }, function (err) {
                    _this.dialog.alert(JSON.stringify(err));
                });
            }
        })
            .catch(function (e) { return console.log('Error is', e); });
    };
    QrloginPage.prototype.getStatus = function (status) {
        return this.status = status;
    };
    QrloginPage.prototype.closeScanner = function () {
        this.isOn = false;
        this.qr.hide();
        this.qr.destroy();
        document.getElementsByTagName('body')[0].style.opacity = '1';
    };
    QrloginPage.prototype.successLogin = function (data) {
        this.qrService.saveOrdersData(this.orderInfo);
    };
    QrloginPage.prototype.random = function () {
        var rand = Math.floor(Math.random() * 20) + 1;
        return rand;
    };
    QrloginPage.prototype.setCustId = function (id) {
        this.custid = id;
    };
    QrloginPage.prototype.getCustId = function () {
        console.log(this.custid);
    };
    QrloginPage.prototype.getOccupiedTable = function () {
        var _this = this;
        this.qrService.getOccupiedTable().then(function (data) {
            _this.occupiedtables = data.OccupiedTables;
        });
    };
    QrloginPage.prototype.saveTransactionInfo = function () {
        var _this = this;
        this.deviceinfo.custid = this.random();
        this.deviceinfo.empid = this.empid;
        this.deviceinfo.date_ordered = Date.now();
        this.deviceinfo.tableno = this.no;
        this.adminService.postToOrder(this.deviceinfo).then(function (data) {
            _this.storageService.saveOrderID(data.order_id);
            console.log(_this.deviceinfo);
            //   console.log(this.storageService.saveOrderID((data as any).order_id);
        });
        console.log(this.no);
        this.setTableOccupied();
        //});
    };
    QrloginPage.prototype.getAllAvailableTableNo = function () {
        var _this = this;
        this.devicetable.getAllAvailableTable().then(function (data) {
            _this.tables = data.AvailableTables;
            console.log(_this.tables);
        });
    };
    QrloginPage.prototype.getTableNo = function () {
        return this.selectedTable;
    };
    QrloginPage.prototype.setTableNo = function (selectedTable) {
        this.selectedTable = selectedTable;
        // this.startScan();
        // console.log(this.selectedTable);
    };
    QrloginPage.prototype.setTableOccupied = function () {
        this.tableStatus.tableno = this.selectedTable;
        this.tableStatus.status = 'Occupied';
        this.qrService.setTableStatus(this.tableStatus);
        console.log('set table status');
    };
    QrloginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-qrlogin',
            template: __webpack_require__(/*! ./qrlogin.page.html */ "./src/app/qrlogin/qrlogin.page.html"),
            styles: [__webpack_require__(/*! ./qrlogin.page.scss */ "./src/app/qrlogin/qrlogin.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _ionic_native_dialogs_ngx__WEBPACK_IMPORTED_MODULE_2__["Dialogs"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_3__["QRScanner"],
            _app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__["QrloginService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__["AuthenticationService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _ionic_native_uid_ngx__WEBPACK_IMPORTED_MODULE_10__["Uid"],
            _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_6__["AdminService"],
            _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__["DeviceTableService"],
            src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_11__["StorageserviceService"],
            _services_device_table_service__WEBPACK_IMPORTED_MODULE_9__["DeviceTableService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]])
    ], QrloginPage);
    return QrloginPage;
}());



/***/ })

}]);
//# sourceMappingURL=qrlogin-qrlogin-module.js.map