(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"],{

/***/ "./src/app/waiter/tab2/tab2.module.ts":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.module.ts ***!
  \********************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab2.page */ "./src/app/waiter/tab2/tab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.html":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-title text-center>Meals</ion-title>\n      \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <ion-slides>\n        <ion-slide>\n            <ion-grid>\n                    <!-- <ion-row><ion-col><ion-text>Ready To Serve</ion-text></ion-col></ion-row> -->\n                <ion-row>\n                    <ion-col size=\"3\" *ngFor=\"let card of toCook\">\n                        <ion-card (click)=\"showConfirm(card.id)\" [ngStyle]=\"{'background' : cardBackgroundColor, 'color': white}\" tappable >\n                            <ion-card-content>\n                                <ion-row>\n                                    <ion-text id = \"status\" >STATUS: {{ card.status }}</ion-text>\n                                </ion-row>\n                                    <ion-text id=\"tableNum\">Table No. {{ card.tableno }}</ion-text>\n                                <ion-row>\n                                    <div >\n                                        <ion-col><ion-text>• {{ card.name }} --</ion-text></ion-col>\n                                        <ion-col text-end><ion-text>{{ card.quantity }}</ion-text></ion-col>\n                                    </div>\n                                </ion-row>\n                            </ion-card-content>\n                        </ion-card>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-slide>\n        <ion-slide>\n            <ion-grid>\n                <ion-row><ion-col><ion-text>Served Orders</ion-text></ion-col></ion-row>\n                <ion-row>\n                        <!-- <ion-card *ngFor=\"let item of allServedItems, let i = index\">\n                            <ion-card-content>\n                                <ion-text id=\"tableNum\">Table No. {{ item.tableno }}</ion-text>\n                                <ion-row>\n                                    <div text-start>\n                                        <ion-col><ion-text>• {{ item.name }} -</ion-text></ion-col>\n                                        <ion-col text-end><ion-text>{{ item.orderQty }}</ion-text></ion-col>\n                                    </div>\n                                </ion-row>\n                            </ion-card-content>\n                        </ion-card> -->\n                    <ion-col size=\"3\" *ngFor=\"let card of servedCard, let i = index\">\n                        <ion-card class=\"card\">\n                            <ion-card-content>\n                                <ion-text id=\"tableNum\">Table No. {{ numContainer[i] }}</ion-text>\n                                <ion-row>\n                                    <div text-start *ngFor=\"let c of card\">\n                                        <ion-col><ion-text>• {{ c.name }} -</ion-text></ion-col>\n                                        <ion-col text-end><ion-text>{{ c.quantity }}</ion-text></ion-col>\n                                    </div>\n                                </ion-row>\n                            </ion-card-content>\n                        </ion-card>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-slide>\n    </ion-slides>\n    <ion-button (click)=\"refreshpage()\">REFRESH</ion-button>\n</ion-content>"

/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.scss":
/*!********************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#status {\n  color: blue; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvd2FpdGVyL3RhYjIvRDpcXFRoZXNpc1xcQ2F0ZXJVQXBwL3NyY1xcYXBwXFx3YWl0ZXJcXHRhYjJcXHRhYjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvd2FpdGVyL3RhYjIvdGFiMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc3RhdHVze1xyXG4gICAgY29sb3I6IGJsdWU7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/waiter/tab2/tab2.page.ts":
/*!******************************************!*\
  !*** ./src/app/waiter/tab2/tab2.page.ts ***!
  \******************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_waiter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/waiter.service */ "./src/app/waiter/service/waiter.service.ts");
/* harmony import */ var src_app_confirmedorderlist_confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/confirmedorderlist/confirmedorderlist.service */ "./src/app/confirmedorderlist/confirmedorderlist.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var Tab2Page = /** @class */ (function () {
    function Tab2Page(waiterService, confirmedOrderList, router, alertCtrl) {
        this.waiterService = waiterService;
        this.confirmedOrderList = confirmedOrderList;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.readyList = [];
        this.orderCard = [];
        this.servedCard = [];
        this.counter = 0;
        this.tableNo = [];
        this.allServedItems = [];
        this.numContainer = [];
        this.cardBackgroundColor = 'white';
        this.cardColor = 'black';
        // orderCard: string[] = [];
        this.flag = false;
        this.status = {};
    }
    Tab2Page.prototype.ngOnInit = function () {
    };
    Tab2Page.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    Tab2Page.prototype.ionViewWillEnter = function () {
        this.getToCookOrders();
    };
    Tab2Page.prototype.getToCookOrders = function () {
        var _this = this;
        this.waiterService.getWaitingOrders().then(function (data) {
            _this.toCook = data.orders;
            console.log('To Cook', _this.toCook);
        });
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! ./tab2.page.html */ "./src/app/waiter/tab2/tab2.page.html"),
            styles: [__webpack_require__(/*! ./tab2.page.scss */ "./src/app/waiter/tab2/tab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_waiter_service__WEBPACK_IMPORTED_MODULE_2__["WaiterService"],
            src_app_confirmedorderlist_confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmedorderlistService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab2-tab2-module.js.map