(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-menu-orderlist2-orderlist2-module"],{

/***/ "./src/app/customer/classes/order-list.ts":
/*!************************************************!*\
  !*** ./src/app/customer/classes/order-list.ts ***!
  \************************************************/
/*! exports provided: OrderList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderList", function() { return OrderList; });
// orderlist is the data after PLACE ORDER
//first 7 data from DB
var OrderList = /** @class */ (function () {
    function OrderList() {
        this.servedQty = 0;
        this.status = 'waiting';
    }
    return OrderList;
}());



/***/ }),

/***/ "./src/app/customer/menu/orderlist2/orderlist2.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/orderlist2/orderlist2.module.ts ***!
  \***************************************************************/
/*! exports provided: Orderlist2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Orderlist2PageModule", function() { return Orderlist2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _orderlist2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orderlist2.page */ "./src/app/customer/menu/orderlist2/orderlist2.page.ts");







var routes = [
    {
        path: '',
        component: _orderlist2_page__WEBPACK_IMPORTED_MODULE_6__["Orderlist2Page"]
    }
];
var Orderlist2PageModule = /** @class */ (function () {
    function Orderlist2PageModule() {
    }
    Orderlist2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_orderlist2_page__WEBPACK_IMPORTED_MODULE_6__["Orderlist2Page"]]
        })
    ], Orderlist2PageModule);
    return Orderlist2PageModule;
}());



/***/ }),

/***/ "./src/app/customer/menu/orderlist2/orderlist2.page.html":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/orderlist2/orderlist2.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button></ion-back-button>\n      </ion-buttons>\n      <ion-title>Order List</ion-title>\n      <ion-buttons>\n\n      </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n  \n<ion-content>\n  <ion-grid>\n      <ion-row>\n          <ion-col>\n              <ion-card class=\"align-center\" id=\"menuCard\">\n                  <!-- <ion-card-header>\n                      <ion-text>Table No. 1 Order List</ion-text>\n                  </ion-card-header> -->\n                  <ion-card-content>\n                    <ion-row text-center>\n                        <ion-col class=\"thin-col-width\"></ion-col>\n                        <ion-col>Name</ion-col>\n                        <ion-col>Price</ion-col>\n                        <ion-col>Quantity</ion-col>\n                    </ion-row>\n                    <div *ngFor=\"let item of oList; let i = index\"> \n                      <ion-row text-center *ngIf=\"item.status === 'waiting' && item.unservedOrderQty != 0\">\n                      <!-- <ion-row> -->\n                        <ion-col class=\"thin-col-width\">\n                          <ion-checkbox color=\"dark\" [(ngModel)]=\"item.isChecked\"></ion-checkbox>\n                        </ion-col>\n                        <ion-col>{{ item.name }}</ion-col>\n                        <ion-col>₱{{ item.price }}</ion-col>\n                        <ion-col>{{ item.unservedOrderQty }}</ion-col>\n                      </ion-row>\n                    </div>\n                  </ion-card-content>\n              </ion-card>\n              <ion-row>\n                  <ion-col text-center>\n                      <ion-button (click)=\"serve()\" color=\"dark\" [disabled]=\"disableServedButton\"><ion-icon name=\"checkmark\"></ion-icon>Served</ion-button>   \n                  </ion-col>\n              </ion-row>\n              <!-- S E R V E  I N P U T -->\n              <ion-card id=\"qtyCard\" *ngIf=\"qtyCard\" text-center>\n                  <ion-input [(ngModel)]=\"servedQty\" placeholder=\"Enter quantity\"></ion-input>\n                  <ion-button color=\"dark\" (click)=\"qtyCardInputSubmit()\"><ion-icon name=\"checkmark\"></ion-icon>Ok</ion-button>\n              </ion-card>\n              <!-- /S E R V E  I N P U T -->\n\n              <!-- S  E R V E D  I T E M S  C A R D -->\n              <ion-card id=\"menuCard\">\n                  <ion-card-header>\n                          <ion-text>Served Items</ion-text>\n                      </ion-card-header>\n                      <ion-card-content>\n                          <ion-row text-center>\n                              <ion-col class=\"thin-col-width\"></ion-col>\n                              <ion-col>Name</ion-col>\n                              <ion-col>Price</ion-col>\n                              <ion-col>Quantity</ion-col>\n                          </ion-row>\n                          <div *ngFor=\"let serveditem of servedItems; let i= index\" >\n                              <!-- <ion-row text-center *ngIf=\"serveditem.status === 'served'\"> -->\n                              <ion-row text-center>\n                                  <ion-col>{{ serveditem.name }}</ion-col>\n                                  <ion-col>₱{{ serveditem.price }}</ion-col>\n                                  <ion-col>{{ serveditem.servedQty }}</ion-col>\n                              </ion-row>\n                          </div>\n                      </ion-card-content>\n              </ion-card>\n              <!-- /S E R V E D  I T E M S  C A R D -->\n              <ion-row>\n                <ion-col text-end>\n                  <ion-button color=\"dark\" (click)=\"showBilloutPage()\">Bill Out</ion-button>\n                </ion-col>\n              </ion-row>\n          </ion-col>\n      </ion-row>\n  </ion-grid>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\" >\n      <ion-fab-button color=\"dark\"><ion-icon name=\"arrow-dropup\"></ion-icon></ion-fab-button>\n\n      <ion-fab-list side=\"top\">\n        <ion-fab-button color=\"dark\" routerLink=\"/menu\"><ion-icon name=\"list-box\"></ion-icon></ion-fab-button>\n        <ion-label>Menu</ion-label>\n        <ion-fab-button color=\"dark\" (click)=\"callWaiterAlert()\"><ion-icon name=\"call\"></ion-icon></ion-fab-button>\n        <ion-label>Call a Waiter</ion-label>\n        <ion-fab-button color=\"dark\" [routerLink]=\"['/table-clear', this.tableNumber]\">\n          \n        </ion-fab-button>\n        <ion-label>Clear Table</ion-label>\n      </ion-fab-list>\n  </ion-fab>\n</ion-content>\n\n<!-- <ion-content>\n  <ion-input type=\"text\" [(ngModel)]=\"postData\"></ion-input>  \n  <ion-button (click)=\"onClick()\">\n      Click me\n  </ion-button>\n</ion-content> -->"

/***/ }),

/***/ "./src/app/customer/menu/orderlist2/orderlist2.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/customer/menu/orderlist2/orderlist2.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyL21lbnUvb3JkZXJsaXN0Mi9vcmRlcmxpc3QyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/customer/menu/orderlist2/orderlist2.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/customer/menu/orderlist2/orderlist2.page.ts ***!
  \*************************************************************/
/*! exports provided: Orderlist2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Orderlist2Page", function() { return Orderlist2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_customer_services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/customer/services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_customer_classes_order_detail__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/customer/classes/order-detail */ "./src/app/customer/classes/order-detail.ts");
/* harmony import */ var src_app_customer_classes_order_list__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/customer/classes/order-list */ "./src/app/customer/classes/order-list.ts");







var Orderlist2Page = /** @class */ (function () {
    function Orderlist2Page(alertController, adminService, router) {
        this.alertController = alertController;
        this.adminService = adminService;
        this.router = router;
        this.items = [];
        this.oList = [];
        this.unserved = true;
        this.checkedItem = [];
        this.servedItems = [];
        this.servedQtySet = false;
        this.qtyCard = false; // card for quantity input
        this.templist = [];
        this.tempservedlist = [];
    }
    Orderlist2Page.prototype.ngOnInit = function () {
        // this.route.paramMap.subscribe(params => {
        //   this.order_id = Number(params.get('order_id'));
        // });
        this.order_id = this.adminService.getOrderID();
        console.log(this.order_id);
        this.setOrderList(this.order_id);
    };
    // ionViewDidEnter() {
    //   // this.oList = this.adminService.getterOrderList();
    //   // console.log(this.oList);
    //   // this.showOrderList();
    // }
    Orderlist2Page.prototype.showBilloutPage = function () {
        this.router.navigateByUrl('/billout/' + this.order_id);
    };
    Orderlist2Page.prototype.callWaiterAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: null,
                            subHeader: null,
                            message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Orderlist2Page.prototype.setOrderList = function (id) {
        var _this = this;
        // this.oListPromise = this.adminService.getOL(this.order_id);
        this.adminService.getOL(this.order_id).then(function (olist) {
            _this.templist = olist.list;
            console.log('GET WAITING LIST FROM SERVICE', olist);
            // tslint:disable-next-line: prefer-for-of
            for (var index = 0; index < _this.templist.length; index++) {
                _this.orderItem = new src_app_customer_classes_order_list__WEBPACK_IMPORTED_MODULE_6__["OrderList"]();
                console.log(_this.templist[index]);
                _this.orderItem.detail_id = _this.templist[index].detail_id;
                _this.orderItem.menuID = _this.templist[index].menuID;
                _this.orderItem.menuName = _this.templist[index].menuName;
                _this.orderItem.orderQty = _this.templist[index].orderQty;
                _this.orderItem.order_id = _this.templist[index].order_id;
                _this.orderItem.subtotal = _this.templist[index].subtotal;
                _this.orderItem.tableno = _this.templist[index].tableno;
                _this.tableNumber = _this.templist[0].tableno;
                _this.orderItem.unservedOrderQty = _this.templist[index].orderQty;
                var menuitem = new src_app_customer_classes_order_detail__WEBPACK_IMPORTED_MODULE_5__["OrderDetail"]();
                menuitem = _this.adminService.getMenuByID(_this.orderItem.menuID);
                _this.orderItem.price = menuitem.price;
                _this.orderItem.name = menuitem.name;
                _this.oList.push(_this.orderItem);
            }
            _this.adminService.setTableNumber(_this.tableNumber);
            console.log(_this.oList);
        });
    };
    // assignList(templist) {
    //   for (const item of templist) {
    //     console.log(item.detail_id);
    //     this.orderItem.detail_id = (item as any).detail_id;
    //     this.orderItem.menuID = (item as any).menuID;
    //     this.orderItem.menuName = (item as any).menuName;
    //     this.orderItem.orderQty = (item as any).orderQty;
    //     this.orderItem.order_id = (item as any).order_id;
    //     this.orderItem.subtotal = (item as any).subtotal;
    //     this.orderItem.tableno = (item as any).tableno;
    //     this.orderItem.unservedOrderQty = (item as any).orderQty;
    //     const menuitem = this.adminService.getMenuByID(this.orderItem.menuID);
    //     this.orderItem.price = menuitem.price;
    //     this.oList.push(this.orderItem);
    //   }
    //   console.log(this.oList);
    // }
    Orderlist2Page.prototype.serve = function () {
        // this.unserved = !this.unserved;
        // get checkbox value
        this.checkedItem = this.oList.filter(function (value) {
            return value.isChecked;
        });
        console.log(this.checkedItem, this.checkedItem.length);
        // process quantity of value
        for (var index = 0; index < this.checkedItem.length; index++) {
            if (this.checkedItem[index].unservedOrderQty > 1) {
                // run quantity input
                this.qtyCard = true; // show input quantity card
                this.itemQty = this.checkedItem[index].unservedOrderQty;
                // this.qtyCardInputSubmit(item.orderQty);
            }
            else if (this.checkedItem[index].unservedOrderQty === 1) {
                console.log('1 quantity to serve');
                if (this.checkedItem[index].orderQty > 1) {
                    this.checkedItem[index].servedQty = this.checkedItem[index].orderQty;
                    this.adminService.setStatus(this.checkedItem[index]);
                    this.servedListProcess(this.checkedItem[index]);
                }
                else {
                    this.checkedItem[index].servedQty = 1;
                    this.adminService.setStatus(this.checkedItem[index]);
                    this.servedListProcess(this.checkedItem[index]);
                    for (var _i = 0, _a = this.oList; _i < _a.length; _i++) {
                        var i = _a[_i];
                        if (i.detail_id === this.checkedItem[index].detail_id) {
                            this.oList.splice(this.oList.indexOf(i), 1);
                        }
                    }
                }
                // this.setServedList(this.checkedItem[index]);
            }
            if (this.checkedItem[index].status === 'served') {
                console.log('if condition served');
                this.checkedItem[index].servedQty = this.checkedItem[index].orderQty;
                this.adminService.setStatus(this.checkedItem[index]);
                // this.setServedList(this.checkedItem[index]);
                this.checkedItem[index].unservedOrderQty = index;
            }
            this.checkedItem[index].isChecked = false;
        }
        // this.adminService.setStatus(this.servedItems);
    };
    Orderlist2Page.prototype.qtyCardInputSubmit = function () {
        // console.log('quantity served: ' + this.servedQty);
        // console.log('itemQty' + ' ' + this.itemQty);
        if (this.servedQty >= 1 && this.servedQty <= this.itemQty) {
            var qtyLeft = this.itemQty - this.servedQty;
            this.checkedItem[0].unservedOrderQty = qtyLeft;
            this.checkedItem[0].servedQty = this.servedQty;
            this.servedListProcess(this.checkedItem[0]);
            // this.setServedItem(this.checkedItem[0]);
            if (this.checkedItem[0].unservedOrderQty === 0) {
                this.checkedItem[0].status = 'served';
                this.checkedItem[0].servedQty = this.checkedItem[0].orderQty;
                this.adminService.setStatus(this.checkedItem[0]);
                this.servedListProcess(this.checkedItem[0]);
            }
        }
        else {
            console.log('invalid');
        }
        this.qtyCard = false;
    };
    Orderlist2Page.prototype.servedListProcess = function (item) {
        var _this = this;
        this.servedListPromise = this.adminService.getServedOL(item.order_id);
        this.servedListPromise.then(function (servedlist) {
            _this.tempservedlist = servedlist.list;
            console.log(_this.tempservedlist);
            for (var _i = 0, _a = _this.tempservedlist; _i < _a.length; _i++) {
                var listItem = _a[_i];
                if (listItem.detail_id === item.detail_id) {
                    _this.servedItem = new src_app_customer_classes_order_list__WEBPACK_IMPORTED_MODULE_6__["OrderList"]();
                    _this.servedItem.detail_id = listItem.detail_id;
                    _this.servedItem.menuID = listItem.menuID;
                    _this.servedItem.menuName = listItem.menuName;
                    _this.servedItem.orderQty = listItem.orderQty;
                    _this.servedItem.order_id = listItem.order_id;
                    _this.servedItem.subtotal = listItem.subtotal;
                    _this.servedItem.status = listItem.status;
                    _this.servedItem.tableno = listItem.tableno;
                    _this.servedItem.unservedOrderQty = listItem.orderQty;
                    _this.servedItem.servedQty = item.servedQty;
                    var menuitem = new src_app_customer_classes_order_detail__WEBPACK_IMPORTED_MODULE_5__["OrderDetail"]();
                    menuitem = _this.adminService.getMenuByID(_this.servedItem.menuID);
                    _this.servedItem.price = menuitem.price;
                    _this.servedItem.name = menuitem.name;
                    console.log('S E R V E D ', _this.servedItem);
                    _this.servedItems.push(_this.servedItem);
                    console.log('served items', _this.servedItems);
                }
            }
            // this.setServedItems(this.servedItems);
        });
    };
    Orderlist2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-orderlist2',
            template: __webpack_require__(/*! ./orderlist2.page.html */ "./src/app/customer/menu/orderlist2/orderlist2.page.html"),
            styles: [__webpack_require__(/*! ./orderlist2.page.scss */ "./src/app/customer/menu/orderlist2/orderlist2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            src_app_customer_services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], Orderlist2Page);
    return Orderlist2Page;
}());



/***/ })

}]);
//# sourceMappingURL=customer-menu-orderlist2-orderlist2-module.js.map