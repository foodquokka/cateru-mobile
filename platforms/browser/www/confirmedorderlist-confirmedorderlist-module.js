(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["confirmedorderlist-confirmedorderlist-module"],{

/***/ "./src/app/confirmedorderlist/confirmedorderlist.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.module.ts ***!
  \*****************************************************************/
/*! exports provided: ConfirmedorderlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedorderlistPageModule", function() { return ConfirmedorderlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./confirmedorderlist.page */ "./src/app/confirmedorderlist/confirmedorderlist.page.ts");







var routes = [
    {
        path: '',
        component: _confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmedorderlistPage"]
    }
];
var ConfirmedorderlistPageModule = /** @class */ (function () {
    function ConfirmedorderlistPageModule() {
    }
    ConfirmedorderlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_confirmedorderlist_page__WEBPACK_IMPORTED_MODULE_6__["ConfirmedorderlistPage"]]
        })
    ], ConfirmedorderlistPageModule);
    return ConfirmedorderlistPageModule;
}());



/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header> \n    <ion-toolbar color=\"dark\">\n        <!-- <ion-buttons slot=\"end\">\n            <ion-button>REFRESH</ion-button>\n        </ion-buttons> -->\n       \n        <ion-title>My Order List</ion-title> \n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    \n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <ion-card class=\"align-center\" id=\"menuCard\">\n                    <ion-card-content text-center>\n                        <ion-row text-center>\n                            <ion-col class=\"thin-col-width\"></ion-col>\n                            <ion-col>Name</ion-col>\n                            <ion-col>Price</ion-col>\n                            <ion-col>Quantity</ion-col>\n                           \n                            <ion-col>Action</ion-col>\n                        </ion-row>\n                        <div >\n                            <!-- <ion-row text-center *ngIf=\"item.status === 'waiting' && item.orderQty > 0\"> -->\n                                <ion-row text-center *ngFor=\"let item of orderlist;let ind = index\" >\n                                <ion-col>{{ item.name }}</ion-col> \n                                <ion-col>{{ item.price }}</ion-col>\n                                <ion-col>{{ item.orderNum }}</ion-col>  \n                              \n                                <ion-col>\n                                    <ion-button  (click)=\"serve(item.id)\"[disabled]=\"false\">SERVE</ion-button> \n                                </ion-col>\n                            </ion-row>\n                     </div>\n                    </ion-card-content>\n                 <!-- (click)=\"changeText(item.status)\" -->\n                </ion-card>\n                <h2> TOTAL: {{ total }}</h2>\n              <ion-button (click)=\"presentAlert()\" >BILLOUT</ion-button>\n              <ion-button (click)=\"refreshpage()\">REFRESH</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\" >\n        <ion-fab-button color=\"dark\"><ion-icon name=\"arrow-dropup\"></ion-icon></ion-fab-button>\n\n        <ion-fab-list side=\"top\">\n          <ion-fab-button color=\"dark\" routerLink=\"/menu\"><ion-icon name=\"list-box\"></ion-icon></ion-fab-button>\n          <ion-label>Menu</ion-label>\n          <ion-fab-button color=\"dark\" ><ion-icon name=\"call\"></ion-icon></ion-fab-button>\n          <ion-label>Call a Waiter</ion-label>\n        </ion-fab-list>\n    </ion-fab>\n</ion-content>"

/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmZpcm1lZG9yZGVybGlzdC9jb25maXJtZWRvcmRlcmxpc3QucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/confirmedorderlist/confirmedorderlist.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/confirmedorderlist/confirmedorderlist.page.ts ***!
  \***************************************************************/
/*! exports provided: ConfirmedorderlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmedorderlistPage", function() { return ConfirmedorderlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./confirmedorderlist.service */ "./src/app/confirmedorderlist/confirmedorderlist.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _customer_services_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../customer/services/cart.service */ "./src/app/customer/services/cart.service.ts");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/fesm5/ngx-socket-io.js");
/* harmony import */ var _cashier_service_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../cashier/service/api.service */ "./src/app/cashier/service/api.service.ts");
/* harmony import */ var _confirmedlistmodal_confirmedlistmodal_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../confirmedlistmodal/confirmedlistmodal.page */ "./src/app/confirmedlistmodal/confirmedlistmodal.page.ts");
/* harmony import */ var _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../customer/services/admin.service */ "./src/app/customer/services/admin.service.ts");










var ConfirmedorderlistPage = /** @class */ (function () {
    function ConfirmedorderlistPage(router, confirmedorderlistservice, alertController, ngZone, events, cartservice, socket, apiservice, modalController, route, adminService) {
        this.router = router;
        this.confirmedorderlistservice = confirmedorderlistservice;
        this.alertController = alertController;
        this.ngZone = ngZone;
        this.events = events;
        this.cartservice = cartservice;
        this.socket = socket;
        this.apiservice = apiservice;
        this.modalController = modalController;
        this.route = route;
        this.adminService = adminService;
        this.confirm = "CONFIRM";
        this.orderArray = [];
        this.id = {};
        this.status = {};
        this.settotal = {};
        this.bill = {};
        this.total = 0;
        this.finaltotal = 0;
        this.orderID = {};
        this.served = {};
    }
    ConfirmedorderlistPage.prototype.ngOnInit = function () {
        var _this = this;
        this.router.paramMap.subscribe(function (params) {
            _this.orderid = Number(params.get('order_id'));
            console.log(_this.orderid);
        });
        this.getConfirmedOrderedList();
    };
    ConfirmedorderlistPage.prototype.refreshpage = function () {
        this.ionViewWillEnter();
    };
    ConfirmedorderlistPage.prototype.ionViewWillEnter = function () {
        // this.getConfirmedOrderedList();
        this.getTotal();
        this.orderID.order_id = this.orderid;
    };
    ConfirmedorderlistPage.prototype.ionViewDidEnter = function () {
        this.getConfirmedOrderedList();
        this.orderID.order_id = this.orderid;
        this.cartservice.removeItem(this.orderID);
    };
    ConfirmedorderlistPage.prototype.getConfirmedOrderedList = function () {
        var _this = this;
        this.confirmedorderlistservice.getOrderByID(this.orderid).then(function (data) {
            _this.orderlist = data.orders;
        });
        //this.saveToKitchen();
    };
    ConfirmedorderlistPage.prototype.getTotal = function () {
        var _this = this;
        this.apiservice.getTotal(this.orderid).then(function (data) {
            _this.total = data.total;
        });
    };
    ConfirmedorderlistPage.prototype.serveItem = function (id) {
        this.confirmedorderlistservice.removeServeItem(id);
    };
    ConfirmedorderlistPage.prototype.changeOrderStatus = function (id) {
        //this.id.id = id
        this.confirmedorderlistservice.changeKitchenStatus(id);
        this.confirmedorderlistservice.changeOrderStatusToServed(id);
        console.log(id);
        //   const alert = await this.alertController.create({
        //   header: 'Serve Menu Now!',
        //   inputs: [
        //     {
        //       name: 'Quantity',
        //       placeholder: this.qty
        //     }
        //   ],
        //   buttons: [
        //     {
        //       text: 'Cancel',
        //       role: 'cancel',
        //       cssClass: 'secondary',
        //       handler: (blah) => {
        //         console.log('Confirm Cancel: blah');
        //       }
        //     }, {
        //       text: 'Serve Now',
        //       handler: (data) => {
        //      //  console.log(this.orderlist[ind].id);
        //         if (data.Quantity <= this.orderlist[ind].quantity) {
        //           this.qty = data.Quantity; 
        //           console.log(this.qty);
        //           console.log(this.orderlist[ind].quantity)
        //           if(this.qty == this.orderlist[ind].quantity){
        //           this.confirmedorderlistservice.changeOrderStatusToServed(this.orderlist[ind].id);
        //           }
        //         } else {
        //           console.log('Invalid data');
        //         }
        //       }
        //     }
        //   ]
        // });
        //   await alert.present();
    };
    ConfirmedorderlistPage.prototype.presentModal = function (orderid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _confirmedlistmodal_confirmedlistmodal_page__WEBPACK_IMPORTED_MODULE_8__["ConfirmedlistmodalPage"],
                            componentProps: { orderid: orderid },
                            cssClass: 'my-custom-modal-css'
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ConfirmedorderlistPage.prototype.serve = function (id) {
        this.confirmedorderlistservice.removeServeItem(id);
        // console.log(data);
        // for(const d of data){
        //   this.served.id = d.id;
        //   this.served.menuID = d.menuID;
        //   this.served.order_id = d.order_id;
        //   this.confirmedorderlistservice.removeServeItem(d.id);
        // }
        // this.served.id = kitchenid;
        // this.served.menuID = 
        // this.confirmedorderlistservice.changeKitchenStatus(data);
    };
    ConfirmedorderlistPage.prototype.presentAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Confirm billout',
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah');
                                    }
                                }, {
                                    text: 'OK',
                                    handler: function () {
                                        _this.billout();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ConfirmedorderlistPage.prototype.billout = function () {
        // const modal = await this.modalController.create({
        //   component: ConfirmedlistmodalPage,
        //   componentProps: { menuID: order_id},
        //   cssClass: 'my-custom-modal-css'
        // });
        // await modal.present();
        console.log(this.orderid);
        this.bill.order_id = this.orderid;
        this.bill.status = 'billout';
        this.bill.total = this.total;
        this.confirmedorderlistservice.billout(this.bill);
        this.presentModal(this.orderid);
        //console.log('billout');
        // this.route.navigate(['/qrlogin']);
    };
    ConfirmedorderlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirmedorderlist',
            template: __webpack_require__(/*! ./confirmedorderlist.page.html */ "./src/app/confirmedorderlist/confirmedorderlist.page.html"),
            styles: [__webpack_require__(/*! ./confirmedorderlist.page.scss */ "./src/app/confirmedorderlist/confirmedorderlist.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _confirmedorderlist_service__WEBPACK_IMPORTED_MODULE_3__["ConfirmedorderlistService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"],
            _customer_services_cart_service__WEBPACK_IMPORTED_MODULE_5__["CartService"],
            ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__["Socket"],
            _cashier_service_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _customer_services_admin_service__WEBPACK_IMPORTED_MODULE_9__["AdminService"]])
    ], ConfirmedorderlistPage);
    return ConfirmedorderlistPage;
}());



/***/ })

}]);
//# sourceMappingURL=confirmedorderlist-confirmedorderlist-module.js.map