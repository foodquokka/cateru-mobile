(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["starting-page-starting-page-module"],{

/***/ "./src/app/customer/classes/order.ts":
/*!*******************************************!*\
  !*** ./src/app/customer/classes/order.ts ***!
  \*******************************************/
/*! exports provided: Order */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Order", function() { return Order; });
var Order = /** @class */ (function () {
    function Order() {
    }
    return Order;
}());



/***/ }),

/***/ "./src/app/customer/starting-page/starting-page.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/customer/starting-page/starting-page.module.ts ***!
  \****************************************************************/
/*! exports provided: StartingPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartingPagePageModule", function() { return StartingPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _starting_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./starting-page.page */ "./src/app/customer/starting-page/starting-page.page.ts");







var routes = [
    {
        path: '',
        component: _starting_page_page__WEBPACK_IMPORTED_MODULE_6__["StartingPagePage"]
    }
];
var StartingPagePageModule = /** @class */ (function () {
    function StartingPagePageModule() {
    }
    StartingPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_starting_page_page__WEBPACK_IMPORTED_MODULE_6__["StartingPagePage"]]
        })
    ], StartingPagePageModule);
    return StartingPagePageModule;
}());



/***/ }),

/***/ "./src/app/customer/starting-page/starting-page.page.html":
/*!****************************************************************!*\
  !*** ./src/app/customer/starting-page/starting-page.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n        <ion-buttons slot=\"start\">\n      <ion-button slot=\"start\">\n          <ion-icon slot=\"icon-only\" name=\"log-out\"></ion-icon>\n        <!-- <ion-menu-button autoHide=\"false\"></ion-menu-button> -->\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Starting Page</ion-title>\n    </ion-toolbar>\n  </ion-header>\n<ion-content id=\"content\">\n  <ion-grid>\n    <div class=\"outer\">\n        <div class=\"middle\">\n          <ion-row>\n            <ion-col text-center><ion-text id=\"cateru\">CaterU</ion-text></ion-col>\n          </ion-row>\n          <!-- <ion-button color=\"dark\" (click)=\"clearTable()\">Clear Table</ion-button>remove later -->\n          <ion-card class=\"inner\">\n              <ion-row text-center>\n                <ion-col> \n                <ion-card-content>\n                  <ion-text class=\"label\">Hello {{ fname  }}</ion-text><br>\n                 \n                  <ion-item>\n                    <ion-label>Choose Available Table</ion-label>\n                    <ion-select  (ngModelChange)=\"selectedTableFn()\">\n                        <ion-select-option value=\"selected\" *ngFor=\"let selected of availabletables\">{{selected.tableno}} </ion-select-option> \n                    </ion-select>\n                    <ion-select>\n                        <ion-select-option></ion-select-option>\n                    </ion-select>\n                    \n                  </ion-item>\n                 \n                 </ion-card-content>\n                </ion-col>\n              </ion-row>\n              <ion-row text-center><ion-col>\n                  <ion-button (click)=\"startOrdering()\" class=\"startBtn\">\n                  Open Menu\n                  </ion-button>\n              </ion-col></ion-row>\n          </ion-card>\n        </div>\n    </div>    \n  </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/customer/starting-page/starting-page.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/customer/starting-page/starting-page.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#content {\n  --background: linear-gradient(#7f1a1f, #3f0f11 );\n  background-repeat: no-repeat;\n  background-size: cover; }\n\n.outer {\n  display: table;\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 100%; }\n\n.middle {\n  display: table-cell;\n  vertical-align: middle; }\n\n.inner {\n  margin-left: auto;\n  margin-right: auto;\n  width: 50%;\n  height: 200px;\n  padding: 20px 0;\n  border-style: solid; }\n\n#cateru {\n  color: #ffffff;\n  font-size: 110px;\n  font-family: billionthine; }\n\n.label {\n  color: #ffffff;\n  font-size: 130%; }\n\n.inputBox {\n  --background: #d3d3d3;\n  width: 50%;\n  border-radius: 5px;\n  margin-left: 24.5%;\n  margin-top: 15px;\n  color: #000000; }\n\n.startBtn {\n  --background: #8d272c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3VzdG9tZXIvc3RhcnRpbmctcGFnZS9EOlxcVGhlc2lzXFxDYXRlclVBcHAvc3JjXFxhcHBcXGN1c3RvbWVyXFxzdGFydGluZy1wYWdlXFxzdGFydGluZy1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdJLGdEQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLHNCQUFxQixFQUFBOztBQUd6QjtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87RUFDUCxZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdmO0VBQ0ksbUJBQW1CO0VBQ25CLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLGFBQWE7RUFDYixlQUFlO0VBQ2YsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksY0FBYztFQUNkLGdCQUFnQjtFQUNoQix5QkFBeUIsRUFBQTs7QUFFN0I7RUFDSSxjQUFjO0VBQ2QsZUFBZSxFQUFBOztBQUVuQjtFQUNJLHFCQUFhO0VBQ2IsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFFbEI7RUFDSSxxQkFBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY3VzdG9tZXIvc3RhcnRpbmctcGFnZS9zdGFydGluZy1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb250ZW50IHtcclxuICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltZy9iYWNrZ3JvdW5kLmpwZycpO1xyXG4gICAgLy8gYmFja2dyb3VuZDogdXJsKCcuLi8uLi9pbWcvZ2V0c3RhcnQucG5nJyk7XHJcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgjN2YxYTFmLCAjM2YwZjExICk7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOmNvdmVyO1xyXG59XHJcblxyXG4ub3V0ZXIge1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5taWRkbGUge1xyXG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbn1cclxuXHJcbi5pbm5lciB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxufVxyXG5cclxuI2NhdGVydSB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTEwcHg7XHJcbiAgICBmb250LWZhbWlseTogYmlsbGlvbnRoaW5lO1xyXG59XHJcbi5sYWJlbCB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTMwJTtcclxufVxyXG4uaW5wdXRCb3gge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZDNkM2QzO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNC41JTtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxufVxyXG4uc3RhcnRCdG4ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjOGQyNzJjO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/customer/starting-page/starting-page.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/customer/starting-page/starting-page.page.ts ***!
  \**************************************************************/
/*! exports provided: StartingPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartingPagePage", function() { return StartingPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _classes_order__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classes/order */ "./src/app/customer/classes/order.ts");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/qrlogin/qrlogin.service */ "./src/app/qrlogin/qrlogin.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");








var StartingPagePage = /** @class */ (function () {
    function StartingPagePage(adminService, qrloginService, alertController, activeRoute, authService) {
        this.adminService = adminService;
        this.qrloginService = qrloginService;
        this.alertController = alertController;
        this.activeRoute = activeRoute;
        this.authService = authService;
        this.table = {};
        this.availabletables = {};
    }
    StartingPagePage.prototype.ngOnInit = function () {
        var _this = this;
        this.dataRcv = this.activeRoute.snapshot.paramMap.get('id');
        this.adminService.getEmpName(this.dataRcv).then(function (data) {
            _this.dataRcvs = data.data;
            for (var _i = 0, _a = _this.dataRcvs; _i < _a.length; _i++) {
                var info = _a[_i];
                _this.fname = info.name;
            }
        });
    };
    //   logout(){
    //       this.authService.logout();
    //   }
    StartingPagePage.prototype.startOrdering = function () {
        this.order = new _classes_order__WEBPACK_IMPORTED_MODULE_2__["Order"]();
        this.order.empid = this.dataRcv;
        this.order.custid = 5; // randomnumber
        this.order.tableno = 3;
        this.order.date_ordered = Date.now();
        console.log('LOGIN DETAILS: ', this.order);
        this.adminService.postToOrder(this.order);
    };
    StartingPagePage.prototype.errorAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: null,
                            subHeader: null,
                            message: 'Invalid username.',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    StartingPagePage.prototype.clearTable = function () {
        this.table = { tableno: 1 };
        this.adminService.sendToClearTable(this.table);
    };
    StartingPagePage.prototype.getAvailableTable = function () {
        var _this = this;
        this.adminService.getAvailableTable().then(function (data) {
            _this.availabletables = data.data;
            console.log(_this.availabletables);
        });
    };
    StartingPagePage.prototype.selectedTableFn = function () {
        console.log(this.selectedTable);
        var selected = this.selectedTable; // Just did this in order to avoid changing the next lines of code :P
        this.table = selected.tableno;
    };
    StartingPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-starting-page',
            template: __webpack_require__(/*! ./starting-page.page.html */ "./src/app/customer/starting-page/starting-page.page.html"),
            styles: [__webpack_require__(/*! ./starting-page.page.scss */ "./src/app/customer/starting-page/starting-page.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"],
            src_app_qrlogin_qrlogin_service__WEBPACK_IMPORTED_MODULE_5__["QrloginService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"]])
    ], StartingPagePage);
    return StartingPagePage;
}());



/***/ })

}]);
//# sourceMappingURL=starting-page-starting-page-module.js.map