(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cashier-home-home-module"],{

/***/ "./src/app/cashier/home/home.module.ts":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/cashier/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/cashier/home/home.page.html":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>  \r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Billout List\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-grid>\r\n      <ion-row *ngFor=\"let a of tables\">\r\n        <ion-col>\r\n          <ion-card (click)=\"presentModal(a.order_id)\" mode=\"ios\"> \r\n              <ion-item>\r\n                  <ion-icon name=\"alert\" slot=\"start\" color=\"danger\"></ion-icon>\r\n                  Table No: {{ a.tableno }}\r\n                </ion-item>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n<ion-footer>\r\n<ion-button button (click)=\"refreshpage()\" class=\"refreshbtn\">REFRESH</ion-button>\r\n</ion-footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/cashier/home/home.page.scss":
/*!*********************************************!*\
  !*** ./src/app/cashier/home/home.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".headertitle {\n  font-size: 50pt; }\n\n.refreshbtn {\n  width: 100%;\n  height: 100px;\n  font-size: 24px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FzaGllci9ob21lL0Q6XFxUaGVzaXNcXENhdGVyVUFwcC9zcmNcXGFwcFxcY2FzaGllclxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksV0FBVztFQUNYLGFBQWE7RUFDYixlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jYXNoaWVyL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVydGl0bGVcclxue1xyXG4gICAgZm9udC1zaXplOiA1MHB0O1xyXG59XHJcbi5yZWZyZXNoYnRue1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/cashier/home/home.page.ts":
/*!*******************************************!*\
  !*** ./src/app/cashier/home/home.page.ts ***!
  \*******************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/api.service */ "./src/app/cashier/service/api.service.ts");
/* harmony import */ var _modal_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modal/modal.page */ "./src/app/cashier/modal/modal.page.ts");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");







var HomePage = /** @class */ (function () {
    // tslint:disable-next-line: max-line-length
    function HomePage(navCtrl, apiService, router, route, modalController, authenticationService) {
        this.navCtrl = navCtrl;
        this.apiService = apiService;
        this.router = router;
        this.route = route;
        this.modalController = modalController;
        this.authenticationService = authenticationService;
        // orderrecord = [];
        this.now = new Date();
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.empid = String(params.get('username'));
            console.log(_this.empid);
        });
        this.getBillOut();
    };
    HomePage.prototype.refreshpage = function () {
        this.ionViewWIllEnter();
    };
    HomePage.prototype.ionViewWIllEnter = function () {
        this.getBillOut();
    };
    HomePage.prototype.getBillOut = function () {
        var _this = this;
        this.apiService.getBillOutDetails().then(function (data) {
            _this.tables = data.table;
        });
    };
    HomePage.prototype.orderId = function (bills) {
        console.log(bills);
        this.router.navigateByUrl('details/' + bills);
    };
    HomePage.prototype.presentModal = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modal_modal_page__WEBPACK_IMPORTED_MODULE_5__["ModalPage"],
                            componentProps: { id: id }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/cashier/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/cashier/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"]])
    ], HomePage);
    return HomePage;
}());

// getAllMenu()
// {
//   this.apiService.getBillOutDetails().then(data => {
//     this.bills = data.allMenus;
//     console.log(this.bills);
//   });
// }
// getOrderRecord()
// {
//   this.apiService.getBillOutDetails().then(data => {
//     this.orderrecord = data.orderRecord;
//     console.log(this.orderrecord);
//   });
// }
// getOrderDetails()
// {
//   this.apiService.getBillOutDetails().then(data => {
//     // this.orderdetais.push(data.orderdetails);
//     this.orderdetails = data.orderdetails;
//     // console.log(this.orderdetais);
// });


/***/ })

}]);
//# sourceMappingURL=cashier-home-home-module.js.map