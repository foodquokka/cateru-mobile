(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab3-rtab3-module"],{

/***/ "./src/app/receptionist/rtab3/rtab3.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.module.ts ***!
  \****************************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab3.page */ "./src/app/receptionist/rtab3/rtab3.page.ts");







var Tab3PageModule = /** @class */ (function () {
    function Tab3PageModule() {
    }
    Tab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _rtab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] }])
            ],
            declarations: [_rtab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
        })
    ], Tab3PageModule);
    return Tab3PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Table List\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card-content>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Table Number</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Capacity</ion-list>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-list size-md=\"4\">Status</ion-list>\r\n      </ion-col>\r\n\r\n    </ion-row>\r\n  </ion-card-content>\r\n\r\n  <ion-row *ngFor=\"let table of tables\">\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>{{ table.tableno }}</ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>{{ table.capacity }}</ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col>\r\n      <ion-list>\r\n        <ion-item>{{ table.status }}</ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n  </ion-row>\r\n\r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY2VwdGlvbmlzdC9ydGFiMy9ydGFiMy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtab3/rtab3.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab3/rtab3.page.ts ***!
  \**************************************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");



var Tab3Page = /** @class */ (function () {
    // constructor(){}
    function Tab3Page(servicesProvider) {
        this.servicesProvider = servicesProvider;
        this.getTables();
    }
    Tab3Page.prototype.getTables = function () {
        var _this = this;
        this.servicesProvider.getTables()
            .then(function (data) {
            _this.tables = data;
            _this.tables = _this.tables.allTables;
            console.log(_this.tables);
        });
    };
    Tab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab3',
            template: __webpack_require__(/*! ./rtab3.page.html */ "./src/app/receptionist/rtab3/rtab3.page.html"),
            styles: [__webpack_require__(/*! ./rtab3.page.scss */ "./src/app/receptionist/rtab3/rtab3.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab3Page);
    return Tab3Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab3-rtab3-module.js.map