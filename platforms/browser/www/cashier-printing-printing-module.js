(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cashier-printing-printing-module"],{

/***/ "./src/app/cashier/printing/printing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.module.ts ***!
  \*****************************************************/
/*! exports provided: PrintingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintingPageModule", function() { return PrintingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _printing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./printing.page */ "./src/app/cashier/printing/printing.page.ts");







var routes = [
    {
        path: '',
        component: _printing_page__WEBPACK_IMPORTED_MODULE_6__["PrintingPage"]
    }
];
var PrintingPageModule = /** @class */ (function () {
    function PrintingPageModule() {
    }
    PrintingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_printing_page__WEBPACK_IMPORTED_MODULE_6__["PrintingPage"]]
        })
    ], PrintingPageModule);
    return PrintingPageModule;
}());



/***/ }),

/***/ "./src/app/cashier/printing/printing.page.html":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>RECEIPT</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content  scrollY=\"true\">\n  <ion-card class=\"receipt\">\n      <ion-card-header>\n        <div text-center>\n          <h1>Magestic Restaurant</h1>\n          <h5>Operated by: CaterU Holdings, Inc</h5>\n          <p>Quiot Pardo -- Branch E.Sabellano St.,Quiot\n          Cebu City</p>\n          <p>Tel. No. 520-5598, 417 - 9726</p>\n          <p>VAT Reg, TIN: 008-381-043-091</p>\n          <p>SN: 41BZ416</p>\n          <p>PN: 0913-082-1562461-091</p>\n          <p>MIN: 1303852174</p>\n          <h4>OFFICIAL RECEIPT</h4>\n        </div>\n      <div text-left>\n        <p>OR No. 6125341381462</p>\n        <p>Cashier: {{ empName }}</p>\n        <p>DATE: {{ date }}  </p><p>POS No. 10926H</p>\n      </div>\n        </ion-card-header>\n        <ion-card-content >\n          <div text-center>\n          <ion-grid>\n              <ion-row style=\"border-bottom: groove; font-size: large;\">\n                  <ion-col>PRODUCT</ion-col>\n                  <ion-col>PRICE</ion-col>\n                  <ion-col>QUANTITY</ion-col>\n                  <ion-col>SUM</ion-col>\n                </ion-row>\n                <ion-row  *ngFor=\"let info of orders\" scrollY=\"true\">\n                    <ion-col>{{ info.name }}</ion-col>\n                    <ion-col>{{ info.price}}</ion-col>\n                    <ion-col>{{ info.orderQty}}</ion-col>\n                    <ion-col>{{ info.subtotal}}</ion-col>\n                  </ion-row>\n                  \n          </ion-grid>\n          <ion-row style=\"border-top: groove; font-size: large;\" text-left> </ion-row>\n          <ion-row>TOTAL:  {{ total }}</ion-row>\n            <ion-row>Cash: {{ cash}}</ion-row>\n            <ion-row>Change: {{ change }}</ion-row>\n         \n          </div>\n        </ion-card-content>\n  </ion-card>\n\n\n</ion-content>\n<!-- <ion-footer>\n  <ion-button class=\"print\" (click)=\"presentLoading()\">PRINT RECEIPT</ion-button>\n</ion-footer> -->\n"

/***/ }),

/***/ "./src/app/cashier/printing/printing.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card {\n  height: 90%;\n  width: 96%; }\n\n.print {\n  width: 100%;\n  height: 150px;\n  font-size: 24px; }\n\n.receipt {\n  overflow-y: scroll; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FzaGllci9wcmludGluZy9EOlxcVGhlc2lzXFxDYXRlclVBcHAvc3JjXFxhcHBcXGNhc2hpZXJcXHByaW50aW5nXFxwcmludGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFXO0VBQ1gsVUFBVSxFQUFBOztBQUVkO0VBQ0ksV0FBVztFQUNYLGFBQWE7RUFDYixlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jYXNoaWVyL3ByaW50aW5nL3ByaW50aW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcclxuICAgIGhlaWdodDogOTAlO1xyXG4gICAgd2lkdGg6IDk2JTtcclxufVxyXG4ucHJpbnR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbn1cclxuLnJlY2VpcHR7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/cashier/printing/printing.page.ts":
/*!***************************************************!*\
  !*** ./src/app/cashier/printing/printing.page.ts ***!
  \***************************************************/
/*! exports provided: PrintingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintingPage", function() { return PrintingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _service_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../service/api.service */ "./src/app/cashier/service/api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var PrintingPage = /** @class */ (function () {
    function PrintingPage(router, route, apiservice, datePipe, loadingController, toastCtlr) {
        this.router = router;
        this.route = route;
        this.apiservice = apiservice;
        this.datePipe = datePipe;
        this.loadingController = loadingController;
        this.toastCtlr = toastCtlr;
        this.myDate = new Date();
    }
    PrintingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.router.paramMap.subscribe(function (params) {
            _this.orderid = Number(params.get('order_id'));
        });
        console.log(this.orderid);
        //this.getReceiptDetails();
    };
    PrintingPage.prototype.ionViewWillEnter = function () {
        this.getReceiptDetails();
        this.getTotal();
        this.presentLoading();
    };
    PrintingPage.prototype.getReceiptDetails = function () {
        var _this = this;
        this.apiservice.getReceiptDetails(this.orderid).then(function (data) {
            _this.orders = data.records;
            for (var _i = 0, _a = _this.orders; _i < _a.length; _i++) {
                var info = _a[_i];
                _this.empName = info.empfirstname;
                _this.tableno = info.tableno;
                _this.date = _this.datePipe.transform(_this.myDate, 'yyyy-MM-dd');
                _this.cash = info.cashTender;
                _this.change = info.change;
            }
        });
        console.log(this.empName, this.date);
    };
    PrintingPage.prototype.getTotal = function () {
        var _this = this;
        this.apiservice.getTotal(this.orderid).then(function (data) {
            _this.total = data.total;
        });
    };
    PrintingPage.prototype.presentLoading = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, _a, role, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'printing your receipt',
                            duration: 2000,
                        })];
                    case 1:
                        loading = _b.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, loading.onDidDismiss()];
                    case 3:
                        _a = _b.sent(), role = _a.role, data = _a.data;
                        this.presentToast();
                        this.setTableAvailable();
                        this.route.navigate(['/cashier']);
                        console.log('Loading dismissed!');
                        return [2 /*return*/];
                }
            });
        });
    };
    PrintingPage.prototype.presentToast = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtlr.create({
                            message: 'Receipt printed!',
                            position: 'top',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PrintingPage.prototype.setTableAvailable = function () {
        this.apiservice.setTableAvailable(this.tableno);
    };
    PrintingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-printing',
            template: __webpack_require__(/*! ./printing.page.html */ "./src/app/cashier/printing/printing.page.html"),
            styles: [__webpack_require__(/*! ./printing.page.scss */ "./src/app/cashier/printing/printing.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _service_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], PrintingPage);
    return PrintingPage;
}());



/***/ })

}]);
//# sourceMappingURL=cashier-printing-printing-module.js.map