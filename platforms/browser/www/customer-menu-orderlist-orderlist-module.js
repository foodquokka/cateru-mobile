(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["customer-menu-orderlist-orderlist-module"],{

/***/ "./src/app/customer/classes/order-detail.ts":
/*!**************************************************!*\
  !*** ./src/app/customer/classes/order-detail.ts ***!
  \**************************************************/
/*! exports provided: OrderDetail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetail", function() { return OrderDetail; });
var OrderDetail = /** @class */ (function () {
    function OrderDetail() {
    }
    return OrderDetail;
}());



/***/ }),

/***/ "./src/app/customer/menu/orderlist/orderlist.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/customer/menu/orderlist/orderlist.module.ts ***!
  \*************************************************************/
/*! exports provided: OrderlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderlistPageModule", function() { return OrderlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _orderlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orderlist.page */ "./src/app/customer/menu/orderlist/orderlist.page.ts");







var routes = [
    {
        path: '',
        component: _orderlist_page__WEBPACK_IMPORTED_MODULE_6__["OrderlistPage"]
    }
];
var OrderlistPageModule = /** @class */ (function () {
    function OrderlistPageModule() {
    }
    OrderlistPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_orderlist_page__WEBPACK_IMPORTED_MODULE_6__["OrderlistPage"]]
        })
    ], OrderlistPageModule);
    return OrderlistPageModule;
}());



/***/ }),

/***/ "./src/app/customer/menu/orderlist/orderlist.page.html":
/*!*************************************************************!*\
  !*** ./src/app/customer/menu/orderlist/orderlist.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"dark\">\n        <ion-buttons slot=\"start\">\n            <ion-back-button></ion-back-button>\n        </ion-buttons>\n        <ion-title>Order List</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n    \n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <ion-card class=\"align-center\" id=\"menuCard\">\n                    <ion-card-content>\n                        <ion-row text-center>\n                            <ion-col class=\"thin-col-width\"></ion-col>\n                            <ion-col>Name</ion-col>\n                            <ion-col>Price</ion-col>\n                            <ion-col>Quantity</ion-col>\n                            <ion-col>Action</ion-col>\n                        </ion-row>\n                        <div *ngFor=\"let item of cart\" >\n                            <!-- <ion-row text-center *ngIf=\"item.status === 'waiting' && item.orderQty > 0\"> -->\n                                <ion-row text-center>\n                                <ion-col class=\"thin-col-width\">\n                                    <!-- <ion-checkbox color=\"dark\" [(ngModel)]=\"item.isChecked\"></ion-checkbox> -->\n                                    <ion-checkbox color=\"dark\"></ion-checkbox>\n                                </ion-col>\n                                <ion-col>{{ item.name }}</ion-col>\n                                <ion-col name=\"itemPrice\">₱{{ getTotalPerItem(item.menuID) }}</ion-col>\n                                <ion-col name=\"qty\"><ion-button (click) = \"subtractQty(item.menuID)\">-</ion-button>{{ item.qty }}<ion-button (click) = \"addQty(item.menuID)\">+</ion-button></ion-col>\n                                <ion-col> \n                                    <ion-button color=\"light\" [disabled]=\"false\" (click)=\"remove(item.menuID)\"><ion-icon name=\"create\"></ion-icon>Remove</ion-button> \n                                </ion-col>\n                            </ion-row>\n                        </div>\n                        <h2 >TOTAL: {{subtotal()}} </h2>\n                        \n                    </ion-card-content>\n                 \n                </ion-card>\n                <ion-row>\n                    <ion-col text-center>\n                        <ion-button color=\"light\" routerLink=\"/menu\" routerDirection=\"forward\">\n                            <ion-icon name=\"add\"></ion-icon>Add More\n                        </ion-button>\n                        <!-- <ion-button (click)=\"sendOrderList()\" color=\"dark\" [disabled]=\"disablePlaceOrderButton\"> -->\n                        <ion-button (click)=\"sendOrderList()\"  routerLinkActive=\"router-link-active\"   color=\"dark\">    \n                            <ion-icon name=\"checkmark\"></ion-icon>Place Order\n                        </ion-button>\n                    </ion-col>\n                </ion-row>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\" >\n        <ion-fab-button color=\"dark\"><ion-icon name=\"arrow-dropup\"></ion-icon></ion-fab-button>\n\n        <ion-fab-list side=\"top\">\n          <ion-fab-button color=\"dark\" routerLink=\"/menu\"><ion-icon name=\"list-box\"></ion-icon></ion-fab-button>\n          <ion-label>Menu</ion-label>\n          <ion-fab-button color=\"dark\" (click)=\"callWaiterAlert()\"><ion-icon name=\"call\"></ion-icon></ion-fab-button>\n          <ion-label>Call a Waiter</ion-label>\n        </ion-fab-list>\n    </ion-fab>\n</ion-content>\n\n<!-- <ion-content>\n    <ion-input type=\"text\" [(ngModel)]=\"postData\"></ion-input>\n    <ion-button (click)=\"onClick()\">\n        Click me\n    </ion-button>\n</ion-content> -->"

/***/ }),

/***/ "./src/app/customer/menu/orderlist/orderlist.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/customer/menu/orderlist/orderlist.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2N1c3RvbWVyL21lbnUvb3JkZXJsaXN0L29yZGVybGlzdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/customer/menu/orderlist/orderlist.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/customer/menu/orderlist/orderlist.page.ts ***!
  \***********************************************************/
/*! exports provided: OrderlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderlistPage", function() { return OrderlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_admin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/admin.service */ "./src/app/customer/services/admin.service.ts");
/* harmony import */ var src_app_customer_classes_order_detail__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/customer/classes/order-detail */ "./src/app/customer/classes/order-detail.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storageservice.service */ "./src/app/services/storageservice.service.ts");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/customer/services/cart.service.ts");
/* harmony import */ var src_app_services_socket_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/socket.service */ "./src/app/services/socket.service.ts");












var OrderlistPage = /** @class */ (function () {
    function OrderlistPage(http, alertController, adminService, actionSheetController, navCtrl, router, 
    // private cartService: CartService,
    storageService, 
    // private deviceTableService: DeviceTableService,
    // private route: ActivatedRoute,
    events, socket, cartservice, route) {
        // this.getAllCartItems();
        this.http = http;
        this.alertController = alertController;
        this.adminService = adminService;
        this.actionSheetController = actionSheetController;
        this.navCtrl = navCtrl;
        this.router = router;
        this.storageService = storageService;
        this.events = events;
        this.socket = socket;
        this.cartservice = cartservice;
        this.route = route;
        this.kitchenorders = {};
        this.inputValue = [{ name: 'Myra' }];
        // itemPrice: (id: any) => any;
        this.orderList = [];
        this.orderListTotal = 0;
        this.buttonClicked = false;
        this.unserved = true;
        this.checkedItem = [];
        this.servedItems = [];
        this.servedQtySet = false;
        this.qtyCard = false; // card for quantity input
        this.settotal = {};
        this.orderArray = [];
    }
    OrderlistPage.prototype.ngOnInit = function () {
        var _this = this;
        this.storageService.currentData.subscribe(function (data) {
            return _this.cartData = data;
        });
        this.cart = JSON.parse(this.cartData);
        console.log(this.cart);
    };
    OrderlistPage.prototype.goToMenu = function () {
        this.navCtrl.navigateForward('/menu');
    };
    OrderlistPage.prototype.callWaiterAlert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: null,
                            subHeader: null,
                            message: 'A waiter has been called. Please wait a moment for the waiter to arrive at your table. Thank you.',
                            buttons: ['OK']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    OrderlistPage.prototype.getOrderList = function () {
        return this.orderList = this.adminService.getOrderList();
    };
    OrderlistPage.prototype.remove = function (menuID) {
        var processed = this.cart.filter(function (value) { return value.menuID !== menuID; });
        this.storageService.changeData(JSON.stringify(processed));
        this.cart = processed;
        console.log(this.cart);
    };
    OrderlistPage.prototype.addQty = function (menuID) {
        ++this.cart.find(function (item) { return item.menuID === menuID; }).qty;
    };
    OrderlistPage.prototype.subtractQty = function (menuID) {
        --this.cart.find(function (item) { return item.menuID === menuID; }).qty;
    };
    OrderlistPage.prototype.getTotalPerItem = function (menuID) {
        var qty = this.cart.find(function (item) { return item.menuID === menuID; }).qty;
        var price = this.cart.find(function (item) { return item.menuID === menuID; }).price * qty;
        return price;
    };
    OrderlistPage.prototype.subtotal = function () {
        var total = 0;
        for (var _i = 0, _a = this.cart; _i < _a.length; _i++) {
            var data = _a[_i];
            total += (data.price * data.qty);
        }
        // return this.formatPrice(total);
        return total;
    };
    OrderlistPage.prototype.formatPrice = function (value) {
        var val = (value / 1);
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
    OrderlistPage.prototype.sendOrderList = function () {
        // console.log(this.cart);
        for (var _i = 0, _a = this.cart; _i < _a.length; _i++) {
            var c = _a[_i];
            this.orderid = c.order_id;
            this.orders = new src_app_customer_classes_order_detail__WEBPACK_IMPORTED_MODULE_4__["OrderDetail"]();
            this.orders.menuID = c.menuID;
            this.orders.order_id = c.order_id;
            this.orders.orderQty = c.qty;
            this.orders.status = 'waiting';
            this.orders.subtotal = this.getTotalPerItem(c.menuID);
            this.orders.date_ordered = Date.now();
            this.orders.updated_at = Date.now();
            this.orderList.push(this.orders);
            console.log(this.orderList);
        }
        this.adminService.saveToTemporaryKitchenTable(this.orderList);
        this.adminService.processPlaceOrder(this.orderList);
        // this.saveToKitchen();
        this.socket.sendMessage('from orderlist');
        // this.sample = 'HELLO';
        this.router.navigate(['/confirmedorderlist', this.orderid]);
        this.emptyCurrentCart();
    };
    OrderlistPage.prototype.goToConfirmedOrders = function () {
        console.log(this.orderid);
        this.router.navigate(['/confirmedorderlist', this.orderid]);
        // this.emptyCurrentCart();
    };
    OrderlistPage.prototype.emptyCurrentCart = function () {
        var processed = (this.cart = []);
        this.storageService.changeData(JSON.stringify(processed));
        this.cart = processed;
        console.log(this.cart);
    };
    OrderlistPage.prototype.getAllCartItems = function () {
        var _this = this;
        this.cartservice.getItems(this.orderid).then(function (data) {
            _this.cart = data.data;
            console.log(_this.cart);
        });
    };
    OrderlistPage.prototype.setTotal = function () {
        this.settotal.order_id = this.orderid;
        this.settotal.total = this.subtotal();
        this.adminService.setTotal(this.settotal);
        console.log('SETTOTAL');
    };
    OrderlistPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-orderlist',
            template: __webpack_require__(/*! ./orderlist.page.html */ "./src/app/customer/menu/orderlist/orderlist.page.html"),
            styles: [__webpack_require__(/*! ./orderlist.page.scss */ "./src/app/customer/menu/orderlist/orderlist.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _services_admin_service__WEBPACK_IMPORTED_MODULE_3__["AdminService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            src_app_services_storageservice_service__WEBPACK_IMPORTED_MODULE_7__["StorageserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"],
            src_app_services_socket_service__WEBPACK_IMPORTED_MODULE_9__["SocketService"],
            _services_cart_service__WEBPACK_IMPORTED_MODULE_8__["CartService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], OrderlistPage);
    return OrderlistPage;
}());



/***/ })

}]);
//# sourceMappingURL=customer-menu-orderlist-orderlist-module.js.map