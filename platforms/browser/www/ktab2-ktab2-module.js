(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ktab2-ktab2-module"],{

/***/ "./src/app/kitchen/ktab2/ktab2.module.ts":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab2/ktab2.module.ts ***!
  \***********************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ktab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ktab2.page */ "./src/app/kitchen/ktab2/ktab2.page.ts");







var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _ktab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }])
            ],
            declarations: [_ktab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/kitchen/ktab2/ktab2.page.html":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab2/ktab2.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Completed\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col size=\"12\" size-md=\"6\" *ngFor=\"let list of lists\">\r\n          <ion-card mode=\"ios\">\r\n            <ion-card-content>\r\n              <ion-card-header>\r\n                <ion-card-title>\r\n                  <h1>\r\n                  Table No. {{ list.tableno}}\r\n                  </h1>\r\n                </ion-card-title>\r\n                <ion-card-subtitle>\r\n                  <ion-label>\r\n                      {{list.name}}\r\n                  </ion-label> <br>\r\n                  <ion-label>\r\n                    Quantity: {{list.quantity}}\r\n                  </ion-label>\r\n                </ion-card-subtitle>\r\n              </ion-card-header>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/kitchen/ktab2/ktab2.page.scss":
/*!***********************************************!*\
  !*** ./src/app/kitchen/ktab2/ktab2.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-card {\n  border-top: 7px solid  #3f0f11; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAva2l0Y2hlbi9rdGFiMi9EOlxcVGhlc2lzXFxDYXRlclVBcHAvc3JjXFxhcHBcXGtpdGNoZW5cXGt0YWIyXFxrdGFiMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBOEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2tpdGNoZW4va3RhYjIva3RhYjIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmQge1xyXG4gICAgYm9yZGVyLXRvcDogN3B4IHNvbGlkICAjM2YwZjExO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/kitchen/ktab2/ktab2.page.ts":
/*!*********************************************!*\
  !*** ./src/app/kitchen/ktab2/ktab2.page.ts ***!
  \*********************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ "./src/app/kitchen/rest.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var Tab2Page = /** @class */ (function () {
    // complete: any;
    function Tab2Page(navCtrl, restService) {
        this.navCtrl = navCtrl;
        this.restService = restService;
        this.getComplete();
    }
    Tab2Page.prototype.getComplete = function () {
        var _this = this;
        // this.restService.getCompleteList().then(data => {
        // this.complete = DataTransferItemList;
        // // console.log(this.restService.getOrderList());
        // console.log(this.complete);
        this.restService.getCompleteList().then(function (data) {
            _this.lists = data.orders;
            // this.lists = this.lists;
            console.log(_this.restService.getCompleteList());
            console.log(_this.lists);
        });
    };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! ./ktab2.page.html */ "./src/app/kitchen/ktab2/ktab2.page.html"),
            styles: [__webpack_require__(/*! ./ktab2.page.scss */ "./src/app/kitchen/ktab2/ktab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ })

}]);
//# sourceMappingURL=ktab2-ktab2-module.js.map