(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rtab1-rtab1-module"],{

/***/ "./src/app/receptionist/rtab1/rtab1.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab1/rtab1.module.ts ***!
  \****************************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtab1.page */ "./src/app/receptionist/rtab1/rtab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _rtab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_rtab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtab1/rtab1.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab1/rtab1.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title>\r\n      Enter Queue Details\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Phone Number</ion-label>\r\n    <ion-input type=\"tel\" pattern=\"^(09|\\+639)\\d{9}$\" [(ngModel)]=\"pno\"></ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Party Size</ion-label>\r\n    <ion-input type=\"number\" [(ngModel)]=\"psize\"></ion-input>\r\n  </ion-item>\r\n\r\n  <ion-item>\r\n    <ion-label position=\"floating\">Customer Name</ion-label>\r\n    <ion-input type=\"string\" [(ngModel)]=\"cname\"></ion-input>\r\n  </ion-item>\r\n\r\n  <div margin-vertical text-center>\r\n    <ion-button fill=\"outline\" color=\"danger\" id=\"btn-clear\" (click)=\"cancelBtn()\">\r\n      <ion-icon slot=\"start\" name=\"close\"></ion-icon>Clear\r\n    </ion-button>\r\n    <ion-button id=\"btn-add\" (click)=\"addBtn()\">\r\n      <ion-icon slot=\"start\" name=\"add\"></ion-icon>Add to Queue\r\n    </ion-button>\r\n  </div>\r\n  \r\n  \r\n  <!-- {{ pno }}\r\n  {{ psize }} -->\r\n</ion-content>"

/***/ }),

/***/ "./src/app/receptionist/rtab1/rtab1.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtab1/rtab1.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWIxL0Q6XFxUaGVzaXNcXENhdGVyVUFwcC9zcmNcXGFwcFxccmVjZXB0aW9uaXN0XFxydGFiMVxccnRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcmVjZXB0aW9uaXN0L3J0YWIxL3J0YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XHJcbiAgbWF4LWhlaWdodDogMzV2aDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtab1/rtab1.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtab1/rtab1.page.ts ***!
  \**************************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/rest.service */ "./src/app/receptionist/services/rest.service.ts");



var Tab1Page = /** @class */ (function () {
    function Tab1Page(serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.pk = 0;
    }
    Tab1Page.prototype.cancelBtn = function () {
        console.log('Please work!!!');
        this.pno = null;
        this.psize = null;
        this.cname = null;
    };
    Tab1Page.prototype.addBtn = function () {
        if (this.pno == null || this.psize == null) {
            alert('Provide Details for Queuing!');
            return;
        }
        else {
            if (this.pno.toString().length !== 11 || this.psize < 1 || this.psize > 10) {
                console.log(this.pno.toString().length);
                alert('Provide necessary details');
                return;
            }
            else {
                this.pk += 1;
            }
        }
        this.serviceProvider.enqueue(this.pk, this.psize, this.cname, this.pno);
        this.pno = null;
        this.psize = null;
        this.cname = null;
    };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! ./rtab1.page.html */ "./src/app/receptionist/rtab1/rtab1.page.html"),
            styles: [__webpack_require__(/*! ./rtab1.page.scss */ "./src/app/receptionist/rtab1/rtab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_rest_service__WEBPACK_IMPORTED_MODULE_2__["RestService"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=rtab1-rtab1-module.js.map