(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["receptionist-rtabs-rtabs-module"],{

/***/ "./src/app/receptionist/rtabs/rtabs.module.ts":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtabs/rtabs.module.ts ***!
  \****************************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _rtabs_router_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rtabs.router.module */ "./src/app/receptionist/rtabs/rtabs.router.module.ts");
/* harmony import */ var _rtabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rtabs.page */ "./src/app/receptionist/rtabs/rtabs.page.ts");







var TabsPageModule = /** @class */ (function () {
    function TabsPageModule() {
    }
    TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _rtabs_router_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
            ],
            declarations: [_rtabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
        })
    ], TabsPageModule);
    return TabsPageModule;
}());



/***/ }),

/***/ "./src/app/receptionist/rtabs/rtabs.page.html":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtabs/rtabs.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\r\n\r\n  <ion-tab-bar slot=\"bottom\">\r\n    <ion-tab-button tab=\"rtab1\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n      <ion-label>Queueing</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"rtab2\">\r\n      <ion-icon name=\"ios-list\"></ion-icon>\r\n      <ion-label>Queue List</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"rtab3\">\r\n      <ion-icon name=\"list\"></ion-icon>\r\n      <ion-label>Table List</ion-label>\r\n    </ion-tab-button>\r\n  </ion-tab-bar>\r\n\r\n</ion-tabs>\r\n"

/***/ }),

/***/ "./src/app/receptionist/rtabs/rtabs.page.scss":
/*!****************************************************!*\
  !*** ./src/app/receptionist/rtabs/rtabs.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlY2VwdGlvbmlzdC9ydGFicy9ydGFicy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/receptionist/rtabs/rtabs.page.ts":
/*!**************************************************!*\
  !*** ./src/app/receptionist/rtabs/rtabs.page.ts ***!
  \**************************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TabsPage = /** @class */ (function () {
    // tables: any;
    function TabsPage() {
    }
    TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__(/*! ./rtabs.page.html */ "./src/app/receptionist/rtabs/rtabs.page.html"),
            styles: [__webpack_require__(/*! ./rtabs.page.scss */ "./src/app/receptionist/rtabs/rtabs.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());



/***/ }),

/***/ "./src/app/receptionist/rtabs/rtabs.router.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/receptionist/rtabs/rtabs.router.module.ts ***!
  \***********************************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _rtabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rtabs.page */ "./src/app/receptionist/rtabs/rtabs.page.ts");




var routes = [
    {
        path: 'rtabs',
        component: _rtabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'rtab1',
                children: [
                    {
                        path: '',
                        loadChildren: '../rtab1/rtab1.module#Tab1PageModule'
                    }
                ]
            },
            {
                path: 'rtab2',
                children: [
                    {
                        path: '',
                        loadChildren: '../rtab2/rtab2.module#Tab2PageModule'
                    }
                ]
            },
            {
                path: 'rtab3',
                children: [
                    {
                        path: '',
                        loadChildren: '../rtab3/rtab3.module#Tab3PageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/receptionist/rtabs/rtab1',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/receptionist/rtabs/rtab1',
        pathMatch: 'full'
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=receptionist-rtabs-rtabs-module.js.map